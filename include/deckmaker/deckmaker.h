#ifndef __DECK_MAKER_H__
#define __DECK_MAKER_H__

#include "Common.h"

//#include "IzvorCommonPCH.h"
#include "Card.h"
#include "Colour.h"
#include "ManaCost.h"
#include "database/Core.h"
#include "database/Record.h"
#include "database/RecordSet.h"
#include "GameMgr.h"

#include <QMainWindow>
#include <QLabel>

namespace Ui {
  class DeckMaker;
}

class DeckMaker : public QMainWindow
{
  Q_OBJECT

  public:
  explicit DeckMaker(QWidget *parent = 0);
  ~DeckMaker();

public slots:
  void doSearch();
  void toggleExact( bool toggle );
  void resetInput();
  void updateAddBtn( int );
  void addCardToDeck();
  void removeCardFromDeck();
  void emptyDeck();
  void emptySideDeck();
  void updateUI();
  void saveAs();
  void open();
  void save();
  void newDeck();
  void updateTitle( bool );
	     
signals:
  void modified( bool );

  //private:
protected:
  void showCard( const Card* pCard );
  std::string constructQuery() const;
  
  bool mModified, mSaved;

  QLabel mStatus;
  QString mFileName;
  Ui::DeckMaker *ui;
  static std::map<const Card::Info::Edition, const std::string> sImgPath;
};

#endif /* !__DECK_MAKER_H__ */
