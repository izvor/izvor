#ifndef __CARD_TREEVIEW_H__
#define __CARD_TREEVIEW_H__

//#include "IzvorCommonPCH.h"
#include "Deck.h"
#include "Card.h"
#include "ManaCost.h"
#include "GameMgr.h"

#include "database/Core.h"
#include "database/Record.h"
#include "database/RecordSet.h"

#include <QList>
#include <QTreeView>
#include <QStandardItemModel>
#include <QStandardItem>

class CardTreeView : public QTreeView
{
  //  Q_OBJECT

public:
  
  // prosirenje DB::Column
  static const uint32 EDITION;
  static const uint32 NUM;
  static const uint32 COUNT;
    
  CardTreeView( QWidget *parent = NULL )
    : QTreeView( parent ),
      mColumnMask( DB::ID ),
      mCardsCounted( false ),
      mIDHidden( false )
  {
    mModel = new QStandardItemModel;
    mDeck = new Deck;

    setModel( mModel );
  };
  
  void setColumns( uint32 mask )
  { 
    mColumnMask = mask; 
    mModel->setColumnCount( getColumnCount( mColumnMask ) );
    updateHeaders();
  };

  void setCardsCounted( bool flag ) { mCardsCounted = flag; };
  void setIDHidden( bool flag ) { mIDHidden = flag; };
  uint32 addItem( uint32 id, uint8 new_count = 1 ) // vraca red u kome se nalazi karta
  {
    if( mCardsCounted && mDeck->cardCount( id ) == 4 ) return 0;
    const Card* pCard = mDeck->addCard( id, new_count );
    uint32 count = mDeck->cardCount( id );    
    if( !mCardsCounted || (count == new_count) ) {
      // nije dodata
      for( uint32 i = 0; i < new_count; i++ ) {
	const QList<QStandardItem*>& ql = createRow( pCard );
	
	// dodaj reprint kao child ako ga ima
	if( pCard->info()->reprintNum() ) {
	  const QList<QStandardItem*>& ql_reprint = createRow( pCard, true );
	  ql.value( 0 )->appendRow( ql_reprint );
	}
    
	mModel->appendRow( ql );
	if( mCardsCounted ) break;
      }
      return mModel->rowCount();
    } 

    // vec dodata, nadji red i azuriraj kolonu sa brojem
    QStandardItem* i = mModel->findItems( QString( "%0" ).arg( id ) ).value( 0 );
    mModel->itemFromIndex( mModel->index( i->index().row(), mColumnIndex[COUNT] ) )->setText( QString( "%0" ).arg( count ) );
    return i->index().row();
  }
  void deleteItem( uint32 id )
  {
    if( !mDeck->isInside( id ) ) return;
    mDeck->removeCard( id );
    uint32 count = mDeck->cardCount( id );
    if( !mCardsCounted || !count ) {
      // nadji red i ukloni ga
      QStandardItem* i = mModel->findItems( QString( "%0" ).arg( id ) ).value( 0 );
      mModel->removeRow( i->index().row() );
    }
    else {
      // nadji red i azuriraj kolonu
      QStandardItem* i = mModel->findItems( QString( "%0" ).arg( id ) ).value( 0 );
      mModel->itemFromIndex( mModel->index( i->index().row(), mColumnIndex[COUNT] ) )->setText( QString( "%0" ).arg( count ) );
    }
  }
  void selectionUp()
  {
    QList<QModelIndex> lst = this->selectionModel()->selectedIndexes();
    if( lst.empty() ) return;

    if( model()->index( lst.value( 0 ).row() - 1, 0 ).isValid() )
      selectionModel()->select( model()->index( lst.value(0).row() - 1, 0 ), 
				QItemSelectionModel::ClearAndSelect | 
				QItemSelectionModel::Rows );
  }
  void selectionDown()
  {
    QList<QModelIndex> lst = this->selectionModel()->selectedIndexes();
    if( lst.empty() ) return;
    
    if( model()->index( lst.value( 0 ).row() + 1, 0 ).isValid() )
      selectionModel()->select( model()->index( lst.value(0).row() + 1, 0 ), 
				QItemSelectionModel::ClearAndSelect | 
				QItemSelectionModel::Rows );
  }
  void selectRow( uint32 row )
  {
    if( model()->index( row, 0 ).isValid() )
      selectionModel()->select( model()->index( row, 0 ),
				QItemSelectionModel::ClearAndSelect | 
				QItemSelectionModel::Rows );
    else if( model()->index( row - 1, 0 ).isValid() ) // probaj iznad
      selectionModel()->select( model()->index( row - 1, 0 ),
				QItemSelectionModel::ClearAndSelect | 
				QItemSelectionModel::Rows );
  }
  const Card* getSelection();
  void updateHeaders();

  Deck* getDeck() { return mDeck; };

  ~CardTreeView() { SafeDelete( mModel ); SafeDelete( mDeck ); };
  static uint32 getColumnCount( uint32 col_mask );

  void clear()
  {
    mModel->clear();
    mDeck->clear();

    mModel->setColumnCount( getColumnCount( mColumnMask ) );
    updateHeaders();
  }

protected:
  void keyPressEvent( QKeyEvent *e );

private:
  QList<QStandardItem*> createRow( const Card* pCard, bool reprint = false ) const;

  uint32 mColumnMask; // koje kolone
  std::map<uint32, uint32> mColumnIndex; // rb kolone u modelu

  bool mCardsCounted, mIDHidden;
  QStandardItemModel *mModel;
  Deck *mDeck;
};

#endif /* !__CARD_TREEVIEW_H__ */
