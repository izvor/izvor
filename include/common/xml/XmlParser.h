#ifndef __XML_PARSER_H__INCL__
#define __XML_PARSER_H__INCL__

#include "log/Log.h"

class TiXmlDocument;

class XmlParser
{
public:

	XmlParser() : mXmlDoc( NULL ) {}
	XmlParser( const std::string& xmlFile )
		: mXmlDoc( NULL ), 
		  mXmlFile( xmlFile ),
		  mIsOpen( false )
	{}

	~XmlParser() { if( mXmlDoc ) SafeDelete( mXmlDoc ); }

	void setXmlFile( const std::string& xmlFile )
	{
		mXmlFile = xmlFile;
	}

	bool parse()
	{
		if( !mXmlDoc && !open() ) return false;

		bool ret = _parse( mXmlDoc->RootElement() );
		if( !ret ) sLog.error( "XmlParser", "Error while parsing file: %s.", mXmlFile.c_str() );
		
		return ret;
	}

protected:
	bool open()
    {
		if( mIsOpen ) return true;

        mXmlDoc = new TiXmlDocument( mXmlFile );

        if( !mXmlDoc->LoadFile() ) {
			SafeDelete( mXmlDoc );
			//sLog.error( "XmlParser", "Can't open XML file, or bad XML syntax: %s.", mXmlFile.c_str() );
			return false;
		}

		mIsOpen = true;

		return true;
    }

	virtual bool _parse( TiXmlNode *pParent ) = 0;
	
	TiXmlDocument *mXmlDoc;
	std::string mXmlFile;
	bool mIsOpen;
};

#endif /* !__XML_PARSER_H__INCL__ */
