/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __DATABASE__DECODABLE_H__INCL__
#define __DATABASE__DECODABLE_H__INCL__

namespace DB
{
	class Record;
	// This interface means that the class that uses it can and must be
	// created from a single database record passed to it. Classes that 
	// utilize it are tightly connected to the database. Decode() method
	// provides a way to decode data contained in the database and create
	// a new object from it.

	// Call decode within constructor of child class! Virtual functions 
	// should never be called within constructors/destructors, see:
	// http://www.artima.com/cppsource/nevercall.html

	class Decodable
	{
	protected:
		virtual void decode( const Record& r ) = 0;
	};
}

#endif /* !__DATABASE__DECODABLE_H__INCL__ */
