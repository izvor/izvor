/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __DATABASE__RECORD_SET_H__INCL__
#define __DATABASE__RECORD_SET_H__INCL__

#include "CommonPCH.h"

#include "util/ExcMsg.h"
#include "Record.h"

namespace DB
{
  class RecordSet
  {
  public:
    Record& getRecord( uint32 index = 0 )
    {
      if( index + 1 > mSet.size() ) 
	throw ExcMsg( "DB::RecordSet",
		      "Trazeni indeks (%d) je van opsega u skupu unosa",
		      index );
      return mSet[index];
    }

    const Record& getRecord( uint32 index = 0 ) const
    {
      if( index + 1 > mSet.size() ) 
	throw ExcMsg( "DB::RecordSet",
		      "Trazeni indeks (%d) je van opsega u skupu unosa",
		      index );
      return mSet[index];
    }

    Record& operator[]( uint32 index )
    {
      return getRecord( index );
    }

    const Record& operator[]( uint32 index ) const
    {
      return getRecord( index );
    }

    void addRecord( uint32 row_count, char **values, char **columns );
    
    uint32 getSize() const { return mSet.size(); }
    bool empty() const { return mSet.empty(); }
    void print() const;

    friend std::ostream& operator<<( std::ostream& os, const RecordSet& r );

  private:

    std::vector<Record> mSet;
  };
}

#endif /* !__DATABASE__RECORD_SET_H__INCL__ */
