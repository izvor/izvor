/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __DATABASE__CORE_H__INCL__
#define __DATABASE__CORE_H__INCL__

#include "CommonPCH.h"

#include "util/ExcMsg.h"
#include "util/Singleton.h"
#include "log/Log.h"
#include "Record.h"
#include "RecordSet.h"

namespace DB
{
	class Core : public Singleton< Core >
	{
	public:
		Core()
			: mDB( NULL ), mErrMsg( NULL )
		{
			sqlite3_unicode_load();
		}
		~Core();
    
		void open( const std::string& db );
    
		// void query_write( const std::string& query )
		// {
		//   mRetVal = sqlite3_exec( mDB, query.c_str(), NULL,
		// 			      NULL, &mErrMsg );
		//   if( mRetVal != SQLITE_OK )
		// 	throw ExcSQLError( query, mErrMsg, mDBFile, mRetVal );
		// }

		RecordSet query( const std::string& query );
	  
		// jako bezbedna metoda, vraca iskljucivo jedan unos, u svim ostalim
		// slucajevima baca izuzetke
		Record querySingle( const std::string& query );

		static int callback( void *rs, int row_count, char **values, char **columns )
		{
			if( rs ) static_cast<RecordSet *>( rs )->addRecord( row_count, values, columns );
			return 0;
		}

	private:

		sqlite3 *mDB;
		char    *mErrMsg;
		int32    mRetVal;

		std::string mDBFile;
	};
}

//#define sDB					
//  ( DB::Core::get() )

#endif /* !__DATABASE__CORE_H__INCL__ */
