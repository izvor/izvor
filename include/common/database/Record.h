/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __DATABASE__RECORD_H__INCL__
#define __DATABASE__RECORD_H__INCL__

#include "CommonPCH.h"

#include "util/ExcMsg.h"

namespace DB
{
  enum Column
    {
      ID             = 0x0001,
      COLOR          = 0x0002,
      COST           = 0x0004,
      PRIMARY_TYPE   = 0x0008,
      SECONDARY_TYPE = 0x0010,
      ABILITY        = 0x0020,
      ATTACK         = 0x0040,
      DEFENSE        = 0x0080,
      NAME           = 0x0100,
      RARITY         = 0x0200,
      LORE           = 0x0400,
      IMAGE_AUTHOR   = 0x0800,
      REPRINT_ID     = 0x1000
    };

  extern std::map<const Column, const std::string> colStr;  

  class Record
  {
  public:
    typedef std::map<std::string, std::string> Data;
    
    void addData( const std::string& col,
		  const std::string& val )
    {
      mData.insert( Data::value_type( col, val ) );
    }
    
    const std::string& getVal( const std::string& col ) const
    {
      Data::const_iterator e = mData.find( col );
      if( e == mData.end() )
	throw ExcMsg( "DB::Record", 
		      "Trazena kolona (%s) ne postoji u unosu", col.c_str() );
      return e->second;
    }
    const std::string& operator[]( const Column col ) const
    {
      return getVal( colStr[col] );
    }
    bool empty() const { return mData.empty(); }

    friend std::ostream& operator<<( std::ostream& os, const Record& r );

  private:

    Data mData;
  };
}

#endif /* !__DATABASE__RECORD_H__INCL__ */
