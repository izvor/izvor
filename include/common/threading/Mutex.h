/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __THREADING__MUTEX_H__INCL__
#define __THREADING__MUTEX_H__INCL__

#include "util/Lock.h"

/**
 * @brief Common wrapper for platform-specific mutexes.
 *
 * @author Zhur, Bloody.Rabbit
 */
class Mutex
  : public Lockable
{
public:
  /**
   * @brief Primary contructor.
   */
  Mutex();
  /**
   * @brief Destructor, releases allocated resources.
   */
  ~Mutex();

  /**
   * @brief Locks the mutex.
   */
  void lock();
  /**
   * @brief Attempts to lock the mutex.
   *
   * @retval true  Mutex successfully locked.
   * @retval false Mutex locked by another thread.
   */
  bool tryLock();

  /**
   * @brief Unlocks the mutex.
   */
  void unlock();

protected:
# ifdef WIN32
  /// A critical section used for mutex implementation on Windows.
  CRITICAL_SECTION mCriticalSection;
# else
  /// A pthread mutex used for mutex implementation using pthread library.
  pthread_mutex_t mMutex;
# endif
};

/// Convenience typedef for Mutex's lock.
typedef Lock< Mutex > MutexLock;

#endif /* !__THREADING__MUTEX_H__INCL__ */
