/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __EVENT__EVENT_H__INCL__
#define __EVENT__EVENT_H__INCL__

#include "Common.h"

// Observable class containes inner Event-derived classes.
// Those classes need to be friendly to Observable class.
// Analogous - Observer for Listener

// Having a parent in an Event allows for
// state inspection of parent object, provided that
// we know it's type.
class Observable { virtual void doNothing() {} };
class Observer { virtual void doNothing() {} };
// doNothing() provides polymorphism, it's there until we
// think of something useful that Observable/Observer should do or until
// we dump it

// Event is templatized - it should be used as a base for derived classes:
// for example, inside observable class: 
//     class LibraryEmptyEvt : public Event< LibraryEmptyEvt >
// inside observer class:
//     class EndGameListener : public Event< LibraryEmptyEvt >::Listener

// ------------- GenericListener ------------- //
// This class can listen to all events, it doesn't
// require a parent and is inherited by specific Listeners.
//class GenericListener
//{
//	template< typename Evt >
//	virtual void eventOccured( Evt* event ) = 0;
//};
// ------------- !GenericListener ------------- //

// ------------- Event ------------- //
template< typename Evt >
class Event
{
public:
	// -------------- Listener -------------- //
	class Listener
	{
	public:
		Listener( Observer *parent = NULL )
			: mParent( parent )
		{}

		virtual void eventOccured( Evt *event ) = 0;
		Observer* getParent() const { return mParent; }

	protected:
			
		Observer *mParent;
	};
	// -------------- !Listener -------------- //
	Event( Observable *parent ) 
		: mParent( parent )
	{}

	virtual void registerListener( Listener *l )
	{
		mListeners.insert( l );
	}
		
	virtual void unregisterListener( Listener *l )
	{
		mListeners.erase( l );
	}
	virtual void clearListeners() { mListeners.clear(); }
	virtual uint32 getListenerCount() const { return mListeners.size(); }
		
	virtual void notify();

	void setParent( Observable *parent ) { mParent = parent; }
	Observable* getParent() const { return mParent; }

protected:

	Observable *mParent;

private:

	std::set< Listener* > mListeners;
};
// ------------- !Event ------------- //

// notifies only when object changes state
// use setChanged and clearChanged to define when
// to notify
// ------------- StateEvent ------------- //
template< typename Evt >
class StateEvent : public Event< Evt >
{
public:
	StateEvent( Observable *parent )
		: Event< Evt >( parent ),
		  mChanged( false )
	{}

	virtual bool hasChanged() const { return mChanged; }
	virtual void notify();

protected:
	// These are called within the code of derived classes
	// depends on logic
	virtual void setChanged() { mChanged = true; }
	virtual void clearChanged() { mChanged = false; }		

private:

	bool mChanged;
};
// ------------- !StateEvent ------------- //

template< typename Evt >
void Event< Evt >::notify()
{
	for( typename std::set< Listener* >::iterator i = mListeners.begin();
		 i != mListeners.end();
		 i++ )
		(*i)->eventOccured( dynamic_cast< Evt* >( this ) );
}

template< typename Evt >
void StateEvent< Evt >::notify()
{
	if( !mChanged ) return;
	
	clearChanged();

	Event< Evt >::notify();
}

// evt and listener are references to Event and Listener objects of the same template
#define EVT_CONNECT(evt,listener) \
(evt).registerListener(&(listener));

#define EVT_DISCONNECT(evt,listener) \
(evt).unregisterListener(&(listener));

#endif /* !__EVENT__EVENT_H__INCL__ */
