/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __UTIL__MISC_H__INCL__
#define __UTIL__MISC_H__INCL__

// Pairing funkcija, preslikava (N, N) -> N
uint32 CantorPair( uint32 a, uint32 b );

void CantorUnPair( uint32 pair, uint32 *a, uint32 *b );

std::ptrdiff_t random( ptrdiff_t i );

/**
 * @brief Calculates next (greater or equal)
 *        power-of-two number.
 *
 * @param[in] num Base number.
 *
 * @return Power-of-two number which is greater than or
 *         equal to the base number.
 */
uint64 npowof2( uint64 num );

#endif /* !__UTIL__MISC_H__INCL__ */
