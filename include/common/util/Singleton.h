/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __UTIL__SINGLETON_H__INCL__
#define __UTIL__SINGLETON_H__INCL__

#include "CommonPCH.h"

/**
 * @brief Template used for singleton classes.
 *
 * This template shall be used as base for classes
 * which are intended to be singleton (i.e. there
 * should be only 1 instance of this class at all).
 *
 * Uses lazy construction (i.e. object is constructed
 * on first access request).
 *
 * @author Bloody.Rabbit
 */

template< typename X >
class Singleton
{
public:
  /**
   * @brief Primary constructor.
   *
   * Checks if the instance being constructed is first, i.e.
   * mInstance hasn't been filled yet. This only makes sense
   * if the actual class is derived from Singleton.
   */
  Singleton()
  {
    assert( NULL == mInstance.get() );
  }

  /** @return Reference to the singleton instance. */
  static X& get()
  {
    if( NULL == mInstance.get() )
      mInstance.reset( new X );

    return *mInstance;
  }

protected:
  /** Pointer to the singleton instance. */
  static std::auto_ptr< X > mInstance;
};

template< typename X >
std::auto_ptr< X > Singleton< X >::mInstance( NULL );

#endif /* !__UTIL__SINGLETON_H__INCL__ */
