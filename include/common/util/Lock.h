/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __UTIL__LOCK_H__INCL__
#define __UTIL__LOCK_H__INCL__

/**
 * @brief Basic interface of a lockable object.
 *
 * @author Bloody.Rabbit
 */
class Lockable
{
public:
  /**
   * @brief Locks the object.
   *
   * This method will block until the lock has been obtained.
   */
  virtual void lock() = 0;
  /**
   * @brief Attempts to lock the object.
   *
   * If the lock cannot be obtained immediately, the method returns.
   *
   * @retval true  The object has been locked.
   * @retval false Could not obtain the lock immediately.
   */
  virtual bool tryLock() = 0;

  /**
   * @brief Unlocks a locked object.
   */
  virtual void unlock() = 0;
};

/**
 * @brief A lock for a Lockable object.
 *
 * The object is locked during contruction and unlocked
 * during destruction.
 *
 * The passed typename should be a child of the class Lockable.
 *
 * @author Bloody.Rabbit
 */
template< typename T >
class Lock
{
public:
  /**
   * @brief Primary contructor, locks the object.
   *
   * @param[in] object Object to bound this lock to.
   * @param[in] lock   Lock the object during construction.
   */
  Lock( T& object, bool lock = true )
    : mObject( object ),
      mLocked( false )
  {
    if( lock )
      relock();
  }
  /**
   * @brief Destructor, unlocks the object.
   */
  ~Lock()
  {
    unlock();
  }

  /**
   * @brief Obtains the lock state of the object.
   *
   * @retval true  The object is locked.
   * @retval false The object is not locked.
   */
  bool isLocked() const { return mLocked; }

  /**
   * @brief Locks the object.
   */
  void relock()
  {
    if( !isLocked() )
      mObject.lock();

    mLocked = true;
  }
  /**
   * @brief Unlocks the object.
   */
  void unlock()
  {
    if( isLocked() )
      mObject.unlock();

    mLocked = false;
  }

protected:
  /// The object this lock is bound to.
  T& mObject;
  /// True the @a mObject is locked, false if not.
  bool mLocked;
};

#endif /* !__UTIL__LOCK_H__INCL__ */
