/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __UTIL__EXCMSG_H__INCL__
#define __UTIL__EXCMSG_H__INCL__

#include "CommonPCH.h"

#include "log/Log.h"

class ExcMsg
{
public:
  ExcMsg( const char *src, const char *fmt, ... )
  {
    va_list ap;
    va_start( ap, fmt );
    
    sLog.printMsgVa( Log::COLOR_RED, 'E', src, fmt, ap );

    va_end( ap );
  }
};

#endif /* !__UTIL__EXCMSG_H__INCL__ */
