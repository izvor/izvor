/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __NETWORK__SOCKET_H__INCL__
#define __NETWORK__SOCKET_H__INCL__

#include "CommonPCH.h"

class Socket
{
public:
  Socket( int af, int type, int protocol );
  ~Socket();

  int connect( const sockaddr* name, unsigned int namelen );

  unsigned int recv( void* buf, unsigned int len, int flags );
  unsigned int recvfrom( void* buf, unsigned int len, int flags, 
			 sockaddr* from, unsigned int* fromlen );
  unsigned int send( const void* buf, unsigned int len, int flags );
  unsigned int sendto( const void* buf, unsigned int len, int flags, 
		       const sockaddr* to, unsigned int tolen );

  int bind( const sockaddr* name, unsigned int namelen );
  int listen( int backlog = SOMAXCONN );

  Socket* accept( sockaddr* addr, unsigned int* addrlen );

  int setopt( int level, int optname, const void* optval, 
	      unsigned int optlen );
# ifdef WIN32
  int ioctl( long cmd, unsigned long* argp );
# else
  int fcntl( int cmd, long arg );
# endif /* !WIN32 */

protected:
  Socket( SOCKET sock );

  SOCKET mSock;
};

#endif /* !__SOCKET_H__ */
