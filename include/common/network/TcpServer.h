/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __NETWORK__TCP_SERVER_H__INCL__
#define __NETWORK__TCP_SERVER_H__INCL__

#include "network/Socket.h"
#include "threading/Mutex.h"

/** Size of error buffer BaseTcpServer uses. */
extern const uint32 TCPSRV_ERRBUF_SIZE;
/** Time (in milliseconds) between periodical checks of socket for new connections. */
extern const uint32 TCPSRV_LOOP_GRANULARITY;

/**
 * @brief Generic class for TCP server.
 *
 * @author Zhur, Bloody.Rabbit
 */
class BaseTcpServer
{
public:
  /**
   * @brief Creates empty TCP server.
   */
  BaseTcpServer();
  /**
   * @brief Cleans server up.
   */
  virtual ~BaseTcpServer();

  /** @return TCP port the server listens on. */
  uint16 getPort() const { return mPort; }
  /** @return True if listening has been opened, false if not. */
  bool isOpen() const;

  /**
   * @brief Start listening on specified port.
   *
   * @param[in]  port   Port on which listening should be started.
   * @param[out] errbuf Error buffer which receives description of error.
   *
   * @return True if listening has been started successfully, false if not.
   */
  bool open( uint16 port, char* errbuf = 0 );
  /**
   * @brief Stops started listening.
   */
  void close();

protected:
  /**
   * @brief Starts worker thread.
   *
   * This function just starts worker thread; it doesn't check
   * whether there already is one running!
   */
  void startLoop();
  /**
   * @brief Waits for worker thread to terminate.
   */
  void waitLoop();

  /**
   * @brief Does periodical stuff to keep the server alive.
   *
   * @return True if the server should be further processed, false if not (eg. error occurred).
   */
  virtual bool process();
  /**
   * @brief Accepts all new connections.
   */
  void listenNewConnections();

  /**
   * @brief Processes new connection.
   *
   * This function must be overloaded by children to process new connections.
   * Called every time a new connection is accepted.
   */
  virtual void createNewConnection( Socket* sock, uint32 rIP, uint16 rPort ) = 0;

  /**
   * @brief Loop for worker threads.
   *
   * This function only casts arg to BaseTcpServer and calls
   * member TcpServerLoop().
   *
   * @param[in] arg Pointer to BaseTcpServer.
   */
  static thread_return_t TcpServerLoop( void* arg );
  /**
   * @brief Loop for worker threads.
   */
  thread_return_t TcpServerLoop();

  /** Mutex to protect socket and associated variables. */
  mutable Mutex mMSock;
  /** Socket used for listening. */
  Socket* mSock;
  /** Port the socket is listening on. */
  uint16 mPort;

  /** Worker thread acquires this mutex before it starts processing; used for thread synchronization. */
  mutable Mutex mMLoopRunning;
};

/**
 * @brief Connection-type-dependent version of TCP server.
 *
 * @author Zhur, Bloody.Rabbit
 */
template<typename X>
class TcpServer : public BaseTcpServer
{
public:
  /**
   * @brief Deletes all stored connections.
   */
  ~TcpServer()
  {
    MutexLock lock( mMQueue );

    X* conn;
    while( ( conn = popConnection() ) )
      SafeDelete( conn );
  }

  /**
   * @brief Pops connection from queue.
   *
   * @return Popped connection.
   */
  X* popConnection()
  {
    MutexLock lock( mMQueue );

    X* ret = NULL;
    if( !mQueue.empty() )
      {
	ret = mQueue.front();
	mQueue.pop();
      }

    return ret;
  }

protected:
  /**
   * @brief Adds connection to the queue.
   *
   * @param[in] con Connection to be added to the queue.
   */
  void addConnection( X* con )
  {
    MutexLock lock( mMQueue );

    mQueue.push( con );
  }

  /** Mutex to protect connection queue. */
  Mutex mMQueue;
  /** Connection queue. */
  std::queue<X*> mQueue;
};

#endif /* !__NETWORK__TCP_SERVER_H__ */
