/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __NETWORK__STREAM_PACKETIZER_H__INCL__
#define __NETWORK__STREAM_PACKETIZER_H__INCL__

#include "util/Buffer.h"

/**
 * @brief Turns a stream into packets.
 *
 * @author Bloody.Rabbit
 */
class StreamPacketizer
{
public:
  /**
   * @brief A destructor.
   *
   * Performs cleanup by calling ClearBuffers.
   */
  ~StreamPacketizer();

  /**
   * @brief Inputs new data.
   *
   * @param[in] data The data.
   */
  void inputData( const Data& data );
  /**
   * @brief Processes input data into packets.
   */

  // dradojevic: process() first examines the first sizeof(uint32) bytes
  // of received data because packet size iz stored there. Then it creates
  // the packet buffer and puts it in the queue if current data size is
  // less or equal to packet size. Else, it waits for next data input.
  void process();

  /**
   * @brief Obtains a next packet in the queue.
   *
   * @return The packet.
   */
  Buffer* popPacket();

  /**
   * @brief Deletes all remaining packets.
   */
  void clearBuffers();

protected:
  /// Unprocessed or uncomplete packet data.
  Buffer mBuffer;

  /// A queue of processed packets.
  std::queue< Buffer* > mPackets;
};


#endif /* !__NETWORK__STREAM_PACKETIZER_H__INCL__ */
