/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __LOG__LOG_H__INCL__
#define __LOG__LOG_H__INCL__

#include "util/Singleton.h"
#include "threading/Mutex.h"

/**
 * @brief a small and simple logging system.
 *
 * This class is designed to be a simple logging system that both logs to file
 * and console regarding the settings.
 *
 * @author Captnoord.
 * @date August 2009
 */
class Log
  : public Singleton< Log >
{
  friend class ExcMsg;

public:
  /// Primary constructor, initializes logging.
  Log();
  /// Destructor, closes the logfile.
  ~Log();

  /// Checks if we have an opened logfile.
  bool hasLogfile() const { return NULL != mLogfile; }

  /**
   * @brief Logs a message to file.
   *
   * @param[in] source is the source from where the message is printed.
   * @param[in] format is the message itself.
   */
  void message( const char* source, const char* format, ... );
  /**
   * @brief Logs error message to console and file.
   *
   * @param[in] source is the source from where the message is printed.
   * @param[in] format is the error message itself.
   */
  void error( const char* source, const char* format, ... );
  /**
   * @brief Logs a warning message to file.
   *
   * @param[in] source is the source from where the message is printed.
   * @param[in] format is the message itself.
   */
  void warning( const char* source, const char* format, ... );
  /**
   * @brief Logs a success message to file.
   *
   * @param[in] source is the source from where the message is printed.
   * @param[in] format is the message itself.
   */
  void success( const char* source, const char* format, ... );
  /**
   * @brief Logs a debug message to file and console.
   *
   * Optimized out on a release build.
   *
   * @param[in] source is the source from where the message is printed.
   * @param[in] format is the message itself.
   */
  void debug( const char* source, const char* format, ... );
  /**
   * @brief Print a hex dump with custom message to file and console.
   *
   * @param[in] source is the source from where the message is printed.
   * @param[in] data   is the data to be dumped.
   * @param[in] length is the length of data.
   * @param[in] format is the custom message.
   */
  void dump( const char* source, const void* data, size_t length,
	     const char* format, ... );

  /**
   * @brief Opens a logfile.
   *
   * @param[in] filename A name of the logfile.
   *
   * @retval true  The new logfile was successfully opened.
   * @retval false Failed to open the new logfile.
   */
  bool openLogfile( const char* filename );
  /**
   * @brief Sets the logfile to be used.
   *
   * Passed <var>file</var> is closed during destruction.
   *
   * @param[in] file A handle to file.
   *
   * @retval true  The new logfile was successfully opened.
   * @retval false Failed to open the new logfile.
   */
  bool setLogfile( FILE* file );

protected:
  /// A convenience color enum.
  enum Color
    {
      COLOR_DEFAULT, ///< A default color.
      COLOR_BLACK,   ///< Black color.
      COLOR_RED,     ///< Red color.
      COLOR_GREEN,   ///< Green color.
      COLOR_YELLOW,  ///< Yellow color.
      COLOR_BLUE,    ///< Blue color.
      COLOR_MAGENTA, ///< Magenta color.
      COLOR_CYAN,    ///< Cyan color.
      COLOR_WHITE,   ///< White color.

      COLOR_COUNT    ///< Number of colors.
    };

  /**
   * @brief Prints a hexadecimal dump.
   *
   * @param[in] color  Color of the message.
   * @param[in] prefix Single-character prefix/identificator.
   * @param[in] source Origin of message.
   * @param[in] data   Data to be dumped.
   * @param[in] length Length of <var>data</var> (in bytes).
   */
  void printDump( Color color, char prefix, const char* source,
		  const void* data, size_t length );
  /**
   * @brief Prints a line of a hexadecimal dump.
   *
   * @param[in] color  Color of the message.
   * @param[in] prefix Single-character prefix/identificator.
   * @param[in] source Origin of message.
   * @param[in] data   Data to be dumped.
   * @param[in] length Length of <var>data</var> (in bytes).
   * @param[in] offset An offset in <var>data</var> (in bytes).
   */
  void printDumpLine( Color color, char prefix, const char* source,
		      const void* data, size_t length, size_t offset );

  /**
   * @brief Prints a message.
   *
   * This prints a generic message.
   *
   * @param[in] color  Color of the message.
   * @param[in] prefix Single-character prefix/identificator.
   * @param[in] source Origin of message.
   * @param[in] format The format string.
   * @param[in] ...    The arguments.
   */
  void printMsg( Color color, char prefix, const char* source,
		 const char* format, ... );
  /**
   * @brief Prints a message.
   *
   * This prints a generic message.
   *
   * @param[in] color  Color of the message.
   * @param[in] prefix Single-character prefix/identificator.
   * @param[in] source Origin of message.
   * @param[in] format The format string.
   * @param[in] ap     The arguments.
   */
  void printMsgVa( Color color, char prefix, const char* source,
		   const char* format, va_list ap );
  /// Prints time.
  void printTime();
  /**
   * @brief Sets the log system time every main loop.
   *
   * @param[in] time is the timestamp.
   */
  void setTime( time_t time ) { mTime = time; }

  /**
   * @brief Prints a raw message.
   *
   * This method only handles printing to all desired
   * destinations (standard output and logfile at the moment).
   *
   * @param[in] format The format string.
   * @param[in] ...    The arguments.
   */
  void print( const char* format, ... );
  /**
   * @brief Prints a raw message.
   *
   * This method only handles printing to all desired
   * destinations (standard output and logfile at the moment).
   *
   * @param[in] format The format string.
   * @param[in] ap     The arguments.
   */
  void printVa( const char* format, va_list ap );

  /**
   * @brief Sets the color of the output text.
   *
   * @param[in] color The new color of output text.
   */
  void setColor( Color color );

  /// The active logfile.
  FILE* mLogfile;
  /// Current timestamp.
  time_t mTime;
  /// Protection against concurrent log messages
  Mutex mMutex;

# ifdef WIN32
  /// Handle to standard output stream.
  const HANDLE mStdOutHandle;
  /// Handle to standard error stream.
  const HANDLE mStdErrHandle;
  
  /// Color translation table.
  static const WORD COLOR_TABLE[ COLOR_COUNT ];
# else
  /// Color translation table.
  static const char* const COLOR_TABLE[ COLOR_COUNT ];
# endif /* !WIN32 */
};

/// Evaluates to a Log instance.
#define sLog \
  ( Log::get() )

#endif /* !__LOG__LOG_H__INCL__ */
