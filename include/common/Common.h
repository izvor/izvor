/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic, Thunder
*/

#ifndef __COMMON_H__INCL__
#define __COMMON_H__INCL__

/*
 * Custom config include.
 *
 * config.h (along with command-line parameters) should
 * describe the following (using defines listed below;
 * the default is in parenthesis):
 *
 *   a) the target platform
 *        X64, (x86)
 *        WIN32, CYGWIN, FREE_BSD, APPLE, (other)
 *   b) the compiler
 *        MSVC, GNUC, MINGW, (other)
 *   c) the configuration
 *        NDEBUG, (debug)
 *   d) other #defines
 *        RES_PATH - absolute path to resource directory
 *
 * The rest of code relies on these defines.
 */
#ifdef HAVE_CONFIG_H
#  include "config.h"
#endif /* HAVE_CONFIG_H */

/*
 * (Non-)MSVC configuration.
 */
#ifdef MSVC
#  define _CRT_SECURE_NO_WARNINGS                  1
#  define _CRT_SECURE_NO_DEPRECATE                 1
#  define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES  1

#  ifndef NDEBUG
//#    define _CRTDBG_MAP_ALLOC 1 - breaks Qt build in Debug mode
#  endif /* !NDEBUG */

#  define _SCL_SECURE_NO_WARNINGS 1
#endif /* MSVC */

/* 
 * Platform-dependant headers and stuff.
 */
#ifdef WIN32
// Define basic Windows configuration
#  define WIN32_LEAN_AND_MEAN
#  define _WIN32_WINNT 0x0500 // building for Win2k
#  define NOMINMAX
#  define MSG_NOSIGNAL 0
#  include <process.h>
#  include <windows.h>
#  include <winsock2.h>
#  include <ws2tcpip.h>
#  include <sys/timeb.h>

int gettimeofday( timeval* tv, void* reserved );
#else /* !WIN32 */
// Network
#  include <arpa/inet.h>
#  include <netdb.h>
#  include <netinet/in.h>
#  include <sys/socket.h>
#  include <fcntl.h>
// Threads
#  include <pthread.h>
// Time
#  include <sys/time.h>
// Miscellaneous
#  include <unistd.h>
#endif /* !WIN32 */

/*
 * Standard C library includes.
 */
#ifndef MSVC
#  include <inttypes.h>
#endif /* !MSVC */

/*
 * Standard C++ library includes.
 */
#include <cmath>
#include <csignal>
#include <cassert>
#include <cstdarg>
#include <cstdio>
#include <cerrno>

/*
 * Standard Template Library includes.
 */
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <list>
#include <set>
#include <stack>
#include <deque>
#include <queue>
#include <algorithm>
#include <new>
#include <memory>

/*
 * Boost includes.
 */
#include <boost/lexical_cast.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
#include <boost/functional/hash.hpp>
#include <boost/thread.hpp>
#include <boost/date_time.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/condition.hpp>

/*
 * Technical Report 1 Standard Template Library includes.
 *
 * Note: my fellow developers please read 'http://en.wikipedia.org/wiki/Technical_Report_1'. I know its a wiki page
 *       but it gives you the general idea.
 */
#ifdef MSVC
#  include <unordered_map>
#else /* !MSVC */
#  include <tr1/unordered_map>
#endif /* !MSVC */

/*
 * MSVC memory leak detection includes.
 */
#ifdef MSVC
#  ifndef NDEBUG
#    include <crtdbg.h>
//#    define new new( _NORMAL_BLOCK, __FILE__, __LINE__ ) - breaks Qt build in Debug mode
#  endif /* !NDEBUG */
#endif /* MSVC */

/*
 * Convenience typedefs.
 */

/*
 * u?int(8|16|32|64)
 */
#ifdef MSVC
typedef   signed __int8   int8;
typedef unsigned __int8  uint8;
typedef   signed __int16  int16;
typedef unsigned __int16 uint16;
typedef   signed __int32  int32;
typedef unsigned __int32 uint32;
typedef   signed __int64  int64;
typedef unsigned __int64 uint64;
#else /* !MSVC */
typedef  int8_t   int8;
typedef uint8_t  uint8;
typedef  int16_t  int16;
typedef uint16_t uint16;
typedef  int32_t  int32;
typedef uint32_t uint32;
typedef  int64_t  int64;
typedef uint64_t uint64;
#endif /* !MSVC */

/*
 * Additional utility macros.
 */

/*
 * MSVC:
 *   snprintf
 *   va_copy
 */
#ifdef MSVC
#  define snprintf _snprintf

#  define localtime_r( x, y ) \
   ( localtime_s( y, x ) )
#  define va_copy( a, b ) \
   ( memcpy( &( a ), &( b ), sizeof( va_list ) ) )
#endif /* MSVC */

/*
 * Some stuff that is Windows-specific and needs
 * to be explicitly defined for other platforms.
 */
#ifndef WIN32
typedef int SOCKET;
#  define INVALID_SOCKET ( (SOCKET)-1 )
#  define SOCKET_ERROR   ( (SOCKET)-1 )
#  if defined( FREE_BSD )
#    define MSG_NOSIGNAL 0
#  elif defined( APPLE )
#    define MSG_NOSIGNAL SO_NOSIGPIPE
#  endif
void Sleep( uint32 x );
uint32 GetTickCount();
#endif /* !WIN32 */

/*
 * MINGW:
 *   localtime_r
 *
 * Define localtime_r as a call to localtime.
 *
 * The page below says that MS's version of localtime uses
 * thread-specific storage, so this should not be a problem.
 *
 * http://msdn.microsoft.com/en-us/library/bf12f0hc(VS.80).aspx
 */
#ifdef MINGW
#  define localtime_r( x, y ) \
   ( (tm*)( memcpy( ( y ), localtime( x ), sizeof( tm ) ) ) )
#endif /* MINGW */

// Return thread macro's
// URL: http://msdn.microsoft.com/en-us/library/hw264s73(VS.80).aspx
// Important Quote: "_endthread and _endthreadex cause C++ destructors pending in the thread not to be called."
// Result: mem leaks under windows
#ifdef WIN32
#  define THREAD_RETURN( x ) ( x )
typedef void thread_return_t;
#else /* !WIN32 */
#  define THREAD_RETURN( x ) return ( x )
typedef void* thread_return_t;
#endif /* !WIN32 */

// Basic programming tips
// URL: http://nedprod.com/programs/index.html
// Note: always nullify pointers after deletion, why? because its safer on a MT application
#define SafeDelete( p )      { delete p; p = NULL; }
#define SafeDeleteArray( p ) { if( p != NULL ) { delete[] p; p = NULL; } }
#define SafeFree( p )        { if( p != NULL ) { free( p ); p = NULL; } }

#endif /* !__COMMON_H__INCL__ */
