/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __IZVOR_SERVER_PCH_H___INCL__
#define __IZVOR_SERVER_PCH_H___INCL__

/************************************************************************/
/* dep includes                                                         */
/************************************************************************/

#include "CommonPCH.h"

/************************************************************************/
/* common includes                                                      */
/************************************************************************/

#include "log/Log.h"
#include "database/Core.h"

#include "network/TcpConnection.h"
#include "network/TcpServer.h"

/************************************************************************/
/* izvor-common includes                                                */
/************************************************************************/

#include "network/IzvorTcpServer.h"
#include "network/IzvorTcpConnection.h"
#include "GameMgr.h"

/************************************************************************/
/* izvor-server includes                                                */
/************************************************************************/

#include "Client.h"

#endif /* !__IZVOR_SERVER_PCH_H___INCL__ */
