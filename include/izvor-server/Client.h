#ifndef __CLIENT_H__INCL__
#define __CLIENT_H__INCL__

#include "network/IzvorTcpConnection.h"

class Client
{
public:
    Client( IzvorTcpConnection **con )
        : mCon( *con )
    {
        *con = NULL;
    }
    ~Client()
    {
        SafeDelete( mCon );
    }

    void process();

private:

    IzvorTcpConnection* mCon;
};

#endif /* !__CLIENT_H__INCL__ */
