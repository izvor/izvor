#ifndef __NETWORK__PKT_REP_H__INCL__
#define __NETWORK__PKT_REP_H__INCL__

#include "util/RefPtr.h"

/**
* debug macro's to ease the increase and decrease of references of a object
* using this also increases the possibility of debugging it.
*/
#define PktIncRef(op) (op)->IncRef()
#define PktDecRef(op) (op)->DecRef()

/* Macros to use in case the object pointer may be NULL */
#define PktSafeIncRef(op) if( NULL == (op) ) ; else PktIncRef( op )
#define PktSafeDecRef(op) if( NULL == (op) ) ; else PktDecRef( op )

class PktVisitor;

class PktRep : public RefObject
{
public:

    // widen the access modifier for IncRef and DecRef
    using RefObject::IncRef;
    using RefObject::DecRef;

    enum Type
    {
        PktIntType,
        PktBoolType,
        PktStringType,
        PktTupleType,
        PktListType
    };

    bool isInt() const { return mType == PktIntType; }
    bool isBool() const { return mType == PktBoolType; }
    bool isString() const { return mType == PktIntType; }
    bool isTuple() const { return mType == PktTupleType; }
    bool isList() const { return mType == PktListType; }

    virtual PktRep* clone() const = 0;
    virtual bool visit( PktVisitor& v ) const = 0;
    void dump() const;

protected:
    PktRep( Type type );
    virtual ~PktRep();

    const Type mType;  
};

class PktInt : public PktRep
{
public:
    PktInt( const int32 i );
    PktInt( const PktInt& oth );

    PktRep* clone() const;
    bool visit( PktVisitor& v ) const;

    int32 value() const { return mValue; }

protected:
    const int32 mValue;
};

class PktString : public PktRep
{
public:
    PktString( const std::string& str );
    PktString( const char* str, size_t len );

    template< typename Iter >
    PktString( Iter first, Iter last )
        : PktRep( PktRep::PktStringType ), 
        mValue( first, last )
    {}


    PktString( const PktString& oth );

    PktRep* clone() const;
    bool visit( PktVisitor& v ) const;

    const std::string& content() const { return mValue; }

protected:
    const std::string mValue;
};

class PktBool : public PktRep
{
public:
    PktBool( bool b );
    PktBool( const PktBool& oth );

    PktRep* clone() const;
    bool visit( PktVisitor& v ) const;

    bool value() const { return mValue; }

protected:
    const bool mValue;
};

class PktTuple : public PktRep
{
public:
    typedef std::vector<PktRep*>            storage_type;
    typedef storage_type::iterator          iterator;
    typedef storage_type::const_iterator    const_iterator;

    PktTuple( size_t item_count );
    PktTuple( const PktTuple& oth );

    PktRep* clone() const;
    bool visit( PktVisitor& v ) const;

    const_iterator begin() const { return items.begin(); }
    const_iterator end() const { return items.end(); }
    size_t size() const { return items.size(); }
    bool empty() const { return items.empty(); }

    void clear();

    PktRep* getItem( size_t index ) const { return items.at( index ); }

    void setItem( size_t index, PktRep* object )
    {
        PktRep** rep = &items.at( index );
        PktSafeDecRef( *rep );
        *rep = object;
    }

    PktTuple& operator=( const PktTuple& oth );

    // public for now
    storage_type items;

protected:
    virtual ~PktTuple();
};

class PktList : public PktRep
{
public:
    typedef std::vector< PktRep* >            storage_type;
    typedef storage_type::iterator          iterator;
    typedef storage_type::const_iterator    const_iterator;

    PktList( size_t item_count = 0 );
    PktList( const PktList& oth );

    PktRep* clone() const;
    bool visit( PktVisitor& v ) const;

    const_iterator begin() const { return items.begin(); }
    const_iterator end() const { return items.end(); }

    size_t size() const { return items.size(); }
    bool empty() const { return items.empty(); }
    void clear();

    PktRep* getItem( size_t index ) const { return items.at( index ); }

    void setItem( size_t index, PktRep* object )
    {
        PktRep** rep = &items.at( index );

        PktSafeDecRef( *rep );
        *rep = object;
    }

    void addItem( PktRep* i ) { items.push_back( i ); }
    PktList& operator=( const PktList& oth );

    storage_type items;

protected:
    virtual ~PktList();
};

#endif /* !__NETWORK__PKT_REP_H__INCL__ */
