/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __NETWORK__IZVOR_TCP_CONNECTION_H__INCL__
#define __NETWORK__IZVOR_TCP_CONNECTION_H__INCL__

#include "network/TcpConnection.h"
#include "network/StreamPacketizer.h"

class PktRep;

class IzvorTcpConnection : public TcpConnection
{
    friend class IzvorTcpServer;

public:
    /// Time (in milliseconds) after which the connection is dropped if no data were received.
    static const uint32 TIMEOUT_MS;
    /// Hardcoded limit of packet size (NetClient.dll).
    static const uint32 PACKET_SIZE_LIMIT;

    /**
    * @brief Creates empty connection.
    */
    IzvorTcpConnection();

    void queueRep( PktRep *rep );
    PktRep* popRep();

protected:
    /**
    * @brief Creates new connection from existing socket.
    *
    * @param[in] sock  Socket to be used for connection.
    * @param[in] rIP   Remote IP the socket is connected to.
    * @param[in] rPort Remote TCP port the socket is connected to.
    */
    IzvorTcpConnection( Socket* sock, uint32 rIP, uint16 rPort );

    bool recvData( char* errbuf = 0 );
    bool processReceivedData( char* errbuf = 0 );

    void clearBuffers();

    /// Timer used to implement timeout.
    //    Timer mTimeoutTimer;

    /// Mutex to protect received data queue.
    Mutex mMInQueue;
    /// Received data queue.
    StreamPacketizer mInQueue;
};

#endif /* !__NETWORK__IZVOR_TCP_CONNECTION_H__INCL__ */
