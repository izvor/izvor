#ifndef __PACKET_FACTORY_H__INCL__
#define __PACKET_FACTORY_H__INCL__

#include "log/Log.h"
#include "network/Packet.h"
#include "network/AuthPacket.h"

class PacketFactory
{
public:

    static Packet* create( const Buffer& buff )
    {
        Packet *ret = NULL;

        Buffer::ConstIterator< uint8 > cur, end;
        cur = buff.begin< uint8 >();
        end = buff.end< uint8 >();

        Packet::Type packet_type = static_cast<Packet::Type>( *cur );
        const Buffer::ConstIterator< uint8 > start = ( cur + 1 ).as< uint8 >();

        Buffer *data = new Buffer( start, end );

        switch( packet_type ) {
        case Packet::AUTH:
            ret = new AuthPacket( *data );
            break;

        default:
            sLog.error( "PacketFactory", "Wrong packet type." );
        }

        if( !ret->decode() )
            sLog.error( "PacketFactory", "Error while decoding Packet." );

        SafeDelete( data );
        return ret;
    }
};

#endif /* !__PACKET_FACTORY_H__INCL__ */
