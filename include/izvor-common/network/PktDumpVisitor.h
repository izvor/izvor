#ifndef __NETWORK__PKT_DUMP_VISITOR_H__INCL__
#define __NETWORK__PKT_DUMP_VISITOR_H__INCL__

#include "Common.h"
#include "network/PktVisitor.h"

class PktDumpVisitor : public PktVisitor
{
public:
    PktDumpVisitor();

    bool visitInteger( const PktInt* rep );
    bool visitString( const PktString* rep );
    bool visitBoolean( const PktBool* rep );

    bool visitTuple( const PktTuple* rep );
    bool visitList( const PktList* rep );

    void _print( const char* fmt, ... );
};

#endif /* ! __NETWORK__PKT_DUMP_VISITOR_H__INCL__ */
