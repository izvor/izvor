#ifndef __NETWORK__PKT_VISITOR_H__INCL__
#define __NETWORK__PKT_VISITOR_H__INCL__

class PktInt;
class PktBool;
class PktString;
class PktTuple;
class PktList;

class PktVisitor
{
public:
    virtual ~PktVisitor() {}

    virtual bool visitInteger( const PktInt* rep ) { return true; }
    virtual bool visitBoolean( const PktBool* rep ) { return true; }
    virtual bool visitString( const PktString* rep ) { return true; }


    virtual bool visitTuple( const PktTuple* rep );
    virtual bool visitList( const PktList* rep );
};

#endif /* !__NETWORK__PKT_VISITOR_H__INCL__ */
