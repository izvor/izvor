/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __NETWORK__IZVOR_TCP_SERVER_H__INCL__
#define __NETWORK__IZVOR_TCP_SERVER_H__INCL__

#include "network/TcpServer.h"
#include "network/IzvorTcpConnection.h"

class IzvorTcpServer : public TcpServer<IzvorTcpConnection>
{
protected:
    virtual void createNewConnection( Socket *sock, uint32 rIP, uint16 rPort ) {
        addConnection( new IzvorTcpConnection( sock, rIP, rPort ) );
    }
};

#endif /* !__NETWORK__IZVOR_TCP_SERVER_H__INCL__ */
