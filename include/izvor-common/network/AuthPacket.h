#ifndef __AUTH_PACKET_H__INCL__
#define __AUTH_PACKET_H__INCL__

#include "network/Packet.h"
#include "log/Log.h"

class AuthPacket : public Packet
{
public:
    explicit AuthPacket( const Buffer& buff )
        : Packet( buff )
    {}

    explicit AuthPacket( const std::string& msg )
        : mStr( msg )
    {}

    bool encode( Buffer& dest ) const
    {
        if( mStr.empty() ) return false;

        Buffer::Iterator< uint8 > data = dest.end< uint8 >();

        dest.resizeAt( data, mStr.length() + 2 );

        *data++ = Packet::AUTH;

        for( std::string::const_iterator i = mStr.begin();
            i != mStr.end();
            i++ )
            *data++ = *i;

        sLog.dump( "queuePacket", &dest[0], dest.size(), "encoded: " );
        return true;
    }

    const std::string& getStr() const { return mStr; }

private:
    std::string mStr;

    bool decode()
    {
        for( Buffer::ConstIterator< uint8 > i = mPayload->begin< uint8 >();
            i != mPayload->end< uint8 >();
            i++ )
            mStr += static_cast< char >( *i );

        return true;
    }
};

#endif /* !__AUTH_PACKET_H__INCL__ */
