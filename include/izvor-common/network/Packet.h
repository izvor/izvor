#ifndef __NETWORK__PACKET_H__INCL__
#define __NETWORK__PACKET_H__INCL__

#include "util/Buffer.h"

class PacketFactory;

class Packet
{
public:

    enum Type
    {
        AUTH,
        PACKET_TYPE_COUNT
    };

    Packet()
        : mPayload( NULL )
    {}

    explicit Packet( const Buffer& buff )
        : mPayload( new Buffer( buff ) )
    {}

    virtual ~Packet() { SafeDelete( mPayload ); }

    virtual bool encode( Buffer& dest ) const = 0;

protected:
    friend class PacketFactory;

    virtual bool decode() = 0;

    Buffer *mPayload;
};

#endif
