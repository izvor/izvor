#ifndef __MARSHAL__MARSHAL_OPCODES_H__INCL__
#define __MARSHAL__MARSHAL_OPCODES_H__INCL__

enum PktRepOpcode
{
    Op_PktInt = 0x01,
    Op_PktTrue = 0x02,
    Op_PktFalse = 0x03,
    Op_PktEmptyString = 0x04,
    Op_PktCharString = 0x05,
    Op_PktLongString = 0x06,
    Op_PktTuple = 0x07,
    Op_PktList = 0x08,
    PktRepOpcodeMask = 0x0F
};

static const uint8 MarshalHeaderByte = 0x7E; // '~'
static const uint8 PktRepUnknownMask = 0x20;
static const uint8 PktRepSaveMask    = 0x10;

#endif /* !__MARSHAL__MARSHAL_OPCODES_H__INCL__ */
