#ifndef __MARSHAL__MARSHAL_STREAM_H__INCL__
#define __MARSHAL__MARSHAL_STREAM_H__INCL__

class PktRep;

#include "util/Buffer.h"
#include "network/PktVisitor.h"

extern bool marshal( const PktRep* rep, Buffer& into );
extern bool marshalDeflate( const PktRep* rep, Buffer& into, const uint32 deflationLimit = 0x2000 );

class MarshalStream
    : protected PktVisitor
{
public:
    MarshalStream();
    bool save( const PktRep* rep, Buffer& into );

protected:
    bool saveStream( const PktRep* rep );

    template< typename T >
    void put( const T& value ) { mBuffer->append< T >( value ); }

    template< typename Iter >
    void put( Iter first, Iter last ) { mBuffer->append< Iter >( first, last ); }

    void putSizeEx( uint32 size )
    {
        if( size < 0xFF ) {
            put< uint8 >( size );
        }
        else {
            put< uint8 >( 0xFF );
            put< uint32 >( size );
        }
    }

    bool visitInteger( const PktInt* rep );
    bool visitBoolean( const PktBool* rep );
    bool visitString( const PktString* rep );
    bool visitTuple( const PktTuple* rep );

private:
    Buffer *mBuffer;
};

#endif /* !__MARSHAL__MARSHAL_STREAM_H__INCL__ */
