#ifndef __MARSHAL__UNMARSHAL_H__INCL__
#define __MARSHAL__UNMARSHAL_H__INCL__

#include "network/PktRep.h"
#include "util/Buffer.h"

/**
* @brief Turns marshal stream into Pkt object.
*
* @param[in] data Marshal stream.
*
* @return Ownership of Pkt object.
*/
extern PktRep* unmarshal( const Buffer& data );
/**
* @brief Turns possibly inflated marshal stream into Pkt object.
*
* @param[in] data Possibly inflated marshal stream.
*
* @return Ownership of Pkt object.
*/
extern PktRep* inflateUnmarshal( const Buffer& data );

/**
* @brief Class which turns marshal bytecode into Pkt object.
*
* @author Zhur, Bloody.Rabbit
*/
class UnmarshalStream
{
public:
    UnmarshalStream()
        : mStoredObjects( NULL )
    {}
    /**
    * @brief Loads Pkt object from given bytecode.
    *
    * @param[in] data Buffer containing marshal bytecode.
    *
    * @return Loaded Pkt object.
    */

    PktRep* load( const Buffer& data );
protected:
    /** Peeks element from stream. */
    template< typename T > 
    const T& peek() const { return *peek< T >( 1 ); }
    /** Peeks element from stream. */
    template< typename T >
    Buffer::ConstIterator< T > peek( size_t count ) const { return mInItr.as< T >(); }

    /** Reads element from stream. */
    template< typename T >
    const T& read() { return *read< T >( 1 ); }
    /** Reads element from stream. */
    template< typename T >
    Buffer::ConstIterator< T > read( size_t count )
    {
        Buffer::ConstIterator< T > res = peek< T >( count );
        mInItr = ( res + count ).template as< uint8 >();
        return res;
    }

    /** Reads extended size from stream. */
    uint32 readSizeEx()
    {
        uint32 size = read< uint8 >();
        if( 0xFF == size )
            size = read< uint32 >();

        return size;
    }

    /** Initializes loading and loads rep from stream. */
    PktRep* loadStream( size_t streamLength );

    /** Loads rep from stream. */
    PktRep* loadRep();

    /**
    * @brief Initializes object store.
    *
    * @param[in] streamLength Length of stream.
    * @param[in] saveCount    Number of saved objects within the stream.
    */
    void createObjectStore( size_t streamLength, uint32 saveCount );
    /**
    * @brief Destroys object store.
    */
    void destroyObjectStore();

    /**
    * @brief Obtains storage index for StoreObject.
    *
    * @return Storage index.
    */
    uint32 getStorageIndex() { return *mStoreIndexItr++; }
    /**
    * @brief Obtains previously stored object.
    *
    * @param[in] index Index of stored object.
    *
    * @return The stored object.
    */
    PktRep* getStoredObject( uint32 index );
    /**
    * @brief Stores object.
    *
    * @param[in] index  Index of object.
    * @param[in] object The object to be stored.
    */
    void storeObject( uint32 index, PktRep* object );

private:

    /** Loads integer from stream. */
    PktRep* loadInteger() { return new PktInt( read< uint32 >() ); }
    /** Loads true boolean from stream. */
    PktRep* loadBoolTrue() { return new PktBool( true ); }
    /** Loads false boolean from stream. */
    PktRep* loadBoolFalse() { return new PktBool( false ); }
    /** Loads empty string from stream. */
    PktRep* loadStringEmpty() { return new PktString( "" ); }
    /** Loads single character string from stream. */
    PktRep* loadStringChar();
    /** Loads long (no limit) string from stream. */
    PktRep* loadStringLong();
    /** Loads tuple from stream. */
    PktRep* loadTuple();
    /** Loads list from stream. */
    PktRep* loadList();
    /** Buffer iterator we are processing. */
    Buffer::ConstIterator< uint8 > mInItr;
    PktRep* loadError();

    /** Next store index for referencing in the buffer. */
    Buffer::ConstIterator< uint32 > mStoreIndexItr;
    /** Referenced objects within the buffer. */
    PktList* mStoredObjects;

    /** Load function map. */
    static PktRep* ( UnmarshalStream::* const sLoadMap[] )();

};

#endif /* !__MARSHAL__UNMARSHAL_H__INCL__ */
