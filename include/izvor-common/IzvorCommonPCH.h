/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __IZVOR_COMMON_PCH_H__INCL__
#define __IZVOR_COMMON_PCH_H__INCL__

/*************************************************************************/
/* common + dep includes                                                 */
/*************************************************************************/
#include "CommonPCH.h"

/*************************************************************************/
/* database includes                                                     */
/*************************************************************************/
#include "database/Record.h"
#include "database/RecordSet.h"
#include "database/Decodable.h"
#include "database/Core.h"

/*************************************************************************/
/* log includes                                                          */
/*************************************************************************/
#include "log/Log.h"

/*************************************************************************/
/* network includes                                                      */
/*************************************************************************/
#include "network/NetUtils.h"
#include "network/Socket.h"
#include "network/StreamPacketizer.h"
#include "network/TcpConnection.h"
#include "network/TcpServer.h"

/*************************************************************************/
/* event includes                                                        */
/*************************************************************************/
#include "event/Event.h"

/*************************************************************************/
/* threading includes                                                    */
/*************************************************************************/
#include "threading/Mutex.h"

/*************************************************************************/
/* util includes                                                         */
/*************************************************************************/
#include "util/Buffer.h"
#include "util/Data.h"
#include "util/Deflate.h"
#include "util/ExcMsg.h"
#include "util/Lock.h"
#include "util/Misc.h"
#include "util/RefPtr.h"
#include "util/Singleton.h"

/*************************************************************************/
/* xml includes                                                          */
/*************************************************************************/
#include "xml/XmlParser.h"

#endif /* !__IZVOR_COMMON_PCH_H__INCL__ */
