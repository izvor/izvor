#ifndef __SCRIPT__SCRIPT_PARSER_H__INCL__
#define __SCRIPT__SCRIPT_PARSER_H__INCL__

#include "xml/XmlParser.h"
#include "core/CardElement.h"
#include "core/BaseCard.h"

namespace IzvorCommon
{
	class ScriptParser : public XmlParser
	{
	public:
		static const char* SCRIPT_PATH;
	
		ScriptParser( IzvorCommon::Core::CardElement *ce )
			: XmlParser( SCRIPT_PATH + boost::lexical_cast< std::string >
						 ( ce->getBaseCard()->getDBID() ) + ".xml" ),
			  mTarget( ce )
		{}

	protected:
		virtual bool _parse( TiXmlNode *pParent );

	private:

		IzvorCommon::Core::CardElement *mTarget;
	};
}

#endif /* !__SCRIPT__SCRIPT_PARSER_H__INCL__*/
