/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __SCRIPT__EVENT_MGR_H__INCL__
#define __SCRIPT__EVENT_MGR_H__INCL__

#include "event/Event.h"
#include "core/Game.h"

#define DECL_EVT_NOARG(evt)									\
	class evt##Event : public Event< evt##Event >			\
	{														\
	public: friend class TriggerManager;					\
			evt##Event() : Event< evt##Event >( NULL ) {}	\
	};														\
	typedef Event< evt##Event >::Listener evt##Listener

#define DECL_EVT_START(evt)									\
	class evt##Event : public Event< evt##Event >			\
	{														\
	public: friend class TriggerManager;					\
			evt##Event() : Event< evt##Event >( NULL ) {}

#define DECL_EVT_END(evt)									\
	};														\
	typedef Event< evt##Event >::Listener evt##Listener

namespace IzvorCommon
{	
	class TriggerManager
	{
	public:
		
		enum {
			EVT_NONE                  = 0x00,
			EVT_DRAW_CARD             = 0x01,
			EVT_PERMANENT_ENTERS_PLAY = 0x02,
			EVT_ALL                   = 0xFF
		} EventType;
			
		DECL_EVT_START( DrawCard );
		// attributes go here, i am too lazy to write a propper class,
		// so i am using these lame macros, will need to fix that when
		// i have nuff time
		bool mPlayerOneDrew; // does not change
		Core::CardElement *mDrawn;
		DECL_EVT_END( DrawCard );

		DECL_EVT_START( PermanentEntersPlay );
		Core::Permanent *mEntered;
		DECL_EVT_END( PermanentEntersPlay );

		static void init( Core::Game *game );
		
		// this is an adapter! redefine virtual methods in children classes
		// to respond to an event
		class GenericListener : public DrawCardListener,
								public PermanentEntersPlayListener
		{
		public:
			GenericListener( uint32 type )
				: DrawCardListener(),
				  PermanentEntersPlayListener(),
				  mType( type )
			{}
			
			void connect();
			void disconnect();

			uint32 getType() const { return mType; }
		protected:
			virtual void eventOccured( DrawCardEvent *evt );
			virtual void eventOccured( PermanentEntersPlayEvent *evt );
			// inherit all listeners
		private:
			
			const uint32 mType; // mask of TriggerManager::EventTypes
			                    // which events this listener is subscribed to
		};

		// for all events
		static void onEventOccured( DrawCardEvent *evt, uint32 objectID );
		static void onEventOccured( PermanentEntersPlayEvent *evt, uint32 objectID );

		static DrawCardEvent s_mDrawCardP1;
		static DrawCardEvent s_mDrawCardP2;
		static PermanentEntersPlayEvent s_mPermanentEntersPlay;
	};
}

#endif /* !__SCRIPT__EVENT_MGR_H__INCL__ */
