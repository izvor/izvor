/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __REQUESTS_H__INCL__
#define __REQUESTS_H__INCL__

namespace IzvorCommon
{
	// note: maybe pass pointers in GuiRequest... cuz its faster?
   	// ---------- Request ---------- //
	class Request
	{
	public:

		enum ReqType {
			// no-operation request, we might need it as a
			// keep-alive later
			REQ_NOP,
			
			// non-blocking GUI requests
			// =========================
			// CREATE_DECK:
			// * args: list of N + 1 elements
			//         1. player owner ID
			//         2. N card IDs in the deck
			// * ret : none
			REQ_GUI_CREATE_DECK,
			// DRAW_CARD:
			// * args: list of 2 elements
			//         1. card id
			//         2. player id
			// * ret : none
			REQ_GUI_DRAW_CARD,
			// MOVE_CARD:
			// * args: list of 3 elements:
			//         1. card id
			//         2. player id
			//         3. what zone - see the enum in GuiMoveCardRequest
			// * ret : none
			REQ_GUI_MOVE_CARD,
			// =========================

			// blocking GUI requests
			// =========================
			// CLICK_CARD:
			// * args: none
			// * ret : call to Game::setClickedID( uint32 id ) from
			//         within GUI (game thread is blocked until calling
			//         this method), id - clicked card ID
			// =========================
			REQ_GUI_CHOOSE_CARD,
			// CLICK_CARDS:
			// * args: none
			// * ret : call to Game::setClickedList( std::list< uint32 > *lst ) 
			//         from within GUI (game thread is blocked until calling
			//         this method), lst - newly allocated list containing IDs of
			//         clicked cards. GUI must provide a way for the player to stop
			//         selecting cards (like a button or a key).
			// =========================
			REQ_GUI_CHOOSE_CARDS,

			// sentinel for iteration
			REQ_SENTINEL
		};

		Request( ReqType reqT )
			: mType( reqT ), mData( NULL )
		{}
		virtual ~Request() { SafeDelete( mData ); }

		ReqType getType() const { return mType; }

	protected:
		friend class RequestHandler;

		virtual bool marshal() = 0;
		virtual bool unmarshal() =0;

		ReqType mType;
		
		// the only reason we can send ptrs is because
		// threads use the same address space, we will
		// need to change this later.
		std::list< uint32 > *mData;
	};
	// ---------- !Request ---------- //

	// ---------- GuiNoArgRequest ---------- //
	class GuiNoArgRequest : public Request
	{
	public:
		GuiNoArgRequest( Request::ReqType reqT )
			: Request( reqT )
		{}

	protected:
		bool marshal() { return true; } // no args = no marshalling
        bool unmarshal() { return true; }
	};
	// ---------- !GuiNoArgRequest ---------- //

	// ---------- GuiCreateDeckRequest ---------- //
	class GuiCreateDeckRequest : public Request
	{
	public:
		GuiCreateDeckRequest( uint32 playerID )
			: Request( Request::REQ_GUI_CREATE_DECK ),
			  mPlayerID( playerID )
		{}

	protected:
		bool marshal();
        bool unmarshal() {}
	private:

		uint32 mPlayerID;
	};
	// ---------- !GuiCreateDeckRequest ---------- //

	// ---------- GuiDrawCardRequest ---------- //
	class GuiDrawCardRequest : public Request
	{
	public:
		GuiDrawCardRequest( uint32 cardID )
			: Request( Request::REQ_GUI_DRAW_CARD ),
			  mCardID( cardID )
		{}

	protected:
		bool marshal();
        bool unmarshal();

	private:
		
		uint32 mCardID;
	};
	// ---------- !GuiDrawCardRequest ---------- //

	// ---------- GuiMoveCardRequest ---------- //
	class GuiMoveCardRequest : public Request
	{
	public:
		enum Dest {
			DEST_HAND,
			DEST_ATTACK_DEFENSE,
			DEST_PERMANENTS,
			DEST_ARTEFACTS,
			DEST_LIB,
			DEST_GY,
			DEST_SOURCES,
			DEST_STACK
		};

		GuiMoveCardRequest( uint32 cardID, Dest dest )
			: Request( Request::REQ_GUI_MOVE_CARD ),
			  mCardID( cardID ), mDest( dest )
		{}

	protected:
		bool marshal();
        bool unmarshal();

	private:
		
		uint32 mCardID;
		Dest mDest;
	};
	// ---------- !GuiMoveCardRequest ---------- //

};

#endif /* !__REQUESTS_H__INCL__ */
