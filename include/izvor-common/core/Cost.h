/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__COST_H__INCL__
#define __CORE__COST_H__INCL__

#include "database/Decodable.h"
#include "core/Mana.h"
#include "script/ScriptParser.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Symbol;
		class BaseCard;
		class ActivatedAbility;
		class Player;

		// ---------- Cost ---------- //
		class Cost
		{
		public:
			Cost() {}
			~Cost();

			void addSymbol( Symbol* symbol )
			{
				mSymbols.push_back( symbol );
			}
			// populates mana, returnes false if arbitrary 
			// (X symbol encountered) 
			bool convertToMana( Mana *dest ) const;
			bool isEmpty() const { return mSymbols.empty(); }

			virtual bool pay( Player *targetPlayer ) = 0;

			void print() const;

		protected:

			std::list< Symbol* > mSymbols;			
		};
		// ---------- !Cost ---------- //

		// fix this, inheritance tree fucked up
		// ---------- AbilityCost ---------- //
		class AbilityCost : public Cost
		{
		public:
			AbilityCost( ActivatedAbility *parent )
				: mParent( parent )
			{}

			bool pay( Player *targetPlayer );
		private:
		   
			ActivatedAbility *mParent;
		};
		// ---------- !AbilityCost ---------- //

		// ---------- ManaCost ---------- //
		// contains only mana symbols,
		// this class is used with BaseCard
		// can have following symbols: ManaSymbol, XSymbol
		class ManaCost : public Cost,
						 public DB::Decodable
		{
		public:
			bool pay( Player *targetPlayer );

		protected:
			friend class BaseCard;

			ManaCost( const DB::Record& r, BaseCard *parent ) 
				: Cost(),
				  mParent( parent )
			{
				decode( r );
			}
			void decode( const DB::Record& r );

			friend class IzvorCommon::ScriptParser; // for mana tags

			static Symbol* createSymbol( const Color& color, char token );
			static void parseStr( const std::string& manaString, Cost *dest );

		private:

			BaseCard *mParent;
		};
		// ---------- !ManaCost ---------- //
	}
}

#endif /* !__CORE__COST_H__INCL__ */
