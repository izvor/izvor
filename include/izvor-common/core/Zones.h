/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__ZONES_H__INCL__
#define __CORE__ZONES_H__INCL__

#include "event/Event.h"
#include "core/CardSet.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Player;
		class Permanent;

		// Don't know if we will need this, but let it stay
		class Zone : public CardSet
		{
		public:
			Zone( Object *owner )
				: CardSet( owner )
			{}
		};
	
		class Library : public Observable,
						public Zone
					
		{
		public:
			Library( Player *owner )
				: Zone( owner ),
				  m_DrawCardEvt( this ),
				  m_LibraryEmptyEvt( this )
			{}

			friend class DrawCardEvt;
			class DrawCardEvt : public Event< DrawCardEvt >
			{
			public:
				DrawCardEvt( Library *parent )
					: Event< DrawCardEvt >( parent )
				{}
			} m_DrawCardEvt;		

			friend class LibraryEmptyEvt;
			class LibraryEmptyEvt : public Event< LibraryEmptyEvt >
			{
			public:
				LibraryEmptyEvt( Observable *parent )
					: Event< LibraryEmptyEvt >( parent ) {}
			} m_LibraryEmptyEvt;

			void shuffle();
			void drawToHand( uint32 count = 1 );
			void loadFromDeck();
		};
	
		class Hand : public Zone
		{
		public:
			Hand( Player *owner )
				: Zone( owner )
			{}
		
			bool play( uint32 ceID, bool asSource = false );
		};
	
		class Exile : public Zone
		{
		public:
			Exile( Game *owner )
				: Zone( (Object*) owner )
			{}
		};
	
		class Graveyard : public Zone
		{
		public:
			Graveyard( Player *owner )
				: Zone( owner )
			{}
		};

		class Battlefield : public Zone,
							public Observable
		{
		public:
			friend class CardEntersPlayEvt;
			class PermanentEntersPlayEvt : public Event< PermanentEntersPlayEvt >
			{
			public:
				PermanentEntersPlayEvt( Observable *owner )
					: Event< PermanentEntersPlayEvt >( owner )
				{}
			
				Permanent *pEntered;
			} m_PermanentEntersPlayEvt;

			Battlefield( Game *owner )
				: Zone( (Object*) owner ),
				  m_PermanentEntersPlayEvt( this )
			{}
		
			void addCardElement( Permanent *pe );
			void untapAll( Player *ctrl );
		
			// a part of stateEffectsCheck, sends creatures
			// with toughness <= 0 to graveyard
			void checkCreatureToughness();
		};

		// Stack may also be a zone but it's certainly not a card set...
	}
}

#endif /* !__CORE__ZONE_H__INCL__ */
