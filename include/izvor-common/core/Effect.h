/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__EFFECT_H__INCL__
#define __CORE__EFFECT_H__INCL__

#include "core/EffectMgr.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Effect
		{

		public:
			typedef std::tr1::unordered_map< const std::string, std::string,
											 boost::hash< const std::string > > ArgumentMap;

			Effect( uint32 id )
				: mID( id )
			{}

			void addArgument( const std::string& key, const std::string& value )
			{
				mArgs[key] = value;
			}

			void print() const;

			uint32 getID() const { return mID; }
			ArgumentMap* getArgs() { return &mArgs; }

		protected:
			friend class StackAbility;
			void resolve();

			uint32 mID;
			ArgumentMap mArgs;
		};

		class TextChangingEffect : public Effect
		{};

		class OneShotEffect : public Effect
		{};

		class ContinuousEffect : public Effect
		{};

		class ReplacementEffect : public Effect
		{};

		class PreventionEffect : public Effect
		{};
	}
}

#endif /* !__CORE__EFFECT_H__INCL__ */
