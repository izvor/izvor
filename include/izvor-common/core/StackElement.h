/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__STACK_ELEMENT_H__INCL__
#define __CORE__STACK_ELEMENT_H__INCL__

#include "core/Object.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Stack;
   
		class Stackable
		{
		public:
			friend class StackElement;

			virtual ~Stackable() {};

		protected:
			virtual void resolve() = 0;
			virtual bool prevented() = 0;
			virtual bool cleanup() = 0;

			void push( Stack *stack );
		};

		// A proxy class for Stackable, for deletion issues and multiple
		// Object inheritance
		class StackElement : public Object,
							 public Stackable
		{			
		protected:

			friend class Stackable;

			StackElement( Stackable *element, Stack *parent, uint32 id = 0 );

			virtual void resolve() { mElement->resolve(); }
			virtual bool prevented() { 
				if( mElement->prevented() ) SafeDelete( mElement );
				return true; // doesnt mean anything yet
			}
			virtual bool cleanup() { 
				if( mElement->cleanup() ) SafeDelete( mElement );
				return true;
			}

			bool isPrevented() const { return mPrevented; }
			void setPrevented() { mPrevented = true; }

		private:

			friend class Stack;
			Stackable *mElement;
			Stack *mParent;

			bool mPrevented;
		};
	}
}

#endif /* !__CORE__STACK_ELEMENT_H__INCL__ */
