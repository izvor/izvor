/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__PLAYER_H__INCL__
#define __CORE__PLAYER_H__INCL__

#include "core/Object.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Game;
		class Library;
		class Graveyard;
		class Hand;
		class Deck;
		class Mana;

		class Player : public Object
		{
		public:
			friend class Game;

			Player( Game *parent );
			~Player();

			void dealDamage( uint32 ammount, const Color& color ) {}
		
			Game* getGame() { return mParent; }
		
			Library*     getLibrary()     { return mLibrary; }
			Graveyard*   getGraveyard()   { return mGraveyard; }
			Hand*        getHand()        { return mHand; }

			Deck*        getDeck()        { return mDeck; }

			bool isActive() const { return mActive; }
			bool hasPriority() const { return mHasPriority; }

			void setActive( bool active ) { mActive = active; }
			void setPriority( bool priority ) { mHasPriority = priority; }

			int32 getLife() const { return mLife; }
			Mana* getManaPool() const { return mManaPool; }

			bool canPlaySourceThisTurn() const { return !mPlayedSourceThisTurn; }

		private:

			Game *mParent;

			int32 mLife;
			bool mActive, mHasPriority;

			Library *mLibrary;
			Graveyard *mGraveyard;
			Hand *mHand;

			Deck *mDeck;

			Mana *mManaPool;
		
			bool mPlayedSourceThisTurn;
		};
	}
}

#endif /* !__CORE__PLAYER_H__INCL__ */
