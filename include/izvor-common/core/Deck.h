/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__DECK_H__INCL__
#define __CORE__DECK_H__INCL__

#include "core/CardSet.h"

namespace IzvorCommon
{
	namespace Core
	{
		class BaseCard;

		class Deck
		{
		public:
			
			typedef std::tr1::unordered_map< uint32, uint32,
											 boost::hash< uint32 > > DeckContainer;

			void addCard( uint32 cardDBID, uint32 count );
			void addSideCard( uint32 cardDBID, uint32 count );

			static const uint32 MIN_SZ = 40;
			
			void setName( const std::string& name ) { mName = name; }
			void setAuthor( const std::string& author ) { mAuthor = author; }
			void setDesc( const std::string& desc ) { mDescription = desc; }
		
			const std::string& getName() const { return mName; }
			const std::string& getAuthor() const { return mAuthor; }
			const std::string& getDesc() const { return mDescription; }

			uint32 getCardCount( uint32 cardDBID, bool sideDeck ) const;

			bool isEmpty( bool sideDeck = false ) const
			{ return (sideDeck) ? mSideCards.empty() : mCards.empty(); }

		private:
			friend class DeckParser;
			friend class Library;

			std::string mName;
			std::string mAuthor;
			std::string mDescription;

			DeckContainer mCards, mSideCards;
		};
	}
}

#endif /* !__CORE__DECK_H__INCL__ */
