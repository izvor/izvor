/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__CARD_MGR_H__INCL__
#define __CORE__CARD_MGR_H__INCL__

#include "util/Singleton.h"

namespace IzvorCommon
{
	namespace Core
	{
		class BaseCard;

		// Acts as a card manager, a database interface and a BaseCard factory. Singleton.
		class CardManager : public Singleton< CardManager >
		{
		public:

			typedef std::tr1::unordered_map< const uint32, const BaseCard*, 
											 boost::hash< const uint32 > > CardMap;
		
			~CardManager();
		
			const BaseCard* getBaseCard( uint32 cardDBID );
			const BaseCard* getBaseCard( const std::string cardName )
			{
				return getBaseCard( getDBIDByName( cardName ) );
			}

			uint32          getCachedCount() const { return mCache.size(); }
		
		private:
			uint32           getDBIDByName( const std::string& cardName ) const;
			// hash searching - fast
			const BaseCard*  findCached( uint32 cardDBID ) const
			{
				if( !mCache.size() )
					return NULL;

				CardMap::const_iterator i = mCache.find( cardDBID );
				return (i == mCache.end()) ? NULL : i->second;
			}
			const BaseCard* fetch( uint32 cardDBID );

			// ID --> BaseCard* hash map
			CardMap mCache;
		};	
	}
}

#endif /* !__CORE__CARD_MGR_H__INCL__ */
