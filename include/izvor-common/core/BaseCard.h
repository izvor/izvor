/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__BASE_CARD_H__INCL__
#define __CORE__BASE_CARD_H__INCL__

#include "database/Decodable.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Color;
		class ManaCost;
		class CardManager;

		// ------------------- BaseCard ------------------- //
		// We should only have ONE instance of BaseCard throughout the game.
		// First created at the start of the game, it's a link between the DB
		// and the game. All card instances that represent this card containt a
		// reference to it.
		class BaseCard : public DB::Decodable
		{
		public:
			// ------------------- BaseCard::Info ------------------- //
			// irrelevant for gameplay, pretty info for displaying
			class Info : public DB::Decodable
			{
			public:

				enum Rarity {
					OBICNA            = 0x4,  // DBID 0
					SREBRNA           = 0x2,  // DBID 1
					ZLATNA            = 0x1,  // DBID 2
					NEPOZNATA_RETKOST = 0x0   // -1
				};

				enum Edition {
					IZVORISTE         = 0x10, // DBID 0
					PLEMENSKI_RATOVI  = 0x08, // DBID 1
					DOBA_SAVEZA       = 0x04, // DBID 2
					DOBA_MISTERIJA    = 0x02, // DBID 3
					TRECA_EDICIJA     = 0x01, // DBID 4
					NEPOZNATA_EDICIJA = 0x00  // -1
				};

				static const uint8 RARITY_NUM = 3;  
				static const uint8 EDITION_NUM = 5;

				static Rarity getRarityByID( uint8 id );
				static const std::string& getRarityStr( Rarity rarity )
				{
					return sRarityName[rarity];
				}
				static const std::string& getRarityStrByID( uint8 id )
				{
					return sRarityName[ getRarityByID( id ) ];
				}

				static Edition getEditionByID( uint8 id );
				static const std::string& getEditionStr( Edition edition )
				{
					return sEditionName[edition];
				}
			
				static const std::string& getEditionStrByID( uint8 id )
				{
					return sEditionName[ getEditionByID( id ) ];
				}
				static const std::string& getEditionImgPath( Edition edition )
				{
					return sEditionImgPath[ edition ];
				}
				static const uint32 getEditionSize( Edition edition )
				{
					return sEditionSize[edition];
				}
				static const uint32 getEditionSizeByID( uint8 id )
				{
					return sEditionSize[ getEditionByID( id ) ];
				}

				const std::string& getName()           const { return mName; }
				const std::string& getAbilityStr()     const { return mAbilityStr; }
				const std::string& getLore()           const { return mLore; }
				const std::string& getImageAuthor()    const { return mImageAuthor; }
				Edition            getEdition()        const { return mEdition; }
				Edition            getReprintEdition() const { return mReprintEdition; }
				uint32             getNum()            const { return mNumber; }
				uint32             getReprintNum()     const { return mReprintNumber; }
				Rarity             getRarity()         const { return mRarity; }

				bool isFromEdition( Edition edition ) const
				{
					return mEdition == edition || mReprintEdition == edition;
				}

			protected:
				friend class BaseCard;
				Info( const DB::Record& r, BaseCard *parent );
				void decode( const DB::Record& r );
			
			private:
				void addEdition( uint32 CantorID )
				{
					// hardcoded in db
					uint32 num, edition;

					CantorUnPair( CantorID, &num, &edition );
					if( mEdition == NEPOZNATA_EDICIJA ) {
						mEdition = getEditionByID( edition - 1 );
						mNumber = num;
					}
					else {
						mReprintEdition = getEditionByID( edition - 1 );
						mReprintNumber = num;
					}
				}

				static std::tr1::unordered_map< const Rarity, const std::string, 
												boost::hash< const Rarity > > sRarityName;
				static std::tr1::unordered_map< const Edition, const uint32, 
												boost::hash< const Edition > > sEditionSize;
				static std::tr1::unordered_map< const Edition, const std::string, 
												boost::hash< const Edition > > sEditionName;
				static std::tr1::unordered_map< const Edition, const std::string, 
												boost::hash< const Edition > > sEditionImgPath;

				std::string mName;
				std::string mAbilityStr;
				std::string mLore;
				std::string mImageAuthor;
				Rarity      mRarity;
				Edition     mEdition, mReprintEdition;
				uint32      mNumber, mReprintNumber;

				BaseCard   *mParent;
			};
			// ------------------- !BaseCard::Info ------------------- //

			enum Type {
				IZVOR         = 0x20, // DBID 0
				ARTEFAKT      = 0x10, // DBID 1
				// ARTEFAKT_BICE = 0x10, // DBID 2 // pravila kazu da to nije poseban tip
				// ARTEFAKT_OPREMA,
				BICE          = 0x08, // DBID 3
				//SIMBOL_BICE,
				IZNENADJENJE  = 0x04, // DBID 4
				VRADZBINA     = 0x02, // DBID 5
				PROMENA       = 0x01, // DBID 6
				// PROMENA_OREOL,
				NEPOZNAT_TIP  = 0x00  // -1
			};

			static const uint8 TYPE_NUM = 7; 
		
			static uint8 getTypeByID( uint8 id );
			static const std::string& getTypeStr( Type type ) {
				return sTypeName[type];
			}
			static const std::string& getTypeStrByID( uint8 id ) {
				return sTypeName[ static_cast< Type >( getTypeByID( id ) ) ];
			}

			uint32    getDBID()          const { return mDBID; }
			int32     getBasePower()     const { return mBasePower; }
			int32     getBaseToughness() const { return mBaseToughness; }
			uint8     getBaseType()      const { return mBaseType; }
			Color*    getBaseColor()     const { return mBaseColor; }
			ManaCost* getBaseManaCost()  const { return mBaseCost; }
			Info*     getInfo()          const { return mInfo; }

			// ability is obtained through AbilityMgr

			// slow
			std::string getBaseSecondaryTypeStr() const;
			const std::list< std::string >& getBaseSecondaryType() const { return mBaseSecondaryType; }

			// checks both primary and secondary types
			bool isOfType( const std::string& type ) const;
			// checks only primary types
			bool isOfType( Type type ) const 
			{ 
				return mBaseType & static_cast< uint8 >( type );
			}

		protected:
			friend class CardManager;

			// BaseCard()
			// 	: mDBID( 0 ),
			// 	  mBaseType( NEPOZNAT_TIP ),
			// 	  mBasePower( 0 ), mBaseToughness( 0 ),
			// 	  mBaseColor( NULL ), mBaseCost( NULL ), mInfo( NULL )
			// {}
			BaseCard( const DB::Record& r )
				: mBasePower( 0 ), mBaseToughness( 0 ),
				  mBaseColor( NULL ), mBaseCost( NULL ), mInfo( NULL )
			{
				decode( r );
			}

			~BaseCard();
			void decode( const DB::Record& r );

		private:
			BaseCard( const BaseCard& c ) {}
		
			uint32      mDBID;

			uint8       mBaseType;
			int32       mBasePower, mBaseToughness;
			Color*      mBaseColor;
			ManaCost*   mBaseCost;
			Info*       mInfo;

			std::list< std::string > mBaseSecondaryType;

			static std::tr1::unordered_map< const Type, const std::string, 
											boost::hash< const Type >  > sTypeName;
		};
		// ------------------- !BaseCard ------------------- //
	}
}

#endif /* !__CORE__BASE_CARD_H__INCL__ */
