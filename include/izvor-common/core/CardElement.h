/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__CARD_ELEMENT_H__INCL__
#define __CORE__CARD_ELEMENT_H__INCL__

#include "core/Object.h"

namespace IzvorCommon
{
	namespace Core
	{
		class BaseCard;
		class CardSet;
		class StaticAbility;
		class ActivatedAbility;
		class TriggeredAbility;

		// This represents an ingame Card object
		class CardElement : public Object
		{
		public:
			CardElement( CardSet *owner, const BaseCard *parent, uint32 id = 0 );
			CardElement( CardElement& c, CardSet *newOwner );

			~CardElement();

			void setStaticAbility( StaticAbility *statics )
			{
				mStatics = statics;
			}

			StaticAbility* getStaticAbility() const
			{
				return mStatics;
			}

			void pushActivatedAbility( ActivatedAbility* ability )
			{
				mActives.push_back( ability );
			}
			void pushTriggeredAbility( TriggeredAbility* ability )
			{
				mTrigs.push_back( ability );
			}

			// do we need type introspection at all?
			enum Type
				{
					TYPE_UNDEFINED,
					TYPE_GRAVEYARD_ELEMENT,
					TYPE_PERMANENT,
					TYPE_HAND_ELEMENT,
					TYPE_EXILE_ELEMENT,
					TYPE_LIBRARY_ELEMENT,
					TYPE_NUM
				};

			Type getType() const { return mType; }
			const BaseCard* getBaseCard() const { return mCard; }
			const char* getTypeStr()
			{
				switch( mType )
				{
				case TYPE_GRAVEYARD_ELEMENT: return "graveyard";
				case TYPE_PERMANENT: return "permanent";
				case TYPE_HAND_ELEMENT: return "hand";
				case TYPE_EXILE_ELEMENT: return "exile";
				case TYPE_LIBRARY_ELEMENT: return "lib";
				default: return "undefined";
				}
			}

			void connectTriggers();
			void disconnectTriggers();

		protected:
			Type mType;

			const BaseCard *mCard;

			// As soon as we create a card element (i.e. putting the card into the game)
			// we need to populate it's abilites
			StaticAbility *mStatics;
			std::vector< ActivatedAbility* > mActives;
			// do NOT forget to connect/disconnect triggers upon changing the zone
			std::vector< TriggeredAbility* > mTrigs;
		};
	}
}

#endif /* !__CORE__CARD_ELEMENT_H__INCL__ */
