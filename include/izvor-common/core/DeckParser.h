/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__DECK_PARSER_H__INCL__
#define __CORE__DECK_PARSER_H__INCL__

#include "xml/XmlParser.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Deck;

		class DeckParser : public XmlParser
		{
		public:
			DeckParser( Deck *deck, const std::string& xmlFile )
				: XmlParser( xmlFile ),
				  mDeck( deck )
			{}

			bool load() { return parse(); }
			bool save();

			static bool loadDeck( Deck *deck, const std::string& xmlFile )
			{
				DeckParser dp( deck, xmlFile );
				return dp.load();
			}
	
			static bool saveDeck( Deck *deck, const std::string& xmlFile )
			{
				DeckParser dp( deck, xmlFile );
				return dp.save();
			}

		private:
			bool _parse( TiXmlNode *pParent );

			Deck *mDeck;
		};
	}
}

#endif /* !__CORE__DECK_PARSER_H__INCL__ */
