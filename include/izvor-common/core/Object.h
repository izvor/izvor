/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, dradojevic
*/

#ifndef __CORE__OBJECT_H__INCL__
#define __CORE__OBJECT_H__INCL__

#include "core/Color.h"
#include "core/ObjectMgr.h"

// we should probably have object manager for memory stuff
namespace IzvorCommon
{
	namespace Core
	{
		class Player;

		class Object
		{
		public:
			typedef uint32 ObjectID;

			uint32 getID() const { return mID; }
			Object* getOwner() const { return mOwner; }
			void    setOwner( Object *owner ) { mOwner = owner; }

			virtual void dealDamage( uint32 ammount, const Color& color ) 
			{}; // should be pure virtual f, but this is easier for debugging

			Player* getPlayerOwner();

		protected:
			
			ObjectID mID;
			Object *mOwner; 
			// NULL za Playera, tkd mozemo da dodjemo do igraca prateci niz,
			// NULL je i za StackElement!
			// npr. (ovo vise ne vazi StackElement (triggered ability neke karte) ->) CardElement -> CardSet -> Player -> NULL

			Object( Object *owner, uint32 id = 0 )
				: mOwner( owner ) 
			{
				if( !id ) mID = ObjectManager::get().add( this );
				else {
					ObjectManager::get().remove( id );
					mID = id;
					ObjectManager::get().addExisting( this );
				}
			}

			~Object() {
				if( this == ObjectManager::get().find< Object >( mID ) )
					// we are still the owners of our ID, so mark it free
					// (invalidate the entry)
					ObjectManager::get().markInvalid( mID );
			}		
		}; 
	}
}

#endif /* !__CORE__OBJECT_H__INCL__ */
