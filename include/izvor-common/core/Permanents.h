/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__PERMANENTS_H__INCL__
#define __CORE__PERMANENTS_H__INCL__

#include "core/ZoneElements.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Battlefield;
		class BaseCard;

		class Creature : public Permanent
		{
		public:
			Creature( Battlefield *owner, const BaseCard *parent, uint32 id = 0 )
				: Permanent( owner, parent, id ),
				  mDamageCounter( 0 )
			{}
			Creature( CardElement *ce, Battlefield *newOwner )
				: Permanent( ce, newOwner ),
				  mDamageCounter( 0 )
			{}
		
			void regenerate() { mDamageCounter = 0; }
			void dealDamage( uint32 ammount, Color color );

		private:
		
			uint32 mDamageCounter;
		};

		class Source : public Permanent
		{
		public:
			Source( Battlefield *owner, const BaseCard *parent, uint32 id = 0 )
				: Permanent( owner, parent, id )
			{}
			Source( CardElement *ce, Battlefield *newOwner )
				: Permanent( ce, newOwner )
			{}
		};
	
		class Artefact : virtual public Permanent
		{
		public:
			Artefact( Battlefield *owner, const BaseCard *parent, uint32 id = 0 )
				: Permanent( owner, parent, id )
			{}
		};
		
		class Aura : public Permanent 
		{
		public:
			Aura( Battlefield *owner, const BaseCard *parent, uint32 id = 0 )
				: Permanent( owner, parent, id )
			{}
		};
	}
}

#endif /* !__CORE__PERMANENTS_H__INCL__ */
