/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__OBJECT_MGR_H__INCL__
#define __CORE__OBJECT_MGR_H__INCL__

#include "util/Singleton.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Object;

		// no memory management, just fast access to objects via their ids
		class ObjectManager : public Singleton< ObjectManager >
		{
		public:
			typedef std::tr1::unordered_map< uint32, Object*,
											 boost::hash< uint32 > > ObjectHash;

			// generates a random Object ID
			uint32 add( Object *object );
		
			// adds an already existing object with its ID
			void addExisting( Object *object );
		
			// marks the spot for removal
			void markInvalid( uint32 objectID );
		
			// frees invalid entries
			void dump();

			void remove( uint32 objectID );
		
			template< typename ObjectClass >
			ObjectClass* find( uint32 objectID ) const;

		private:
		
			ObjectHash mObjectHash;
		};
	
		template< typename ObjectClass >
		ObjectClass* ObjectManager::find( uint32 objectID ) const
		{
			return dynamic_cast< ObjectClass* >( find< Object >( objectID ) );
		}

		//	Object specialization of find method
		template<>
		Object* ObjectManager::find< Object >( uint32 objectID ) const;
	}
}

#endif /* !__CORE__OBJECT_MGR_H__INCL__ */
