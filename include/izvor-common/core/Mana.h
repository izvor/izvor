/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__MANA_H__INCL__
#define __CORE__MANA_H__INCL__

#include "core/Color.h"

namespace IzvorCommon
{
	namespace Core
	{
		// Ovo predstavlja odredjenu kolicinu mane, ali NE predstavlja
		// cost. Cost moze da ima X, dvobojnu manu, aktivaciju, zrtvovanje itd.
		// Moze se koristiti kao mana pool, ili kao deo costa.
		class Mana
		{
		public:

			Mana();
			Mana( Color::Type color, uint32 quantity = 1 )
			{
				Mana();
				add( color, quantity );
			}

			void add( Color::Type color, uint32 quantity = 1 )
			{
				mPool[color] += quantity;
			}
			void add( Mana *other );
		
			bool destroy( Color::Type color, uint32 quantity = 1 )
			{
				if( mPool[color] < quantity ) return false;
				mPool[color] -= quantity;
				return true;
			}
			bool destroy( Mana *other );
		
			void empty() { mPool.clear(); }
			bool isEmpty() const { return mPool.empty(); }

			void print();
		private:
		
			std::map< Color::Type, uint32 > mPool;
		};
	}
}

#endif /* !__CORE__MANA_H__INCL__ */
