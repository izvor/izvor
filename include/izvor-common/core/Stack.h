/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__STACK_H__INCL__
#define __CORE__STACK_H__INCL__

namespace IzvorCommon
{
	namespace Core
	{
		class Game;
		class StackElement;

		class Stack
		{
		public:
			Stack( Game *parent )
				: mParent( parent )
			{}

			void resolve();
			void push( StackElement *el ) { mStack.push_back( el ); }
			bool isEmpty() const { return mStack.empty(); }

			void print() const;

		private:

			Game *mParent;
			std::vector< StackElement* > mStack;
		};
	}
}

#endif /* !__CORE__STACK_H__INCL__ */
