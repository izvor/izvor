
/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__ABILITY_H__INCL__
#define __CORE__ABILITY_H__INCL__

#include "core/Object.h"
#include "core/CardElement.h"
#include "core/Cost.h"
#include "core/StackElement.h"
#include "script/TriggerMgr.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Effect;
	
		class StackAbility : public Object,
							 public Stackable
		{
		public:
			StackAbility( CardElement *owner, uint32 id = 0 )
				: Object( owner, id )
			{}
			~StackAbility();

			void pushEffect( Effect *effect )
			{
				mEffects.push_back( effect );
			}

			virtual void print() const;

		protected:
			void resolve();
			bool cleanup() { return false; } // do not delete us pl0x, we needz to stay with the card
			bool prevented() { return false; }

			std::list< Effect* > mEffects;
		};

		class ActivatedAbility : public StackAbility
		{
		public:
			ActivatedAbility( CardElement *owner, uint32 id = 0 )
				: StackAbility( owner, id ),
				  mCost( this )
			{}
			
			Cost* getCost() { return &mCost; }

			void print() const;

		protected:
			friend class HandElement;

			void resolve() { 
				// check targets
				// pay mana etc.
				StackAbility::resolve(); }

		private:

			AbilityCost mCost;
		};

		class TriggeredAbility : public StackAbility,
								 public IzvorCommon::TriggerManager::GenericListener
		{
		public:
			TriggeredAbility( CardElement *owner, uint32 evtType, uint32 id = 0 )
				: StackAbility( owner, id ),
				  IzvorCommon::TriggerManager::GenericListener( evtType )
			{}
			~TriggeredAbility() { disconnect(); }
			
			void print() const;

		protected:
			void eventOccured( TriggerManager::DrawCardEvent *evt );
			void eventOccured( TriggerManager::PermanentEntersPlayEvent *evt );
		};

		class StaticAbility : public Object
		{
		public:
			typedef enum
				{
					NONE           = 0x0,
					PRESRETAC      = 0x1,
					JURISNIK       = 0x2,
					UBRZANJE       = 0x4,
					LETAC          = 0x8,
					BLOKIRA_LETACE = 0x10,
					NE_BLOKIRA     = 0x20
				} Type;
		
			StaticAbility( CardElement *owner, uint32 id = 0 ) 
				: Object( owner, id ),
				  mType( 0 ) 
			{
				owner->setStaticAbility( this );
			}

			void addAbility( Type type )
			{
				mType |= static_cast< uint32 >( type );
			}

			void removeAbility( Type type )
			{
				mType &= ~static_cast< uint32 >( type );
			}

			bool hasAbility( Type type ) 
			{ 
				return mType & static_cast< uint32 >( type );
			}

		private:

			uint32 mType;
		};
	}
}

#endif /* !__CORE__ABILITY_H__INCL__ */
