/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__STACK_DAMAGE_H__INCL__
#define __CORE__STACK_DAMAGE_H__INCL__

#include "core/StackElement.h"

namespace IzvorCommon
{
	namespace Core
	{
		// use like this (will get deleted automatically)
		// sd = new StackDamage();
		// sd.assignDamage(...);
		// ...
		// sd.push( stackptr );
		class StackDamage : public Stackable
		{
		public:
			struct Damage
			{
				Damage( uint32 _ammount, Color _color )
					: ammount( _ammount ), color( _color )
				{}
			
				uint32 ammount;
				Color color;
			};

			~StackDamage();

			void assignDamage( uint32 objectID, uint32 dmg, Color color )
			{ mDamage[objectID] = new Damage( dmg, color ); }

		protected:
			void resolve();
			bool cleanup() { return true; }
			bool prevented() { return true; }

		private:
		
			// objectID --> dmg
			std::map< uint32, Damage* > mDamage;
		};
	}
}

#endif /* !__CORE__STACK_DAMAGE_H__INCL__ */
