/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__ZONE_ELEMENTS_H__INCL__
#define __CORE__ZONE_ELEMENTS_H__INCL__

#include "core/CardElement.h"
#include "core/StackElement.h"
#include "core/Zones.h"
#include "core/Game.h"

namespace IzvorCommon
{
	namespace Core
	{
		class BaseCard;

		class LibraryElement : public CardElement
		{
		public:
			LibraryElement( Library *owner, const BaseCard *parent, uint32 id = 0 )
				: CardElement( (CardSet*) owner, parent, id )
			{
				mType = TYPE_LIBRARY_ELEMENT;
			}
		};

		class Permanent : public CardElement
		{
		public:
			class HandElement;
			// battlefield does not have a player owner!
			Permanent( Battlefield *owner, const BaseCard *parent, uint32 id = 0 )
				: CardElement( (CardSet*) owner, parent, id ),
				  mController( owner->getPlayerOwner() ),
				  mTapped( false )
			{
				mType = TYPE_PERMANENT;
			}
			Permanent( CardElement *ce, Battlefield *newOwner )
				: CardElement( *ce, (CardSet*) newOwner ),
				  mController( ce->getPlayerOwner() ),
				  mTapped( false )
			{
				mType = TYPE_PERMANENT;
			}

			virtual bool tap() { return (mTapped) ? false : mTapped = true; }
			virtual bool untap() { return (mTapped) ? mTapped = false : false; }
		
			bool isTapped() const { return mTapped; }
			Player* getController() const { return mController; }

			void sacrifice();
		private:
		
			// Owner is in object
			Player *mController;
			bool mTapped;
		};

		class HandElement : public CardElement,
							public Stackable
		{
		public:
			HandElement( Hand *owner, const BaseCard *parent, uint32 id = 0 )
				: CardElement( (CardSet*) owner, parent, id )
			{
				mType = TYPE_HAND_ELEMENT;
			}
			HandElement( LibraryElement *le )
				: CardElement( *le, le->getPlayerOwner()->getHand() )
			{
				mType = TYPE_HAND_ELEMENT;
			}

			virtual bool play();
			virtual bool playAsSource();

		protected:
			virtual bool isPlayable() const;
			virtual void resolve();
			virtual bool cleanup();
			virtual bool prevented();
		};

		class ExileElement : public CardElement
		{
		public:
			ExileElement( Exile *owner, const BaseCard *parent, uint32 id = 0 )
				: CardElement( (CardSet*) owner, parent, id )
			{
				mType = TYPE_EXILE_ELEMENT;
			}
		};

		class GraveyardElement : public CardElement
		{
		public:
			GraveyardElement( Graveyard *owner, const BaseCard *parent, uint32 id = 0 )
				: CardElement( (CardSet*) owner, parent, id )
			{
				mType = TYPE_GRAVEYARD_ELEMENT;
			}

			GraveyardElement( HandElement *he )
				: CardElement( *he, he->getPlayerOwner()->getGraveyard() )
			{
				mType = TYPE_GRAVEYARD_ELEMENT;
			}
		};	

	}
}

#endif /* !__CORE__ZONE_ELEMENTS_H__INCL__ */
