/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, dradojevic
*/

#ifndef __CORE__CARD_SET_H__INCL__
#define __CORE__CARD_SET_H__INCL__

#include "core/Object.h"
#include "core/Player.h"

namespace IzvorCommon
{
	namespace Core
	{
		class CardElement;

		// This is card container super class
		// Deletes all the elements inside on destruction
		class CardSet : public Object
		{
		public:
			typedef std::deque< CardElement* > CardContainer;
		
			CardSet( Object *owner )
				: Object( owner )
			{}
			~CardSet();

			virtual void addCardElement( CardElement *ce );
			// Does not delete the pointer!
			bool removeCardElement( CardElement *ce );
			CardElement* getCardElement( uint32 ceID ) const;

			uint32 getSize() const { return mCards.size(); }
			bool isEmpty() const { return mCards.empty(); }
			virtual void print();

			// za gui
			std::list< Object::ObjectID >* getCardList() const;

		protected:
			friend class DeckParser;
			CardContainer mCards;
		};
	}
}

#endif /* !__CORE__CARD_SET_H__INCL__ */
