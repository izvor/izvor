/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __CORE__SYMBOL_H__INCL__
#define __CORE__SYMBOL_H__INCL__

#include "core/Color.h"
#include "core/Mana.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Permanent;

		// ----------- Symbol ----------- //
		// deo cost-a (u ability-ju) i mana-costa karte
		class Symbol
		{
		public:
			enum Type
				{
					MANA, // od 0 do 16
					X,    // proizvoljan
					A,    // aktivacija (bezbojan),
					SACRIFICE // zrtvovanje
				};
		
			Type getType() const { return mType; }
			virtual bool convertToMana( Mana *dest ) const = 0;

		protected:
			Symbol( Type type )
				: mType( type ) {};

			Type mType;
		};
		// ----------- !Symbol ----------- //

		// ----------- ManaSymbol ----------- //
		class ManaSymbol : public Symbol
		{
		public:
			ManaSymbol( const Color& color, uint32 quantity = 1 )
				: Symbol( Symbol::MANA ),
				  mColor( color ), mQuantity( quantity )
			{}
			bool convertToMana( Mana *dest ) const
			{
				// za sada ignorisemo visebojnu manu
				dest->add( static_cast< const Color::Type >( mColor.getRawColor() ), mQuantity );
				return true;
			}

		private:

			Color mColor; // moze biti visebojna
			uint32 mQuantity;
		};
		// ----------- !ManaSymbol ----------- //
	
		// ----------- TapSymbol ----------- //
		class TapSymbol : public Symbol
		{
		public:
			TapSymbol()
				: Symbol( Symbol::A )
			{}
			bool convertToMana( Mana *dest ) const
			{
				return true;
			}
		};
		// ----------- !TapSymbol ----------- //
	
		// ----------- XSymbol ----------- //
		class XSymbol : public Symbol
		{
		public:
			XSymbol( Color::Type color )
				: Symbol( Symbol::X ),
				  mColor( color )
			{}

			bool convertToMana( Mana *dest ) const
			{
				return false;
			}
			
			Color::Type getColor() const { return mColor; }

		private:

			Color::Type mColor; // jednobojna		
		};

		class SacrificeSymbol : public Symbol
		{
		public:
			SacrificeSymbol( Permanent *target )
				: Symbol( Symbol::SACRIFICE ),
				  mTarget( target )
			{}
			
			bool convertToMana( Mana *dest ) const
			{
				return true;
			}

			Permanent* getTarget() const { return mTarget; }

		private:
			
			Permanent *mTarget;
		};
	}
}

#endif /* !__CORE__SYMBOL_H__INCL__ */
