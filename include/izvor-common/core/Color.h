/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, dradojevic
*/

#ifndef __CORE__COLOR_H__INCL__
#define __CORE__COLOR_H__INCL__

#include "IzvorCommonPCH.h"

namespace IzvorCommon
{
	namespace Core
	{
		class Color
		{
		public:

			enum Type {
				BEZBOJNA  = 0x20, // DBID 0
				CRVENA    = 0x10, // DBID 1
				ZELENA    = 0x08, // DBID 2
				PLAVA     = 0x04, // DBID 3
				ZUTA      = 0x02, // DBID 4
				CRNA      = 0x01, // DBID 5
				NEPOZNATA = 0x00  // -1
			};

			Color( uint8 color = 0 );

			bool operator==( const Color& c ) const 
			{ 
				return mColor == c.mColor;
			}

			bool operator!=( const Color& c ) const 
			{ 
				return mColor != c.mColor;
			}

			Color& operator+=( const Color& c )
			{
				mColor |= c.mColor;
				return *this;
			}

			Color& operator-=( const Color& c )
			{
				mColor &= ~c.mColor;
				return *this;
			}

			friend Color operator+( const Color& c1, const Color& c2 )
			{
				Color n( c1 );
				n += c2;
				return n;
			}

			friend Color operator-( const Color& c1, const Color& c2 )
			{
				Color n( c1 );
				n -= c2;
				return n;
			}

			uint8 color() const { return mColor; }

			bool hasColor( Type t ) const
			{
				return (mColor & static_cast<uint8>( t )) != 0;
			}

			static const std::string& getTypeStr( Type type )
			{
				return sTypeName[type];
			}
			static const char getColorCode( Type type )
			{
				return sColorCode[type];
			}

			static Type getTypeByID( uint8 id );
			static Type getTypeByCode( char code );
			// slow like hell
			std::string getDescriptionStr() const;
			static const uint8 NUM = 6;
		
			uint8 getRawColor() const { return mColor; }

		protected:
			// obsolete
			//		friend std::ostream& operator<<( std::ostream& os, const Color& c );

			uint8 mColor;

			static const uint8 SIZE = BEZBOJNA + CRVENA + ZELENA + PLAVA + ZUTA + CRNA;
			static std::tr1::unordered_map<const Type, const std::string, boost::hash< const Type > > sTypeName;
			static std::tr1::unordered_map<const Type, const char, boost::hash< const Type > > sColorCode;
		};
	}
}

#endif /* !__CORE__COLOR_H__INCL__ */
