/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#ifndef __CORE__GAME_H__INCL__
#define __CORE__GAME_H__INCL__

#include "event/Event.h"
#include "threading/Thread.h"

#include "core/Player.h"
#include "core/Stack.h"
#include "core/Zones.h"

// As pointed out by:
// http://www.parashift.com/c++-faq-lite/pointers-to-members.html#faq-33.7
#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

namespace IzvorCommon
{
	namespace Core
	{
		class GameManager;

		// This is a single, independent game unit with two players
		// implemented as a state-machine
		class Game : public Observer,
					 public Thread
		{
		public:
			enum State {
				STATE_NOT_STARTED,

				STATE_UNTAP,       // untap (no priority) -> 
				STATE_UPKEEP,      // upkeep ->
				STATE_DRAW,        // draw

				STATE_MAIN1,       // first main phase

				STATE_COMBAT_BEGINNING,  // combat beginning ->
				STATE_COMBAT_DECL_ATT,   // declare attackers ->
				STATE_COMBAT_DECL_BLK, // declare blockers ->
				STATE_COMBAT_EXTRA_DMG,  // extra combat step for dmg (skipped if no first strike) ->
				STATE_COMBAT_DMG,        // combat damage ->
				STATE_COMBAT_ENDING,     // combat ends

				STATE_MAIN2,       // second main phase

				STATE_END_STEP,    // 'at the beginning of end step' triggers
				STATE_CLEANUP,     // regen creatures, discard cards, 
				// 'until this turn', 'this turn' triggers
				STATE_FINISHED,

				STATE_MAX
			};

			Game( GameManager *parent )
				: m_EndGameListener( this ),
				  mGameID( ++s_mGameID ), mCurrentState( STATE_NOT_STARTED ),
				  mPlayerOne( new Player( this ) ), 
				  mPlayerTwo( new Player( this ) ),
				  mStack( new Stack( this ) ),
				  mBattlefield( new Battlefield( this ) ),
				  mExile( new Exile( this ) ),
				  mParent( parent ),
				  mInteractionDone( false )
			{
				EVT_CONNECT( mPlayerOne->getLibrary()->m_LibraryEmptyEvt,
							 m_EndGameListener );
				EVT_CONNECT( mPlayerTwo->getLibrary()->m_LibraryEmptyEvt,
							 m_EndGameListener );
			}

			friend class EndGameListener;
			// make events templates and templatize listeners
			class EndGameListener : public Event< Library::LibraryEmptyEvt >::Listener
			{
			public:
				EndGameListener( Game *parent )
					: Listener( parent )
				{}

				void eventOccured( Library::LibraryEmptyEvt *evt );
			} m_EndGameListener;		

			~Game();

			void run();
			void signal() { mCond.notify_one(); }
			
			// called from GUI
			void initGui();

			void setClickedID( uint32 id ) 
			{
				mClickedID = id;
				signal();
			}
			void setClickedList( std::list< uint32 > *lst )
			{
				mClickedIDList = lst;
				signal();
			}

			Stack*       getStack()       { return mStack; }
			Battlefield* getBattlefield() { return mBattlefield; }
			Exile*       getExile()       { return mExile; }

			Player* getPlayerOne() { return mPlayerOne; }
			Player* getPlayerTwo() { return mPlayerTwo; }

			Player* getActivePlayer() 
			{ 
				return (mPlayerOne->isActive()) ? mPlayerOne : mPlayerTwo; 
			}

			// use this for priority change! this checks for all
			// the State Effects (efekti stanja, D21, sudijska pravila)
			void givePriority( bool player );
		
			State getState() const { return mCurrentState; }
		private:
			void stateEffectsCheck();
			void endGame( bool whoWon );

			State runState() { return CALL_MEMBER_FN( *this, mStateActions[ mCurrentState ] )(); }

			State doPreparation();

			State doUntap();
			State doUpkeep();
			State doDraw();

			State doMainOne();

			State doCombatBeginning();
			State doCombatDeclAtt();
			State doCombatDeclBlk();
			State doCombatExtraDmg();
			State doCombatDmg();
			State doCombatEnding();
		
			State doMainTwo();

			State doEndStep();
			State doCleanup();
		
			State doFinished();

			// no need for setting up state transition table, since states
			// are pre-ordered
			typedef State (Core::Game::*StateAction)();
			//		typedef void EventAction( Event *evt );

			static StateAction mStateActions[ STATE_MAX ];
		
			uint32 mGameID;
			static uint32 s_mGameID;

			bool mFirstTurn;
			State mCurrentState;
			Player *mPlayerOne, *mPlayerTwo;

			// Zones
			Stack *mStack;
			Battlefield *mBattlefield;
			Exile *mExile;

			GameManager *mParent;

			// GUI interaction
			bool mInteractionDone;
			uint32 mClickedID;
			std::list< uint32 > *mClickedIDList;

			// Threading
			mutable boost::mutex mMutex;
			boost::condition mCond;
		};
	}
}

#endif /* !__CORE__GAME_H__INCL__ */
