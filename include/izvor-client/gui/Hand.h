/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac
*/

#ifndef HAND_H
#define HAND_H

#include "gui/Common.h"
#include "gui/CardContainer.h"
#include "Ogre.h"

#include <list>

namespace Gui
{

    //------------------------------------------------------------------------------
	class Hand
	{
	public:
		Hand();
		~Hand();

        // Draw from any zone
        virtual void draw(CardId) =0;

        //ObjectId getId() const { return mId; }

	private:
        //Fan* mFan;

        //ObjectId mId;
	};
// 	//------------------------------------------------------------------------------
//     class PlayerHand : public Hand
//     {
//     public:
//     	PlayerHand();
//     	~PlayerHand();
// 
//         virtual void draw(CardId);
//     protected:
//     	
//     private:
// 
//     };
//     //------------------------------------------------------------------------------
//     class OpponentHand : public Hand
//     {
//     public:
//     	OpponentHand();
//     	~OpponentHand();
// 
//         virtual void draw(CardId);
//     protected:
//     	
//     private:
//     };

}
#endif
