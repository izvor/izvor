#ifndef IZVOR_GUI_H
#define IZVOR_GUI_H

// TODO: Popuniti kad dodje vreme za to

#include "gui/Animation.h"
#include "gui/Camera.h"
#include "gui/Card.h"
#include "gui/CardContainer.h"
#include "gui/CardManager.h"
#include "gui/Core.h"
#include "gui/Deck.h"
#include "gui/Fan.h"
#include "gui/Updatable.h"
#include "gui/UpdatableManager.h"
#include "gui/Hand.h"
#include "gui/IzvorField.h"
#include "gui/PlayerPanel.h"
#include "gui/PlayingField.h"
#include "gui/SceneRoot.h"
#include "gui/StatePanel.h"
#include "gui/Table.h"
#include "gui/Common.h"
#include "gui/Utility.h"

#endif 
