/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/
#ifndef GUI_SCENE_ROOT_H
#define GUI_SCENE_ROOT_H

#include "Ogre.h"
#include "gui/Common.h"
#include "gui/Fan.h"

#include "Requests.h"

namespace Gui
{
    class SceneRoot 
	{
	public:
		SceneRoot( Ogre::SceneManager* scene_manager );
		~SceneRoot();


        void update();


        Table* getTable()
        {
            return mTable;
        }

        Ogre::SceneManager* getSceneManager()
        {
            return mSceneManager;
        }

        // Mozda samo trenutno resenje
        void setMouseOverCardId( CardId card_id ) { mMouseOverCardId = card_id; }
        void setMouseOver(bool isOver ) { mIsMouseOverCard = isOver; }
        void playCard(CardId id);
        void zoom() { mIsZoom = true; }

        void create();

        /// Test
        void createDeck(const CardsIdList& );
        void drawCard(const CardId);

        CardContainer* getContainer(const ObjectId player, const IzvorCommon::GuiMoveCardRequest::Dest container);

	private:
        Ogre::SceneManager* mSceneManager;
        //Camera* mCamera; //Mozda bi ovde trebalo da stoji?

		Table*	mTable;

		//StatePanel* mStatePanel;

        OpponentHand* mOpponentHand;
        LinealFan* mPlayerHand;

        Deck* mPlayerLibrary;
        Deck* mPlayerGraveyard;

        Deck* mOpponentLibrary;
        Deck* mOpponentGraveyard;

        PermanentsField* mPlayerPermanentsField;
        PermanentsField* mPlayerAttackAndBlockField;

        PermanentsField* mOpponentPermanentsField;
        PermanentsField* mOpponentAttackAndBlockField;

        // Izvori...
		IzvorField* mPlayerIzvorField;
		IzvorField* mOpponentIzvorField;

        // Stack Field temporarily
        PermanentsField* mStack;

        PlayerPanel* mPlayerPanel;
        //PlayerPanel* mOpponentPanel;

        ZoomField* mZoomField;

        ObjectId mPlayerId;
        ObjectId mOpponentId;

        CardId  mMouseOverCardId;
        CardId  mZoomedCard;
        bool    mIsMouseOverCard;
        bool    mIsZoom, mIsZoomState;

        bool mIsStarted;

	};

}
#endif
