/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder, mikronac
*/

#ifndef IZVOR_FIELD_H
#define IZVOR_FIELD_H

#include "gui/Common.h"
#include "gui/CardContainer.h"
#include "IzvorPCH.h"

#include <Ogre.h>

namespace Gui
{

    class IzvorField : public CardContainer
    {
    public:
        IzvorField(const Ogre::Vector3& position, const Ogre::Quaternion& orientation);
    	virtual ~IzvorField();

        virtual void acceptCard(CardId);
        virtual void dispatchCard(CardId);

    protected:
        void reorganisePositions();
		Ogre::Vector3 calculateStartPosition( int num );
		bool hasTappedCards(const CardsList& list);

		struct IzvorDeck
		{
			IzvorDeck(const CardsList& list, const IzvorCommon::Core::Color &color, Ogre::SceneNode* parent_node)
				: cards_list(list), color(color)

			{
				scene_node = parent_node->createChildSceneNode();
			}

			~IzvorDeck()
			{
				if(scene_node)
					if(scene_node->getParentSceneNode())
						scene_node->getParentSceneNode()->removeAndDestroyChild( scene_node->getName() );
			}

			CardsList cards_list;
			IzvorCommon::Core::Color color;
			Ogre::SceneNode* scene_node;
		};
    	
    private:
        Ogre::SceneNode* mNode;

		typedef std::list< IzvorDeck* > DecksList;
		DecksList mDecks;

        static const Ogre::Real DISTANCE;
		static const Ogre::Real TAP_DISTANCE;
    };
}

#endif