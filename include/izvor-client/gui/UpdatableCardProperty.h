/*
---------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder
*/

#ifndef GUI_UPDATABLE_CARD_PROPERTY_H
#define GUI_UPDATABLE_CARD_PROPERTY_H

#include "gui/Updatable.h"
#include "gui/CardProperty.h"
#include "gui/Core.h"
#include "gui/UpdatableManager.h"
#include "gui/Card.h"
#include "gui/Common.h"

namespace Gui
{
    template<typename T>
    class UpdatableCardProperty : public Updatable, public CardProperty
    {
    public:
        UpdatableCardProperty(boost::weak_ptr<Card> card)
            : CardProperty(card)
        {
            // this can't be done!
            //Gui::Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable( UpdatableCardPropertyPtr(this) );
        }

        virtual ~UpdatableCardProperty() {}

        bool update( const Ogre::FrameEvent& evt)
        {
            if (_update_impl(evt) && isEnabled() )
            {
                return true;
            }

            // this is a c++0x feature, will use template instead
            //mCard->removeProperty<decltype(*this)>();
            // lolo msvc compiler segfaults here.
            // We can conclude that c++0x is not yet really implemented
            try
            {
                if(mEnabled)
                {
                    boost::shared_ptr<Card> card(mCard);
                    card->removeProperty<T>();
                }
            } catch (const std::exception& e)
            {
                std::cerr << e.what() << std::endl;
            }

            return false;
        }

        static void registerProperty(boost::shared_ptr< UpdatableCardProperty< T > > card_property_ptr)
        {
            Gui::Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(card_property_ptr);
        }

    protected:
        virtual bool _update_impl( const Ogre::FrameEvent& evt ) = 0;
    };
}

#endif /* GUI_UPDATABLE_CARD_PROPERTY_H */
