/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#ifndef GUI_TABLE_H
#define GUI_TABLE_H

#include "gui/Common.h"

#include <Ogre.h>
#include <boost/noncopyable.hpp>

namespace Gui
{
	class Table : public boost::noncopyable
	{
	public:
		Table();
		~Table();

        Ogre::SceneNode* getSceneNode()
        {
            return mNode;
        }
/*
		
        Deck* getLibraryDeck()
        {
            return mLibraryDeck;
        }

        Deck* getOpponentLibraryDeck()
        {
            return mOpponentLibraryDeck;
        }

        Deck* getGraveyardDeck()
        {
            return mGraveyardDeck;
        }

        Deck* getOpponentGraveyardDeck()
        {
            return mOpponentGraveyardDeck;
        }

        // test
        PlayingField* getPlayingField()
        {
            return mPlayersField;
        }
*/

	private:
        /*
            Parent SceneNode za sve objekte na tabli, cuva orijentaciju table,
            i ostalih objekata. Mozda da napravimo klasu koja ce sadrzati Ogre::SceneNode i get metodu
            samo ne znam kako da je nazovem. Pa izvodimo iz nje sve koje imaju SceneNode, cisto zbog urednosti.
        */
        Ogre::SceneNode* mNode;
        /*
            Kasnije ovde dolazi entitet table
        */
        Ogre::Entity* mEntity;

        // methods that will somehow figure out exact position of 'fields'
        // on table's 3d model.
        //Ogre::Vector3 getLibraryDeckPosition();
        //Ogre::Vector3 getOpponentLibraryDeckPosition();
        //Ogre::Vector3 getGraveyardPosition(); itd


/*
        Mislim da ovo nije potrebno ovde da stoji

        Deck *mLibraryDeck;
        Deck *mOpponentLibraryDeck;
        Deck *mGraveyardDeck;
        Deck *mOpponentGraveyardDeck;
        */


        //PlayingField *mPlayersField, *mOpponentsField;
        // IzvorPlayingField *mPlayersIzvor, mOpponentsIzvor;

        const static unsigned int DEFAULT_DECK_SIZE = 10;
	};

}
#endif
