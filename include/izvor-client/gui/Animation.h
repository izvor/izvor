/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Thunder, mikronac
*/
#ifndef GUI_ANIMATION_H
#define GUI_ANIMATION_H

#include <Ogre.h>

#include "gui/Common.h"
#include "gui/Updatable.h"



namespace Gui
{
	class Animation : public Updatable
	{
	public:
		Animation();
        virtual ~Animation();

        virtual bool update(const Ogre::FrameEvent& evt);

    protected:
		bool hasEnded();
        virtual bool _hasEndedImpl() =0;

        Ogre::AnimationState* mAnimationState;

        Ogre::Real mAnimationFactor;

		bool mReallyEnd;
    };


	class MoveAnimation : public Animation
	{
	public:
        MoveAnimation() {}
		virtual ~MoveAnimation() {}
	protected:
		virtual bool _hasEndedImpl() { return mAnimationState->hasEnded(); }
	private:
        
	};


	class ReverseAnimation : public Animation
	{
	public:
		ReverseAnimation() : mIsReverse(true) { mAnimationFactor = -1.0f; }
		virtual ~ReverseAnimation() {}

		virtual void setReverse( bool reverse ) 
		{
			mIsReverse = reverse; 
			if( reverse ) mAnimationFactor = -1.0f;
			else mAnimationFactor = 1.0f;
		}
	protected:
		virtual bool _hasEndedImpl()
		{
			return mIsReverse && (mAnimationState->getTimePosition() <= 0.0f);
		}

		bool mIsReverse;
	private:
	};


	class LoopAnimation : public Animation
	{
	public:
		LoopAnimation();
		virtual ~LoopAnimation() {}

		virtual void setStop( bool stop ) 
		{ 
			mIsStoped = stop; 
			mAnimationState->setLoop(false);
			mAnimationFactor = Ogre::Real(-1.0);
		}
	protected:
		virtual bool _hasEndedImpl() { return mIsStoped && (mAnimationState->getTimePosition() <= 0.0f); }

		bool mIsStoped;
		
	private:
	};

	class ReverseHoldAnimation : public ReverseAnimation
	{
	public:
		ReverseHoldAnimation() : mIsHold(true) {}
		virtual ~ReverseHoldAnimation() {}

		virtual void discard() 
		{ 
			mIsHold = false;
			setReverse(true);
		}
	protected:
		virtual bool _hasEndedImpl() 
		{
			return !mIsHold && ReverseAnimation::_hasEndedImpl();
		}

		bool mIsHold;
		
	private:
	};


	// Specialised animations...

	class LinearMove : public MoveAnimation
	{
	public:
		LinearMove();
		virtual ~LinearMove() {}
	protected:
		
	private:
	};

	class LinealFanHoverAnimation : public ReverseHoldAnimation
	{
	public:
		LinealFanHoverAnimation( CardsList&, CardId);
		virtual ~LinealFanHoverAnimation() {}

	protected:
		
	private:
		static const Ogre::Real ANIMATON_DURATION;
	};

    class DrawCardAnimation : public MoveAnimation
    {
    public:
        DrawCardAnimation(Card*, const Ogre::Vector3, const Ogre::Quaternion);
        virtual ~DrawCardAnimation() {}
    protected:
    	
    private:
    };

    class DragAnimation : public MoveAnimation
    {
    public:
        DragAnimation(Card*, const Ogre::Vector3, const Ogre::Quaternion  = Ogre::Quaternion::IDENTITY);
        DragAnimation(Card*);
		DragAnimation(Ogre::SceneNode*);
        virtual ~DragAnimation() {}
    protected:
    	
    private:
    };

    class TapAnimation : public ReverseAnimation
    {
    public:
        TapAnimation(Card*);
        ~TapAnimation() {}
    protected:
    	
    private:
    };

    class ZoomAnimation : public ReverseHoldAnimation
    {
    public:
    	ZoomAnimation(Card*, const Ogre::Vector3, const Ogre::Quaternion  = Ogre::Quaternion::IDENTITY);
        ~ZoomAnimation() {}
    protected:
    	
    private:
    };

    

}

#endif //GUI_ANIMATION_H
