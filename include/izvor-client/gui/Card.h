/*
---------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac
*/

#ifndef CARD_H
#define CARD_H


#include "Ogre.h"
#include "gui/Animation.h"
#include "gui/Buff.h"
#include "gui/Common.h"
#include "gui/CardContainer.h"
#include "gui/CardProperty.h"
#include "gui/CardGlow.h"
#include "gui/Core.h"
#include "gui/CardManager.h"

#include <boost/noncopyable.hpp>
#include <boost/unordered_map.hpp>
#include <boost/shared_ptr.hpp>

namespace Gui
{
    class Card : public ContainerElement, boost::noncopyable
	{
	public:
        Card( CardId id, Ogre::SceneNode* scene_node, Ogre::Entity* entity);
        ~Card();


        CardId getId() const
        {
            return mId;
        }

        // this contains position and orientation 
        Ogre::SceneNode* getSceneNode()
        {
            return mNode;
        }

        Ogre::Entity* getEntity()
        {
            return mEntity;
        }

        void show() { mNode->setVisible(true); }
        void hide() { mNode->setVisible(false); }

        template <typename T>
        void addProperty();
        template <typename T>
        void removeProperty();
        template <typename T>
        bool hasProperty();

        void saveState();
        SceneNodeState calculateAnimationPositionAndOrientation();
        SceneNodeState getSavedState();

        void tap();
        void untap();

        bool isTap() const { return mIsTap; }

		void setRenderQueueGroupAndPriority(Ogre::uint8 queueID, Ogre::ushort priority);
		Ogre::ushort getPriority()
		{
			return mPriority;
		}

		IzvorCommon::Core::Color& getColor()
		{
			return mCardColor;
		}

    private:
		Card() {};
        /// Unique mechanics id.
        CardId mId;
		IzvorCommon::Core::Color mCardColor;

        /// Cards elements.
		Ogre::SceneNode* mNode;
        Ogre::Entity* mEntity;

        Ogre::ushort mPriority;

        SceneNodeState mSceneNodeState;

		// Ovo mozda i nije potrebno ovde da stoji
		Ogre::uint8 mAttack;
		Ogre::uint8 mDamage;

        bool mIsTap;
        TapAnimation* mTapAnimation;


        // std::type_info::hash_code() needs to be enabled (c++0x feature)
        // using std::type_info::name is not really smart
        // lololo gcc segfaulted
        //typedef boost::unordered_map< const std::type_index&, CardPropertyPtr> CardPropertyHashMap;
        typedef boost::unordered_map< const char*, CardPropertyPtr> CardPropertyHashMap;
        CardPropertyHashMap mCardProperties;

		//// Buff management
		typedef std::list<Buff*> BuffsList;
		BuffsList mBuffsList;

		friend class Buff;
		void addBuff( Buff* buff );
		void removeBuff( Buff* buff );
		void reorganiseBuffs();
		
		static const unsigned int NUMBER_OF_BUFFS_PER_ROW = 4;
	};

    template <typename T>
    void Card::addProperty()
    {
        if (hasProperty<T>() == false)
        {
            boost::shared_ptr<T> t( new T(Gui::Core::getSingletonPtr()->getCardManager()->getCard(mId)) );
            T::registerProperty(t);

            mCardProperties.insert(std::make_pair(typeid(T).name(), t) );
        } else
		{
			std::cout << "Card " << mId << " already has property " << typeid(T).name() << std::endl;
		}
    }

    template <typename T>
    void Card::removeProperty()
    {
        CardPropertyHashMap::iterator it = mCardProperties.find( typeid(T).name() );
        if(it != mCardProperties.end())
        {
            (*it).second->disable();
            mCardProperties.erase(it);
        } else
		{
			std:: cout << "Card " << mId << " does not have property " << typeid(T).name() << std::endl;
		}
    }

    template <typename T>
    bool Card::hasProperty()
    {
        CardPropertyHashMap::iterator it = mCardProperties.find( typeid(T).name() );
        if ( it != mCardProperties.end() )
        {
            return true;
        }
        return false;
    }
}
#endif
