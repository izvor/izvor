/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac
*/

#ifndef GUI_TYPES_H
#define GUI_TYPES_H

#include "IzvorCommonPCH.h"

#ifdef _MSC_VER
  #if _MSC_VER < 1600
	#ifndef nullptr // zastita od redefinicije , posto MyGui takodje ga definise
		#define nullptr 0L
	#endif
  #endif
#endif

#include <Ogre.h>
#include <list>
#include <boost/shared_ptr.hpp>

namespace Gui
{
    // macros
    // Skracenica za ogre log.
    #define GUI_LOG( str ) Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::String("#IGUI: ") + Ogre::String(str))
    #define GUI_LOG_TO_STR( str ) Ogre::LogManager::getSingletonPtr()->logMessage(Ogre::String("#IGUI: ") + Ogre::StringConverter::toString(str))

    // enums
	enum DeckType
	{
		LIBRARY,
		OPONENT_LIBRARY,
		GRAVEYARD,
		OPONENT_GRAVEYARD
	};

	enum State
	{
		MAIN
	};

    enum QueryMasks
    {
        QM_NOT_SCAN = 1 << 0,
        QM_CARDS    = 1 << 1
        //QM_CARDS_IN_HAND = 1 << 0,
        //QM_CARDS_ON_TABLE = 1 << 1,
        //QM_PLAYERS_CARDS = 1 << 2,
        //QM_OPPONENTS_CARDS = 1 << 3,
        //QM_PLAYERS_DECK = 1 << 4,
        //QM_OPPONENTS_DECK = 1 << 5
        // HANDY
    };

    enum RenderQueueGroup
    {
        RQG_TABLE   = 30,
        RQG_HAND    = 70 // opseg  +- MAX_CARDS_IN_HAND
    };

    enum RenderPriority
    {
        RP_DEFAULT = 1,
        // ...
    };

    // forward declarations
    class Animation;
    class Buff;
    class Camera;
    class Card;
    class CardComposite;
    class CardGlow;
    class CardGlowingFrame;
    class CardManager;
    class CardProperty;
    class Core;
    class Deck;
    class Fan;
    class GuiListener;
    class Hand;
	class IzvorField;
    class OpponentHand;
    class PermanentsField;
    class PlayerPanel;
    class PlayingField;
    class SceneRoot;
    class Stack;
    class StatePanel;
    class Table;
	class TempBuff;
    class Updatable;
    template<typename T>
    class UpdatableCardProperty;
    class UpdatableManager;
    class Utility;
    class ZoomField;

    // typedefs
	typedef std::size_t CardModelId;
	typedef unsigned int CardId;
    typedef unsigned int ObjectId;

	typedef std::list<CardId> CardsIdList;
	typedef std::list<Card*> CardsList;
	
	typedef boost::shared_ptr<Animation> AnimationPtr;
	typedef boost::shared_ptr<Card> CardPtr;
    typedef boost::shared_ptr<CardProperty> CardPropertyPtr;
	typedef boost::shared_ptr<Updatable> UpdatablePtr;
	typedef boost::shared_ptr<Buff> BuffPtr;

    // structs
    struct SceneNodeState
    {
        Ogre::Vector3       position;
        Ogre::Vector3       scale;
        Ogre::Quaternion    orientation;
    };

    struct GameInitParameters
    {
        CardsIdList* sPlayerHand;
        CardsIdList* sOpponentHand;
        CardsIdList* sPlayerLibrary;
        CardsIdList* sOpponentLibrary;
        ObjectId    sPlayerId;
        ObjectId    sOpponentId;
    };
}
#endif
