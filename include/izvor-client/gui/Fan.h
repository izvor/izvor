///*
//--------------------------------------------------------------------------------
//	  _                 _                   _   _     
//	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
//	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
//	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
//									  |___/  |__/     
//    -----------------------------------------------------------------------
//    LICENSE:
//    -----------------------------------------------------------------------
//    This file is part of Izvor: Izvori Magije (C) TCG simulator
//    Copyright 2010 EGDC++ Team
//    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
//    -----------------------------------------------------------------------
//    Izvor is free software: you can redistribute it and/or modify it under
//    the terms of the GNU Lesser General Public License as published by the 
//    Free Software Foundation, either version 3 of the License, or (at your 
//    option) any later version.
//
//    Izvor is distributed in the hope that it will be useful, but WITHOUT
//    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
//    License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
//    -----------------------------------------------------------------------
//    Author: mikronac, Thunder
//*/
//
#ifndef GUI_FAN_H
#define GUI_FAN_H
//
#include "gui/CardContainer.h"
#include "gui/Animation.h"

#include <OISMouse.h>
#include <Ogre.h>


#include <map>

#include "gui/Common.h"

namespace Gui
{
    class Fan : public CardContainer 
    {
    public:
	
		Fan() {}
		virtual ~Fan() {}

        virtual void mouseOver( CardId ) =0;

		// Dohvatanje 
		Ogre::SceneNode* getSceneNode() { return mNode; }

	protected:
		// Proverava da li se karta trenutno nalazi u lepezi.
		bool hasCard( CardId id ) { return mCardsList.end() != find(id); }

		CardsList::iterator find( CardId );

        CardsList mCardsList;

        Ogre::SceneNode* mNode;

	};


	class LinealFan : public Fan
	{
	public:
		/// Konstruktor za kreiranje lepeza sa pocetnim kartama
        LinealFan(Ogre::SceneNode*);
		virtual ~LinealFan();

		virtual void acceptCard(CardId card_id);
		virtual void dispatchCard(CardId card_id);

		virtual void mouseOver( CardId );
        void deselect();

        void fill(const CardsIdList& );

	protected:
		
	private:
        void reorganiseRenderQueueGroup(CardsList::iterator top);
        void reorganisePositions();
        void createHoverAnimations();

		/// Metoda za racunanje pozicije prve karte s leva u odnosu na centar
		/// lepeze u zavisnosti od broja karata, mMainSceneNode. Korisno je i za kreiranje animacije reorganizacije.
		Ogre::Vector3 calculateStartPosition( int );
        Ogre::Vector3 calculateInsertPosition();

		typedef std::map<CardId, LinealFanHoverAnimation*> AnimationsList;
		AnimationsList mHoverAnimationList;
        LinealFanHoverAnimation* mLastAnimation;

		static const Ogre::Real DISTANCE;
	};
}

#endif //GUI_FAN_H
