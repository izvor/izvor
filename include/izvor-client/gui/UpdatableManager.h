/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#ifndef GUI_GAME_OBJECT_MANAGER_H
#define	GUI_GAME_OBJECT_MANAGER_H

#include "gui/Common.h"

#include <Ogre.h>
#include <boost/shared_ptr.hpp>
#include <boost/unordered_set.hpp>

namespace Gui
{
    // manages lifetime of updatable objects
	class UpdatableManager
	{
	public:
        UpdatableManager();
        ~UpdatableManager();

        void registerUpdatable(UpdatablePtr game_object);
        void registerUpdatable(Updatable* game_object);

        // we need to unregister upon destruction of other parent objects
        void unregisterUpdatable(UpdatablePtr game_object);

        // updates all
        void update(const Ogre::FrameEvent& evt);

	private:
        typedef boost::unordered_set<UpdatablePtr> UpdatableList;
        UpdatableList mUpdatableObjects;
	};
}
#endif
