/*
---------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder
*/

#ifndef GUI_CARD_COMPOSITE_H
#define GUI_CARD_COMPOSITE_H


#include "gui/Card.h"
#include "gui/Common.h"

#include <boost/enable_shared_from_this.hpp>

namespace Gui
{
    class CardComposite : public Card, public boost::enable_shared_from_this<CardComposite>
    {
    public:
        CardComposite();
        ~CardComposite();

        // insert child after this card
        void insertChild( boost::weak_ptr<Card> card );
        // push at the back of the chain
        void pushBackChild( boost::weak_ptr<Card> card );
        void detachThis();
        CardPtr getChild();

    private:
        void _setParent(boost::weak_ptr<Card> parent);

        boost::weak_ptr<Card> mChild;
        boost::weak_ptr<Card> mParent;
    };
}


#endif /* GUI_CARD_COMPOSITE_H */
