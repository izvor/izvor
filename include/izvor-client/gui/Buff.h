/*
--------------------------------------------------------------------------------
_                 _                   _   _
(_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___
| |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
|_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
|___/  |__/
-----------------------------------------------------------------------
LICENSE:
-----------------------------------------------------------------------
This file is part of Izvor: Izvori Magije (C) TCG simulator
Copyright 2010 EGDC++ Team
For the latest info visit: http://sites.google.com/site/etfgamedevcrew
-----------------------------------------------------------------------
Izvor is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

Izvor is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
-----------------------------------------------------------------------
Author: thunder
*/

#ifndef GUI_BUFF_H
#define GUI_BUFF_H

#include "gui/Common.h"
#include "gui/CardProperty.h"

#include <Ogre.h>
#include <boost/shared_ptr.hpp>

namespace Gui
{

	class Buff : public CardProperty
	{
	public:
		Buff(boost::weak_ptr<Card> subject);
		virtual ~Buff();


		Ogre::SceneNode* getSceneNode()
		{
			return mSceneNode;
		}

		Ogre::BillboardSet* getBillboardSet()
		{
			return mBillboardSet;
		}

		Ogre::Billboard* getBillboard()
		{
			return mBillboard;
		}

		boost::weak_ptr<Card> getParent()
		{
			return mCard;
		}

		void static registerProperty(boost::shared_ptr< Buff > buff_ptr);
		virtual std::string getMaterialName() const = 0;

	protected:
		friend class Card;

		Ogre::SceneNode* mSceneNode;
		Ogre::BillboardSet* mBillboardSet;
		Ogre::Billboard* mBillboard;

		boost::weak_ptr<Card> mCard;
	};


	// This is how you make a new bufffffff
	class TestBuff : public Buff
	{
	public:
		TestBuff(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test";
		}
	};

	class TestBuff2 : public Buff
	{
	public:
		TestBuff2(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test2";
		}
	};

	class TestBuff3 : public Buff
	{
	public:
		TestBuff3(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test3";
		}
	};

	class TestBuff4 : public Buff
	{
	public:
		TestBuff4(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test4";
		}
	};

	class TestBuff5 : public Buff
	{
	public:
		TestBuff5(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test5";
		}
	};

	class TestBuff6 : public Buff
	{
	public:
		TestBuff6(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test6";
		}
	};

	class TestBuff7 : public Buff
	{
	public:
		TestBuff7(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test7";
		}
	};

	class TestBuff8 : public Buff
	{
	public:
		TestBuff8(boost::weak_ptr<Card> subject) 
			: Buff(subject)
		{
		}

		virtual std::string getMaterialName() const
		{
			return "Buff/Test8";
		}
	};



}

#endif /* GUI_BUFF_H */
