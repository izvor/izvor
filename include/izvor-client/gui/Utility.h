#ifndef GUI_UTILITY_H
#define GUI_UTILITY_H

#include <Ogre.h>

namespace Gui
{
    class Utility
    {
    public:
        static Ogre::String getUniqueString(const Ogre::String& prefix = "")
        {
            return prefix + Ogre::StringConverter::toString(count++);
        }

    private:
        static std::size_t count;
    };
}

#endif