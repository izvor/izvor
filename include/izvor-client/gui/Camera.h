/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/
#ifndef GUI_CAMERA_H
#define GUI_CAMERA_H

#include "Updatable.h"

#include <Ogre.h>
#include <OIS.h>
//#include <SdkCameraMan.h>

namespace Gui
{
	class Camera : /*public OgreBites::SdkCameraMan,*/ public Updatable, public OIS::MouseListener, public OIS::KeyListener
	{
	public:
        Camera( Ogre::SceneManager* scene_manager );
		virtual ~Camera();

        Ogre::Camera* getOgreCamera()
        {
            return mCamera;
        }

        Ogre::SceneNode* getSceneNode() { return mCameraNode; }

		// OIS::MouseListener
        virtual bool mouseMoved( const OIS::MouseEvent &arg );
        virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
        virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

        // OIS::KeyListener
        virtual bool keyPressed( const OIS::KeyEvent &arg );
        virtual bool keyReleased( const OIS::KeyEvent &arg );

        virtual bool update(const Ogre::FrameEvent& evt);

        void sceneInit();

    private:
        Ogre::Camera* mCamera;
        Ogre::SceneNode* mCameraNode, *mTarget;

		bool mOrbiting, mZooming;
	};

}
#endif
