/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#ifndef CORE_H
#define CORE_H

#include <Ogre.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include <boost/noncopyable.hpp>

#include "gui/Common.h"
#include "gui/Fan.h"
#include "gui/PermanentsField.h"
#include "gui/Deck.h"

#include <stack>

#include "core/Game.h"



namespace Gui
{
	/*
		Stavio sam da bude singleton, tako da bi trebalo da se izbrisu svi pokazivaci na core u klasama
		i na SceneManager.
	*/

	class Core : boost::noncopyable , public Ogre::Singleton<Core>
	{
	public:
		Core( IzvorCommon::Core::Game *izvor_game );
		~Core();

		void initialise( Ogre::SceneManager* scene_manager);

		void createScene();

		void update( const Ogre::FrameEvent& evt );


        Ogre::SceneManager* getSceneManager() 
        {
            return mSceneManager; 
        }

		CardManager* getCardManager() 
		{
			return mCardManager;
		}

        UpdatableManager* getUpdatableManager()
        {
            return mUpdatableManager;
        }

        SceneRoot* getSceneRoot()
        {
            return mSceneRoot;
        }

        Camera* getCamera()
        {
            return mCamera;
        }

		IzvorCommon::Core::Game* getGame() const
		{
			return mGame;
		}

        static GameInitParameters& getInitParameters() { return mInitParameters; }


        // OIS::KeyListener
        void  injectKeyPressed( const OIS::KeyEvent &arg );
        void  injectKeyReleased( const OIS::KeyEvent &arg );
        // OIS::MouseListener
        void  injectMouseMoved( const OIS::MouseEvent &arg );
        void  injectMousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
        void  injectMouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

	private:

		/// Za sada se sve ovo nalazi ovde dok ne smislimo nesto bolje(verovatno SceneRoot)
		CardId getMouseOverCardId();
		Ogre::RaySceneQuery* mRaySceneQuery;
		Ogre::RaySceneQueryResult* mRaySceneQueryResult;
		/// Pronalazi entitete za podesenu masku, vraca true ako 
		/// ray pogadja bar jedan entitet sa podesenom maskom
		/// Korisno ako u zavisnosti od stanja igre proveravamo samo odredjene entitete
		/// i ako ih ray pogadja  obavestavamo samo one kojima je to od interesa.
		bool raySceneQueryExecute( Gui::QueryMasks );
		/// Proracunava mouse ray u zavisnosti od trenutne pozicije kursora
		void calculateMouseRay( const OIS::MouseState &arg );

		Ogre::SceneManager* mSceneManager;

		SceneRoot* mSceneRoot;
        Camera* mCamera;

		CardManager* mCardManager;
        UpdatableManager* mUpdatableManager;

		IzvorCommon::Core::Game *mGame;

        Ogre::SceneNode* mExpNode;

        static GameInitParameters mInitParameters;

	};

}

#endif
