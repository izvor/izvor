/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Thunder
*/

#ifndef GUI_PLAYING_FIELD_H
#define	GUI_PLAYING_FIELD_H

#include "gui/CardContainer.h"
#include "gui/Common.h"

#include <Ogre.h>
#include <list>

namespace Gui
{
	/*
		Za sada mi nije jasna ova klasa, ali cini mi se da je nepotrebna. Umesto nje 
		bi trebalo da se naprave specijalizovanije (PermanentsField, ArtifacField, SourceField, Stack, Attack/Block, etc.)
		Fan, Deck vec postoje...
	*/
	

    class PlayingField : public CardContainer
    {
    public:
        PlayingField(Ogre::SceneManager* scene_manager, Ogre::Real width, Ogre::Real height,
            const Ogre::Vector3& position, const Ogre::Quaternion& orientation = Ogre::Quaternion::IDENTITY);

        virtual ~PlayingField();

        virtual void acceptCard(CardId card_id);
        virtual void dispatchCard(CardId card_id);  
	
    private:
        typedef std::list<CardId> CardIdList;
        
        Ogre::Vector3 getNextPosition();
        Ogre::Vector3 getPositionAt(unsigned int col, unsigned int row);
        void resize(Ogre::Real new_h_step, Ogre::Real new_w_step);
        void reposition();
	
        Ogre::Real mWidth, mHeight;
        Ogre::Real mHorizontalStep, mVerticalStep;
        
        unsigned int mCurrentRow, mCurrentCol;
        unsigned int mMaxRows, mMaxCols;
		
        Ogre::SceneNode* mSceneNode;
        
        CardIdList mCardsList;

        static const Ogre::Real DEFAULT_HORIZONTAL_STEP;
        static const Ogre::Real DEFAULT_VERTICAL_STEP;
    };
}
#endif
