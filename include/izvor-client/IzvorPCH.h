/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#ifndef __IZVOR_PCH_H__INCL__
#define __IZVOR_PCH_H__INCL__

/*************************************************************************/
/* izvor-common includes                                                 */
/*************************************************************************/
#include "IzvorCommonPCH.h"

/*************************************************************************/
/* core includes                                                         */
/*************************************************************************/
#include "core/Ability.h"
#include "core/BaseCard.h"
#include "core/CardElement.h"
#include "core/CardMgr.h"
#include "core/CardSet.h"
#include "core/CEFactory.h"
#include "core/Color.h"
#include "core/Cost.h"
#include "core/Deck.h"
#include "core/DeckParser.h"
#include "core/Effect.h"
#include "core/Game.h"
#include "core/Mana.h"
#include "core/Object.h"
#include "core/ObjectMgr.h"
#include "core/EffectMgr.h"
#include "core/Permanents.h"
#include "core/Player.h"
#include "core/StackDamage.h"
#include "core/StackElement.h"
#include "core/Stack.h"
#include "core/Symbol.h"
#include "core/ZoneElements.h"
#include "core/Zones.h"

/*************************************************************************/
/* marshal includes                                                      */
/*************************************************************************/
#include "marshal/MarshalOpcodes.h"
#include "marshal/MarshalStream.h"
#include "marshal/UnmarshalStream.h"

/*************************************************************************/
/* network includes                                                      */
/*************************************************************************/
#include "network/AuthPacket.h"
#include "network/IzvorTcpConnection.h"
#include "network/IzvorTcpServer.h"
#include "network/PacketFactory.h"
#include "network/Packet.h"
#include "network/PktDumpVisitor.h"
#include "network/PktRep.h"
#include "network/PktVisitor.h"

/*************************************************************************/
/* script includes                                                       */
/*************************************************************************/
#include "script/ScriptParser.h"
#include "script/TriggerMgr.h"

#include "ReqHandler.h"

#endif /* !__IZVOR_PCH_H__INCL__ */
