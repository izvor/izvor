#ifndef PLAY_GAME_STATE_H
#define PLAY_GAME STATE_H

#include <GameState.hpp>
#include <EngineManager.hpp>
#include <GameStateManager.hpp>

#include "MyGUI.h"
#include "MyGUI_OgrePlatform.h"

#include "gui/Core.h"
#include "gui/Fan.h"



class PlayGameState : public Mge::GameState, public OIS::KeyListener, public OIS::MouseListener
{
public:
    PlayGameState();
    ~PlayGameState();

    void Initialise();
    void Shutdown();

    void Pause();
    void Resume();

    void CreateScene();

    // Ogre rendering calls
	bool frameEnded(const Ogre::FrameEvent& evt);
    bool frameRenderingQueued(const Ogre::FrameEvent& evt);

    // OIS::KeyListener
    virtual bool keyPressed( const OIS::KeyEvent &arg );
    virtual bool keyReleased( const OIS::KeyEvent &arg );
    // OIS::MouseListener
    virtual bool mouseMoved( const OIS::MouseEvent &arg );
    virtual bool mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id );
    virtual bool mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id );

private:
    Ogre::SceneManager* mSceneManager;
    Ogre::Camera* mCamera;
    Ogre::Viewport *mViewport;

    Gui::Core* mGuiCore;

    MyGUI::Gui* mMyGui;
    MyGUI::OgrePlatform* mMyGuiOgrePlatform;
};

#endif