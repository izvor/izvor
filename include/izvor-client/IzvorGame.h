#ifndef IZVOR_GAME_H
#define IZVOR_GAME_H

#include <Game.hpp>

#include "IzvorPCH.h"
#include "gui/Core.h"

class IzvorGame : public Mge::Game
{
public:
    IzvorGame( IzvorCommon::Core::Game* izvor_game );
    ~IzvorGame();

private:
    void Initialise();
    void Shutdown();

    // helpers for easier access to singletons
	// Da li je potrebno ovo, posto Game vec ima ova polja?
    Mge::EngineManager    *mEngineManager;
    Mge::GameStateManager *mGameStateManager;

	Gui::Core* mGuiCore;
};

#endif
