#ifndef ENGINE_MANAGER_HPP
#define ENGINE_MANAGER_HPP

#include <Ogre.h>
#include <OIS.h>

#include "MiniGameEngine.hpp"

namespace Mge
{
	/*
		Class EngineManager is a singleton class. It is used to initialise
		various subsystems, and later to get some objects relevant to those
		subsystems. Currently it hosts Ogre rendering engine and OIS input
		library. If you want to extend functionality, i.e. add an audio lib
		put it's initialisation code here, and make a get method that gets 
		whatever it should get.
		Thats sloppy design, and really shouldn't be used in a game engine,
		but this is a MINI game engine. It works for now, and there are more
		important things to do. Something like smart plugging in different 
		modules would be cool. 
	*/
	class _MgeExport EngineManager : public Ogre::Singleton<EngineManager>
	{
	public:
		EngineManager();
		~EngineManager();

		// initialises everything here
		void Initialise(const Ogre::String& window_title);

		// this is not necessary because Ogre::Root is a singleton
		Ogre::Root* GetOgreRoot() const
		{
			return mOgreRoot;
		}

		// we need to know which window we started up
		Ogre::RenderWindow* GetRenderWindow() const
		{
			return mWindow;
		}

		OIS::InputManager* GetInputManager() const
		{
			return mInputManager;
		}

		// used for grabbing keyboard input
		OIS::Keyboard* GetKeyboard() const
		{
			return mKeyboard;
		}

		// used for grabbing mouse 
		OIS::Mouse* GetMouse() const
		{
			return mMouse;
		}
	private:
		// initialisations of components are separated, so we can move them to separate classes if we wanted to
		void InitialiseInput(bool buffered_keyboard_input, bool buffered_mouse_input, bool buffered_joystick_input);
		void ShutdownInput();
		void ParseResourceConfig();
		void InitialiseWindow(const Ogre::String& window_title);
		// Initialises ALL resources, should be done in smaller chunks when resources become big
		// also MAKE A LOADING SCREEN
		void InitialiseResources(); 

		Ogre::Root *mOgreRoot;
		Ogre::RenderWindow *mWindow;

		OIS::InputManager *mInputManager;
		OIS::Keyboard *mKeyboard;

		OIS::Mouse *mMouse;
		OIS::JoyStick *mJoystick;
	};
}

#endif