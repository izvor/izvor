#ifndef GAME_HPP
#define GAME_HPP

#include <boost/utility.hpp>

#include "MiniGameEngine.hpp"

#include "GameStateManager.hpp"
#include "EngineManager.hpp"

namespace Mge
{
	/*
		Abstract class representing a game. It serves as a helper class
		and a base class for deriving a specific initialisation, running, 
		and shutdown code. It owns two most important singletons,
		so the end user wouldn't have to bother. Potential disaster if user
		tries to delete something. 
	*/
	class _MgeExport Game : public boost::noncopyable
	{
	public:
		Game();
		virtual ~Game();

		void Go();

	private:
		virtual void Initialise() = 0;
		virtual void Run(); // default run which invokes mGameStateManager->Run() loop
		virtual void Shutdown() = 0;

		EngineManager    *mEngineManager;
		GameStateManager *mGameStateManager;

	};
}
#endif