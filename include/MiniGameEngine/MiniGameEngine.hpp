#ifndef MINI_GAME_ENGINE
#define MINI_GAME_ENGINE

#include <Ogre.h>

namespace Mge
{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#   if defined(MINIGAMEENGINE_EXPORTS)
#       define _MgeExport //__declspec(dllexport)
#   else
#       if defined( __MINGW32__ )
#           define _MgeExport
#       else
#           define _MgeExport //__declspec(dllimport)
#       endif
#   endif
#else
#	define _MgeExport
#endif

}

#endif // MINI_GAME_ENGINE