#ifndef GAME_STATE_MANAGER_HPP
#define GAME_STATE_MANAGER_HPP

#include <string>
#include <boost/ptr_container/ptr_deque.hpp>
#include <boost/ptr_container/ptr_unordered_map.hpp>

#include "GameState.hpp"
#include "MiniGameEngine.hpp"

#include <stack>

namespace Mge
{
	typedef boost::ptr_unordered_map<std::string, GameState> StringStateMap;
	typedef std::stack<GameState*> GameStateStack;

	/*
		Singleton class that manages game states. User registers state that 
		can be managed via AddStateToManage(). GameState::mStateName serves
		as a key. Idea is to have one container that stores various states,
		and a stack that stores information about hierarchy among states.
		How this works in action, ask me <thunderzon@gmail.com>. I don't feel
		like writing anymore. 
		Bottom line is that there is a chance that we might need some kind of 
		functionality that this current implementation doesn't support, and that
		this system probably could be implemented better using mementos or 
		something like that. 
		This class requires extensive case testing!!
	*/
	class _MgeExport GameStateManager : public Ogre::Singleton<GameStateManager>
	{
	public:
		GameStateManager();
		~GameStateManager();

		// push state onto a stack (and initialise it), pause old state
		void PushState(const std::string &state_name);
		// change top state, to a new state (similar to popping old and pushing new)
		void ChangeState(const std::string &state_name);
		// pop state from a stack (and shut it down), resume old state
		void PopState();
		// run the state manager
		void Run();

		// add a new state to string state map
		// returns true on success 
		void AddStateToManage(GameState* state);

	private:
		GameStateStack mStatesStack;
		StringStateMap mStatesMap; 
	};
}
#endif