#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include <boost/utility.hpp>
#include <Ogre.h>
#include <string>

#include "MiniGameEngine.hpp"

namespace Mge
{
	/* 
		Abstract game state. More info in GameStateManager. States are used
		to encapsulate logic of a game state, and to provide control.
	*/
	class _MgeExport GameState : boost::noncopyable, public Ogre::FrameListener
	{
	public:
		GameState(const std::string& state_name) : mStateName(state_name)  {};
		virtual ~GameState() {};

		// gets called when state enters/leaves state stack
		virtual void Initialise() = 0;
		virtual void Shutdown() = 0;

		// gets called when state is 'not on top'/'resumes to top' of state stack
		virtual void Pause() = 0;
		virtual void Resume() = 0;

		// main loop goes to overriden frameRenderingQueued(), or similar
		// this is not obvious, that's a problem

		const std::string& GetStateName() const
		{
			return mStateName;
		}

	private:
		std::string mStateName;
	};

}
#endif