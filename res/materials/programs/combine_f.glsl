uniform sampler2D RT;
uniform sampler2D Blur1;

uniform float time;

void main()
{
    vec4 sharp;
    vec4 blur;
	
    sharp = texture2D( RT, gl_TexCoord[0].xy);
    blur = texture2D( Blur1, gl_TexCoord[0].xy);
    
    gl_FragColor = blur * (time + 1.0) / 2.0 + sharp;
}
