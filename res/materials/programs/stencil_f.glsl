uniform sampler2D tex0;
uniform int invTexSize;

varying vec2 uv;

#define CRVENA 0x10
#define ZELENA 0x08
#define PLAVA 0x04
#define ZUTA 0x02
#define CRNA 0x01
#define BEZBOJNA 0x20

uniform int card_type;
uniform int active;

void main(void)
{
    vec4 color;
    color = texture2D(tex0, uv);

    if(active == 1)
    {
        if(card_type == CRVENA)
        {
            if(color.x < 0.1 || color.y > 0.2 || color.z > 0.9)
            {
                discard;
            }
        } else if(card_type == ZELENA)
        {
            if(color.x > 0.3 || color.y < 0.1 || color.z > 0.5)
            {
                discard;
            } 
        } else if(card_type == PLAVA)
        {
            if(color.x > 0.3 || color.y > 0.9 || color.z < 0.2)
            {
                discard;
            }
        } else if(card_type == ZUTA)
        {
            if(color.x < 0.1 || color.y < 0.1 || color.z > 0.2)
            {  
                discard;
            } 
        } else if(card_type == CRNA)
        {
            if(color.x > 0.1 && color.y > 0.1 && color.z > 0.2)
            {
                discard;
            } 
        } else //if(card_type == BEZBOJNA)
        {
            if(color.x > 0.1 && color.y > 0.1 && color.z > 0.2)
            {
                discard;
            } 
        }
    } else discard;
    
    gl_FragColor = color;
}