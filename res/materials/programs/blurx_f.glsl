uniform sampler2D src;

varying vec2 uv;

float texScaler = 1.0/128.0;
float texOffset = -0.5/128.0;

vec4 gaussFilter[7];

void main(void)
{
   vec4 color = vec4(0.0,0.0,0.0,0.0);
   
   gaussFilter[0] = vec4(-3.0, 0.0, 0.0,  1.0/64.0);
   gaussFilter[1] = vec4(-2.0, 0.0, 0.0,  6.0/64.0);
   gaussFilter[2] = vec4(-1.0, 0.0, 0.0, 15.0/64.0);
   gaussFilter[3] = vec4( 0.0, 0.0, 0.0, 20.0/64.0);
   gaussFilter[4] = vec4( 1.0, 0.0, 0.0, 15.0/64.0);
   gaussFilter[5] = vec4( 2.0, 0.0, 0.0,  6.0/64.0);
   gaussFilter[6] = vec4( 3.0, 0.0, 0.0,  1.0/64.0);
   
   int i;
   for (i=0;i<7;i++)
   {
      color += texture2D(src,vec2(uv.x + gaussFilter[i].x * texScaler + texOffset,
                                  uv.y + gaussFilter[i].y * texScaler + texOffset)) * 
                    gaussFilter[i].w;
   } // End for
   
   color.a *= 2.0;
   
   gl_FragColor = color;
}