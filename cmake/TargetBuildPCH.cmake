# - Define macro to build precompiled header for a target
#
# TARGET_BUILD_PCH( TARGET PRECOMPILED_HEADER BOUND_SOURCE )
#     Builds a precompiled header while building target; currently MSVC support only.
#     Arguments:
#         TARGET             - target to which add the precompiled header
#         PRECOMPILED_HEADER - the header itself, FULL PATH!
#         BOUND_SOURCE       - the source the header is bound to, i.e. it is built during
#                              compilation of given source file (MSVC only, should be the
#                              last source file in the target)
#

MACRO( TARGET_BUILD_PCH TARGET PRECOMPILED_HEADER BOUND_SOURCE )
    GET_FILENAME_COMPONENT( PCH_NAME ${PRECOMPILED_HEADER} NAME )

    IF( MSVC )
        GET_PROPERTY( FLAGS TARGET ${TARGET} PROPERTY COMPILE_FLAGS )
        SET_PROPERTY( TARGET ${TARGET}       PROPERTY COMPILE_FLAGS "${FLAGS} /Yu\"${PCH_NAME}\"" )
        SET_PROPERTY( SOURCE ${BOUND_SOURCE} PROPERTY COMPILE_FLAGS "${FLAGS} /Yc\"${PCH_NAME}\"" )
    ENDIF( MSVC )

    IF( CMAKE_COMPILER_IS_GNUCXX )
        # build output name
 	SET( PCH_OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${PCH_NAME}.gch" )

	# chain compiler flags together
    	SET( COMPILER_FLAGS ${CMAKE_CXX_FLAGS} )
    
	GET_DIRECTORY_PROPERTY( I_DIR_FLAGS INCLUDE_DIRECTORIES )
    	FOREACH( ITEM ${I_DIR_FLAGS} )
       	    LIST( APPEND COMPILER_FLAGS "-I${ITEM}" )
	ENDFOREACH( ITEM )

  	GET_DIRECTORY_PROPERTY( I_DIR_FLAGS DEFINITIONS )
      	LIST( APPEND COMPILER_FLAGS ${I_DIR_FLAGS} )

    	SEPARATE_ARGUMENTS( COMPILER_FLAGS )

    	MESSAGE( "* Adding precompiled header ${PCH_NAME}.gch to ${TARGET}." )
	
	# create command to build it
	# IMPLICIT_DEPENDS scans the file in a lexical manner to find out what other
	# files it depends on, MSVC does this automatically
	ADD_CUSTOM_COMMAND(
	    OUTPUT ${PCH_OUTPUT}
      	    COMMAND ${CMAKE_CXX_COMPILER} ${COMPILER_FLAGS} -x c++-header -o ${PCH_OUTPUT} ${PRECOMPILED_HEADER}
      	    DEPENDS ${PRECOMPILED_HEADER} 
	    IMPLICIT_DEPENDS CXX ${PRECOMPILED_HEADER} )

	# add a custom PCH dependent target to main target
	ADD_CUSTOM_TARGET( ${TARGET}_gch DEPENDS ${PCH_OUTPUT} )
        ADD_DEPENDENCIES( ${TARGET} ${TARGET}_gch )

	# automatically include PCH with each compile unit in main target
        SET_TARGET_PROPERTIES( ${TARGET} PROPERTIES COMPILE_FLAGS "-include ${PCH_NAME} -Winvalid-pch")
    ENDIF(CMAKE_COMPILER_IS_GNUCXX)
ENDMACRO( TARGET_BUILD_PCH )
