/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"
#include "database/RecordSet.h"

namespace DB
{

  void RecordSet::addRecord( uint32 row_count, char **values, char **columns )
  {
    mSet.push_back( Record() );      
    for( uint32 i = 0; i < row_count; i++ ) {
      if( values[i] == NULL ) continue;
      mSet.back().addData( columns[i], values[i] );
    }
  }
  
  std::ostream& operator<<( std::ostream& os, const RecordSet& r )
  {
    for( std::vector<Record>::const_iterator i = r.mSet.begin();
	 i < r.mSet.end();
	 i++ )
      os << *i << std::endl;
    return os;
  }

}
