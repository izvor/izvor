/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "database/Core.h"

using namespace DB;

Core::~Core()
{
	sqlite3_unicode_free();
	sLog.message( "DB::Core", "Closing database." );
	if( mErrMsg ) sqlite3_free( mErrMsg );
	if( mDB ) sqlite3_close( mDB );
}

void Core::open( const std::string& db )
{
	mDBFile = db;
	//      mRetVal = sqlite3_open_v2( mDBFile.c_str(), &mDB, SQLITE_OPEN_READWRITE, NULL );
	mRetVal = sqlite3_open_v2( mDBFile.c_str(), &mDB, SQLITE_OPEN_READONLY, NULL );
	if( mRetVal != SQLITE_OK ) 
		throw ExcMsg( "DB::Core", "%s (err %d)", 
					  sqlite3_errmsg( mDB ), mRetVal );
	sLog.success( "DB::Core", "Opened database file '%s'.", mDBFile.c_str() );
}

RecordSet Core::query( const std::string& query )
{
	if( !mDB ) throw ExcMsg( "DB::Core", 
							 "Cannot query a closed database." );
	
	RecordSet rs;
	mRetVal = sqlite3_exec( mDB, query.c_str(), callback,
							&rs, &mErrMsg );
	if( mRetVal != SQLITE_OK )
		throw ExcMsg( "DB::Core", "SQL error: %s, in query:\n%s\n", mErrMsg, query.c_str() );
	
	return rs;
}

Record Core::querySingle( const std::string& query )
{
	RecordSet rs( this->query( query ) );
	if( rs.getSize() > 1 )
		throw ExcMsg( "DB::Core", "Single query returned multiple results, in query:\n%s\n", query.c_str() );
	if( !rs.getSize() )
		throw ExcMsg( "DB::Core", "Single query returned no results, in query:\n%s\n", query.c_str() );
      
	return rs[0];
}
