/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "database/Record.h"

namespace DB
{
  std::map<const Column, const std::string> colStr = 
    boost::assign::map_list_of
    (ID,             "card_id"             )
    (COLOR,          "card_colour"         )
    (COST,           "card_cost"           )
    (PRIMARY_TYPE,   "card_primary_type"   )
    (SECONDARY_TYPE, "card_secondary_type" )
    (ABILITY,        "card_ability"        )
    (ATTACK,         "card_attack"         )
    (DEFENSE,        "card_defense"        )
    (NAME,           "card_name"           )
    (RARITY,         "card_rarity"         )
    (LORE,           "card_lore"           )
    (IMAGE_AUTHOR,   "card_image_author"   )
    (REPRINT_ID,     "card_reprint_id"     );

  std::ostream& operator<<( std::ostream& os, const Record& r )
  {
    for( Record::Data::const_iterator i = r.mData.begin();
	 i != r.mData.end();
	 i++ )
      os << i->first << " = " << i->second << std::endl;
    return os;
  }
}
