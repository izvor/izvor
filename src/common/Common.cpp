/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#ifndef WIN32

void Sleep( uint32 x )
{
    if( 0 < x )
        usleep( 1000 * x );
}

uint32 GetTickCount()
{
    timeval tv;
    gettimeofday( &tv, NULL );

    return ( tv.tv_sec * 1000 ) + ( tv.tv_usec / 1000 );
}
#else /* !WIN32 */
int gettimeofday( timeval* tp, void* reserved )
{
    timeb tb;
    ftime( &tb );

    tp->tv_sec  = (long)tb.time;
    tp->tv_usec = tb.millitm * 1000;

    return 0;
}
#endif /* !WIN32 */
