/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "log/Log.h"

/*************************************************************************/
/* Log                                                                   */
/*************************************************************************/

#ifdef WIN32
const WORD Log::COLOR_TABLE[ COLOR_COUNT ] =
  {
    ( FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE                        ), // COLOR_DEFAULT
    ( 0                                                                          ), // COLOR_BLACK
    ( FOREGROUND_RED                                      | FOREGROUND_INTENSITY ), // COLOR_RED
    (                  FOREGROUND_GREEN                   | FOREGROUND_INTENSITY ), // COLOR_GREEN
    ( FOREGROUND_RED | FOREGROUND_GREEN                   | FOREGROUND_INTENSITY ), // COLOR_YELLOW
    (                                     FOREGROUND_BLUE | FOREGROUND_INTENSITY ), // COLOR_BLUE
    ( FOREGROUND_RED                    | FOREGROUND_BLUE | FOREGROUND_INTENSITY ), // COLOR_MAGENTA
    (                  FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY ), // COLOR_CYAN
    ( FOREGROUND_RED | FOREGROUND_GREEN | FOREGROUND_BLUE | FOREGROUND_INTENSITY )  // COLOR_WHITE
  };
#else /* !WIN32 */
const char* const Log::COLOR_TABLE[ COLOR_COUNT ] =
  {
    "\033[" "00"    "m", // COLOR_DEFAULT
    "\033[" "30;22" "m", // COLOR_BLACK
    "\033[" "31;22" "m", // COLOR_RED
    "\033[" "32;22" "m", // COLOR_GREEN
    "\033[" "33;01" "m", // COLOR_YELLOW
    "\033[" "34;01" "m", // COLOR_BLUE
    "\033[" "35;22" "m", // COLOR_MAGENTA
    "\033[" "36;01" "m", // COLOR_CYAN
    "\033[" "37;01" "m"  // COLOR_WHITE
  };
#endif /* !WIN32 */

Log::Log()
  : mLogfile( NULL ),
    mTime( 0 )
#   ifdef WIN32
    ,mStdOutHandle( GetStdHandle( STD_OUTPUT_HANDLE ) ),
    mStdErrHandle( GetStdHandle( STD_ERROR_HANDLE ) )
#   endif /* WIN32 */
{
  // open a default logfile
  if( openLogfile( "izvor.log" ) )
    {
      success( "Log", "Opened default logfile '%s'",
	       "izvor.log" );
    }
  else
    {
      warning( "Log", "Could not open default logfile '%s': %s",
	       "izvor.log", strerror( errno ) );
    }
}

Log::~Log()
{
  setLogfile( NULL );
}

void Log::message( const char* source, const char* format, ... )
{
  va_list ap;
  va_start( ap, format );

  printMsgVa( COLOR_DEFAULT, 'L', source, format, ap );

  va_end( ap );
}

void Log::error( const char* source, const char* format, ... )
{
  va_list ap;
  va_start( ap, format );

  printMsgVa( COLOR_RED, 'E', source, format, ap );

  va_end( ap );
}

void Log::warning( const char* source, const char* format, ... )
{
  va_list ap;
  va_start( ap, format );

  printMsgVa( COLOR_YELLOW, 'W', source, format, ap );

  va_end( ap );
}

void Log::success( const char* source, const char* format, ... )
{
  va_list ap;
  va_start( ap, format );

  printMsgVa( COLOR_GREEN, 'S', source, format, ap );

  va_end( ap );
}

void Log::debug( const char* source, const char* format, ... )
{
# ifndef NDEBUG
  va_list ap;
  va_start( ap, format );

  printMsgVa( COLOR_CYAN, 'D', source, format, ap );

  va_end( ap );
# endif /* !NDEBUG */
}

void Log::dump( const char* source, const void* data, size_t length,
		const char* format, ... )
{
# ifndef NDEBUG
  MutexLock l( mMutex );

  va_list ap;
  va_start( ap, format );

  printMsgVa( COLOR_MAGENTA, 'H', source, format, ap );

  va_end( ap );

  printDump( COLOR_MAGENTA, 'H', source, data, length );
# endif /* !NDEBUG */
}

bool Log::openLogfile( const char* filename )
{
  FILE* file = fopen( filename, "a" );
  if( NULL == file )
    return false;

  return setLogfile( file );
}

bool Log::setLogfile( FILE* file )
{
  MutexLock l( mMutex );

  if( hasLogfile() )
    {
      if( 0 != fclose( mLogfile ) )
	return false;
    }

  mLogfile = file;
  return true;
}

void Log::printDump( Color color, char prefix, const char* source,
		     const void* data, size_t length )
{
  for( size_t i = 0; i < length; i += 0x10 )
    printDumpLine( color, prefix, source, data, length, i );
}

void Log::printDumpLine( Color color, char prefix, const char* source,
			 const void* data, size_t length, size_t offset )
{
  char line[ 80 ];
  size_t lineLen = 0;

  char printable[ 0x10 ];

  for( size_t i = 0; i < 0x10; ++i )
    {
      if( 0x08 == i )
        {
	  lineLen += snprintf( &line[ lineLen ],
			       sizeof( line ) - lineLen,
			       " -" );
        }

      if( ( offset + i ) < length )
        {
	  const uint8 b = *( static_cast< const uint8* >( data ) + offset + i );

	  lineLen += snprintf( &line[ lineLen ],
			       sizeof( line ) - lineLen,
			       " %02X", b );
	  printable[ i ] = ( isgraph( b ) || isspace( b )
			     ? static_cast< const char >( b )
			     : '.' );
        }
      else
        {
	  lineLen += snprintf( &line[ lineLen ],
			       sizeof( line ) - lineLen,
			       "   " );
	  printable[ i ] = ' ';
        }
    }

  printMsg( color, prefix, source, "%04X:%.*s | %.*s",
	    offset, lineLen, line, sizeof( printable ), printable );
}

void Log::printMsg( Color color, char prefix, const char* source,
		    const char* format, ... )
{
  va_list ap;
  va_start( ap, format );

  printMsgVa( color, prefix, source, format, ap );

  va_end( ap );
}

void Log::printMsgVa( Color color, char prefix, const char* source,
		      const char* format, va_list ap )
{
  MutexLock l( mMutex );

  printTime();

  setColor( color );
  print( " %c ", prefix );

  if( source && *source )
    {
      setColor( COLOR_WHITE );
      print( "%s: ", source );

      setColor( color );
    }

  printVa( format, ap );
  print( "\n" );

  setColor( COLOR_DEFAULT );
}

void Log::printTime()
{
  MutexLock l( mMutex );

  setTime( time( NULL ) );
    
  tm t;
  localtime_r( &mTime, &t );

  timeval tv;
  gettimeofday( &tv, NULL );
    
  print( "%04d-%02d-%02d %02d:%02d:%02d.%06d",
	 1900 + t.tm_year, 1 + t.tm_mon, t.tm_mday,
	 t.tm_hour, t.tm_min, t.tm_sec,
	 tv.tv_usec );
}

void Log::print( const char* format, ... )
{
  va_list ap;
  va_start( ap, format );

  printVa( format, ap );

  va_end( ap );
}

void Log::printVa( const char* format, va_list ap )
{
  MutexLock l( mMutex );

  if( hasLogfile() )
    {
      // this is a design flaw ( UNIX related )
      va_list ap2;
      va_copy( ap2, ap );

      vfprintf( mLogfile, format, ap2 );

#ifndef NDEBUG
      // flush immediately so the logfile is accurate if we crash
      fflush( mLogfile );
#endif /* !NDEBUG */

      va_end( ap2 );
    }

  vprintf( format, ap );
}

void Log::setColor( Color color )
{
  assert( 0 <= color && color < COLOR_COUNT );

  MutexLock l( mMutex );

# ifdef WIN32
  SetConsoleTextAttribute( mStdOutHandle, COLOR_TABLE[ color ] );
# else /* !WIN32 */
  fputs( COLOR_TABLE[ color ], stdout );
# endif /* !WIN32 */
}
