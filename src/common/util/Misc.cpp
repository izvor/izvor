/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "util/Misc.h"

inline uint32 CantorPair( uint32 a, uint32 b )
{
  return (a + b) * (a + b + 1) / 2 + b;
}

void CantorUnPair( uint32 pair, uint32 *a, uint32 *b )
{
  uint32 w = static_cast<uint32>( sqrt( static_cast<float>( 8 * pair + 1 ) ) - 1 ) / 2;
  uint32 t = (w * w + w) / 2;

  *b = pair - t;
  *a = w - *b;
}

std::ptrdiff_t random( std::ptrdiff_t i ) { return rand() % i; }

uint64 npowof2( uint64 num )
{
    --num;
    num |= ( num >>  1 );
    num |= ( num >>  2 );
    num |= ( num >>  4 );
    num |= ( num >>  8 );
    num |= ( num >> 16 );
    num |= ( num >> 32 );
    ++num;

    return num;
}

