/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "util/Deflate.h"

const uint8 deflateHeaderByte = 0x78; //'x'

bool isDeflated( const Buffer& data )
{
    return ( deflateHeaderByte == data[0] );
}

bool deflateData( Buffer& data )
{
    Buffer dataDeflated;
    if( !deflateData( data, dataDeflated ) )
        return false;

    data = dataDeflated;
    return true;
}

bool deflateData( const Buffer& input, Buffer& output )
{
    const Buffer::Iterator<uint8> out = output.end<uint8>();

    size_t outputSize = compressBound( input.size() );
    output.resizeAt( out, outputSize );

    int res = compress( &*out, (uLongf*)&outputSize, &input[0], input.size() );

    if( Z_OK == res )
    {
        output.resizeAt( out, outputSize );
        return true;
    }
    else
    {
        output.resizeAt( out, 0 );
        return false;
    }
}

bool inflateData( Buffer& data )
{
    Buffer dataInflated;
    if( !inflateData( data, dataInflated ) )
        return false;

    data = dataInflated;
    return true;
}

bool inflateData( const Buffer& input, Buffer& output )
{
    const Buffer::Iterator<uint8> out = output.end<uint8>();

    size_t outputSize = 0;
    size_t sizeMultiplier = 0;

    int res = 0;
    do
    {
        outputSize = ( input.size() << ++sizeMultiplier );
        output.resizeAt( out, outputSize );

        res = uncompress( &*out, (uLongf*)&outputSize, &input[0], input.size() );
    } while( Z_BUF_ERROR == res );

    if( Z_OK == res )
    {
        output.resizeAt( out, outputSize );
        return true;
    }
    else
    {
        output.resizeAt( out, 0 );
        return false;
    }
}
