/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "threading/Mutex.h"

/*************************************************************************/
/* Mutex                                                                 */
/*************************************************************************/

Mutex::Mutex()
{
# ifdef WIN32
  InitializeCriticalSection( &mCriticalSection );
# else
  pthread_mutexattr_t attr;
  pthread_mutexattr_init( &attr );

# if defined( CYGWIN ) || defined( APPLE )
  pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_RECURSIVE );
# else
  pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_RECURSIVE_NP );
# endif

  pthread_mutex_init( &mMutex, &attr );
  pthread_mutexattr_destroy( &attr );
# endif
}

Mutex::~Mutex()
{
#ifdef WIN32
  DeleteCriticalSection( &mCriticalSection );
#else
  pthread_mutex_destroy( &mMutex );
#endif
}

void Mutex::lock()
{
#ifdef WIN32
  EnterCriticalSection( &mCriticalSection );
#else
  pthread_mutex_lock( &mMutex );
#endif
}

bool Mutex::tryLock()
{
#ifdef WIN32
  return TRUE == TryEnterCriticalSection( &mCriticalSection );
#else
  return 0 == pthread_mutex_trylock( &mMutex );
#endif
}

void Mutex::unlock()
{
#ifdef WIN32
  LeaveCriticalSection( &mCriticalSection );
#else
  pthread_mutex_unlock( &mMutex );
#endif
}
