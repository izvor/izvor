/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "network/NetUtils.h"

uint32 resolveIP( const char* hostname, char* errbuf )
{
#ifdef WIN32
  static InitWinsock ws;
#endif
  if( errbuf )
    errbuf[0] = 0;
  
  if( hostname == NULL )
    {
      if( errbuf )
	snprintf(errbuf, ERRBUF_SIZE, "ResolveIP(): hostname == NULL");
      return 0;
    }
  
  hostent* phostent = gethostbyname( hostname );
  if( phostent == NULL)
    {
#ifdef WIN32
      if( errbuf )
	snprintf( errbuf, ERRBUF_SIZE, "Unable to get the host name. Error: %i", WSAGetLastError() );
#else
      if( errbuf )
	snprintf( errbuf, ERRBUF_SIZE, "Unable to get the host name. Error: %s", strerror( errno ) );
#endif
      return 0;
    }
  
  in_addr addr;
  memcpy( &addr, phostent->h_addr, phostent->h_length );
  
  return addr.s_addr;
}
