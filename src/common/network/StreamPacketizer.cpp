/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    Copyright 2006, 2007, 2008 The EVEmu Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "CommonPCH.h"

#include "network/StreamPacketizer.h"

/*************************************************************************/
/* StreamPacketizer                                                      */
/*************************************************************************/
StreamPacketizer::~StreamPacketizer()
{
  clearBuffers();
}

void StreamPacketizer::inputData( const Data& data )
{
  mBuffer.append( data.begin< uint8 >(), data.end< uint8 >() );
}

void StreamPacketizer::process()
{
  Buffer::ConstIterator< uint8 > cur, end;
  cur = mBuffer.begin< uint8 >();
  end = mBuffer.end< uint8 >();
  while( true )
    {
      if( sizeof( uint32 ) > ( end - cur ) )
	break;

      const Buffer::ConstIterator< uint32 > len = cur.as< uint32 >();
      const Buffer::ConstIterator< uint8 > start = ( len + 1 ).as< uint8 >();

      if( *len > (uint32)( end - start ) )
	break;

      mPackets.push( new Buffer( start, start + *len ) );
      cur = ( start + *len );
    }

  if( cur != mBuffer.begin< uint8 >() )
    mBuffer.assign( cur, end );
}

Buffer* StreamPacketizer::popPacket()
{
  Buffer* ret = NULL;
  if( !mPackets.empty() )
    {
      ret = mPackets.front();
      mPackets.pop();
    }

  return ret;
}

void StreamPacketizer::clearBuffers()
{
  Buffer* buf;
  while( ( buf = popPacket() ) )
    SafeDelete( buf );
}
