/*
  -----------------------------------------------------------------------
  LICENSE:
  -----------------------------------------------------------------------
  This file is part of Izvor: Izvori Magije (C) TCG simulator
  Copyright 2010 EGDC++ Team
  Copyright 2006, 2007, 2008 The EVEmu Team
  For the latest info visit: http://sites.google.com/site/etfgamedevcrew
  -----------------------------------------------------------------------
  Izvor is free software: you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the 
  Free Software Foundation, either version 3 of the License, or (at your 
  option) any later version.

  Izvor is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
  -----------------------------------------------------------------------
  Author: dradojevic
*/

#include "CommonPCH.h"

#include "network/TcpServer.h"
#include "log/Log.h"

const uint32 TCPSRV_ERRBUF_SIZE = 1024;
const uint32 TCPSRV_LOOP_GRANULARITY = 5;

BaseTcpServer::BaseTcpServer()
  : mSock( NULL ),
    mPort( 0 )
{
}

BaseTcpServer::~BaseTcpServer()
{
  // Close socket
  close();

  // Wait until worker thread terminates
  waitLoop();
}

bool BaseTcpServer::isOpen() const
{
  bool ret;

  mMSock.lock();
  ret = ( mSock != NULL );
  mMSock.unlock();

  return ret;
}

bool BaseTcpServer::open( uint16 port, char* errbuf )
{
  if( errbuf != NULL )
    errbuf[0] = 0;

  // mutex lock
  MutexLock lock( mMSock );

  if( isOpen() )
    {
      if( errbuf != NULL )
	snprintf( errbuf, TCPSRV_ERRBUF_SIZE, "Listening socket already open" );
      return false;
    }
  else
    {
      mMSock.unlock();

      // Wait for thread to terminate
      waitLoop();

      mMSock.lock();
    }

  // Setting up TCP port for new TCP connections
  mSock = new Socket( AF_INET, SOCK_STREAM, 0 );

  // Quag: don't think following is good stuff for TCP, good for UDP
  // Mis: SO_REUSEADDR shouldn't be a problem for tcp - allows you to restart
  //      without waiting for conn's in TIME_WAIT to die
  // dradojevic: see http://www.unixguide.net/network/socketfaq/4.5.shtml
  unsigned int reuse_addr = 1;
  mSock->setopt( SOL_SOCKET, SO_REUSEADDR, &reuse_addr, sizeof( reuse_addr ) );

  // Setup internet address information.
  // This is used with the bind() call
  sockaddr_in address;
  memset( &address, 0, sizeof( address ) );

  address.sin_family = AF_INET;
  address.sin_port = htons( port );
  address.sin_addr.s_addr = htonl( INADDR_ANY );

  if( mSock->bind( (sockaddr*)&address, sizeof( address ) ) < 0 )
    {
      if( errbuf != NULL )
	sprintf( errbuf, "bind(): < 0" );

      SafeDelete( mSock );
      return false;
    }

  // dradojevic: default on my box is 124 kbytes :S
  unsigned int bufsize = 64 * 1024; // 64kbyte receive buffer, up from default of 8k
  mSock->setopt( SOL_SOCKET, SO_RCVBUF, &bufsize, sizeof( bufsize ) );

# ifdef WIN32
  unsigned long nonblocking = 1;
  mSock->ioctl( FIONBIO, &nonblocking );
# else
  mSock->fcntl( F_SETFL, O_NONBLOCK );
# endif

  if( mSock->listen() == SOCKET_ERROR )
    {
      if( errbuf != NULL )
#       ifdef WIN32
	snprintf( errbuf, TCPSRV_ERRBUF_SIZE, "listen() failed, Error: %u", WSAGetLastError() );
#       else
        snprintf( errbuf, TCPSRV_ERRBUF_SIZE, "listen() failed, Error: %s", strerror( errno ) );
#       endif

      SafeDelete( mSock );
      return false;
    }

  mPort = port;

  // Start processing thread
  startLoop();

  return true;
}

void BaseTcpServer::close()
{
  MutexLock lock( mMSock );

  SafeDelete( mSock );
  mPort = 0;
}

void BaseTcpServer::startLoop()
{
# ifdef WIN32
  _beginthread( BaseTcpServer::TcpServerLoop, 0, this );
# else
  pthread_t thread;
  pthread_create( &thread, NULL, &BaseTcpServer::TcpServerLoop, this );
# endif
}

void BaseTcpServer::waitLoop()
{
  //wait for loop to stop.

  //this will block until mMLoopRunning is unlocked by
  //dying worker thread
  mMLoopRunning.lock();
  mMLoopRunning.unlock();
}

bool BaseTcpServer::process()
{
  MutexLock lock( mMSock );

  if( !isOpen() )
    return false;

  listenNewConnections();
  return true;
}

void BaseTcpServer::listenNewConnections()
{
  Socket*         sock;
  sockaddr_in     from;
  unsigned int    fromlen;

  from.sin_family = AF_INET;
  fromlen = sizeof( from );

  MutexLock lock( mMSock );

  // Check for pending connects
  while( ( sock = mSock->accept( (sockaddr*)&from, &fromlen ) ) != NULL )
    {
#ifdef WIN32
      unsigned long nonblocking = 1;
      sock->ioctl( FIONBIO, &nonblocking );
#else
      sock->fcntl( F_SETFL, O_NONBLOCK );
#endif /* !WIN32 */
      
      unsigned int bufsize = 64 * 1024; // 64kbyte receive buffer, up from default of 8k
      sock->setopt( SOL_SOCKET, SO_RCVBUF, &bufsize, sizeof( bufsize ) );
      
      // New TCP connection, this must consume the socket.
      createNewConnection( sock, from.sin_addr.s_addr, ntohs( from.sin_port ) );
    }
}

thread_return_t BaseTcpServer::TcpServerLoop( void* arg )
{
  BaseTcpServer* tcps = reinterpret_cast<BaseTcpServer*>( arg );
  assert( tcps != NULL );

  THREAD_RETURN( tcps->TcpServerLoop() );
}

thread_return_t BaseTcpServer::TcpServerLoop()
{
#ifdef WIN32
  SetThreadPriority( GetCurrentThread(), THREAD_PRIORITY_ABOVE_NORMAL );
#endif

#ifndef WIN32
  sLog.message( "Threading", "Starting TcpServerLoop with thread ID %d", pthread_self() );
#endif

  mMLoopRunning.lock();

  uint32 start = GetTickCount();
  uint32 etime;
  uint32 last_time;

  while( process() )
    {
      /* UPDATE */
      last_time = GetTickCount();
      etime = last_time - start;

      // do the stuff for thread sleeping
      if( TCPSRV_LOOP_GRANULARITY > etime )
	Sleep( TCPSRV_LOOP_GRANULARITY - etime );

      start = GetTickCount();
    }

  mMLoopRunning.unlock();

#ifndef WIN32
  sLog.message( "Threading", "Ending TcpServerLoop with thread ID %d", pthread_self() );
#endif

  THREAD_RETURN( NULL );
}

