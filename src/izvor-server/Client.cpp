#include "IzvorServerPCH.h"

#include "Client.h"
#include "network/PktRep.h"

void Client::process()
{
    assert( NULL != mCon );
    // assert( TcpConnection::STATE_CONNECTED == mCon->getState() );

    PktRep *a;
    while( (a = mCon->popRep() ) != NULL ) {
        sLog.message( "Client", "Received" );
        a->dump();
        PktSafeDecRef( a );
    }


    // uint32 *a = mCon->popPacket();
    //   sLog.message( "Client", "Received %d", *a );
    // }
    // sLog.dump( "Client", (void*)&a, sizeof( uint32 ), "int" );
    //  process();
}
