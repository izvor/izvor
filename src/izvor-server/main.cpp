#include "IzvorServerPCH.h"

static void setupSignals();
static void catchSignal( int sig_num );

static const uint32 MAIN_LOOP_DELAY = 10; // delay 10 ms.

static volatile bool runLoops = true;

int main()
{
#if defined( MSVC ) && !defined( NDEBUG )
    // Under Visual Studio setup memory leak detection
    _CrtSetDbgFlag( _CRTDBG_LEAK_CHECK_DF | _CrtSetDbgFlag( _CRTDBG_REPORT_FLAG ) );
#endif /* defined( MSVC ) && !defined( NDEBUG ) */

    sGame.getDB()->open( RES_PATH"/db/izvor_cards.db" );
    IzvorTcpServer tcps;
    //char errbuf[TCPCONN_ERRBUFF_SIZE];
    char errbuf[1024];
    if( tcps.open( 26001, errbuf ) )
        sLog.success( "server init", "TCP listener started on port %u.", 26001 );
    else {
        sLog.error( "server init", "Failed to start TCP listener on port %u: %s.", 26001, errbuf );
        return 1;
    }

    /* program events system */
    setupSignals();

    uint32 start;
    uint32 etime;
    uint32 ltime = GetTickCount();

    IzvorTcpConnection *tcpc;
    std::list<Client*> listC;
    while( runLoops ) {
        start = GetTickCount();

        while( (tcpc = tcps.popConnection()) ) {
            Client *c = new Client( &tcpc );
            listC.push_back( c );
        }

        for( std::list<Client*>::iterator i = listC.begin();
            i != listC.end();
            i++ ) 
            (*i)->process();

        ltime = GetTickCount();
        etime = ltime - start;

        if( MAIN_LOOP_DELAY > etime )
            Sleep( MAIN_LOOP_DELAY - etime );
    }

    for( std::list<Client*>::iterator i = listC.begin();
        i != listC.end();
        i++ ) delete *i;

    sLog.message( "server shutdown", "Main loop stopped" );
    tcps.close();

    sLog.message( "server shutdown", "TCP listener stopped" );

    // std::string result;
    // CryptoPP::SHA1 hash;
    // CryptoPP::StringSource proba( "Danas je lep dan.", true,
    // 				new CryptoPP::HashFilter
    // 				( hash, new CryptoPP::HexEncoder
    // 				  ( new CryptoPP::StringSink( result ) ) ) );
    // sLog.message( "SHA1", result.c_str() );

    return 0;
}

static void setupSignals()
{
    signal( SIGINT, catchSignal );
    signal( SIGTERM, catchSignal );
    signal( SIGABRT, catchSignal );
#ifdef WIN32
    signal( SIGBREAK, catchSignal );
    signal( SIGABRT_COMPAT, catchSignal );
#else
    signal( SIGHUP, catchSignal );
#endif
}

static void catchSignal( int sig_num )
{
    sLog.message( "Signal system", "Caught signal: %d", sig_num );
    runLoops = false;
}
