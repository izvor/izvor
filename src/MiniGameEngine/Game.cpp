#include "Game.hpp"

namespace Mge
{
	Game::Game() 
	{
		mGameStateManager = new Mge::GameStateManager();
		mEngineManager    = new Mge::EngineManager();
	}

	Game::~Game()
	{
		if(mEngineManager)
		{
			delete mEngineManager;
			mEngineManager = 0;
		}

		if(mGameStateManager)
		{
			delete mGameStateManager;
			mGameStateManager = 0;
		}
	}

	void Game::Go()
	{
		Initialise();
		Run();
		Shutdown();
	}

	void Game::Run()
	{
		mGameStateManager->Run();
	}
}