#include "EngineManager.hpp"

// this is required to properly instantiate Ogre::Singleton
template<> 
Mge::EngineManager* Ogre::Singleton<Mge::EngineManager>::ms_Singleton = 0;

namespace Mge
{
	EngineManager::EngineManager()
	{
		mOgreRoot = 0;
		mInputManager = 0;
		mKeyboard = 0;
		mMouse = 0;
		mJoystick = 0;
	}

	EngineManager::~EngineManager()
	{	
		ShutdownInput();

		// use OGRE_DELETE with OGRE_NEW, where Ogre provides them
		if (mOgreRoot)
		{
			OGRE_DELETE mOgreRoot;
			mOgreRoot = 0;
		}
	}

	void EngineManager::Initialise(const Ogre::String& window_title)
	{
		mOgreRoot = OGRE_NEW Ogre::Root();

		InitialiseWindow(window_title);
		ParseResourceConfig();
		InitialiseResources();

		// true true true means use buffered input for all devices. We want buffered input so we can use input listeners.
		InitialiseInput(true, true, true);
	}

	void EngineManager::InitialiseInput(bool buffered_keyboard_input, bool buffered_mouse_input, bool buffered_joystick_input)
	{
		Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
		// this is standard way to initialise OIS, from ogre wiki
		OIS::ParamList pl;
		size_t windowHnd = 0;
		std::ostringstream windowHndStr;

		// windows needs to be created prior to OIS initialisation
		mWindow->getCustomAttribute("WINDOW", &windowHnd);
		windowHndStr << windowHnd;
		pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));

		mInputManager = OIS::InputManager::createInputSystem( pl );

		// if user doesn't have a keyboard and a mouse throw
		try
		{
			mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, buffered_keyboard_input));
			mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, buffered_mouse_input));
		}
		catch (const OIS::Exception &e)
		{
			throw Ogre::Exception(42, e.eText, "Application::setupInputSystem");
		}

		// if user doesn't have a joystick, just set it to NULL
		try {
			mJoystick = static_cast<OIS::JoyStick*>(mInputManager->createInputObject( OIS::OISJoyStick, buffered_joystick_input ));
		}
		catch(...) {
			mJoystick = 0;
		}	
	}
	void EngineManager::ShutdownInput()
	{
		if( mInputManager )
		{
			mInputManager->destroyInputObject(mMouse);
			mInputManager->destroyInputObject(mKeyboard);
			mInputManager->destroyInputObject(mJoystick);

			OIS::InputManager::destroyInputSystem(mInputManager);
			mInputManager = 0;
		}
	}

	void EngineManager::ParseResourceConfig()
	{
		Ogre::LogManager::getSingletonPtr()->logMessage("*** Parsing Resource Cfg ***");

		Ogre::String sec_name, type_name, arch_name;
		Ogre::ConfigFile cf;
		cf.load("resources.cfg");

		Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
		while (seci.hasMoreElements())
		{
			sec_name = seci.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;

			for (i = settings->begin(); i != settings->end(); ++i)
			{
				type_name = i->first;
				arch_name = i->second;
				// adds resource locations from resrources.cfg
				Ogre::ResourceGroupManager::getSingletonPtr()->addResourceLocation(arch_name, type_name, sec_name);
			}
		}
	}

	void EngineManager::InitialiseWindow(const Ogre::String& window_title)
	{
		Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing Window ***");

		// autocreate window from Ogres dialog. This should be changed to creating window from conf file.
		if(Ogre::Root::getSingletonPtr()->showConfigDialog())
			Ogre::Root::getSingletonPtr()->initialise(true, window_title); 
		else
			// should not throw but this was copy paste
			throw Ogre::Exception(52, "User canceled the config dialog!", "Application::setupRenderSystem()");

		mWindow = mOgreRoot->getAutoCreatedWindow();
	}

	void EngineManager::InitialiseResources()
	{
		Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing Resources ***");
		// mipmap level should be set before loading textures to have effect.
		Ogre::TextureManager::getSingletonPtr()->setDefaultNumMipmaps(5);
		// loads everything from resources.cfg
		Ogre::ResourceGroupManager::getSingletonPtr()->initialiseAllResourceGroups();
	}
}