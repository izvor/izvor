#include "GameStateManager.hpp"

template<> 
Mge::GameStateManager* Ogre::Singleton<Mge::GameStateManager>::ms_Singleton = 0;

namespace Mge
{
	GameStateManager::GameStateManager()
	{

	}

	GameStateManager::~GameStateManager()
	{
		while(!mStatesStack.empty())
			PopState();

		// boost ptr container takes care of this now
		// Should GameState::Shutdown() be called upon destruction of state manager?

		//while(!mStatesMap.empty())
		//{
		//	GameState *current_state = mStatesMap.begin()->second;
		//	//current_state->Shutdown();
		//	delete current_state;
		//	mStatesMap.erase(mStatesMap.begin());
		//}
	}

	void GameStateManager::ChangeState(const std::string &state_name)
	{
		// find state in string state map
		StringStateMap::iterator it = mStatesMap.find(state_name);
		if (it != mStatesMap.end())
		{
			// if top exists shut it down, and pop it
			if (!mStatesStack.empty())
			{
				mStatesStack.top()->Shutdown();
				mStatesStack.pop();
			}
			// retrieve state from string state map, push it, and initialise it
			GameState *state = it->second;
			mStatesStack.push(state);
			mStatesStack.top()->Initialise();
		} else throw std::runtime_error("Unknown Game state: " + state_name);
	}

	void GameStateManager::PushState(const std::string &state_name)
	{
		// find state in string state map
		StringStateMap::iterator it = mStatesMap.find(state_name);
		if (it != mStatesMap.end())
		{
			// pause top if exists
			if (!mStatesStack.empty())
			{
				mStatesStack.top()->Pause();
			}

			// retrieve state from string state map, push it, and initialise it
			GameState *state = it->second;
			mStatesStack.push(state);
			mStatesStack.top()->Initialise();
		} else throw std::runtime_error("Unknown Game state: " + state_name);
	}

	void GameStateManager::PopState()
	{
		// if top exists shut it down, and pop it
		if(!mStatesStack.empty())
		{
			mStatesStack.top()->Shutdown();
			mStatesStack.pop();
		}

		// if top still exists resume it
		if(!mStatesStack.empty())
		{
			mStatesStack.top()->Resume();
		}
	}

	void GameStateManager::Run()
	{
		// this might change in future
		while(!mStatesStack.empty())
		{
			// when Ogre starts rendering it enters it's own loop and doesn't return until any of 
			// registered frame listeners return false from frameRendering() methods.
			// it might be better not to call this from while(!mStatesStack.empty()) loop,
			// but then states should take care of what they are returning
			Ogre::Root::getSingletonPtr()->startRendering();
		}
	}

	void GameStateManager::AddStateToManage(GameState* state)
	{
		std::string key(state->GetStateName());
		if(!mStatesMap.insert(key, state).second)
			throw std::runtime_error("State with same name already exists: " + key);
	}
}

