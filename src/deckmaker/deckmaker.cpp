#include "deckmaker.h"
#include "ui_deckmaker.h"

#include <QFileDialog>
#include <QMessageBox>

std::map<const Card::Info::Edition, const std::string> DeckMaker::sImgPath = boost::assign::map_list_of
  ( Card::Info::IZVORISTE,        RES_PATH"/img/izvoriste/"        )
  ( Card::Info::PLEMENSKI_RATOVI, RES_PATH"/img/plemenski_ratovi/" )
  ( Card::Info::DOBA_SAVEZA,      RES_PATH"/img/doba_saveza/"      )
  ( Card::Info::DOBA_MISTERIJA,   RES_PATH"/img/doba_misterija/"   )
  ( Card::Info::TRECA_EDICIJA,    RES_PATH"/img/treca_edicija/"     );

DeckMaker::DeckMaker(QWidget *parent) :
    QMainWindow(parent),
    mModified( false ), mSaved( false ),
    mStatus( "Neimar spreman za rad" ),
    mFileName( "Neimenovani.xml" ),
    ui(new Ui::DeckMaker)
{ 
  ui->setupUi(this);

  ui->lstDeck->setCardsCounted( true );
  ui->lstSideDeck->setCardsCounted( true );

  ui->lstDeck->setIDHidden( true );
  ui->lstSideDeck->setIDHidden( true );

  ui->lstResults->setColumns( DB::ID | DB::NAME | CardTreeView::NUM | CardTreeView::EDITION );
  ui->lstDeck->setColumns( CardTreeView::COUNT | DB::NAME | DB::COLOUR |
			   DB::PRIMARY_TYPE | DB::SECONDARY_TYPE | CardTreeView::EDITION ); 

  connect( this, SIGNAL( modified( bool ) ),
	   this, SLOT( updateTitle( bool ) ) );

  statusBar()->addPermanentWidget( &mStatus );
  updateTitle( false );
}

void DeckMaker::updateTitle( bool modified )
{
  if( modified )
    setWindowTitle( QString( "Neimar - " ) + mFileName + QString( "*" ) );
  else
    setWindowTitle( QString( "Neimar - " ) + mFileName );

  mModified = modified;
}

void DeckMaker::open()
{
  if( mModified ) {
    // pitaj da li zeli da sacuva prvo
    QMessageBox msgBox;
    msgBox.setText( QString::fromUtf8( "Trenutno učitani špil je izmenjen." ) );
    msgBox.setInformativeText( QString::fromUtf8( "Da li želite da sačuvate promene?" ) );
    msgBox.setStandardButtons( QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel );
    msgBox.setDefaultButton( QMessageBox::Save );
    int ret = msgBox.exec();
  
    switch( ret ) {
    case QMessageBox::Save:
      save();
      break;
    case QMessageBox::Cancel: case QMessageBox::Escape:
      return; 
    }
  }
  
  #ifndef WIN32
  QString new_fname = QFileDialog::getOpenFileName( this, QString::fromUtf8( "Učitaj špil" ),
					    QString( "" ), QString::fromUtf8( "Neimar XML špil (*.nei *.xml );;Sve datoteke (*)" ) );
#else
  QString new_fname = QFileDialog::getOpenFileName( this, QString::fromUtf8( "Učitaj špil" ),
						    "~/", QString::fromUtf8( "Sve datoteke (*.*);;Neimar XML špil (*.nei *.xml )" ) );
#endif
  
  Deck tmp;
  if( !tmp.load( new_fname.toStdString() ) )
	  QMessageBox::warning( this, QString::fromUtf8( "Greška pri parsovanju" ),
							QString::fromUtf8( "Ne mogu da parsujem XML datoteku. Razloga može biti više: dogodila se greška pri otvaranju, "
											   "datoteka nije u validnom XML formatu ili se dogodila greška prilikom parsovanja (najverovatnije "
											   "zbog pogrešnog formata XML špila)." ) );
  
  else {
	  // ubaci karte u deck
	  emptyDeck();
	  for( BaseDeck::CardDeck::const_iterator i = tmp.deck().begin();
		   i != tmp.deck().end();
		   i++ )
		  ui->lstDeck->addItem( (*i)->id() );

	  // ubaci karte u sidedeck
	  emptySideDeck();
	  for( BaseDeck::CardDeck::const_iterator i = tmp.sideDeck().begin();
		   i != tmp.sideDeck().end();
		   i++ )
		  ui->lstSideDeck->addItem( (*i)->id() );
    
	  mFileName = new_fname;
	  mSaved = true;
	  statusBar()->showMessage( QString::fromUtf8( "Datoteka %0 uspešno učitana" ).arg( mFileName ) );
    
	  emit modified( false );
  }
}

void DeckMaker::save()
{
  if( !mSaved && mModified) {
    // otvori dialog
    mFileName = QFileDialog::getSaveFileName( this, QString::fromUtf8( "Sačuvaj špil..." ),
											  mFileName, QString::fromUtf8( "Neimar XML špil (*.xml )" ) );
  }

  // spoji deckove, mozda koristi samo jedan deck, ali treba
  // puno toga promeniti
  ui->lstDeck->getDeck()->sideDeck() = ui->lstSideDeck->getDeck()->deck();
  
  if( !ui->lstDeck->getDeck()->save( mFileName.toStdString() ) )
	  QMessageBox::warning( this, QString::fromUtf8( "Greška pri čuvanju" ),
							QString::fromUtf8( "Ne mogu da sačuvam XML datoteku." ) );
  else {
	  statusBar()->showMessage( QString::fromUtf8( "Datoteka %0 uspešno sačuvana" ).arg( mFileName ) );
	  mSaved = true;

	  emit modified( false );
  }
}

void DeckMaker::saveAs()
{
  mFileName = QFileDialog::getSaveFileName( this, QString::fromUtf8( "Sačuvaj špil kao..." ),
					    mFileName, QString::fromUtf8( "Neimar XML špil (*.xml )" ) ); // dodaj csv

  // spoji deckove, mozda koristi samo jedan deck, ali treba
  // puno toga promeniti
  ui->lstDeck->getDeck()->sideDeck() = ui->lstSideDeck->getDeck()->deck();
  if( !ui->lstDeck->getDeck()->save( mFileName.toStdString() ) )
	  QMessageBox::warning( this, QString::fromUtf8( "Greška pri čuvanju" ),
							QString::fromUtf8( "Ne mogu da sačuvam XML datoteku" ) );
  else {
	  statusBar()->showMessage( QString::fromUtf8( "Datoteka %0 uspešno sačuvana" ).arg( mFileName ) );

	  mSaved = true;
	  emit modified( false );

	  statusBar()->showMessage( QString::fromUtf8( "Datoteka %0 uspešno sačuvana" ).arg( mFileName ) );
  }
}

void DeckMaker::newDeck()
{
  if( mModified ) {
    // pitaj da li zeli da sacuva prvo
    QMessageBox msgBox;
    msgBox.setText( QString::fromUtf8( "Trenutno učitani špil je izmenjen." ) );
    msgBox.setInformativeText( QString::fromUtf8( "Da li želite da sačuvate promene?" ) );
    msgBox.setStandardButtons( QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel );
    msgBox.setDefaultButton( QMessageBox::Save );
    int ret = msgBox.exec();
  
    switch( ret ) {
    case QMessageBox::Save:
      save();
      break;
    case QMessageBox::Cancel:
      return;
    }
  }
  emptyDeck();
  emptySideDeck();
  mFileName = "Neimenovani.xml";
  emit modified( false );
}

void DeckMaker::emptyDeck()
{
  ui->lstDeck->clear();
  ui->btnRemoveDeck->setEnabled( false );
}

void DeckMaker::emptySideDeck()
{
  ui->lstSideDeck->clear();
  ui->btnRemoveSideDeck->setEnabled( false );
}

void DeckMaker::addCardToDeck() 
{
  const Card *pCard = ui->lstResults->getSelection();
  if( !pCard ) return; 
  // ovde nam nije bitno sta je selektovano u Deck-u
  uint32 row = ui->lstDeck->addItem( pCard->id(), ui->sldCountDeck->value() );
  if( ui->lstDeck->getDeck()->cardCount( pCard->id() ) == 4 ) {
    ui->btnAddDeck->setEnabled( false );
    ui->sldCountDeck->setMaximum( 1 );
    ui->sldCountDeck->setEnabled( false );
  }
  else ui->sldCountDeck->setMaximum( 4 - ui->lstDeck->getDeck()->cardCount
				     ( pCard->id() ) );

  ui->btnRemoveDeck->setEnabled( true );
  
  ui->lstDeck->selectRow( row );
  emit modified( true );
}

void DeckMaker::removeCardFromDeck()
{
  uint32 row = ui->lstDeck->selectionModel()->selectedIndexes().value( 0 ).row();

  const Card *pCard = ui->lstDeck->getSelection();
  if( !pCard ) return;
  ui->lstDeck->deleteItem( pCard->id() );
  if( ui->lstDeck->getDeck()->isEmpty() )
    ui->btnRemoveDeck->setEnabled( false );
 
  if( ui->lstResults->getSelection() && ui->lstDeck->getDeck()->cardCount( ui->lstResults->getSelection()->id() ) < 4 ) { 
    ui->sldCountDeck->setEnabled( true );
    ui->sldCountDeck->setMaximum( 4 - ui->lstDeck->getDeck()->cardCount
				  ( ui->lstResults->getSelection()->id() ) );
    ui->btnAddDeck->setEnabled( true );
  }
  //else {// mozda disable?
      
  ui->lstDeck->selectRow( row );
  emit modified( true );
}

void DeckMaker::updateUI() 
{ 
  const Card *pCard = ui->lstResults->getSelection();
  if( pCard ) {
    showCard( pCard );    
    ui->btnAddSideDeck->setEnabled( true );
    if( ui->lstDeck->getDeck()->cardCount( pCard->id() ) < 4 ) {
      ui->btnAddDeck->setEnabled( true );
      ui->sldCountDeck->setMaximum( 4 - ui->lstDeck->getDeck()->cardCount( pCard->id() )  );
      ui->sldCountDeck->setEnabled( true );
    }
    else {
      ui->btnAddDeck->setEnabled( false );
      ui->sldCountDeck->setMaximum( 1 );
      ui->sldCountDeck->setEnabled( false );
    }
  }
  else {
    const Card* dpCard = ui->lstDeck->getSelection();
    if( dpCard ) {
      showCard( dpCard );
      ui->btnRemoveDeck->setEnabled( true );
    }				    
  }
}
  
void DeckMaker::showCard( const Card *pCard )
{  
  // napravi putanju do slike
  QString imgPath = QString::fromUtf8( sImgPath[pCard->info()->edition()].c_str() )
    + QString( "%0" ).arg( pCard->info()->num()  )
    + QString( ".jpg" );
  QPixmap *pix = new QPixmap( imgPath );
  if( !*pix ) { 
    // ne postoji fajl...
    SafeDelete( pix );
    // probaj reprint sliku
    if( pCard->info()->reprintNum() ) {
      imgPath = QString::fromUtf8( sImgPath[pCard->info()->reprintEdition()].c_str() )
	+ QString( "%0" ).arg( pCard->info()->reprintNum() )
	+ QString( ".jpg" );
      pix = new QPixmap( imgPath );
      if( !*pix ) SafeDelete( pix );
    }
    
    if( !pix )
      pix = new QPixmap( QString( ":img/back" ) );
  }

  ui->lblCardImage->setPixmap( *pix );
  delete pix;

  ui->lblCardName->setText( QString( "<b>" ) +
			    QString::fromUtf8( pCard->info()->name().c_str() ) +
			    QString( "</b>" ) );
			    
  QString type = QString::fromUtf8( Card::getTypeStr( pCard->type() ).c_str() );
  if( !pCard->secondaryType().empty() )
    type += QString( " - " ) + QString::fromUtf8( pCard->secondaryType().c_str() );
  ui->lblCardType->setText( type );
  ui->lblCardRarity->setText( QString::fromUtf8( Card::Info::getRarityStr( pCard->info()->rarity() ).c_str() ) );
  ui->lblCardNumEdition->setText( QString( "%0 (%1 / %2)" )
								  .arg( QString::fromUtf8( Card::Info::getEditionStr( pCard->info()->edition() ).c_str() ) )
								  .arg( pCard->info()->num() )
								  .arg( Card::Info::getEditionSize( pCard->info()->edition() ) ) );
  ui->lblCardColor->setText( QString::fromUtf8( pCard->colour()->getDescriptionStr().c_str() ) );
											
  if( pCard->type() == Card::BICE || pCard->type() == Card::ARTEFAKT_BICE )
	  ui->lblCardAttributes->setText( QString( "%0 / %1" ).arg( (pCard->power() == -1) ? QString( "*" ) : QString::number( pCard->power() ) )
									  .arg( (pCard->toughness() == -1) ? QString( "*" ) : QString::number( pCard->toughness() ) ) );
  else ui->lblCardAttributes->setText( "" );

  // isprazni mana cost layout
  while( ui->layManaCost->count() ) {
    QLayoutItem *item = ui->layManaCost->takeAt( 0 );
    QWidget *widget = item->widget();
    if( widget ) widget->deleteLater();
  }
  ui->layManaCost->insertStretch( -1 );

  uint8 i = Colour::NUM;
  while( i-- ) {
    const std::list<uint8>& lst = pCard->manaCost()->count( Colour::getTypeByID( i ) );
	if( !lst.size() ) continue;
    for( std::list<uint8>::const_iterator j = lst.begin();
	 j != lst.end();
	 j++ ) {
      uint8 count = *j;
	  //      if( !count ) continue;
      
      QLabel *lbl = new QLabel;
	  lbl->setAlignment( Qt::AlignRight | Qt::AlignVCenter );
	  lbl->setText( "" );
      if( count == 'x' ) {
	if( Colour::getTypeByID( i ) == Colour::BEZBOJNA )
	  lbl->setPixmap( QPixmap( QString( ":pixmaps/X" ) ) );
	else lbl->setPixmap( QPixmap( QString( ":pixmaps/%1X" ).arg
				      ( Colour::getColourCode( Colour::getTypeByID( i ) ) ) ) );
      }
      else {
	if( Colour::getTypeByID( i ) == Colour::BEZBOJNA ) {
	  if( count == 10 ) lbl->setPixmap( QPixmap( QString( ":pixmaps/D" ) ) );
	  else if( count == 16 ) lbl->setPixmap( QPixmap( QString( ":pixmaps/S" ) ) );
	  else lbl->setPixmap( QPixmap( QString( ":pixmaps/%0" ).arg( count ) ) );
	}
	else if( count == 1 ) lbl->setPixmap( QPixmap( QString( ":pixmaps/%0" ).arg
						       ( Colour::getColourCode( Colour::getTypeByID( i ) ) ) ) );
	else lbl->setPixmap( QPixmap( QString( ":pixmaps/%0%1" ).arg
				      ( Colour::getColourCode( Colour::getTypeByID( i ) ) ).arg( count ) ) );
      }
      ui->layManaCost->addWidget( lbl );
    }
  }
  ui->layManaCost->setSpacing( 2 );

  ui->lblCardInfo->clear();
  
  QString html;
  if( !pCard->ability().empty() )
    html += QString::fromUtf8( pCard->ability().c_str() ).replace
      ( QRegExp( "\\{([ARZPTC0-9XDS])([0-9X]*)\\}" ), "<img src=\":pixmaps/\\1\\2\" />" );

  if( !pCard->info()->lore().empty() )
    html += QString( "<br/><br/>---<br/><i>" ) + QString::fromUtf8( pCard->info()->lore().c_str() ) +
      QString( "</i>" );
  
  ui->lblCardInfo->setHtml( html );
}

void DeckMaker::toggleExact( bool toggle )
{
  ui->leSecondaryType->setDisabled( toggle );
  ui->leAbility->setDisabled( toggle );
  ui->leLore->setDisabled( toggle );
  
  if( toggle )
    ui->lblName->setText( QString::fromUtf8( "Klj&učna reč" ) );
  else 
    ui->lblName->setText( QString::fromUtf8( "&Ime" ) );
}

void DeckMaker::resetInput()
{
  ui->leName->setText( QString( "" ) );
  ui->leSecondaryType->setText( QString( "" ) );
  ui->leAbility->setText( QString( "" ) );
  ui->leLore->setText( QString( "" ) );

  ui->cmbEdition->setCurrentIndex( 0 );

  ui->cbIzvor->setChecked( false );
  ui->cbArtefakt->setChecked( false );
  ui->cbArtefaktBice->setChecked( false );
  ui->cbBice->setChecked( false );
  ui->cbPromena->setChecked( false );
  ui->cbIznenadjenje->setChecked( false );
  ui->cbVradzbina->setChecked( false );

  ui->cbObicna->setChecked( false );
  ui->cbSrebrna->setChecked( false );
  ui->cbZlatna->setChecked( false );

  ui->cbExact->setChecked( false );

  ui->cbBezbojna->setChecked( false );
  ui->cbCrvena->setChecked( false );
  ui->cbZelena->setChecked( false );
  ui->cbPlava->setChecked( false );
  ui->cbZuta->setChecked( false );
  ui->cbCrna->setChecked( false );

  ui->rbOR->setChecked( true );
  ui->rbAND->setChecked( false );

  ui->lblCardImage->setPixmap( QPixmap( QString( ":/img/back" ) ) );
  ui->lblCardName->setText( QString( "" ) );
  ui->lblCardType->setText( QString( "" ) );
  ui->lblCardRarity->setText( QString( "" ) );
  ui->lblCardNumEdition->setText( QString( "" ) );
  ui->lblCardAttributes->setText( QString( "" ) );
  ui->lblCardColor->setText( QString( "" ) );
 
  // isprazni mana cost layout
  while( ui->layManaCost->count() ) {
    QLayoutItem *item = ui->layManaCost->takeAt( 0 );
    QWidget *widget = item->widget();
    if( widget ) widget->deleteLater();
  }

  ui->lblCardInfo->clear();

  ui->lstResults->clear();
  ui->btnAddDeck->setEnabled( false );
  ui->btnAddDeck->setText( "Dodaj jednu" );
  ui->sldCountDeck->setEnabled( false );
  ui->sldCountDeck->setValue( 1 );
  ui->btnAddSideDeck->setEnabled( false );

  statusBar()->clearMessage();
}

std::string DeckMaker::constructQuery() const
{
  // izem ti ove zavrzlame oko UTF8 encoding-a za std::string
  // moze da se resi tako sto ce svugde da se koristi QString
	std::string query( "SELECT card_id, card_name, card_reprint_id FROM cards_fts4 WHERE" );
  
  bool first = true;
  
  if( ui->cbExact->isChecked() ) {
    query += " cards_fts4 MATCH '" + std::string( ui->leName->text().toUtf8().data() ) + '\'';
    first = false;
  }
  else {
    // name
    if( !ui->leName->text().isEmpty() ) {
      if( !first ) query += " AND";
      query += " card_name LIKE '%" + std::string( ui->leName->text().toUtf8().data() ) + "%'";
      first = false;
    }
    
    // secondary type
    if( !ui->leSecondaryType->text().isEmpty() ) {
      if( !first ) query += " AND";
      query += " card_secondary_type LIKE '%" + std::string( ui->leSecondaryType->text().toUtf8().data() )
	+ "%'";
      first = false;
    }
    
    // ability
    if( !ui->leAbility->text().isEmpty() ) {
      if( !first ) query += " AND";
      query += " card_ability LIKE '%" + std::string( ui->leAbility->text().toUtf8().data() )
	+ "%'";
      first = false;
    }
    
    // lore
    if( !ui->leLore->text().isEmpty() ) {
      if( !first ) query += " AND";
      query += " card_lore LIKE '%" + std::string( ui->leLore->text().toUtf8().data() )
	+ "%'";
      first = false;
    }
  }
  
  // tipovi
  if( ui->cbIzvor->isChecked() || ui->cbArtefakt->isChecked() || ui->cbArtefaktBice->isChecked() ||
      ui->cbBice->isChecked() || ui->cbIznenadjenje->isChecked() || ui->cbVradzbina->isChecked() ||
      ui->cbPromena->isChecked() ) {
    if( !first ) query += " AND";
    query += " (";
    first = true;

    if( ui->cbIzvor->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 0";
      first = false;
    }
    if( ui->cbArtefakt->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 1";
      first = false;
    }
    if( ui->cbArtefaktBice->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 2";
      first = false;
    }
    if( ui->cbBice->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 3";
      first = false;
    }
    if( ui->cbIznenadjenje->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 4";
      first = false;
    }
    if( ui->cbVradzbina->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 5";
      first = false;
    }
    if( ui->cbPromena->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_primary_type = 6";
      first = false;
    }

    query += ')';
  }
  
  // retkosti
  if( ui->cbObicna->isChecked() || ui->cbSrebrna->isChecked() || ui->cbZlatna->isChecked() ) {
    if( !first ) query += " AND";
    query += " (";
    first = true;

    if( ui->cbObicna->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_rarity = 0";
      first = false;
    }
    if( ui->cbSrebrna->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_rarity = 1";
      first = false;
    }
    if( ui->cbZlatna->isChecked() ) {
      if( !first ) query += " OR";
      query += " card_rarity = 2";
      first = false;
    }

    query += ')';
  }

  // boje
  if( ui->cbBezbojna->isChecked() || ui->cbCrvena->isChecked() || ui->cbZelena->isChecked() ||
      ui->cbPlava->isChecked() || ui->cbZuta->isChecked() || ui->cbCrna->isChecked() ) {
    if( !first ) query += " AND";

    if( ui->rbAND->isChecked() ) { // AND
      Colour boja;
      if( ui->cbBezbojna->isChecked() ) boja += Colour::BEZBOJNA;
      if( ui->cbCrvena->isChecked() ) boja += Colour::CRVENA;
      if( ui->cbZelena->isChecked() ) boja += Colour::ZELENA;
      if( ui->cbPlava->isChecked() ) boja += Colour::PLAVA;
      if( ui->cbZuta->isChecked() ) boja += Colour::ZUTA;
      if( ui->cbCrna->isChecked() ) boja += Colour::CRNA;

      query += " card_colour = " + boost::lexical_cast<std::string>( static_cast<uint32>( boja.colour() ) );
      first = false;
    }
    else { // OR
      query += " (";
      first = true;
      
      if( ui->cbBezbojna->isChecked() ) 
	{      
	  if( !first ) query += " OR";
	  query += " card_colour = " + boost::lexical_cast<std::string>( Colour::BEZBOJNA );
	  first = false;
	}

      if( ui->cbCrvena->isChecked() ) {
	if( !first ) query += " OR";
	query += " card_colour = " + boost::lexical_cast<std::string>( Colour::CRVENA );
	first = false;
      }
      if( ui->cbZelena->isChecked() ) {
	if( !first ) query += " OR";
	query += " card_colour = " + boost::lexical_cast<std::string>( Colour::ZELENA );
	first = false;
      }
      if( ui->cbPlava->isChecked() ) {
	if( !first ) query += " OR";
	query += " card_colour = " + boost::lexical_cast<std::string>( Colour::PLAVA );
	first = false;
      }
      if( ui->cbZuta->isChecked() ) {
	if( !first ) query += " OR";
	query += " card_colour = " + boost::lexical_cast<std::string>( Colour::ZUTA );
	first = false;
      }
      if( ui->cbCrna->isChecked() ) {
	if( !first ) query += " OR";
	query += " card_colour = " + boost::lexical_cast<std::string>( Colour::CRNA );
	first = false;
      }

      query += ')';
    }
  }

  if( ui->cbExact->isChecked() )
	  query += " ORDER BY matchinfo(cards_fts4) DESC";
  else query += " COLLATE NOCASE";
  
  if( first ) // prazan upit, dovuci sve
	  query = "SELECT card_id, card_name, card_reprint_id FROM cards_fts4";

  return query;
}

void DeckMaker::doSearch()
{
  ui->lstResults->clear();
  DB::RecordSet rs = sGame.getDB()->query
    ( constructQuery() );
  
  if( rs.empty() ) {
    statusBar()->showMessage( tr( "Nema rezultata pretrage" ) );
    return;
  }
  
  // broj rezultata (posto edicije filtriramo rucno)
  uint32 count = 0;
  try {

    for( uint32 i = 0; i < rs.getSize(); i++ ) {
      // proveri glavnu ediciju
      
      bool reprint = true;

      uint32 edicija = 0, broj = 0, reprint_edicija = 0, reprint_broj = 0;
      CantorUnPair( boost::lexical_cast<uint32>( rs[i][DB::ID] ), &broj, &edicija );
      if( boost::lexical_cast<uint32>( rs[i][DB::REPRINT_ID] ) )
	CantorUnPair( boost::lexical_cast<uint32>( rs[i][DB::REPRINT_ID] ), &reprint_broj,
		      &reprint_edicija );
      else reprint = false;
      
      const std::string& str = Card::Info::getEditionStr( Card::Info::getEditionByID( edicija - 1 ) );
	  const std::string& str_reprint = (reprint) ? Card::Info::getEditionStr( Card::Info::getEditionByID ( reprint_edicija - 1 ) ) : "";
      
      if( ui->cmbEdition->currentIndex() != 0 ) {
	if( str != std::string( ui->cmbEdition->currentText().toUtf8().data() ) ) {
	  // proveri reprint
	  if( !reprint ) continue;
	  if( str_reprint != std::string( ui->cmbEdition->currentText().toUtf8().data() ) ) continue;
	}
      }
      ui->lstResults->addItem( boost::lexical_cast<uint32>( rs[i][DB::ID] ) );
      count++;
    }
  }
  catch ( ExcMsg& ) {}

  statusBar()->showMessage( tr( "%0 rezultata pretrage" ).arg( count ) );
}

DeckMaker::~DeckMaker()
{
  delete ui;
  //  Singleton<GameManager>::destroy();
}

void DeckMaker::updateAddBtn( int i )
{
  QString tekst = "Dodaj ";
  switch( i ) {
  case 1:
    tekst += "jednu";
    break;
  case 2:
    tekst += "dve";
    break;
  case 3:
    tekst += "tri";
    break;
  case 4:
    tekst += QString::fromUtf8( "četiri" );
    break;
  }
  ui->btnAddDeck->setText( tekst );
}
