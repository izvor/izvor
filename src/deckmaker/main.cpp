//#include "IzvorCommonPCH.h"
#include "deckmaker.h"

#include <QtGui/QApplication>

using namespace std;

int main( int argc, char **argv )
{
  sGame.getDB()->open( RES_PATH"/db/izvor_cards.db" );

  QApplication a(argc, argv);
  DeckMaker w;
  w.show();

  //return  a.exec();
  a.exec();
}
