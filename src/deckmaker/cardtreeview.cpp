#include "cardtreeview.h"

#include <QKeyEvent>

const uint32 CardTreeView::EDITION = 0x2000;
const uint32 CardTreeView::NUM = 0x4000;
const uint32 CardTreeView::COUNT = 0x8000;

void CardTreeView::keyPressEvent( QKeyEvent *e )
{
  if( e->key() == Qt::Key_Up )
    selectionUp();
  
  if( e->key() == Qt::Key_Down )
    selectionDown();

  emit clicked( currentIndex() );
}

const Card* CardTreeView::getSelection()
{
  // uzmi selektovan red
  QList<QModelIndex> lst = selectionModel()->selectedIndexes();
  if( lst.empty() ) return NULL;

  // ako je selektovan reprint, selektuj original ediciju (roditelj)
  if( mModel->itemFromIndex( lst.value(0) )->parent() ) {
    selectionModel()->select( lst.value(0).parent(),
			      QItemSelectionModel::ClearAndSelect |
			      QItemSelectionModel::Rows );
    lst = selectionModel()->selectedIndexes();
  }
  
  return mDeck->getCard( mModel->itemFromIndex( lst.value( 0 ) )->text().toUInt() );
}

QList<QStandardItem*> CardTreeView::createRow( const Card* pCard,
					       bool reprint ) const
{
  QList<QStandardItem*> ql;

  //  if( mColumnMask & DB::ID ) {
  // prva kolona uvek id
  if( reprint )
    ql.append( new QStandardItem( QString( "--" ) ) );
  else
    ql.append( new QStandardItem( QString( "%0" ).arg( pCard->id() ) ) );
  if( mColumnMask & COUNT ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "--" ) ) );
    else 
      ql.append( new QStandardItem( QString( "%0" ).arg( mDeck->cardCount( pCard->id() ) ) ) );
  }
  if( mColumnMask & DB::NAME ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "--" ) ) );
    else
      ql.append( new QStandardItem( QString::fromUtf8( pCard->info()->name().c_str() ) ) );
  }
  if( mColumnMask & DB::COLOUR ) {
    if( reprint )
		ql.append( new QStandardItem( QString( "--" ) ) );
    else 
		ql.append( new QStandardItem( QString::fromUtf8( pCard->colour()->getDescriptionStr().c_str() ) ) );
  }
  if( mColumnMask & DB::PRIMARY_TYPE ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "--" ) ) );
    else
      ql.append( new QStandardItem( QString::fromUtf8( Card::getTypeStr( pCard->type() ).c_str() ) ) );
  }
  if( mColumnMask & DB::SECONDARY_TYPE ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "--" ) ) );
    else 
      ql.append( new QStandardItem( QString::fromUtf8( pCard->secondaryType().c_str() ) ) );
  }
  if( mColumnMask & DB::COST ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "--" ) ) );
    else
      ql.append( new QStandardItem( QString::fromUtf8( "" ) ) ); //sGame.getCardMgr()->get)[DB::COST].c_str() ) ) );
  }

  if( mColumnMask & DB::RARITY ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "--" ) ) );
    else
      ql.append( new QStandardItem( QString::fromUtf8( Card::Info::getRarityStr( pCard->info()->rarity() ).c_str() ) ) );
  }
  if( mColumnMask & EDITION ) {
    if( reprint )
      ql.append( new QStandardItem( QString::fromUtf8( Card::Info::getEditionStr( pCard->info()->reprintEdition() ).c_str() ) ) );
    else 
      ql.append( new QStandardItem( QString::fromUtf8( Card::Info::getEditionStr( pCard->info()->edition() ).c_str() ) ) );
  }
  if( mColumnMask & NUM ) {
    if( reprint )
      ql.append( new QStandardItem( QString( "%0" ).arg( pCard->info()->reprintNum() ) ) );
    else 
      ql.append( new QStandardItem( QString( "%0" ).arg( pCard->info()->num() ) ) );
  }
  // ignorisemo reprint_id, image_author, lore, ability, att/def,
  // mada se i oni mogu dodati
  return ql;
}

void CardTreeView::updateHeaders()
{  
  uint32 column_count = 0;
  // ISTI redosled kao u createRow
  // prva kolona uvek id
  mModel->setHeaderData( column_count, Qt::Horizontal, QString( "ID" ) );
  mColumnIndex[DB::ID] = column_count++;

  if( mColumnMask & COUNT ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "#" ) );
    mColumnIndex[COUNT] = column_count++;
  }
  if( mColumnMask & DB::NAME ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Ime" ) );
    mColumnIndex[DB::NAME] = column_count++;
  }
  if( mColumnMask & DB::COLOUR ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Boja" ) );
    mColumnIndex[DB::COLOUR] = column_count++;
  }
  if( mColumnMask & DB::PRIMARY_TYPE ) {
      mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Tip" ) );
      mColumnIndex[DB::PRIMARY_TYPE] = column_count++;
  }
  if( mColumnMask & DB::SECONDARY_TYPE ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Podtip" ) );
    mColumnIndex[DB::SECONDARY_TYPE] = column_count++;
  }
  if( mColumnMask & DB::COST ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Cena" ) );
    mColumnIndex[DB::COST] = column_count++;
  }
  if( mColumnMask & DB::RARITY ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Retkost" ) );
    mColumnIndex[DB::RARITY] = column_count++;
  }
  if( mColumnMask & EDITION ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Edicija" ) );
    mColumnIndex[EDITION] = column_count++;
  }
  if( mColumnMask & NUM ) {
    mModel->setHeaderData( column_count, Qt::Horizontal, QString( "Broj" ) );
    mColumnIndex[NUM] = column_count++;
  }

  setColumnHidden( 0, mIDHidden );

  // ignorisemo reprint_id, image_author, lore, ability, att/def,
  // mada se i oni mogu dodati
}

uint32 CardTreeView::getColumnCount( uint32 col_mask )
{
  uint32 column_count = 1; // za id
  if( col_mask & COUNT ) column_count++;
  if( col_mask & DB::NAME ) column_count++;
  if( col_mask & DB::COLOUR ) column_count++;
  if( col_mask & DB::PRIMARY_TYPE ) column_count++;
  if( col_mask & DB::SECONDARY_TYPE ) column_count++;
  if( col_mask & DB::COST ) column_count++;
  if( col_mask & DB::RARITY ) column_count++;
  if( col_mask & EDITION ) column_count++;
  if( col_mask & NUM ) column_count++;
  return column_count;
}
