#include "PlayGameState.h"
#include "gui/IzvorGui.h"
#include "gui/Core.h"
#include "gui/Table.h"

#include "ReqHandler.h"

#include <cstdlib>

PlayGameState::PlayGameState() : 
    Mge::GameState("PlayGameState"), mSceneManager(0), mCamera(0), mViewport(0)
        
{
}

PlayGameState::~PlayGameState()
{
}


void PlayGameState::Initialise()
{
    Mge::EngineManager* engine_manager = Mge::EngineManager::getSingletonPtr();

    mSceneManager = engine_manager->GetOgreRoot()->createSceneManager(Ogre::ST_GENERIC, Ogre::String("GameSceneManager"));
    mGuiCore = Gui::Core::getSingletonPtr();
	mGuiCore->initialise(mSceneManager);

    mCamera = mGuiCore->getCamera()->getOgreCamera();

    /// Da li da kamera bude u SceneRoot?
    //// fast hack
    if(!mViewport)
        mViewport = engine_manager->GetRenderWindow()->addViewport(mCamera);

    mCamera->setAspectRatio(
        Ogre::Real(mViewport->getActualWidth()) / Ogre::Real(mViewport->getActualHeight()));

    mViewport->setBackgroundColour(Ogre::ColourValue::Black);

    mMyGuiOgrePlatform = new MyGUI::OgrePlatform();
    mMyGuiOgrePlatform->initialise(engine_manager->GetRenderWindow() , mSceneManager);
    mMyGui = new MyGUI::Gui();
    mMyGui->initialise();

    const OIS::MouseState &mouseState = engine_manager->GetMouse()->getMouseState(); // mMouse is type of OIS::Mouse*
    mouseState.width = mViewport->getActualWidth(); // your rendering area width
    mouseState.height = mViewport->getActualHeight(); // your rendering area height

    Resume();
}

void PlayGameState::Shutdown()
{
    Mge::EngineManager::getSingletonPtr()->GetOgreRoot()->removeFrameListener(this);
    delete mGuiCore;
	mMyGui->shutdown();
    delete mMyGui;
	mMyGuiOgrePlatform->shutdown();
	delete mMyGuiOgrePlatform;
    Ogre::Root::getSingletonPtr()->destroySceneManager(mSceneManager);
}

void PlayGameState::Pause()
{
    Mge::EngineManager::getSingletonPtr()->GetOgreRoot()->removeFrameListener(this);
    Mge::EngineManager::getSingletonPtr()->GetMouse()->setEventCallback(this);
    Mge::EngineManager::getSingletonPtr()->GetKeyboard()->setEventCallback(this);
}

void PlayGameState::Resume()
{
    mSceneManager->clearScene();

    Mge::EngineManager::getSingletonPtr()->GetOgreRoot()->addFrameListener(this);
    Mge::EngineManager::getSingletonPtr()->GetMouse()->setEventCallback(this);
    Mge::EngineManager::getSingletonPtr()->GetKeyboard()->setEventCallback(this);

    mViewport->setCamera(mCamera);

    CreateScene();
}

bool PlayGameState::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    Mge::EngineManager::getSingletonPtr()->GetKeyboard()->capture();
    Mge::EngineManager::getSingletonPtr()->GetMouse()->capture();

    if (Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_ESCAPE)) 
    {
        Mge::GameStateManager::getSingletonPtr()->PopState();
        return false;
    } 

	sReqHndl.process();

    mGuiCore->update(evt);
    return true;	
}

bool PlayGameState::frameEnded(const Ogre::FrameEvent& evt)
{
//	mGuiCore->update(evt);
	return true;
}

void PlayGameState::CreateScene()
{

    mGuiCore->createScene();

}


bool PlayGameState::keyPressed( const OIS::KeyEvent &arg )
{
    MyGUI::InputManager::getInstancePtr()->injectKeyPress(MyGUI::KeyCode::Enum(arg.key), arg.text);

    mGuiCore->injectKeyPressed(arg);
        
    static bool toggle = true;

    switch(arg.key)
    {

    case OIS::KC_F:

        if(toggle) 
            mCamera->setPolygonMode(Ogre::PM_WIREFRAME);
        else 
            mCamera->setPolygonMode(Ogre::PM_SOLID);
        toggle = !toggle;
        break;

	default: break;
    }

    return true;
}

bool PlayGameState::keyReleased( const OIS::KeyEvent &arg )
{
    MyGUI::InputManager::getInstancePtr()->injectKeyRelease(MyGUI::KeyCode::Enum(arg.key));
    mGuiCore->injectKeyReleased(arg);
    return true;
}

bool PlayGameState::mouseMoved( const OIS::MouseEvent &arg )
{
    MyGUI::InputManager::getInstancePtr()->injectMouseMove(arg.state.X.abs, arg.state.Y.abs, arg.state.Z.abs);
    mGuiCore->injectMouseMoved(arg);
    return true;
}

bool PlayGameState::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    MyGUI::InputManager::getInstancePtr()->injectMousePress(arg.state.X.abs, arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
    mGuiCore->injectMousePressed(arg, id);
    return true;
}

bool PlayGameState::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    MyGUI::InputManager::getInstancePtr()->injectMouseRelease(arg.state.X.abs, arg.state.Y.abs, MyGUI::MouseButton::Enum(id));
    mGuiCore->injectMouseReleased(arg, id);
    return true;
}
