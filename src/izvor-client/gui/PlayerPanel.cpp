/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#include "gui/PlayerPanel.h"

#include <sstream>

namespace Gui
{
		PlayerPanel::PlayerPanel()
		{
			// rucni metod
			mMyGui = MyGUI::Gui::getInstancePtr();
			//MyGUI::ButtonPtr button = mMyGui->createWidget<MyGUI::Button>("Button", 10, 10, 300, 26, MyGUI::Align::Default, "Main");
			//button->setCaption("exit");

			// layout metod. mozemo da ucitamo layout, i da spojimo odredjene vidzete 1-1 sa objektima ovde
			MyGUI::LayoutManager::getInstance().loadLayout("test.layout");
		}

		PlayerPanel::~PlayerPanel()
		{

		}

		void PlayerPanel::setHealth(int value)
		{
			try
			{
				MyGUI::StaticTextPtr health = mMyGui->findWidget<MyGUI::StaticText>("Health");
				std::stringstream s;
				s << "Health: " << value;
				health->setCaption(s.str());
			} catch(const std::exception& e)
			{
				std::cerr << e.what() << std::endl;
			}

		}

		void PlayerPanel::setMana(IzvorCommon::Core::Color::Type color, int value)
		{
			try
			{
				std::string name;
				switch(color)
				{
				case IzvorCommon::Core::Color::CRVENA:
					name = "Red";
					break;
				case IzvorCommon::Core::Color::ZELENA:
					name = "Green";
					break;
				case IzvorCommon::Core::Color::PLAVA:
					name = "Blue";
					break;
				case IzvorCommon::Core::Color::ZUTA:
					name = "Yellow";
					break;
				case IzvorCommon::Core::Color::CRNA:
					name = "Black";
					break;
				case IzvorCommon::Core::Color::BEZBOJNA:
					name = "White";
					break;
				default:
					throw std::exception();
				}
				MyGUI::StaticTextPtr mana_field = mMyGui->findWidget<MyGUI::StaticText>(name);
				std::stringstream s;
				s << value;
				mana_field->setCaption(s.str());
			} catch(const std::exception& e)
			{
				std::cerr << e.what() << std::endl;
			}
		}

		void PlayerPanel::setPhase(const std::string& phase_string)
		{
			//try
			//{
			//	MyGUI::PanelPtr phase_field = mMyGui->findWidget<MyGUI::Panel>("PhasePanel");
			//	std::stringstream s;
			//	s << value;
			//	mana_field->setCaption(s.str());
			//} catch(const std::exception& e)
			//{
			//	std::cerr << e.what() << std::endl;
			//}
		}
}