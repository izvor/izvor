/*
---------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder
*/

#include "gui/CardComposite.h"

namespace Gui
{
//    CardComposite::CardComposite()
//    {
//
//    }
//
//    CardComposite::~CardComposite()
//    {
//
//    }
//
//    void CardComposite::insertChild(boost::weak_ptr<Card> card)
//    {
//        if( CardPtr child = mChild.lock() )
//        {
//            CardPtr oldchild = mChild.lock();
//            CardPtr newchild = card.lock();
//
//            if( oldchild && newchild )
//            {
//                oldchild->_setParent( newchild );
//                mChild = card;
//                newchild->_setParent( shared_from_this() );
//            }
//
//        } else // mChild == nullptr
//        {
//            if ( CardPtr newchild = card.lock() )
//            {
//                mChild = card;
//                newchild->_setParent( shared_from_this() );
//            }
//        }
//
//    }
//
//    void CardComposite::pushBackChild(boost::weak_ptr<Card> card)
//    {
//        CardPtr newchild = card.lock();
//        //CardPtr oldchild = card.
//    }
//
//    void CardComposite::_setParent(boost::weak_ptr<Card> parent)
//    {
//        // detach form old parent
//        // attach to new parent
//    }
//
//    void CardComposite::detachThis()
//    {
//
//    }
//
//    CardPtr CardComposite::getChild()
//    {
//
//    }
}
