/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#include "gui/Camera.h"
#include "gui/Utility.h"
#include "gui/Core.h"
#include "gui/SceneRoot.h"
#include "gui/Table.h"

#include <Ogre.h>
#include <EngineManager.hpp>

Gui::Camera::Camera( Ogre::SceneManager* scene_manager ) : mOrbiting(false), mZooming(false)
{
    mCamera = scene_manager->createCamera( Utility::getUniqueString("GameCamera") );
    mCamera->setNearClipDistance(5);

}

Gui::Camera::~Camera()
{

}

bool Gui::Camera::update(const Ogre::FrameEvent &arg)
{
    return true;
}

bool Gui::Camera::mouseMoved( const OIS::MouseEvent &arg )
{
#ifdef OGRE_DEBUG_MODE == 1
	Ogre::Real dist = (mCameraNode->_getDerivedPosition() - mTarget->_getDerivedPosition()).length();

	if (mOrbiting)   // yaw around the target, and pitch locally
	{
		mCameraNode->setPosition(mTarget->_getDerivedPosition());

		mCameraNode->yaw(Ogre::Degree(-arg.state.X.rel * 0.25f));
		mCameraNode->pitch(Ogre::Degree(-arg.state.Y.rel * 0.25f));

        mCameraNode->translate(Ogre::Vector3(0, 0, dist), Ogre::SceneNode::TS_LOCAL);

		// don't let the camera go over the top or around the bottom of the target
	}
	else if (mZooming)  // move the camera toward or away from the target
	{
		// the further the camera is, the faster it moves
        mCameraNode->translate(Ogre::Vector3(0, 0, arg.state.Y.rel * 0.004f * dist), Ogre::SceneNode::TS_LOCAL);
	}
	else if (arg.state.Z.rel != 0)  // move the camera toward or away from the target
	{
		// the further the camera is, the faster it moves
        mCameraNode->translate(Ogre::Vector3(0, 0, -arg.state.Z.rel * 0.0008f * dist), Ogre::SceneNode::TS_LOCAL);
	}
#endif
    return true;
}

bool Gui::Camera::mousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{

	//if (id == OIS::MB_Right) mZooming = true;
    return true;
}

bool Gui::Camera::mouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
	//if (id == OIS::MB_Right) mZooming = false;
    return true;
}

bool Gui::Camera::keyPressed( const OIS::KeyEvent &arg )
{
    switch(arg.key)
    {
    case OIS::KC_SYSRQ:
        Mge::EngineManager::getSingletonPtr()->GetRenderWindow()->writeContentsToTimestampedFile("screenshot",".jpg");
        break;
	
#ifdef OGRE_DEBUG_MODE == 1
	case OIS::KC_LCONTROL:
		mOrbiting = true;
#endif
	default: break;
    }
    return true;
}

bool Gui::Camera::keyReleased( const OIS::KeyEvent &arg )
{
	switch(arg.key)
	{
#ifdef OGRE_DEBUG_MODE == 1
	case OIS::KC_LCONTROL:
		mOrbiting = false;
#endif
	default: break;
    }
    return true;
}

void Gui::Camera::sceneInit()
{
    Ogre::SceneManager* scene_mgr = Core::getSingletonPtr()->getSceneManager();

    mCameraNode = scene_mgr->getRootSceneNode()->createChildSceneNode();
    mCameraNode->attachObject(mCamera);

    mTarget = Core::getSingletonPtr()->getSceneRoot()->getTable()->getSceneNode();
    mCameraNode->setPosition(mTarget->getPosition());

    Ogre::Real dist = 300.0f; 
    mCameraNode->pitch(Ogre::Degree(-45));
    mCameraNode->translate(Ogre::Vector3(0.0f, 0.0f, dist), Ogre::SceneNode::TS_LOCAL);
    mCameraNode->setAutoTracking(true, mTarget); 
    mCameraNode->setFixedYawAxis(true);

}

