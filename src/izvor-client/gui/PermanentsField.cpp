/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder, mikronac
*/

#include "gui/PermanentsField.h"
#include "gui/Core.h"
#include "gui/CardManager.h"
#include "gui/Card.h"
#include "gui/CardContainer.h"
#include "gui/SceneRoot.h"
#include "gui/Table.h"
#include "gui/Animation.h"

#include <Ogre.h>


const Ogre::Real Gui::PermanentsField::DISTANCE = 25.0f;


//------------------------------------------------------------------------------
void Gui::PermanentsField::acceptCard( CardId card_id )
{
    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
    card->getOwner()->dispatchCard(card_id);
    card->setOwner(this);

    card->setRenderQueueGroupAndPriority(Gui::RQG_TABLE, Gui::RP_DEFAULT);

    mCardsList.push_back(card.get());

    mNode->addChild(card->getSceneNode());

    reorganisePositions();    
}
//------------------------------------------------------------------------------
void Gui::PermanentsField::dispatchCard( CardId card_id )
{
    CardsList::iterator card_itr = mCardsList.end();
    for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
    {
        if ((*itr)->getId() == card_id)
        {
            card_itr = itr;
        }
    }
    if (card_itr != mCardsList.end()) mCardsList.erase(card_itr);

    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
    card->saveState();
    mNode->removeChild(card->getSceneNode());

    reorganisePositions();  
}
//------------------------------------------------------------------------------
Gui::PermanentsField::PermanentsField(Ogre::Vector3 position, Ogre::Quaternion orientation)
{
    Ogre::SceneManager* scene_mgr = Core::getSingletonPtr()->getSceneRoot()->getSceneManager();
    mNode = scene_mgr->createSceneNode();
    Gui::Core::getSingletonPtr()->getSceneRoot()->getTable()->getSceneNode()->addChild(mNode);

    mNode->setOrientation(orientation);

    mNode->setPosition(position);

}
//------------------------------------------------------------------------------
Gui::PermanentsField::~PermanentsField()
{

}
//------------------------------------------------------------------------------
void Gui::PermanentsField::reorganisePositions()
{
    Ogre::Vector3 startPosition = calculateStartPosition( mCardsList.size() );

    Ogre::Real position = 0.0f;
    const Ogre::Real tap_distance = 5.0f;

    CardsList::iterator next;

    for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
    {
        next = itr;

        (*itr)->getSceneNode()->setPosition( startPosition + Ogre::Vector3(position, 0.0f, 0.0f) );

        if (++next != mCardsList.end())        
        {
            if((*itr)->isTap() && (*next)->isTap())
                position += DISTANCE + tap_distance;
            else if ( ((*itr)->isTap() && !(*next)->isTap())  || (!(*itr)->isTap() && (*next)->isTap()) )
                position += DISTANCE + tap_distance;
            else
                position += DISTANCE;
        }

        (*itr)->getSceneNode()->setOrientation( Ogre::Quaternion::IDENTITY );
        (*itr)->getSceneNode()->setScale( Gui::CardManager::CARD_SIZE );
        (*itr)->getSceneNode()->setInitialState();
        
        new DragAnimation( *itr );

        (*itr)->saveState();
    }
}

Ogre::Vector3 Gui::PermanentsField::calculateStartPosition( int num )
{
    Ogre::Real wide = 0.0f;
    const Ogre::Real tap_distance = 5.0f;

    CardsList::iterator next;

    for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
    {
        next = itr;

        if (++next != mCardsList.end())        
        {
            if((*itr)->isTap() && (*next)->isTap())
                wide += DISTANCE + 2*tap_distance;
            else if ( ((*itr)->isTap() && !(*next)->isTap())  || (!(*itr)->isTap() && (*next)->isTap()) )
                wide += DISTANCE + tap_distance;
            else
                wide += DISTANCE;
        }
    }

    return Ogre::Vector3(-wide*0.5f, 0.0f, 0.0f);
}
//------------------------------------------------------------------------------