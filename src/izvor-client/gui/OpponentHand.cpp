/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder, mikronac
*/

#include "gui/OpponentHand.h"
#include "gui/Table.h"
#include "gui/Card.h"
#include "gui/SceneRoot.h"
#include "gui/CardManager.h"
#include "gui/Animation.h"

#include <Ogre.h>

Gui::OpponentHand::OpponentHand(const Ogre::Vector3& position, const Ogre::Quaternion& orientation)
{
    Ogre::SceneNode* table_node = Core::getSingletonPtr()->getSceneRoot()->getTable()->getSceneNode();

    mNode = table_node->createChildSceneNode();
    mNode->setPosition(position);
    mNode->setOrientation(orientation);
}

Gui::OpponentHand::~OpponentHand()
{
}

void Gui::OpponentHand::acceptCard(CardId card_id)
{
    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);

    if (card->getOwner() != nullptr) card->getOwner()->dispatchCard(card->getId());
    card->setOwner(this);

    Ogre::SceneNode* card_node = card->getSceneNode();
    mNode->addChild(card_node);

    card_node->setPosition(Ogre::Vector3::ZERO);
    card_node->setOrientation(Ogre::Quaternion::IDENTITY);
    card_node->setScale(CardManager::CARD_SIZE);
    card_node->setInitialState();

    card->setRenderQueueGroupAndPriority( Gui::RQG_TABLE, Gui::RP_DEFAULT);

	SceneNodeState animation = card->calculateAnimationPositionAndOrientation();

    new DragAnimation(card.get(), animation.position, animation.orientation);

    card->saveState();

}

void Gui::OpponentHand::dispatchCard(CardId card_id)
{
    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
    mNode->removeChild(card->getSceneNode());
}