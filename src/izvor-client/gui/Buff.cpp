///*
//--------------------------------------------------------------------------------
//      _                 _                   _   _
//     (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___
//     | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
//     |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
//                                      |___/  |__/
//    -----------------------------------------------------------------------
//    LICENSE:
//    -----------------------------------------------------------------------
//    This file is part of Izvor: Izvori Magije (C) TCG simulator
//    Copyright 2010 EGDC++ Team
//    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
//    -----------------------------------------------------------------------
//    Izvor is free software: you can redistribute it and/or modify it under
//    the terms of the GNU Lesser General Public License as published by the
//    Free Software Foundation, either version 3 of the License, or (at your
//    option) any later version.
//
//    Izvor is distributed in the hope that it will be useful, but WITHOUT
//    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
//    License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
//    -----------------------------------------------------------------------
//    Author: thunder
//*/
//

#include "gui/Buff.h"

#include "gui/Card.h"
#include "gui/Core.h"
#include "gui/CardManager.h"
#include "gui/SceneRoot.h"

namespace Gui
{
	Buff::Buff(boost::weak_ptr<Card> subject)
		: CardProperty(subject), mSceneNode(nullptr), mCard(subject)
	{

#if OGRE_DEBUG_MODE == 1
		//if( CardPtr card = mCard.lock() )
			//std::cout << "Added buff " << getName() << ", on card " << card->getId() << std::endl;
#endif
	}

	Buff::~Buff() 
	{
		if( CardPtr card = mCard.lock() )
		{
			card->removeBuff(this);
#if OGRE_DEBUG_MODE == 1
			//std::cout << "Removed buff " << getName() << ", from card " << card->getId() << std::endl;
#endif
		}
	}

	void Buff::registerProperty(boost::shared_ptr< Buff > buff_ptr)
	{
		// UpdatableCardProperty::registerProperty(buff_ptr);

		if ( CardPtr card = buff_ptr->getParent().lock() )
			card->addBuff(buff_ptr.get());

	}

}