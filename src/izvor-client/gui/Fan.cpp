///*
//--------------------------------------------------------------------------------
//	  _                 _                   _   _     
//	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
//	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
//	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
//									  |___/  |__/     
//    -----------------------------------------------------------------------
//    LICENSE:
//    -----------------------------------------------------------------------
//    This file is part of Izvor: Izvori Magije (C) TCG simulator
//    Copyright 2010 EGDC++ Team
//    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
//    -----------------------------------------------------------------------
//    Izvor is free software: you can redistribute it and/or modify it under
//    the terms of the GNU Lesser General Public License as published by the 
//    Free Software Foundation, either version 3 of the License, or (at your 
//    option) any later version.
//
//    Izvor is distributed in the hope that it will be useful, but WITHOUT
//    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
//    License for more details.
//
//    You should have received a copy of the GNU Lesser General Public
//    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
//    -----------------------------------------------------------------------
//    Author: mikronac, Thunder
//*/
//
#include "gui/Fan.h"
#include "gui/Core.h"
#include "gui/Card.h"
#include "gui/Camera.h"
#include "gui/CardManager.h"
#include "gui/Camera.h"
#include "gui/UpdatableManager.h"
#include "gui/SceneRoot.h"
#include "gui/Animation.h"
#include "gui/Common.h"

#include <list>

#include <Ogre.h>

namespace Gui
{


    const Ogre::Real LinealFan::DISTANCE = 10.0f;

    //------------------------------------------------------------------------------
    CardsList::iterator Fan::find( CardId id )
    {
        CardsList::iterator it = mCardsList.begin();
        for( it; it != mCardsList.end(); it++ )
            if( (*it)->getId() == id )
                return it;

        return mCardsList.end();
    }
    //------------------------------------------------------------------------------
    LinealFan::LinealFan(Ogre::SceneNode* scene_node )
    {
        mNode = scene_node;
        mLastAnimation = nullptr;
        // 
        // 		Ogre::SceneManager* sm = Core::getSingletonPtr()->getSceneRoot()->getSceneManager();
        //         Camera* camera = Core::getSingletonPtr()->getCamera();
        // 		CardManager* cm = Core::getSingletonPtr()->getCardManager();
        // 
        //         //mNode = sm->createSceneNode();
        // 		//mNode = camera->getFanNode();
        // 		//mNode = camera->getSceneNode()->createChildSceneNode();
        //         //camera->getSceneNode()->addChild(mNode);
        // 		//mNode = sm->getRootSceneNode()->createChildSceneNode();
        //         //mNode->setPosition(Ogre::Vector3(0.0f, 0.0f, -30.0f));
        //         //mNode->setOrientation(Ogre::Quaternion::IDENTITY);
        //         //mNode->rotate(Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_X));
        //         //mNode->yaw(Ogre::Degree(90));
        // 		//mNode->setPosition( Ogre::Vector3(0.0f, -250.0f, 250.0f) );
        // 		//mNode->setOrientation(Ogre::Quaternion(Ogre::Degree(135), Ogre::Vector3::UNIT_X));
        // 
        // 		for (CardsIdList::iterator itr = cardIdList.begin(); itr != cardIdList.end(); itr++)
        // 		{
        // 			mCardsList.push_back( cm->getCard( *itr ).get() );
        // 		}
        // 
        // 		Ogre::Vector3 startPosition = calculateStartPosition (mCardsList.size());
        // 
        // 		int i = 0;
        // 		for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
        // 		{
        // 			(*itr)->getSceneNode()->setPosition( startPosition + Ogre::Vector3(i*DISTANCE, 0.0f, 0.0f) );
        // 			(*itr)->getSceneNode()->setInitialState();
        // 			//(*itr)->getEntity()->setRenderQueueGroup( Gui::RQG_HAND + i);
        // 			mNode->addChild( (*itr)->getSceneNode() );
        //             (*itr)->setOwner(this);
        // 			i++;
        // 		}
        // 
        //         reorganiseRenderQueueGroup(mCardsList.end());
        // 
        // 		for (CardsIdList::iterator itr = cardIdList.begin(); itr != cardIdList.end(); itr++)
        // 		{
        // 			mHoverAnimationList[*itr] =  new LinealFanHoverAnimation(mCardsList, *itr);
        // 		}
    }
    //------------------------------------------------------------------------------
    Ogre::Vector3 LinealFan::calculateStartPosition( int num )
    {
        Ogre::Real delta_x;
        // Pozicija prve karte po x, u slucaju parnog broja karata
        if ( (num % 2) == 0 ) delta_x = ((num*0.5f)-1.0f)*DISTANCE + DISTANCE*0.5f ;
        // U slucaju neparnog
        else delta_x = (num - 1)*0.5f*DISTANCE;

        return Ogre::Vector3( -delta_x, 0.0f, 0.0f);
    }
    //------------------------------------------------------------------------------
    void LinealFan::mouseOver( CardId id )
    {   
        if(mLastAnimation) mLastAnimation->setReverse(true);

        if (hasCard(id))
        {
            mHoverAnimationList[id]->setReverse(false);
            mLastAnimation = mHoverAnimationList[id];

            reorganiseRenderQueueGroup(find(id));
        }

    }
    //------------------------------------------------------------------------------
    LinealFan::~LinealFan()
    {

    }
    //------------------------------------------------------------------------------
    void LinealFan::acceptCard( CardId card_id )
    {
        mLastAnimation = nullptr;
        CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
        if(card->getOwner() != nullptr) card->getOwner()->dispatchCard(card_id);
        card->setOwner(this);

        CardsList::iterator itr;

        int n;
        if (mCardsList.size() % 2 == 0 ) n = mCardsList.size() / 2;
        else n = (mCardsList.size() + 1 ) / 2;
        itr = mCardsList.begin();
        for (int i = 0; i < n; i++ ) itr++;

        mCardsList.insert( itr, card.get() );


        //         Ogre::SceneNode* card_node = card->getSceneNode();
        // 
        //         Ogre::Quaternion card_world_orientation = card_node->_getDerivedOrientation();
        //         Ogre::Quaternion animation_orientation = mNode->convertWorldToLocalOrientation(card_world_orientation);
        // 
        //         Ogre::Vector3 card_world_position = card_node->_getDerivedPosition();
        //         Ogre::Vector3 animation_position = mNode->convertWorldToLocalPosition(card_world_position);
        // 
        //         new DrawCardAnimation( card.get(), animation_position, animation_orientation);


        mNode->addChild(card->getSceneNode());


        reorganisePositions();
        reorganiseRenderQueueGroup( mCardsList.end() );
		
        Ogre::SceneNode* card_node = card->getSceneNode();

        //Ogre::Quaternion card_world_orientation = card_node->_getDerivedOrientation();
        //      Ogre::Quaternion animation_orientation = card_node->convertWorldToLocalOrientation(card->getSavedState().orientation);

        //Ogre::Vector3 card_world_position = card_node->_getDerivedPosition();
        //        Ogre::Vector3 animation_position = card_node->convertWorldToLocalPosition(card->getSavedState().position);

        SceneNodeState animation = card->calculateAnimationPositionAndOrientation();

        new DrawCardAnimation( card.get(), animation.position, animation.orientation);

        for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
        {
            (*itr)->saveState();
        }

        CardsList::iterator inserted_card = find(card_id);

        itr = mCardsList.begin();
        short sgn = 1;

        while ( itr != mCardsList.end() )
        {            
            if ( itr != inserted_card )
            {
                new DragAnimation( *itr++, Ogre::Vector3( sgn*DISTANCE*0.5f, 0.0f, 0.0f ));
            }
            else
            {
                sgn = -1; itr++;
            }
        }

        for (AnimationsList::iterator itr = mHoverAnimationList.begin(); itr != mHoverAnimationList.end(); itr++)
        {
            itr->second->discard();
        }
        mHoverAnimationList.erase(mHoverAnimationList.begin(), mHoverAnimationList.end());

        createHoverAnimations();

    }
    //------------------------------------------------------------------------------
    void LinealFan::dispatchCard( CardId card_id )
    {
        CardsList::iterator card = find(card_id);
        (*card)->saveState();
        mNode->removeChild( (*card)->getSceneNode() );

        CardsList::iterator next_card = card;
        next_card++;
        mCardsList.erase(card);

        reorganisePositions();
        reorganiseRenderQueueGroup(mCardsList.end());



        CardsList::iterator itr = mCardsList.begin();
        short sgn = -1;
        while ( itr != mCardsList.end() )
        {
            if (itr != next_card)
            {
                new DragAnimation( *itr++, Ogre::Vector3( sgn*DISTANCE*0.5f, 0.0f, 0.0f ));
            }
            else
            {
                sgn = 1;
                new DragAnimation( *itr++, Ogre::Vector3( sgn*DISTANCE*0.5f, 0.0f, 0.0f ));
            }
        }



        for (AnimationsList::iterator itr = mHoverAnimationList.begin(); itr != mHoverAnimationList.end(); itr++)
        {
            itr->second->discard();
        }
        mHoverAnimationList.erase(mHoverAnimationList.begin(), mHoverAnimationList.end());
        mLastAnimation = nullptr;

        createHoverAnimations();

    }
    //------------------------------------------------------------------------------
    Ogre::Vector3 LinealFan::calculateInsertPosition()
    {
        if (mCardsList.size() % 2 == 0 ) Ogre::Vector3(DISTANCE*0.5f, 0.0f, 0.0f);
        return Ogre::Vector3::ZERO;
    }
    //------------------------------------------------------------------------------
    void LinealFan::reorganiseRenderQueueGroup( CardsList::iterator top )
    {
        CardsList::iterator itr = mCardsList.begin();
        short step = 1;
        short shift = 0;

        unsigned size = mCardsList.size();

        while ( itr != mCardsList.end() )
        {   
            short priority = size + shift;

            (*itr)->setRenderQueueGroupAndPriority( Gui::RQG_HAND, priority );

            if( itr == top ) step = -1;
            shift += step;
            itr++;
        }
    }
    //------------------------------------------------------------------------------
    void LinealFan::deselect()
    {
        if(mLastAnimation) mLastAnimation->setReverse(true);
    }
    //------------------------------------------------------------------------------
    void LinealFan::reorganisePositions()
    {
        Ogre::Vector3 startPosition = calculateStartPosition( mCardsList.size() );
        int i = 0;
        for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
        {

            (*itr)->getSceneNode()->setPosition( startPosition + Ogre::Vector3(i++*DISTANCE, 0.0f, 0.0f) );
            (*itr)->getSceneNode()->setOrientation( Ogre::Quaternion::IDENTITY );
            (*itr)->getSceneNode()->setScale( Gui::CardManager::CARD_SIZE );
            (*itr)->getSceneNode()->setInitialState();
        }
    }
    //------------------------------------------------------------------------------
    void LinealFan::createHoverAnimations()
    {
        for (CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
        {
            mHoverAnimationList[(*itr)->getId()] =  new LinealFanHoverAnimation(mCardsList, (*itr)->getId());
        }
    }
    //------------------------------------------------------------------------------
    void LinealFan::fill(const CardsIdList& cards_list)
    {
        CardManager* cm = Core::getSingletonPtr()->getCardManager();

        for (CardsIdList::const_iterator itr = cards_list.begin(); itr != cards_list.end(); itr++)
        {
            mCardsList.push_back( cm->getCard( *itr ).get() );
        }

        Ogre::Vector3 startPosition = calculateStartPosition (mCardsList.size());

        int i = 0;
        for(CardsList::iterator itr = mCardsList.begin(); itr != mCardsList.end(); itr++)
        {
            (*itr)->getSceneNode()->setPosition( startPosition + Ogre::Vector3(i*DISTANCE, 0.0f, 0.0f) );
            (*itr)->getSceneNode()->setInitialState();
            mNode->addChild( (*itr)->getSceneNode() );
            (*itr)->setOwner(this);
            (*itr)->saveState();
            i++;
        }

        reorganiseRenderQueueGroup(mCardsList.end());

        for (CardsIdList::const_iterator itr = cards_list.begin(); itr != cards_list.end(); itr++)
        {
            mHoverAnimationList[*itr] =  new LinealFanHoverAnimation(mCardsList, *itr);
        }
    }
    //------------------------------------------------------------------------------
}   // namespace Gui
