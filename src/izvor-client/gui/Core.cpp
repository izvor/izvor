/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac
*/

#include "gui/Core.h"
#include "gui/Deck.h"
#include "gui/Card.h"
#include "gui/Hand.h"
#include "gui/CardManager.h"
#include "gui/Fan.h"
#include "gui/Camera.h"
#include "gui/SceneRoot.h"
#include "gui/UpdatableManager.h"
#include "gui/Common.h"
#include "gui/Utility.h"
#include "gui/ZoomField.h"
#include "gui/Table.h"
#include "gui/Buff.h"

#include "gui/CardGlowingFrame.h"

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <Ogre.h>

#include <EngineManager.hpp>

#ifdef OGRE_DEBUG_MODE == 1
	#define POSITION_METER 0 // ovde 1 kad oces
#else
	#define POSITION_METER 0 // u rilis verziji nikad ga nema
#endif

template<> Gui::Core* Ogre::Singleton<Gui::Core>::ms_Singleton = 0;
Gui::GameInitParameters Gui::Core::mInitParameters;

Gui::Core::Core( IzvorCommon::Core::Game *izvor_game )
    : mSceneManager(nullptr), mSceneRoot(nullptr),
	  mGame( izvor_game ), mExpNode(nullptr)
{
//    GUI_LOG( "*** Creating Core" );
}

Gui::Core::~Core()
{
    GUI_LOG( "*** Core Shutdown" );
    delete mCamera;
    delete mCardManager;
    delete mUpdatableManager;
    delete mSceneRoot;
}

void Gui::Core::initialise( Ogre::SceneManager *scene_manager )
{
	GUI_LOG( "*** Initialising Core ***" );

	mSceneManager = scene_manager; 

    Ogre::Entity::setDefaultQueryFlags(0);
    mCardManager = new CardManager( mSceneManager );
    mUpdatableManager = new UpdatableManager();
	mRaySceneQuery = mSceneManager->createRayQuery(Ogre::Ray());

    mCamera = new Camera( mSceneManager );

    GUI_LOG( "*** Core Initialised ***" );
}

void Gui::Core::createScene()
{
    GUI_LOG( "*** Creating Scene ***" );

    mSceneRoot = new Gui::SceneRoot( mSceneManager );
    mSceneRoot->create();

#if POSITION_METER == 1 //defined in core.h
    /// Entitet za merenje pozicija , komande: up, down, left, right, page up, page down, on SPACE print position
    mExpNode = mSceneManager->getRootSceneNode()->createChildSceneNode();
    Ogre::Entity* ent = mSceneManager->createEntity(Ogre::SceneManager::PT_SPHERE);

    mExpNode->attachObject(ent);
    mExpNode->setScale(Ogre::Vector3(0.1f, 0.1f, 0.1f));
    /// ...
#endif

	//////////////////////////
	// IzvorField test
	//getCardManager()->create( 47 );
	//Gui::CardPtr card = getCardManager()->getCard( 47 );

	/////////////////////////
    const Ogre::RenderSystemCapabilities* caps = Mge::EngineManager::getSingletonPtr()->GetOgreRoot()->getRenderSystem()->getCapabilities();
    if ( caps->hasCapability(Ogre::RSC_VERTEX_PROGRAM) && (caps->hasCapability(Ogre::RSC_FRAGMENT_PROGRAM)) )
    {
        Ogre::Viewport *viewport = mCamera->getOgreCamera()->getViewport();
        Ogre::CompositorInstance *compositor_instance =
            Ogre::CompositorManager::getSingletonPtr()->addCompositor(viewport, "CardGlow");
        compositor_instance->setEnabled(true);
    } else
    {
        GUI_LOG("WARNING: Vertex and Fragment programs are not supported.");
    }

    GUI_LOG( "*** Scene Created ***" );
}

void Gui::Core::update(const Ogre::FrameEvent& evt )
{
    try
    {
        mUpdatableManager->update( evt );
        mCamera->update( evt );

        calculateMouseRay(Mge::EngineManager::getSingletonPtr()->GetMouse()->getMouseState());
        if (raySceneQueryExecute(Gui::QM_CARDS))
        { 
            CardId id = getMouseOverCardId();
            mSceneRoot->setMouseOverCardId(id);
            mSceneRoot->setMouseOver(true);
 
           // Treba osmisliti i za deaktivaciju...
            
            if(Mge::EngineManager::getSingletonPtr()->GetMouse()->getMouseState().buttonDown( OIS::MB_Left ))
            {
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_1))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_2))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff2>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_3))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff3>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_4))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff4>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_5))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff5>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_6))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff6>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_7))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff7>();
				if(Mge::EngineManager::getSingletonPtr()->GetKeyboard()->isKeyDown(OIS::KC_8))
					mCardManager->getCard( id )->addProperty<Gui::TestBuff8>();

				//mCardManager->getCard( id )->addProperty<CardGlow>();
    //            mCardManager->getCard( id )->removeProperty<CardGlowingFrame>();
            }
        
            //mCardManager->getCard( id )->removeProperty<CardGlow>();
            //u sustini bi i trebalo da stoji u fan-u, posto on treba da odlucuje koji buff ce se nakaciti na kartu
            //za sad sam ga izbacio posto radim reorganizaciju fana...
            //TODO: Probaj da uradis onaj svetleci okvir oko karte...buuff
            // sta?

        }
        else 
        {
            mSceneRoot->setMouseOver(false);
        }
    } catch(const Ogre::Exception& e)
    {
        std::cout << e.getFullDescription() << std::endl;
    } catch (const std::exception &e)
    {
        std::cout << e.what() << std::endl;
    } 

    mSceneRoot->update();

}

void Gui::Core::injectKeyPressed( const OIS::KeyEvent &arg )
{

#if POSITION_METER == 1

    if ( arg.key == OIS::KC_UP )
    {
        //mSceneRoot->getTable()->getSceneNode()->getChild("light")->translate(Ogre::Vector3(0.0f, 0.0f, 5.0f));
        mExpNode->translate(Ogre::Vector3(0.0f, 0.0f, -1.0f));
    }
    if ( arg.key == OIS::KC_DOWN )
    {
        //mSceneRoot->getTable()->getSceneNode()->getChild("light")->translate(Ogre::Vector3(0.0f, 0.0f, -5.0f));
        mExpNode->translate(Ogre::Vector3(0.0f, 0.0f, 1.0f));
    }
    if ( arg.key == OIS::KC_LEFT )
    {
        mExpNode->translate(Ogre::Vector3(-1.0f, 0.0f, 0.0f));
    }
    if ( arg.key == OIS::KC_RIGHT )
    {
        mExpNode->translate(Ogre::Vector3(1.0f, 0.0f, 0.0f));
    }
    if ( arg.key == OIS::KC_PGUP )
    {
        mExpNode->translate(Ogre::Vector3(0.0f, 1.0f, 0.0f));
    }
    if ( arg.key == OIS::KC_PGDOWN )
    {
        mExpNode->translate(Ogre::Vector3(0.0f, -1.0f, 0.0f));
    }
    if ( arg.key == OIS::KC_SPACE )
    {
        std::cout << mExpNode->getPosition() << std::endl;
    }
#endif
/*

    if ( arg.key == OIS::KC_A )
    {
        if (mAttackStack.size() > 0)
        {
	        mAttack->acceptCard(mAttackStack.top());
            mCardManager->getCard(mAttackStack.top())->tap();
	        mAttackStack.pop();
        }
    }
*/

    mCamera->keyPressed(arg);
}

void Gui::Core::injectKeyReleased( const OIS::KeyEvent &arg )
{
    mCamera->keyReleased(arg);
}

void Gui::Core::injectMouseMoved( const OIS::MouseEvent &arg )
{
	mCamera->mouseMoved(arg);
}

void Gui::Core::injectMousePressed( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    mCamera->mousePressed(arg, id);


    if (id == OIS::MB_Left)
    {
        calculateMouseRay(Mge::EngineManager::getSingletonPtr()->GetMouse()->getMouseState());
        if (raySceneQueryExecute(Gui::QM_CARDS))
        { 
            //mSceneRoot->setMouseOver( getMouseOverCardId() );
            CardId id = getMouseOverCardId();
            mSceneRoot->playCard(id);
        }
    }

    if (id == OIS::MB_Right)
    {
//         calculateMouseRay(Mge::EngineManager::getSingletonPtr()->GetMouse()->getMouseState());
//         if (raySceneQueryExecute(Gui::QM_CARDS))
//         { 
            mSceneRoot->zoom();
//         }
    }
/*
    if (id == OIS::MB_Middle)
    {
        calculateMouseRay(Mge::EngineManager::getSingletonPtr()->GetMouse()->getMouseState());
        if (raySceneQueryExecute(Gui::QM_CARDS))
        { 
            //mSceneRoot->setMouseOver( getMouseOverCardId() );
            CardId id = getMouseOverCardId();
            mZoomField->dispatchCard(id);
        }

    }*/

}

void Gui::Core::injectMouseReleased( const OIS::MouseEvent &arg, OIS::MouseButtonID id )
{
    mCamera->mouseReleased(arg, id);
}

Gui::CardId Gui::Core::getMouseOverCardId()
{
		CardId retval = 0;

        if (mRaySceneQueryResult->size() > 0)
        {
			Ogre::RaySceneQueryResult::iterator itr = mRaySceneQueryResult->begin();  
			Ogre::uint8 maxQueueGroup = 0;
            int maxPriority = 0;
			while(itr != mRaySceneQueryResult->end())
			{
				CardId id = Ogre::StringConverter::parseInt( itr->movable->getName() );
				Ogre::uint8 group = itr->movable->getRenderQueueGroup();
                int priority = mCardManager->getCard(id)->getPriority();
				if (group >= maxQueueGroup && priority > maxPriority)
				{
					maxQueueGroup = group;
                    maxPriority = priority;
					retval = id;
				}
				itr++;
			}
		}
		else throw std::exception();
        
        return retval;
}

void Gui::Core::calculateMouseRay( const OIS::MouseState &arg )
{
    Ogre::Ray mouseRay = mCamera->getOgreCamera()->getCameraToViewportRay( arg.X.abs/float(arg.width), 
        arg.Y.abs/float(arg.height) );

	mRaySceneQuery->setRay(mouseRay);
}

bool Gui::Core::raySceneQueryExecute( Gui::QueryMasks mask )
{
	mRaySceneQuery->setQueryMask(mask);
	mRaySceneQueryResult = &mRaySceneQuery->execute();

	return mRaySceneQueryResult->size() > 0;
}


