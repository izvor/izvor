/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/
#include "gui/Core.h"
#include "gui/SceneRoot.h"
#include "gui/Table.h"
#include "gui/Camera.h"
#include "gui/Deck.h"
#include "gui/PlayingField.h"
#include "gui/Common.h"

#include <Ogre.h>

//#include "gui/Buff.h"

namespace Gui
{
    Table::Table()
    {
		mEntity = Core::getSingletonPtr()->getSceneRoot()->getSceneManager()->createEntity("Table", "table.mesh");
		mEntity->setRenderQueueGroupAndPriority( Gui::RQG_TABLE, 0 );
		mEntity->setQueryFlags( Gui::QM_NOT_SCAN );

		mNode = Core::getSingletonPtr()->getSceneRoot()->getSceneManager()->getRootSceneNode()->createChildSceneNode();
		mNode->attachObject(mEntity);
        //mNode->rotate(Ogre::Quaternion( Ogre::Degree(-45), Ogre::Vector3::UNIT_X));
	
//         Ogre::Entity* ent = Core::getSingletonPtr()->getSceneRoot()->getSceneManager()->createEntity(Ogre::SceneManager::PT_CUBE);
//         ent->setRenderQueueGroup(Gui::RQG_TABLE);
//         Ogre::SceneNode* node = mNode->createChildSceneNode();
//         node->attachObject(ent);
//         node->setScale(Ogre::Vector3(0.3f,0.3f,0.3f));
//         node->setPosition(Ogre::Vector3(0.0f, 0.0f, 30.0f));

        Ogre::SceneManager* scn_mgr = Core::getSingletonPtr()->getSceneRoot()->getSceneManager();
           // Set ambient light
        scn_mgr->setAmbientLight(Ogre::ColourValue(1.0, 1.0, 1.0));

        // Create a light
         Ogre::Light* l = scn_mgr->createLight("MainLight");
         l->setType(Ogre::Light::LT_POINT);

         //l->setDirection(Ogre::Vector3(0,0,-1));
         l->setDiffuseColour(1.0f, 1.0f, 1.0f);
         l->setSpecularColour(0.4f, 0.5f, 0.3f);
         //l->setAttenuation(100.0f, 0.5f, 1.0f, 0.0f);
         //l->setPosition(0.0f,0.0f, 50.0f);
         //l->setDirection(Ogre::Vector3(Ogre::Vector3::NEGATIVE_UNIT_Z));
         //l->setSpotlightRange(Ogre::Degree(35), Ogre::Degree(90));

         Ogre::SceneNode* sn = mNode->createChildSceneNode();
         //mNode->attachObject( l );
         sn->setPosition(-100.0f, 80.0f, 45.0f);
         sn->attachObject(l);
         //sn->lookAt(Ogre::Vector3(0.0f, 0.0f, 0.0f), Ogre::SceneNode::TS_WORLD);

         Ogre::Entity* ent = scn_mgr->createEntity(Ogre::SceneManager::PT_SPHERE);
         sn->attachObject(ent);
         ent->setRenderQueueGroup(Gui::RQG_TABLE);
         //sn->lookAt(Ogre::Vector3(0.0f, 0.0f, 0.0f), Ogre::SceneNode::TS_WORLD);
         sn->setScale(0.01f, 0.01f, 0.01f);

        // Create a light 2
         Ogre::Light* l2 = scn_mgr->createLight("MainLight2");
         l2->setType(Ogre::Light::LT_POINT);

         //l->setDirection(Ogre::Vector3(0,0,-1));
         l2->setDiffuseColour(1.0f, 1.0f, 1.0f);
         l2->setSpecularColour(0.4f, 0.5f, 0.3f);
         //l->setAttenuation(100.0f, 0.5f, 1.0f, 0.0f);
         //l->setPosition(0.0f,0.0f, 50.0f);
         //l->setDirection(Ogre::Vector3(Ogre::Vector3::NEGATIVE_UNIT_Z));
         //l->setSpotlightRange(Ogre::Degree(35), Ogre::Degree(90));

         Ogre::SceneNode* sn2 = mNode->createChildSceneNode();
         //mNode->attachObject( l );
         sn2->setPosition(100.0f, 80.0f, -200.0f);
         sn2->attachObject(l2);
         //sn->lookAt(Ogre::Vector3(0.0f, 0.0f, 0.0f), Ogre::SceneNode::TS_WORLD);

         Ogre::Entity* ent2 = scn_mgr->createEntity(Ogre::SceneManager::PT_SPHERE);
         sn2->attachObject(ent2);
         ent2->setRenderQueueGroup(Gui::RQG_TABLE);
         //sn->lookAt(Ogre::Vector3(0.0f, 0.0f, 0.0f), Ogre::SceneNode::TS_WORLD);
         sn2->setScale(0.01f, 0.01f, 0.01f);
/*
        Ogre::Vector3 lib_deck_pos = getLibraryDeckPosition();
        Ogre::Vector3 oppon_lib_deck_pos = getOpponentLibraryDeckPosition();
        
        mLibraryDeck = new Deck(DEFAULT_DECK_SIZE, mSceneManager, lib_deck_pos, Ogre::Quaternion( Ogre::Degree(90), Ogre::Vector3::UNIT_X) );
        mOpponentLibraryDeck = new Deck(DEFAULT_DECK_SIZE - 5, mSceneManager, oppon_lib_deck_pos, Ogre::Quaternion( Ogre::Degree(90), Ogre::Vector3::UNIT_X ) );

        Ogre::Vector3 field_pos = Ogre::Vector3(0,-50,0);
        mPlayersField = new PlayingField(mSceneManager, 75.0, 30, field_pos, Ogre::Quaternion( Ogre::Degree(90), Ogre::Vector3::NEGATIVE_UNIT_X));
    */
//        Buff *test = new PermanentBuff();
//        Buff *test1 = new PermanentBuff(test);
//        Buff *test2 = new PermanentBuff(test);
//        Buff *test3 = new PermanentBuff(test2);
//
//        delete test;
        //delete test1;
        //delete test2;
        //delete test3;
    }

    Table::~Table()
    {
/*
        delete mLibraryDeck;
        delete mOpponentLibraryDeck;*/

    }

/*
    Ogre::Vector3 Table::getLibraryDeckPosition()
    {
        // TO BE DONE
        Ogre::Vector3 v(120,-50,0);
        return v;
    }

    Ogre::Vector3 Table::getOpponentLibraryDeckPosition()
    {
        // TO BE DONE
        Ogre::Vector3 v(120,-50,-50);
        return v;
    }*/


}
