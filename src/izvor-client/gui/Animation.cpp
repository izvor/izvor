/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Thunder
*/


#include <Ogre.h>

#include "gui/Common.h"
#include "gui/Animation.h"
#include "gui/Core.h"
#include "gui/SceneRoot.h"
#include "gui/Card.h"
#include "gui/Utility.h"
#include "gui/UpdatableManager.h"


const Ogre::Real Gui::LinealFanHoverAnimation::ANIMATON_DURATION = 0.3f;
//------------------------------------------------------------------------------
Gui::Animation::Animation()
    : mAnimationState(nullptr)
    , mAnimationFactor(1.0)
	, mReallyEnd(false)
{

}
//------------------------------------------------------------------------------
Gui::Animation::~Animation()
{
    if(mAnimationState)
    {        
        Ogre::String animation_name = mAnimationState->getAnimationName();
        Core::getSingletonPtr()->getSceneRoot()->getSceneManager()->destroyAnimationState(animation_name);
        Core::getSingletonPtr()->getSceneRoot()->getSceneManager()->destroyAnimation(animation_name);
    }
}
//------------------------------------------------------------------------------
bool Gui::Animation::update( const Ogre::FrameEvent& evt )
{
	mAnimationState->addTime( mAnimationFactor * evt.timeSinceLastFrame );

	return !this->hasEnded();
}

bool Gui::Animation::hasEnded()
{
	if(!mReallyEnd)
	{
		if(_hasEndedImpl())
			mReallyEnd = true;
		return false;
	}
	return true;
}
//------------------------------------------------------------------------------
Gui::LinealFanHoverAnimation::LinealFanHoverAnimation( CardsList& cardsList, CardId hoveredCardId)
{
	if (cardsList.size() == 0) throw std::exception();

	Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

	Ogre::SceneManager* sceneMgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

	CardsList::iterator hoveredCard = cardsList.begin();
	while (hoveredCard != cardsList.end())
	{
		if ((*hoveredCard)->getId() == hoveredCardId) break;
		hoveredCard++;
	}

	Ogre::Animation* animation = sceneMgr->createAnimation( Utility::getUniqueString() , ANIMATON_DURATION);
	animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);
	unsigned short trackHandle = 0;

	Ogre::NodeAnimationTrack* track = animation->createNodeTrack( trackHandle++, (*hoveredCard)->getSceneNode() );
	
	Ogre::TransformKeyFrame* keyFrame;

	keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setTranslate( Ogre::Vector3::ZERO );
    keyFrame->setScale( Ogre::Vector3::UNIT_SCALE );

	keyFrame = track->createNodeKeyFrame(ANIMATON_DURATION);
	keyFrame->setTranslate( Ogre::Vector3(0.0f, 0.0f, -3.0f) );
	keyFrame->setScale( Ogre::Vector3(1.2f, 1.2f, 1.2f));

	CardsList::iterator itr = hoveredCard; itr++;
	while(itr != cardsList.end() )
	{
		track = animation->createNodeTrack( trackHandle++, (*itr++)->getSceneNode() );
		keyFrame = track->createNodeKeyFrame(0.0f);
        keyFrame->setTranslate( Ogre::Vector3::ZERO );

		keyFrame = track->createNodeKeyFrame(ANIMATON_DURATION);
		keyFrame->setTranslate( Ogre::Vector3(5.0f, 0.0f, 0.0f) );
	}
	itr = hoveredCard;
	if (itr != cardsList.begin())
	{
		do 
		{
			track = animation->createNodeTrack( trackHandle++, (*--itr)->getSceneNode() );
			keyFrame = track->createNodeKeyFrame(0.0f);
            keyFrame->setTranslate( Ogre::Vector3::ZERO );
	
			keyFrame = track->createNodeKeyFrame(ANIMATON_DURATION);
			keyFrame->setTranslate( Ogre::Vector3(-5.0f, 0.0f, 0.0f) );
		} while ( itr != cardsList.begin());
	}

	mAnimationState = sceneMgr->createAnimationState( animation->getName() );

	mAnimationState->setEnabled(true);
	mAnimationState->setLoop(false);

}

//------------------------------------------------------------------------------
Gui::DrawCardAnimation::DrawCardAnimation(Card* card, const Ogre::Vector3 position, const Ogre::Quaternion orientation )
{

    Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

    Ogre::SceneManager* scene_mgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    Ogre::Animation* animation = scene_mgr->createAnimation( Utility::getUniqueString(), 1.0f);
    animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);

    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, card->getSceneNode() );
	Ogre::TransformKeyFrame* keyFrame;

    keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setTranslate(position);
    keyFrame->setRotation( orientation );

    keyFrame = track->createNodeKeyFrame(1.0);
    keyFrame->setTranslate( Ogre::Vector3::ZERO);
    keyFrame->setRotation( Ogre::Quaternion::IDENTITY);

    mAnimationState = scene_mgr->createAnimationState( animation->getName() );

    mAnimationState->setEnabled(true);
    mAnimationState->setLoop(false);

}
//------------------------------------------------------------------------------
Gui::DragAnimation::DragAnimation( Card* card, const Ogre::Vector3 position, const Ogre::Quaternion quat )
{
    //std::cout << position << std::endl;
    Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

    Ogre::SceneManager* scene_mgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    Ogre::Animation* animation = scene_mgr->createAnimation( Utility::getUniqueString(), 1.0f);
    animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);

    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, card->getSceneNode() );
	Ogre::TransformKeyFrame* keyFrame;
    
    keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setTranslate(position);
    keyFrame->setRotation(quat);

    keyFrame = track->createNodeKeyFrame(1.0);
    keyFrame->setTranslate( Ogre::Vector3::ZERO );
    keyFrame->setRotation( Ogre::Quaternion::IDENTITY );

    mAnimationState = scene_mgr->createAnimationState( animation->getName() );

    mAnimationState->setEnabled(true);
    mAnimationState->setLoop(false);
}
//------------------------------------------------------------------------------
Gui::DragAnimation::DragAnimation( Card* card )
{

    Ogre::SceneNode* card_node = card->getSceneNode();

    SceneNodeState state = card->calculateAnimationPositionAndOrientation();

    Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

    Ogre::SceneManager* scene_mgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    Ogre::Animation* animation = scene_mgr->createAnimation( Utility::getUniqueString(), 1.0f);
    animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);

    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, card->getSceneNode() );
	Ogre::TransformKeyFrame* keyFrame;
    
    keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setTranslate(state.position);
    keyFrame->setRotation(state.orientation);

    keyFrame = track->createNodeKeyFrame(1.0);
    keyFrame->setTranslate( Ogre::Vector3::ZERO );
    keyFrame->setRotation( Ogre::Quaternion::IDENTITY );

    mAnimationState = scene_mgr->createAnimationState( animation->getName() );

    mAnimationState->setEnabled(true);
    mAnimationState->setLoop(false);
}
//------------------------------------------------------------------------------
Gui::DragAnimation::DragAnimation( Ogre::SceneNode* node)
{

    SceneNodeState state;
    state.position = node->_getDerivedPosition();
    state.orientation = node->_getDerivedOrientation();
	state.scale = node->getScale();

	SceneNodeState animation_state;
    animation_state.position = node->convertWorldToLocalPosition(state.position);
    animation_state.orientation = node->convertWorldToLocalOrientation(state.orientation);
    animation_state.position = animation_state.position * node->getScale();

    Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

    Ogre::SceneManager* scene_mgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    Ogre::Animation* animation = scene_mgr->createAnimation( Utility::getUniqueString(), 1.0f);
    animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);

    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, node );
	Ogre::TransformKeyFrame* keyFrame;
    
    keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setTranslate(animation_state.position);
    keyFrame->setRotation(animation_state.orientation);

    keyFrame = track->createNodeKeyFrame(1.0);
    keyFrame->setTranslate( Ogre::Vector3::ZERO );
    keyFrame->setRotation( Ogre::Quaternion::IDENTITY );

    mAnimationState = scene_mgr->createAnimationState( animation->getName() );

    mAnimationState->setEnabled(true);
    mAnimationState->setLoop(false);
}
//------------------------------------------------------------------------------
Gui::TapAnimation::TapAnimation( Card* card )
{
    Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

    Ogre::SceneManager* scene_mgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    Ogre::Animation* animation = scene_mgr->createAnimation( Utility::getUniqueString(), 1.0f);
    animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);

    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, card->getSceneNode() );
	Ogre::TransformKeyFrame* keyFrame;
    
    keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setRotation(Ogre::Quaternion::IDENTITY);

    keyFrame = track->createNodeKeyFrame(1.0);
    keyFrame->setRotation(Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_Z));

    mAnimationState = scene_mgr->createAnimationState( animation->getName() );

    mAnimationState->setEnabled(true);
    mAnimationState->setLoop(false);
}

//------------------------------------------------------------------------------
Gui::ZoomAnimation::ZoomAnimation(Card* card, const Ogre::Vector3 position, const Ogre::Quaternion  orientation)
{
    Core::getSingletonPtr()->getUpdatableManager()->registerUpdatable(this);

    Ogre::SceneManager* scene_mgr = Gui::Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    Ogre::Animation* animation = scene_mgr->createAnimation( Utility::getUniqueString(), 0.5f);
    animation->setInterpolationMode(Ogre::Animation::IM_LINEAR);

    Ogre::NodeAnimationTrack* track = animation->createNodeTrack(0, card->getSceneNode() );
	Ogre::TransformKeyFrame* keyFrame;
    
    keyFrame = track->createNodeKeyFrame(0.0f);
    keyFrame->setTranslate( Ogre::Vector3::ZERO );
    keyFrame->setRotation( Ogre::Quaternion::IDENTITY );

    keyFrame = track->createNodeKeyFrame(0.5);
    keyFrame->setTranslate(position);
    keyFrame->setRotation(orientation);

    mAnimationState = scene_mgr->createAnimationState( animation->getName() );

    mAnimationState->setEnabled(true);
    mAnimationState->setLoop(false);
}
//------------------------------------------------------------------------------