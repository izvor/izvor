/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#include "gui/CardManager.h"
#include "gui/Common.h"
#include "gui/Card.h"
#include "gui/Core.h"

#include <IzvorPCH.h>

const Ogre::Vector3 Gui::CardManager::CARD_SIZE = Ogre::Vector3(21.5,1.0,30.0);

void Gui::CardManager::create( CardId card_id )
{
	std::pair<Ogre::SceneNode*, Ogre::Entity*> retval =  createCardEntity( card_id );

    mCardMap.insert( std::pair<const CardId, CardPtr>(card_id, CardPtr(new Card( card_id, retval.first, retval.second ))) );

    // exception...
}

/*
void Gui::CardManager::destroy( CardId card_id )
{
    mCardMap.erase( card_id );
}
*/


Gui::CardPtr Gui::CardManager::getCard(  Gui::CardId id )
{
	if ( mCardMap.find(id) == mCardMap.end() ) 
	{
		GUI_LOG( "couldn't find it" );
	    throw std::exception();
	}
	else
	{
		return mCardMap[id];
	}
}

std::pair<Ogre::SceneNode*, Ogre::Entity*> Gui::CardManager::createCardEntity(  CardId card_id )
{
//     Core::BaseCard::getEditionImgPath(
   //BaseCard* base_card = IzvorCommon::Core::ObjectManager::get().find< IzvorCommon::Core::BaseCard >(card_id);
    IzvorCommon::Core::CardElement *ce = IzvorCommon::Core::ObjectManager::get().find< IzvorCommon::Core::CardElement >( card_id );
	if (!ce ) throw std::exception();  
    std::string path = IzvorCommon::Core::BaseCard::Info::getEditionImgPath( ce->getBaseCard()->getInfo()->getReprintEdition() );
    std::string image_name = path + Ogre::StringConverter::toString( ce->getBaseCard()->getInfo()->getReprintNum() ) + ".jpg";

	Ogre::String card_id_str = Ogre::StringConverter::toString( card_id );

	//Ogre::Entity* card_entity = mSceneManager->createEntity( id, Ogre::SceneManager::PT_CUBE );
    Ogre::Entity* card_entity = mSceneManager->createEntity( card_id_str, "Plane.mesh" );
    card_entity->setQueryFlags( Gui::QM_CARDS );

	Ogre::MaterialPtr new_material = Ogre::MaterialManager::getSingleton().create
	        (card_id_str, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME );
	Ogre::MaterialPtr base_material = Ogre::MaterialManager::getSingletonPtr()->getByName("Material/Card");
	base_material->copyDetailsTo(new_material);
	//Ogre::String image_name( Ogre::StringConverter::toString(model_id) );
    //image_name += ".png";
	int colour = ce->getBaseCard()->getBaseColor()->color();
	new_material->getTechnique( 0 )->getPass( 0 )->getTextureUnitState( 0 )->setTextureName(image_name);
	new_material->getTechnique( 1 )->getPass( 0 )->getTextureUnitState( 0 )->setTextureName(image_name);
	new_material->getTechnique( 1 )->getPass( 0 )->getFragmentProgramParameters()->
	                                            setNamedConstant("card_type", colour);
	card_entity->setMaterial(new_material);
    card_entity->getSubEntity(1)->setMaterialName("Material/Back");
    // set more parameters in materials/scripts/card.material
    // it is bad to set depth write off because of occulsion.
    
	Ogre::SceneNode* card_node = mSceneManager->createSceneNode(card_id_str);
	card_node->attachObject(card_entity);
    card_node->setVisible( true );
    card_node->setScale(CARD_SIZE);

	return std::pair<Ogre::SceneNode*, Ogre::Entity*>(card_node, card_entity);
}

Gui::CardManager::CardManager( Ogre::SceneManager *scene_manager )
    : mSceneManager(scene_manager)
{

}

Gui::CardManager::~CardManager()
{
	while(!mCardMap.empty())
	{
		//mCardMap.begin()->reset();
		mCardMap.erase(mCardMap.begin());
	}
}

