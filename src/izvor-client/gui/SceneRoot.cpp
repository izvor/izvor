/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac, Thunder
*/

#include "gui/Core.h"
#include "gui/SceneRoot.h"
#include "gui/Hand.h"
#include "gui/Camera.h"
#include "gui/Table.h"
#include "gui/ZoomField.h"
#include "gui/CardManager.h"
#include "gui/IzvorField.h"
#include "gui/OpponentHand.h"
#include "gui/PlayerPanel.h"

#include <Ogre.h>

Gui::SceneRoot::SceneRoot( Ogre::SceneManager* scene_manager)
    : mSceneManager(scene_manager),
      mIsMouseOverCard(false),
      mIsZoom(false), mIsZoomState(false)
{

}

Gui::SceneRoot::~SceneRoot()
{
    //delete mTable;
}

void Gui::SceneRoot::create()
{
    GameInitParameters& param = Core::getSingletonPtr()->getInitParameters();

    mPlayerId = param.sPlayerId;
    mOpponentId = param.sOpponentId;

    CardsIdList playersCard;
    CardsIdList opponentsCard;
    
    CardManager* card_manager = Core::getSingletonPtr()->getCardManager();

    for (CardsIdList::iterator itr = param.sPlayerHand->begin(); itr != param.sPlayerHand->end(); itr++ )
    {
        card_manager->create(*itr);
        playersCard.push_back(*itr);
    }

    for (CardsIdList::iterator itr = param.sOpponentHand->begin(); itr != param.sOpponentHand->end(); itr++ )
    {
        card_manager->create(*itr);
        opponentsCard.push_back(*itr);
    }

    for (CardsIdList::iterator itr = param.sPlayerLibrary->begin(); itr != param.sPlayerLibrary->end(); itr++ )
    {
        card_manager->create(*itr);
        playersCard.push_back(*itr);
    }

    for (CardsIdList::iterator itr = param.sOpponentLibrary->begin(); itr != param.sOpponentLibrary->end(); itr++ )
    {
        card_manager->create(*itr);
        opponentsCard.push_back(*itr);
    }


    mTable = new Table();    
    
    Camera* camera = Core::getSingletonPtr()->getCamera();
    camera->sceneInit();

    /// Creating hand fan node.
    Ogre::SceneNode* camera_node = camera->getSceneNode();
    Ogre::SceneNode* hand_node =  camera_node->createChildSceneNode();
    // Malo glupo resenje za podesavanje mesta ruke na ekranu,
    // probao sam sa rotacijom ali tada se vise primecuje da su zakrivljene karte.
    // Ray je malo uzjeban, ali videcemo posle sta moze da se ucini. 
    hand_node->setPosition(Ogre::Vector3(0.0f, -40.0f, -150.0f));
    hand_node->setOrientation(Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_X));

    mPlayerHand = new LinealFan(hand_node);
    //mPlayerHand->fill(*param.sPlayerHand);
    /// ---
    Ogre::Quaternion quat = Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y) 
                            * Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_X);
    mOpponentHand = new OpponentHand(Ogre::Vector3(17.0f, 50.0f, -200.0f), quat);

    mPlayerLibrary = new Deck(Ogre::Vector3(124.0f, 3.0f, -13.0f), Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Z));
    mPlayerLibrary->fill(playersCard/**param.sPlayerLibrary*/);
    mPlayerGraveyard = new Deck(Ogre::Vector3(136.0f, -1.0f, 45.0f), Ogre::Quaternion::IDENTITY);

    quat = Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y) 
            * Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Z);
    mOpponentLibrary = new Deck(Ogre::Vector3(-92.0f, 3.0f, -156.0f), quat);
    mOpponentLibrary->fill(opponentsCard/**param.sOpponentLibrary*/);

    mOpponentGraveyard = new Deck(Ogre::Vector3(-105.0f, -1.0f, -211.0), Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y));

    mPlayerPermanentsField = new PermanentsField(Ogre::Vector3(-12.0f, 0.0f, 0.0f), Ogre::Quaternion::IDENTITY );
    mPlayerAttackAndBlockField = new PermanentsField(Ogre::Vector3(17.0f, 0.0f, -40.0f), Ogre::Quaternion::IDENTITY );

    mOpponentPermanentsField = new PermanentsField(Ogre::Vector3(47.0f, 0.0f, -161.0f), Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y));
    mOpponentAttackAndBlockField = new PermanentsField(Ogre::Vector3(17.0f, 0.0f, -126.0f), Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y));

    mStack = new PermanentsField(Ogre::Vector3(17.0f, 10.0f, -82.0f), Ogre::Quaternion::IDENTITY);

    mZoomField = new ZoomField();

	mPlayerIzvorField = new IzvorField(Ogre::Vector3(15.0f,-1.0f,46.0f), Ogre::Quaternion::IDENTITY );
	mOpponentIzvorField = new IzvorField(Ogre::Vector3(30.0f,-1.0f,-209.0f), Ogre::Quaternion(Ogre::Degree(180), Ogre::Vector3::UNIT_Y));

	mPlayerPanel = new PlayerPanel();
	//////TEST///////
	mPlayerPanel->setMana(IzvorCommon::Core::Color::ZUTA, 10);
	mPlayerPanel->setMana(IzvorCommon::Core::Color::PLAVA, 9);
	mPlayerPanel->setMana(IzvorCommon::Core::Color::BEZBOJNA, 8);
	mPlayerPanel->setMana(IzvorCommon::Core::Color::CRNA, 7);
	mPlayerPanel->setMana(IzvorCommon::Core::Color::CRVENA, 6);
	mPlayerPanel->setMana(IzvorCommon::Core::Color::ZELENA, 5);
}

void Gui::SceneRoot::createDeck( const CardsIdList& list )
{
    mPlayerLibrary->fill(list);
}

void Gui::SceneRoot::drawCard( const CardId id )
{
    mPlayerHand->acceptCard(id);
}

void Gui::SceneRoot::update()
{
    if (mIsMouseOverCard && !mIsZoomState)
    {
        mPlayerHand->mouseOver(mMouseOverCardId);
    }
    else if (!mIsZoomState)
    {
        mPlayerHand->deselect();
    }


    if (mIsZoom)
    {
        if (mIsZoomState)
        {
            mZoomField->dispatchCard(mZoomedCard);
            mIsZoomState = false;
            mIsZoom = false;
        } 
        else if (!mIsZoomState && mIsMouseOverCard)
        {
            mZoomField->acceptCard(mMouseOverCardId);
            mZoomedCard = mMouseOverCardId;
            mPlayerHand->deselect();
            mIsZoomState = true;
            mIsZoom = false;
        }
        else mIsZoom = false;
    }

}

void Gui::SceneRoot::playCard( CardId id )
{
    //mPlayerPermanentsField->acceptCard(id);
	mPlayerIzvorField->acceptCard(id); // TEST!
}

Gui::CardContainer* Gui::SceneRoot::getContainer(const ObjectId player, const IzvorCommon::GuiMoveCardRequest::Dest container )
{
    bool isPlayer;
    if (mPlayerId == player) isPlayer = true;
    else isPlayer = false;

    switch (isPlayer)
    {
    case true:
        switch (container)
        {
        case IzvorCommon::GuiMoveCardRequest::DEST_HAND :
            return mPlayerHand;
        case IzvorCommon::GuiMoveCardRequest::DEST_PERMANENTS :
            return mPlayerPermanentsField;
        case IzvorCommon::GuiMoveCardRequest::DEST_ATTACK_DEFENSE :
            return mPlayerAttackAndBlockField;
        case IzvorCommon::GuiMoveCardRequest::DEST_LIB :
            return mPlayerLibrary;
        case IzvorCommon::GuiMoveCardRequest::DEST_GY:
            return mPlayerGraveyard;
        case IzvorCommon::GuiMoveCardRequest::DEST_STACK :
            return mStack;
        case IzvorCommon::GuiMoveCardRequest::DEST_SOURCES :
            return mPlayerIzvorField;
        case IzvorCommon::GuiMoveCardRequest::DEST_ARTEFACTS :
            return mPlayerPermanentsField;
        default: 
            GUI_LOG("Gui::SceneRoot::getContainer(), wrong dest.");
            return nullptr;
        }

    case false:
        switch (container)
        {
        case IzvorCommon::GuiMoveCardRequest::DEST_HAND :
            return mOpponentHand;
        case IzvorCommon::GuiMoveCardRequest::DEST_PERMANENTS :
            return mOpponentPermanentsField;
        case IzvorCommon::GuiMoveCardRequest::DEST_ATTACK_DEFENSE :
            return mOpponentAttackAndBlockField;
        case IzvorCommon::GuiMoveCardRequest::DEST_LIB :
            return mOpponentLibrary;
        case IzvorCommon::GuiMoveCardRequest::DEST_GY:
            return mOpponentGraveyard;
        case IzvorCommon::GuiMoveCardRequest::DEST_STACK :
            return mStack;
        case IzvorCommon::GuiMoveCardRequest::DEST_SOURCES :
            return mOpponentIzvorField;
        case IzvorCommon::GuiMoveCardRequest::DEST_ARTEFACTS :
            return mOpponentPermanentsField;
        default: 
            GUI_LOG("Gui::SceneRoot::getContainer(), wrong dest.");
            return nullptr;
        }    

    default: 
        GUI_LOG("Gui::SceneRoot::getContainer(), wrong player id");
        return nullptr;
    }
}