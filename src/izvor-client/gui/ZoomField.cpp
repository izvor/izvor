/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder, mikronac
*/

#include "gui/ZoomField.h"
#include "gui/Core.h"
#include "gui/Camera.h"
#include "gui/SceneRoot.h"
#include "gui/Animation.h"
#include "gui/Card.h"
#include "gui/CardManager.h"
#include "gui/Table.h"

#include <Ogre.h>


Gui::ZoomField::ZoomField()
: mAnimation(nullptr),
  mNode(nullptr)
{
    mNode = Core::getSingletonPtr()->getSceneRoot()->getSceneManager()->getRootSceneNode()->createChildSceneNode();

    Ogre::SceneNode* camera_node = Core::getSingletonPtr()->getCamera()->getSceneNode();
    mNode = camera_node->createChildSceneNode();

    mNode->setPosition( Ogre::Vector3(0.0f, 0.0f, -50.0f) );
    mNode->setOrientation( Ogre::Quaternion(Ogre::Degree(90), Ogre::Vector3::UNIT_X));

}

Gui::ZoomField::~ZoomField()
{

}

void Gui::ZoomField::acceptCard( CardId card_id)
{
    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);

    mZoomedCard = card_id;

     Ogre::SceneNode* card_node = card->getSceneNode();
     card_node->setPosition(card_node->getInitialPosition());
     card_node->setOrientation(card_node->getInitialOrientation());
     card_node->setScale(card_node->getInitialScale());

     Ogre::Vector3 zoom_field_world_position = mNode->_getDerivedPosition();

     Ogre::Vector3 zoom_position = card_node->convertWorldToLocalPosition(zoom_field_world_position);
     zoom_position = zoom_position * card_node->getScale();
 
     Ogre::Quaternion zoom_field_world_orientation = mNode->_getDerivedOrientation();
     Ogre::Quaternion zoom_orientation = card_node->convertWorldToLocalOrientation(zoom_field_world_orientation);

     // hack
     mRQG = card->getEntity()->getRenderQueueGroup();
     card->getEntity()->setRenderQueueGroup(Gui::RQG_HAND + 1);

    mAnimation = new ZoomAnimation(card.get(), zoom_position, zoom_orientation);
    mAnimation->setReverse(false);
}

void Gui::ZoomField::dispatchCard( CardId card_id )
{
    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(mZoomedCard);

    card->getEntity()->setRenderQueueGroup(mRQG);

    if (mAnimation) 
    {
        mAnimation->setReverse(true);
    }
    mAnimation = nullptr;
}