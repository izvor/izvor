/*
--------------------------------------------------------------------------------
_                 _                   _   _     
(_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
| |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
|_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
|___/  |__/     
-----------------------------------------------------------------------
LICENSE:
-----------------------------------------------------------------------
This file is part of Izvor: Izvori Magije (C) TCG simulator
Copyright 2010 EGDC++ Team
For the latest info visit: http://sites.google.com/site/etfgamedevcrew
-----------------------------------------------------------------------
Izvor is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the 
Free Software Foundation, either version 3 of the License, or (at your 
option) any later version.

Izvor is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
License for more details.

You should have received a copy of the GNU Lesser General Public
License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
-----------------------------------------------------------------------
Author: thunder, mikronac
*/

#include "gui/IzvorField.h"
#include "gui/Core.h"
#include "gui/CardManager.h"
#include "gui/Card.h"
#include "gui/CardContainer.h"
#include "gui/SceneRoot.h"
#include "gui/Table.h"
#include "gui/Animation.h"

#include <Ogre.h>

namespace Gui
{
	const Ogre::Real IzvorField::DISTANCE = 25.0f;
	const Ogre::Real IzvorField::TAP_DISTANCE = 5.0f;

	IzvorField::IzvorField(const Ogre::Vector3& position, const Ogre::Quaternion& orientation)
	{
		Ogre::SceneManager* scene_mgr = Core::getSingletonPtr()->getSceneRoot()->getSceneManager();
		mNode = scene_mgr->createSceneNode();
		Gui::Core::getSingletonPtr()->getSceneRoot()->getTable()->getSceneNode()->addChild(mNode);

		mNode->setPosition(position);
	}

	IzvorField::~IzvorField()
	{
		while(!mDecks.empty())
		{
			delete *mDecks.begin();
			mDecks.erase(mDecks.begin());
		}
	}

	//------------------------------------------------------------------------------
	void IzvorField::acceptCard( CardId card_id )
	{
		CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
		if(card->getOwner() != nullptr ) card->getOwner()->dispatchCard(card_id);
		card->setOwner(this);

        card->setRenderQueueGroupAndPriority(Gui::RQG_TABLE, Gui::RP_DEFAULT);

        Ogre::SceneNode* card_node = card->getSceneNode();

		IzvorCommon::Core::Color card_color = card->getColor();

		DecksList::iterator it = mDecks.begin();
		for(it; it != mDecks.end(); ++it)
		{
			if( (*it)->color == card->getColor() )
			{
				(*it)->cards_list.push_back(card.get());
				(*it)->scene_node->addChild(card->getSceneNode());
				break;
			}
		}

		if( it == mDecks.end() )
		{
			IzvorDeck* deck = new IzvorDeck(CardsList(1,card.get()), card->getColor(), mNode);
			mDecks.push_back( deck );
			deck->scene_node->addChild(card->getSceneNode());
		}

		//mNode->addChild(card->getSceneNode());

		reorganisePositions();    
	}
	////------------------------------------------------------------------------------
	void IzvorField::dispatchCard( CardId card_id )
	{
		bool found = false;
		DecksList::iterator deck_it = mDecks.end();
		for(DecksList::iterator itr = mDecks.begin(); itr != mDecks.end(); itr++)
		{
			CardsList::iterator card_itr = (*itr)->cards_list.end();
			for(CardsList::iterator cards_list_it = (*itr)->cards_list.begin(); cards_list_it != (*itr)->cards_list.end(); ++cards_list_it)
			{
			    if ((*cards_list_it)->getId() == card_id)
			    {
			        card_itr = cards_list_it;
			    }
			}
			
			if (card_itr != (*itr)->cards_list.end()) 
			{
				(*itr)->cards_list.erase(card_itr);
				deck_it = itr;
				found = true;
				break;
			}
		}
		
		if(!found) throw std::exception();		
		
		CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
		card->saveState();
		(*deck_it)->scene_node->removeChild(card->getSceneNode());
		
		if((*deck_it)->cards_list.empty())
		{
			delete *deck_it;
			mDecks.erase(deck_it);
		}
		reorganisePositions();  
	}
	////------------------------------------------------------------------------------
	//
	////------------------------------------------------------------------------------
	void IzvorField::reorganisePositions()
	{
		Ogre::Vector3 startPosition = calculateStartPosition( mDecks.size() );

		Ogre::Real position = 0.0f;

		DecksList::iterator next;
		for(DecksList::iterator itr = mDecks.begin(); itr != mDecks.end(); itr++)
		{
			next = itr;
			(*itr)->scene_node->setPosition( startPosition + Ogre::Vector3(position, 0.0f, 0.0f) );

			bool current_has_tapped_cards = hasTappedCards((*itr)->cards_list);
			if (++next != mDecks.end())        
			{
				bool next_has_tapped_cards = hasTappedCards((*next)->cards_list);
				if(current_has_tapped_cards && next_has_tapped_cards)
					position += DISTANCE + TAP_DISTANCE;
				else if ( (current_has_tapped_cards && !next_has_tapped_cards)  || (!current_has_tapped_cards && next_has_tapped_cards) )
					position += DISTANCE + TAP_DISTANCE;
				else
					position += DISTANCE;
			}

			
			// sort cardatos
			CardsList::iterator card_it = (*itr)->cards_list.begin();
			unsigned int i = 0;
			float step = 1.0f;

			for(card_it; card_it != (*itr)->cards_list.end(); ++card_it, ++i)
			{
                (*card_it)->getSceneNode()->setPosition(Ogre::Vector3(i*step,i*step*0.5f,0.0f));
                (*card_it)->getSceneNode()->setOrientation( Ogre::Quaternion::IDENTITY );
                (*card_it)->getSceneNode()->setScale( Gui::CardManager::CARD_SIZE );
                (*card_it)->getSceneNode()->setInitialState();

                SceneNodeState animation = (*card_it)->calculateAnimationPositionAndOrientation();

                new DragAnimation( (*card_it), animation.position, animation.orientation );
                (*card_it)->saveState();
			}

			//(*itr)->scene_node->setOrientation( mNode->getOrientation() );
			//(*itr)->scene_node->setInitialState();

   //         new DragAnimation( (*itr)->scene_node );
		}
	}


	Ogre::Vector3 Gui::IzvorField::calculateStartPosition( int num )
	{
		Ogre::Real wide = 0.0f;

		DecksList::iterator next;

		for(DecksList::iterator itr = mDecks.begin(); itr != mDecks.end(); itr++)
		{
			next = itr;

			bool current_has_tapped_cards = hasTappedCards((*itr)->cards_list);
			if (++next != mDecks.end())        
			{
				bool next_has_tapped_cards = hasTappedCards((*next)->cards_list);
				if(current_has_tapped_cards && next_has_tapped_cards)
					wide += DISTANCE + 2*TAP_DISTANCE;
				else if ( (current_has_tapped_cards && !next_has_tapped_cards)  || (!current_has_tapped_cards && next_has_tapped_cards ) )
					wide += DISTANCE + TAP_DISTANCE;
				else
					wide += DISTANCE;
			}
		}

		return Ogre::Vector3(-wide*0.5f, 0.0f, 0.0f);
	}

	bool IzvorField::hasTappedCards(const CardsList& cards_list)
	{
		CardsList::const_iterator it = cards_list.begin();
		for(it; it != cards_list.end(); ++it)
			if((*it)->isTap())
				return true;

		return false;
	}
};