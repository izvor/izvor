/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Thunder
*/

#include "gui/PlayingField.h"
#include "gui/Core.h"
#include "gui/SceneRoot.h"
#include "gui/CardManager.h"
#include "gui/Card.h"
#include "gui/Utility.h"

#include <algorithm>

namespace Gui
{
    const Ogre::Real PlayingField::DEFAULT_HORIZONTAL_STEP = Ogre::Real(15.0);
    const Ogre::Real PlayingField::DEFAULT_VERTICAL_STEP = Ogre::Real(20.0);
    
    PlayingField::PlayingField(Ogre::SceneManager* scene_manager, Ogre::Real width, Ogre::Real height,
        const Ogre::Vector3& position, const Ogre::Quaternion& orientation  /* = Ogre::Quaternion::IDENTITY */ )
        : mHeight(height), mWidth(width), mCurrentCol(0), mCurrentRow(0)
    {
        mSceneNode = scene_manager->getRootSceneNode()->createChildSceneNode(position,orientation);
        
        // Debug:
//        Ogre::Entity *ent = scene_manager->createEntity( Utility::getUniqueString("PlayingField"), "plane");
//        ent->setMaterialName("ground");
//        Ogre::SceneNode* node = mSceneNode->createChildSceneNode();
//        node->setScale(mWidth, mHeight, 1);
//        node->attachObject(ent);
        /////////

        resize( DEFAULT_HORIZONTAL_STEP, DEFAULT_VERTICAL_STEP );
    }

    PlayingField::~PlayingField()
    {

    }

    void PlayingField::acceptCard(CardId card_id)
    {
        try
        {
            CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);
            Ogre::Vector3 position = getNextPosition();
            mCardsList.push_back(card_id);

            Ogre::SceneNode* card_scene_node = card->getSceneNode();
            card_scene_node->getParentSceneNode()->removeChild(card_scene_node);
            mSceneNode->addChild( card_scene_node );
            card->getSceneNode()->setPosition(position);
        } catch (const std::exception &e)
        {
            GUI_LOG(e.what());
        }
    }

    void PlayingField::dispatchCard(CardId card_id)
    {
        CardIdList::iterator it = std::find(mCardsList.begin(), mCardsList.end(), card_id);
        if( it != mCardsList.end() )
        {
            // izbaci
            reposition();
        } else
        {
            Ogre::String log_msg("No card with that id: ");
            log_msg += card_id;
            Ogre::LogManager::getSingletonPtr()->logMessage(log_msg);
        }
    }
    
    Ogre::Vector3 PlayingField::getNextPosition()
    {
        if(mCardsList.size() >= mMaxCols*mMaxRows)
        {
            try
            {
                resize(mHorizontalStep / 1.25, mVerticalStep);
            }
            catch (const std::exception& e)
            {
                throw e;
            }
        }

        mCurrentCol = (mCardsList.size()) % (mMaxCols);
        mCurrentRow = (mCardsList.size()) / (mMaxCols);
        Ogre::Vector3 pos = getPositionAt(mCurrentCol, mCurrentRow);
        return pos;
    }

    Ogre::Vector3 PlayingField::getPositionAt(unsigned int col, unsigned int row)
    {
        Ogre::Real x_offset = ( mWidth - (mMaxCols - 1) * mHorizontalStep ) / 2.0;
        Ogre::Vector3 pos( col * mHorizontalStep - mWidth / 2.0 + x_offset, 
            row * (-mVerticalStep) + mHeight / 2.0, 0.0 );
        return pos;
    }
    
    void PlayingField::resize(Ogre::Real new_h_step, Ogre::Real new_w_step)
    {
       GUI_LOG("PlayingField::resize");

       mHorizontalStep = new_h_step;
       mVerticalStep = new_w_step;
       
       mMaxCols = mWidth / mHorizontalStep + 1;
       mMaxRows = mHeight / mVerticalStep + 1;
       
       if(mMaxCols)
       {
           if(mCurrentRow > mMaxRows)
           {
               resize( mHorizontalStep / 1.25, mVerticalStep );
           } 
       } else
       {
           throw std::exception();
       }
       
       reposition();
    }
    
    void PlayingField::reposition()
    {
        CardIdList::iterator it = mCardsList.begin();
        unsigned int col = 0; 
        unsigned int row = 0;
        for(it; it != mCardsList.end(); ++it)
        {
            CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(*it);
            card->getSceneNode()->setPosition( getPositionAt(col,row) );
            if( ++col > mMaxCols - 1 )
            {
                col = 0;
                ++row;
                assert (row-1 <= mMaxRows);
            } 
        }
    }   
}
