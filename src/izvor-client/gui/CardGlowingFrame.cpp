/*
---------------------------------------------------------------------------
      _                 _                   _   _
     (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___
     | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
     |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
                                      |___/  |__/
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the
    Free Software Foundation, either version 3 of the License, or (at your
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder
*/

#include "gui/CardGlowingFrame.h"
#include "gui/Core.h"
#include "gui/UpdatableManager.h"
#include "gui/Card.h"
#include "gui/CardManager.h"

namespace Gui
{

    CardGlowingFrame::CardGlowingFrame(boost::weak_ptr<Card> subject)
        : UpdatableCardProperty(subject), mTimeToLive(5.0)
    {
        if (CardPtr card = mCard.lock())
            std::cout << "Glowing particles light up the screen in the night on card: " << card->getId() << std::endl;
    }

    CardGlowingFrame::~CardGlowingFrame()
    {
        if (CardPtr card = mCard.lock())
            std::cout << "Particles stop glowing on card: " << card->getId() << std::endl;
    } 

    bool CardGlowingFrame::_update_impl(const Ogre::FrameEvent& evt)
    {
        mTimeToLive -= evt.timeSinceLastFrame;

        if(mTimeToLive <= 0)
        {
            return false;
        }
        return true;
    }

} /* namespace Gui */
