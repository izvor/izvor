/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: thunder, mikronac
*/

#include "gui/Card.h"
#include "gui/Animation.h"
#include "IzvorPCH.h"

#include <Ogre.h>


namespace Gui
{
    Card::Card( CardId id, Ogre::SceneNode* scene_node, Ogre::Entity* entity )
        : mNode(scene_node), mId(id), mEntity(entity), mIsTap(false)
    {
		IzvorCommon::Core::CardElement *ce = IzvorCommon::Core::ObjectManager::get().find< IzvorCommon::Core::CardElement >( id );
		if (!ce ) throw std::exception(); 
		mCardColor = ce->getBaseCard()->getBaseColor()->color();
        mSceneNodeState.position = mNode->getPosition();
        mSceneNodeState.orientation = mNode->getOrientation();

		mPriority = Gui::RP_DEFAULT;
    }
    //------------------------------------------------------------------------------
    Card::~Card()
    {
        while(!mCardProperties.empty())
        {
            (*mCardProperties.begin()).second->disable();
            mCardProperties.erase(mCardProperties.begin());
        }
    }

    //------------------------------------------------------------------------------
    void Card::saveState()
    {
        mSceneNodeState.position = mNode->_getDerivedPosition();
        mSceneNodeState.orientation = mNode->_getDerivedOrientation();
        mSceneNodeState.scale = mNode->getScale();
    }

    //------------------------------------------------------------------------------
    SceneNodeState Card::getSavedState()
    {
        return mSceneNodeState;
    }

    void Card::tap()
    {
        if (!mIsTap)
        {
	        mIsTap = true;
	        mTapAnimation = new TapAnimation(this);
	        mTapAnimation->setReverse(false);
        }
    }

    void Card::untap()
    {
        mIsTap = false;
        mTapAnimation->setReverse(true);
        mTapAnimation = nullptr;
    }
    //------------------------------------------------------------------------------

	void Card::addBuff(Buff* buff)
	{
		mBuffsList.push_back(buff);

		//buff->mSceneNode = mNode->createChildSceneNode();
		buff->mBillboardSet = mNode->getCreator()->createBillboardSet( );
		buff->mBillboardSet->setMaterialName(buff->getMaterialName());
		buff->mBillboardSet->setBillboardType(Ogre::BBT_POINT); // ili BBT_ORIENTED_COMMON, da stoje zalepljeni na kartu
		buff->mBillboardSet->setDefaultDimensions(30.0f/90.0f,21.5f/90.0f); // ovo treba bolje, zavisi od slike
//		buff->mSceneNode->setScale(0.01,0.01,0.01); // skaliranje je problem
		buff->mBillboard = buff->getBillboardSet()->createBillboard(Ogre::Vector3(0, 0, 0));
		mNode->attachObject(buff->getBillboardSet());

		reorganiseBuffs();
	}

	void Card::removeBuff(Buff* buff)
	{
		BuffsList::iterator it = std::find(mBuffsList.begin(), mBuffsList.end(), buff);
		if( it != mBuffsList.end() )
		{	
			mBuffsList.erase(it);
			reorganiseBuffs();
		}
	}

	void Card::reorganiseBuffs()
	{
		unsigned int i = 0;
		unsigned int n = NUMBER_OF_BUFFS_PER_ROW;
		BuffsList::iterator it = mBuffsList.begin();

		for(it; it != mBuffsList.end(); ++it, ++i)
		{
		
			Ogre::Vector3 position(((i<n)?-1:1)/2.0f, 0.5f, ((i%n+1) * 30.0f/n - 21.5f)/30.0f);
			(*it)->mBillboard->setPosition(position);
			std::cout << position << std::endl;
		}
	}

    Gui::SceneNodeState Card::calculateAnimationPositionAndOrientation()
    {
        SceneNodeState animation;

        animation.position = mNode->convertWorldToLocalPosition(mSceneNodeState.position);
        animation.orientation = mNode->convertWorldToLocalOrientation(mSceneNodeState.orientation);

        animation.position = animation.position * mNode->getScale();

        return animation;
    }

    void Card::setRenderQueueGroupAndPriority(Ogre::uint8 queueID, Ogre::ushort priority)
	{
		getEntity()->setRenderQueueGroupAndPriority(queueID, priority);
		
		BuffsList::iterator it = mBuffsList.begin();
		for(it; it != mBuffsList.end(); ++it)
		{
			(*it)->getBillboardSet()->setRenderQueueGroupAndPriority(queueID, priority);
		}

		mPriority = priority;
	}

}
