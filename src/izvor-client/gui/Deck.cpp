/*
--------------------------------------------------------------------------------
	  _                 _                   _   _     
	 (_)_____ _____ _ _(_)  _ __  __ _ __ _(_) (_)___ 
	 | |_ /\ V / _ \ '_| | | '  \/ _` / _` | | | / -_)
	 |_/__| \_/\___/_| |_| |_|_|_\__,_\__, |_|_/ \___|
									  |___/  |__/     
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: mikronac
*/
#include "gui/Common.h"

#include "gui/Animation.h"
#include "gui/Deck.h"
#include "gui/Core.h"
#include "gui/Hand.h"
#include "gui/Fan.h"
#include "gui/Table.h"
#include "gui/Card.h"
#include "gui/SceneRoot.h"
#include "gui/CardManager.h"


#include <Ogre.h>


Gui::Deck::Deck(Ogre::Vector3 position, Ogre::Quaternion orientation) 
{
    Ogre::SceneManager* scene_mgr = Core::getSingletonPtr()->getSceneRoot()->getSceneManager();

    mEntity = scene_mgr->createEntity( Ogre::SceneManager::PT_CUBE );

    mEntity->setRenderQueueGroupAndPriority( Gui::RQG_TABLE, Gui::RP_DEFAULT );
    mEntity->setQueryFlags( Gui::QM_NOT_SCAN );
    mEntity->setMaterialName("Material/Back");

    mNode = Core::getSingletonPtr()->getSceneRoot()->getTable()->getSceneNode()->createChildSceneNode();
    mNode->attachObject(mEntity);

    mNode->setPosition(position);
    mNode->setOrientation(orientation);

    mNode->setVisible(false);
}
//------------------------------------------------------------------------------
Gui::Deck::~Deck()
{

}
//------------------------------------------------------------------------------
void Gui::Deck::acceptCard(CardId card_id)
{
    mSize++;

    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);

    if(card->getOwner() != nullptr ) card->getOwner()->dispatchCard( card->getId() );

    card->setOwner(this);

    // Moze da se uradi da se pomera po y osi u zavisnosti od mesta
    // u spilu.
    card->getSceneNode()->setPosition(mNode->getPosition()); 
    card->getSceneNode()->setOrientation(Ogre::Quaternion::IDENTITY);

    SceneNodeState animation = card->calculateAnimationPositionAndOrientation();

    new DragAnimation(card.get(), animation.position, animation.orientation);

    card->saveState();

    card->hide();
    if (mSize > 0) mNode->setVisible(true);
}
//------------------------------------------------------------------------------
void Gui::Deck::dispatchCard(CardId card_id)
{
    CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard(card_id);

    card->show();

    mSize--;

    if (mSize < 0) mSize = 0;
    mNode->setScale( CardManager::CARD_SIZE * Ogre::Vector3(0.01f, mSize * 0.001f, 0.01f));

    if (mSize == 0) mNode->setVisible(false);
}
//------------------------------------------------------------------------------
void Gui::Deck::fill(const CardsIdList& cards_list )
{
    mSize = cards_list.size();

    for (CardsIdList::const_iterator itr = cards_list.begin(); itr != cards_list.end(); itr++)
    {
        CardPtr card = Core::getSingletonPtr()->getCardManager()->getCard( *itr );

        if (card->getOwner() != nullptr) card->getOwner()->dispatchCard( card->getId() );
        card->setOwner(this);

        card->hide();

        card->getSceneNode()->setPosition(mNode->getPosition());
        card->getSceneNode()->setOrientation(mNode->getOrientation());
        card->getSceneNode()->setScale(CardManager::CARD_SIZE);

        card->saveState();
    }

    mNode->setScale( CardManager::CARD_SIZE * Ogre::Vector3(0.01f, mSize * 0.001f, 0.01f));

    if (mSize == 0) mNode->setVisible(false);
    else mNode->setVisible(true);
}
//------------------------------------------------------------------------------