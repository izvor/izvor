/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorPCH.h"
#include "GuiThread.h"
#include "gui/Core.h"
#include "gui/CardManager.h"

using namespace std;

namespace IzvorCommon {
	namespace Core {
		class GameManager {};
	}
}

int main()
{
	IzvorCommon::Core::GameManager main;
	sLog.message( "Random", "Initing seed: RAND_MAX = %d.", RAND_MAX );
	srand( time( 0 ) );

	DB::Core::get().open( RES_PATH"/db/izvor_cards.db" );

	sLog.message( "RequestHandler", "Initializing RequestHandler." );
	IzvorCommon::RequestHandler::get();
	
	IzvorCommon::Core::Game *g = new IzvorCommon::Core::Game( &main );

	// IzvorCommon::Core::DeckParser::loadDeck( g->getPlayerOne()->getDeck(), RES_PATH"/decks/srce_divljine.xml" );
	IzvorCommon::Core::DeckParser::loadDeck( g->getPlayerOne()->getDeck(), RES_PATH"/decks/third_ed.xml" );
	//	Core::DeckParser::loadDeck( g->getPlayerTwo()->getDeck(), RES_PATH"/decks/vucje_carstvo.xml" );
	IzvorCommon::Core::DeckParser::loadDeck( g->getPlayerTwo()->getDeck(), RES_PATH"/decks/third_ed.xml" );
	g->getPlayerOne()->getLibrary()->loadFromDeck();
	g->getPlayerTwo()->getLibrary()->loadFromDeck();

	g->start();

	GuiThread *t = new GuiThread( g );
	t->start();

	g->getPlayerOne()->getHand()->print();
	g->getPlayerTwo()->getHand()->print();

	// ovo moramo pitati igraca
	g->getPlayerOne()->getManaPool()->add( IzvorCommon::Core::Color::ZELENA, 3 );
	g->getPlayerOne()->getManaPool()->add( IzvorCommon::Core::Color::BEZBOJNA, 1 );
	bool end = false;
	while( !end ) {
		cout << "0 - print\n1 - id input\n2 - stop game thread\n3 - draw card\n4+ - exit\n? ";
		
		int choice, id;
		IzvorCommon::Core::Creature *c;
		cin >> choice;
		switch( choice ) {
		case 0: 
			g->getPlayerOne()->getHand()->print();
			cout << "---\n";
			g->getBattlefield()->print();
			cout << "---\nPool\n";
			g->getPlayerOne()->getManaPool()->print();
			cout << "---\nStack\n";
			g->getStack()->print();
			break;
		case 1:
			cout << "id? ";
			cin >> id;
			if( g->getPlayerOne()->getHand()->play( id ) ) {
				cout << "done" << endl;
				g->getStack()->resolve();
			}
			else cout << "failed" << endl;
			break;
		case 2:
			g->signal();
			break;
		case 3:
			g->getPlayerOne()->getLibrary()->drawToHand();
			break;
		default:
			end = true;
		}			
	}

	cout << "Waiting for my children\n";
	g->join();
	t->join();
	cout << "Done\n";
	//	IzvorCommon::Core::ObjectManager::get().dump();
	delete t;
	delete g;

	IzvorCommon::Core::ObjectManager::get().dump();
	return 0;
}
