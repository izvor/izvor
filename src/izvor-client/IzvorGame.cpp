#include "PlayGameState.h"

#include "IzvorGame.h"

IzvorGame::IzvorGame( IzvorCommon::Core::Game* izvor_game )
{
    mEngineManager = Mge::EngineManager::getSingletonPtr();
    mGameStateManager = Mge::GameStateManager::getSingletonPtr();

	mGuiCore = new Gui::Core( izvor_game );
}

IzvorGame::~IzvorGame()
{

}


void IzvorGame::Initialise()
{
    mEngineManager->Initialise("Izvor");
	
    // We must put all possible states here. This can be solved.
    mGameStateManager->AddStateToManage(new PlayGameState());
    //////////////////////////////////////////////////////////////

	// Add meshes from Game (block this thread until libs are loaded!)
	Gui::Core::getSingletonPtr()->getGame()->initGui();
	// unblock game thread;
	Gui::Core::getSingletonPtr()->getGame()->signal();
    // test state for development;
    //	mGameStateManager->AddStateToManage(new TestGameState());
    //mGameStateManager->PushState("TestGameState");

    mGameStateManager->PushState("PlayGameState");
}

void IzvorGame::Shutdown()
{

}
