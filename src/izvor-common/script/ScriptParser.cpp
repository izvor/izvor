#include "IzvorCommonPCH.h"

#include "script/ScriptParser.h"
#include "core/Ability.h"
#include "core/Symbol.h"
#include "core/Effect.h"
#include "core/Cost.h"
#include "script/TriggerMgr.h"

using namespace IzvorCommon;
using namespace IzvorCommon::Core;

const char* ScriptParser::SCRIPT_PATH = RES_PATH"/scripts/";

bool ScriptParser::_parse( TiXmlNode *pParent )
{
    if( !pParent ) return false;
	
	// root tag = ability
    if( pParent->Type() != TiXmlNode::TINYXML_ELEMENT ||
        pParent->ValueStr() != "ability" ||
		!pParent->ToElement()->Attribute( "cardid" ) ||
		boost::lexical_cast< uint32 >( pParent->ToElement()->Attribute( "cardid" ) ) 
		!= mTarget->getBaseCard()->getDBID() ) 
		return false;

	for( TiXmlNode *pChild = pParent->FirstChild(); 
		 pChild; 
		 pChild = pChild->NextSibling() ) { // prvi nivo: static, activated, triggered

		// komentar
		if( pChild->Type() == TiXmlNode::TINYXML_COMMENT ) continue;

		// static
		else if( pChild->ValueStr() == "static" ) {
			StaticAbility* statics = mTarget->getStaticAbility();
			for( TiXmlNode *pGChild = pChild->FirstChild();
				 pGChild;
				 pGChild = pGChild->NextSibling() ) { // drugi nivo: staticke osobine
				if( pGChild->ValueStr() == "presretac" )
					statics->addAbility( StaticAbility::PRESRETAC );
				else if( pGChild->ValueStr() == "jurisnik" )
					statics->addAbility( StaticAbility::JURISNIK );
				else if( pGChild->ValueStr() == "ubrzanje" )
					statics->addAbility( StaticAbility::UBRZANJE );
				else if( pGChild->ValueStr() == "letac" )
					statics->addAbility( StaticAbility::LETAC );
				else if( pGChild->ValueStr() == "blokiraletace" )
					statics->addAbility( StaticAbility::BLOKIRA_LETACE );
				else if( pGChild->ValueStr() == "neblokira" )
					statics->addAbility( StaticAbility::NE_BLOKIRA );
			}
		}

		// active
		else if( pChild->ValueStr() == "active" ) {
			bool costParsed = false, effectListParsed = false;
			ActivatedAbility* active = new ActivatedAbility( mTarget );
			for( TiXmlNode *pGChild = pChild->FirstChild();
			 	 pGChild;
			 	 pGChild = pGChild->NextSibling() ) { // drugi nivo: aktivne osobine
				// cost
				if( pGChild->ValueStr() == "cost" ) {
					for( TiXmlNode *pCostElement = pGChild->FirstChild();
						 pCostElement;
						 pCostElement = pCostElement->NextSibling() ) { // treci nivo: cena
						if( pCostElement->ValueStr() == "tap" )
							active->getCost()->addSymbol( new TapSymbol() );
						else if( pCostElement->ValueStr() == "mana" ) {
							if( !pCostElement->ToElement()->Attribute( "ammount" ) )
								sLog.error( "ScriptParser::parse", "Missing ammount attribute in mana tag!" );
							ManaCost::parseStr( pCostElement->ToElement()->Attribute( "ammount" ),
												active->getCost() );
						}
						else sLog.warning( "ScriptParser::parse", "Unknown cost element: %s.", pCostElement->ValueStr().c_str() );
					}
					costParsed = true;
				}
				else if( pGChild->ValueStr() == "effectlist" ) {
					// effectlist
					for( TiXmlNode *pEffect = pGChild->FirstChild();
						 pEffect;
						 pEffect = pEffect->NextSibling() ) { // treci nivo: efekti
						if( pEffect->ValueStr() != "effect" ) {
							sLog.warning( "ScriptParser::parse", "Unknown effectlist tag: %s.", pEffect->ValueStr().c_str() );
							continue;
						}				
						if( !pEffect->ToElement()->Attribute( "id" ) ) {
							sLog.warning( "ScriptParser::parse", "No id attribute in effect, skipping." );
							continue;
						}
						
						uint32 effectID = boost::lexical_cast< uint32 >( pEffect->ToElement()->Attribute( "id" ) );
						Effect *e = new Effect( effectID );
						for( TiXmlAttribute *pAttrib = pEffect->ToElement()->FirstAttribute();
							 pAttrib;
							 pAttrib = pAttrib->Next() ) {
							if( pAttrib->Name() == std::string( "id" ) ) continue; // we used this info
							e->addArgument( pAttrib->Name(), pAttrib->Value() );
						}
						active->pushEffect( e );
					}
					effectListParsed = true;
				}
				else sLog.warning( "ScriptParser::parse", "Unknown active ability tag: %s.", pGChild->ValueStr().c_str() );
			}
			if( !effectListParsed || !costParsed ) {
				sLog.warning( "ScriptParser::parse", "Not enough info for active ability, skipping." );
				SafeDelete( active );
				return false;
			}
			mTarget->pushActivatedAbility( active );
		}
		// triggered
		else if( pChild->ValueStr() == "triggered" ) {
			bool effectListParsed = false, triggerParsed = false;
			TriggeredAbility* triggered = NULL;
			for( TiXmlNode *pGChild = pChild->FirstChild();
			 	 pGChild;
			 	 pGChild = pGChild->NextSibling() ) { // drugi nivo: aktivne osobine
				if( pGChild->ValueStr() == "trigger" ) {
					// mora biti prvo polje!
					// trigger

					if( triggered != NULL ) {
						sLog.error( "ScriptParser::parse", "Trigger needs to be first tag in triggered ability." );
						continue;
					}
					if( !pGChild->ToElement()->Attribute( "event" ) ) {
						sLog.error( "ScriptParser::parse", "Missing event attribute in trigger." );
						continue;
					}
					uint32 type = TriggerManager::EVT_NONE;
					// tokenize the str
					std::string eventTypeStr = pGChild->ToElement()->Attribute( "event" );
					boost::char_separator< char > sep( ";" );
					boost::tokenizer< boost::char_separator< char > > tokens( eventTypeStr, sep );
					BOOST_FOREACH( std::string s, tokens )
					{
						if( s == "DrawCard" ) 
							type |= TriggerManager::EVT_DRAW_CARD;
						else if( s == "PermanentEntersPlay" )
							type |= TriggerManager::EVT_PERMANENT_ENTERS_PLAY;
						else sLog.warning( "ScriptParser::parse", "Unknown event type string: %s.", s.c_str() );
					}
					sLog.warning( "tye", "r3r3: %d", type ); 
					triggered = new TriggeredAbility( mTarget, type );
					triggerParsed = true;
				}					   
				else if( pGChild->ValueStr() == "effectlist" ) {
					if( !triggerParsed ) {
						sLog.error( "ScriptParser::parse", "Trigger needs to be first tag in triggered ability." );
						continue;
					}
					// effectlist
					for( TiXmlNode *pEffect = pGChild->FirstChild();
						 pEffect;
						 pEffect = pEffect->NextSibling() ) { // treci nivo: efekti
						if( pEffect->ValueStr() != "effect" ) {
							sLog.warning( "ScriptParser::parse", "Unknown effectlist tag: %s.", pEffect->ValueStr().c_str() );
							continue;
						}				
						if( !pEffect->ToElement()->Attribute( "id" ) ) {
							sLog.warning( "ScriptParser::parse", "No id attribute in effect, skipping." );
							continue;
						}
						
						uint32 effectID = boost::lexical_cast< uint32 >( pEffect->ToElement()->Attribute( "id" ) );
						Effect *e = new Effect( effectID );
						for( TiXmlAttribute *pAttrib = pEffect->ToElement()->FirstAttribute();
							 pAttrib;
							 pAttrib = pAttrib->Next() ) {
							if( pAttrib->Name() == std::string( "id" ) ) continue; // we used this info
							e->addArgument( pAttrib->Name(), pAttrib->Value() );
						}
						triggered->pushEffect( e );
					}
					effectListParsed = true;
				}
				else sLog.warning( "ScriptParser::parse", "Unknown active ability tag: %s.", pGChild->ValueStr().c_str() );
			}
			if( !effectListParsed || !triggerParsed ) {
				sLog.warning( "ScriptParser::parse", "Not enough info for triggered ability, skipping." );
				SafeDelete( triggered );
				return false;
			}
			mTarget->pushTriggeredAbility( triggered );
		}

		else sLog.warning( "ScriptParser::parse", "Unknown tag: %s.", pChild->ValueStr().c_str() );
	}

	return true;
}
