/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "script/TriggerMgr.h"

#include "core/ZoneElements.h"
#include "core/BaseCard.h"
#include "core/ObjectMgr.h"

using namespace IzvorCommon;
using namespace IzvorCommon::Core;

TriggerManager::DrawCardEvent TriggerManager::s_mDrawCardP1;
TriggerManager::DrawCardEvent TriggerManager::s_mDrawCardP2;
TriggerManager::PermanentEntersPlayEvent TriggerManager::s_mPermanentEntersPlay;

void TriggerManager::init( Core::Game *game )
{
	s_mDrawCardP1.setParent( game->getPlayerOne()->getLibrary() );
	s_mDrawCardP1.mPlayerOneDrew = true;
	s_mDrawCardP2.setParent( game->getPlayerTwo()->getLibrary() );
	s_mDrawCardP2.mPlayerOneDrew = false;
	
	s_mPermanentEntersPlay.setParent( game->getBattlefield() );
}

void TriggerManager::GenericListener::eventOccured( DrawCardEvent *evt ) 
{
	sLog.error( "GenericListener::DrawCardEvent", "Not implemented stub!" );
}

void TriggerManager::GenericListener::eventOccured( PermanentEntersPlayEvent *evt ) 
{
	sLog.error( "GenericListener::PermanentEntersPlayEvent", "Not implemented stub!" );
}

void TriggerManager::onEventOccured( DrawCardEvent *evt, uint32 objectID )
{
	Permanent *owner = ObjectManager::get().find< Permanent >( objectID );
	assert( owner );
	sLog.error( "TriggerManager::onEventOccured", "Not implemented stub! DBID: %d.",
				owner->getBaseCard()->getDBID() );
}

void TriggerManager::onEventOccured( PermanentEntersPlayEvent *evt, uint32 objectID )
{
	Permanent *owner = ObjectManager::get().find< Permanent >( objectID );
	assert( owner );

	switch( owner->getBaseCard()->getDBID() ) {
	case 1654:
		// Dusebriznik
		if( evt->mEntered == owner )
			sLog.warning( "fee", "preskacem" );
		else if( evt->mEntered->getBaseCard()->isOfType( BaseCard::BICE ) )
			sLog.warning( "gege", "dodajem 10" );
		break;

	default:
		sLog.error( "TriggerManager::onEventOccured", "Not implemented stub: DBID: %d.", owner->getBaseCard()->getDBID() );
		break;		
	}
}

void TriggerManager::GenericListener::connect()
{
	if( mType & static_cast< uint32 >( TriggerManager::EVT_DRAW_CARD ) ) {
		EVT_CONNECT( TriggerManager::s_mDrawCardP1, *this );
		EVT_CONNECT( TriggerManager::s_mDrawCardP2, *this );
	}
	if( mType & static_cast< uint32 >( TriggerManager::EVT_PERMANENT_ENTERS_PLAY ) )
		EVT_CONNECT( TriggerManager::s_mPermanentEntersPlay, *this );
}

void TriggerManager::GenericListener::disconnect()
{
	if( mType & static_cast< uint32 >( TriggerManager::EVT_DRAW_CARD ) ) {
		EVT_DISCONNECT( TriggerManager::s_mDrawCardP1, *this );
		EVT_DISCONNECT( TriggerManager::s_mDrawCardP2, *this );
	}
	if( mType & static_cast< uint32 >( TriggerManager::EVT_PERMANENT_ENTERS_PLAY ) )
		EVT_DISCONNECT( TriggerManager::s_mPermanentEntersPlay, *this );
}
