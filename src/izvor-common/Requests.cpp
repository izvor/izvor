/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "Requests.h"
#include "core/ObjectMgr.h"
#include "core/Player.h"
#include "core/CardElement.h"
#include "core/Zones.h"

#include "gui/Core.h"
#include "gui/SceneRoot.h"
#include "gui/CardContainer.h"

using namespace IzvorCommon;
using namespace IzvorCommon::Core;

// bool Request::unmarshal()
// {
// 
// 	// to bane: nemoj brisati ove poruke dole u sLog-u, zgodne su za debug
// 	switch( mType ) {
// 	case REQ_NOP:
// 		return true;		
// 		
// 	// non-blocking GUI requests
// 	case REQ_GUI_CREATE_DECK: // nije nam potreban
// 		sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_CREATE_DECK request." );
// 		break;
// 	case REQ_GUI_DRAW_CARD:   // nije nam potreban
// 		sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_DRAW_CARD request." );
// 		break;
// 	case REQ_GUI_MOVE_CARD:
// 		sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_MOVE_CARD request." );
// 		break;
// 	// blocking GUI requests
// 	case REQ_GUI_CHOOSE_CARD:
// 		sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_CHOOSE_CARD request." );
// 		break;
// 	case REQ_GUI_CHOOSE_CARDS:
// 		sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_CHOOSE_CARDS request." );
// 		break;
// 
// 	default:
// 		sLog.error( "GuiRequest::unmarshal", "Unknown request type: %d.", 
// 					static_cast< uint32 >( mType ) );
// 		return false;
// 	}
// 
// 	return true;
// }

bool GuiCreateDeckRequest::marshal()
{
	if( mData ) return false;
	
	Player *player = ObjectManager::get().find< Player >( mPlayerID );
	if( !player ) return false;
	
	std::list< uint32 > *list = player->getLibrary()->getCardList();
	list->push_front( mPlayerID );
	
	mData = list;

	return true;
}

bool GuiDrawCardRequest::marshal()
{
	if( mData ) return false;

	CardElement *card = ObjectManager::get().find< CardElement >( mCardID );
	if( !card ) return false;

	std::list< uint32 > *list = new std::list< uint32 >();
	
	list->push_back( mCardID );
	list->push_back( card->getPlayerOwner()->getID() );

	mData = list;

	return true;
}

bool GuiMoveCardRequest::marshal()
{
	if( mData ) return false;

	CardElement *card = ObjectManager::get().find< CardElement >( mCardID );
	if( !card ) return false;

	std::list< uint32 > *list = new std::list< uint32 >();	
	list->push_back( mCardID );
	list->push_back( card->getPlayerOwner()->getID() );
	list->push_back( static_cast< uint32 >( mDest ) );

	mData = list;

	return true;
}

bool GuiMoveCardRequest::unmarshal()
{
    sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_MOVE_CARD request." );

    std::list< uint32 >::const_iterator itr = mData->begin();

    uint32 card_id = *itr++;
    Gui::CardContainer* card_container = Gui::Core::getSingletonPtr()->getSceneRoot()->getContainer(*itr++, Dest(*itr) );

    card_container->acceptCard(card_id);

    return true;
}
// @Dimitrije: draw card i create deck request nisu potrebni
//  create deck se vrsi prilikom inicijalizacije preko init parametara
//  a draw card preko GuiMoveCardRequest!
//  Dodavaj nove zahteve da ih popunjavam. Brisi kad procitas.


bool GuiDrawCardRequest::unmarshal()
{
    //sLog.message( "GuiRequest::unmarshal", "Got REQ_GUI_MOVE_CARD request." );

    std::list< uint32 >::const_iterator itr = mData->begin();

    uint32 card_id = *itr++;
    Gui::CardContainer* card_container = Gui::Core::getSingletonPtr()->getSceneRoot()->getContainer(*itr++, GuiMoveCardRequest::DEST_HAND );

    card_container->acceptCard(card_id);

    return true;
}
