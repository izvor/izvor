/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "ReqHandler.h"
#include "Requests.h"

#include "gui/Core.h"
#include "gui/CardManager.h"
#include "gui/SceneRoot.h"

using namespace IzvorCommon;

void RequestHandler::pushReq( Request *req )
{
	// acquire mutex lock
	boost::mutex::scoped_lock lock( mMutex );
	
	if( !req->marshal() ) {
		sLog.error( "RequestHandler::pushReq", "Error marshalling request type %d, skipping.",
					static_cast< uint32 >( req->getType() ) );
		return;
	}

	mQueue.push( req );
	// implicit release of lock at the end of
	// scope
}

void RequestHandler::process()
{
	// acquire mutex lock
	boost::mutex::scoped_lock lock( mMutex );
	
	if( mQueue.empty() ) return;

	Request *req = mQueue.front();

	if( !req->unmarshal() )
		// dump data here too?
		sLog.error( "RequestHandler::marshal", "Error unmarshalling request type: %d.",
					static_cast< uint32 >( req->getType() ) );
	
	SafeDelete( req );
	mQueue.pop();
}

RequestHandler::~RequestHandler()
{
	if( !mQueue.empty() ) sLog.warning( "RequestHandler", "Queue not empty (%d unprocessed elements).", mQueue.size() );

	while( !mQueue.empty() ) {
		SafeDelete( mQueue.front() );
		mQueue.pop();
	}
}
