/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/StackElement.h"
#include "core/Stack.h"

using namespace IzvorCommon::Core;

StackElement::StackElement( Stackable *element, Stack *parent, uint32 id )
	: Object( NULL, id ), // NULL owners for Players too!
	  mElement( element ),
	  mParent( parent ),
	  mPrevented( false )
{}

void Stackable::push( Stack *stack )
{
	stack->push( new StackElement( this, stack ) );
}
