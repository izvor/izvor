/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/ObjectMgr.h"
#include "core/Object.h"

using namespace IzvorCommon::Core;

uint32 ObjectManager::add( Object *object )
{
	// RAND_MAX is guaranteed to be at least 32767
	uint32 id = random( 32767 );
	while( mObjectHash.find( id ) != mObjectHash.end() )
		id = random( 32767 );

	mObjectHash[id] = object;
	sLog.message( "ObjectManager::add", "Added object %d.", id );
	return id;
}

void ObjectManager::addExisting( Object *object )
{
	assert( mObjectHash.find( object->getID() ) == mObjectHash.end() );
	mObjectHash[object->getID()] = object;
	sLog.message( "ObjectManager::add", "Added existing object %d.", object->getID() );
}

void ObjectManager::markInvalid( uint32 objectID )
{
	assert( mObjectHash[objectID] );
	mObjectHash[objectID] = NULL;
}

// TODO: make this thread safe! and make a thread that periodically
// does this
void ObjectManager::dump()
{
	uint32 count = 0;
	// not sure if iterators are invalidated after erasing, so i'll not use this
	// end = mObjectHash.end
	ObjectHash::iterator i = mObjectHash.begin();
	while( i != mObjectHash.end() ) {
		if( i->second == NULL ) {
			mObjectHash.erase( i++ );
			count++;
		}
		else ++i;
	}
		
	sLog.message( "ObjectManager::dump", "Freed %d object slots, %d slots in use.",
				  count, mObjectHash.size() );
}

void ObjectManager::remove( uint32 objectID )
{
	assert( mObjectHash[objectID] );
	mObjectHash.erase( objectID );
	sLog.message( "ObjectManager::add", "Deleted object %d.", objectID );
}

namespace IzvorCommon {
	namespace Core {
		template<>
		Object* ObjectManager::find< Object >( uint32 objectID ) const
		{
			ObjectHash::const_iterator ret = mObjectHash.find( objectID );
			if( mObjectHash.end() == ret ) {
				sLog.warning( "ObjectManager::find", "Not found %d.", objectID );
				return NULL;
			}
			return ret->second;
		}
	}
}
