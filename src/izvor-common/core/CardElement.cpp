/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/CardElement.h"
#include "core/CardSet.h"
#include "script/ScriptParser.h"
#include "core/Ability.h"

using namespace IzvorCommon::Core;

// used only once for each card in game
CardElement::CardElement( CardSet *owner, const BaseCard *parent, uint32 id )
	: Object( (Object*) owner, id ), mType( TYPE_UNDEFINED ),
	  mCard( parent ),
	  mStatics( new StaticAbility( this ) )
{
	ScriptParser sp( this );
	sp.parse();
}

// use this ctor for moving stuff around
CardElement::CardElement( CardElement& c, CardSet *newOwner )
	: Object( (Object*) newOwner, c.mID ), mType( c.mType ),
	  mCard( c.mCard ),
	  mStatics( c.mStatics )
{
	mStatics->setOwner( this );
	setStaticAbility( mStatics );
	c.setStaticAbility( NULL );

	mActives = c.mActives;
	for( std::vector< ActivatedAbility* >::iterator i = mActives.begin(),
			                                        end = mActives.end();
		 i != end;
		 ++i )
		(*i)->setOwner( this );
	c.mActives.clear();

	mTrigs = c.mTrigs;
	for( std::vector< TriggeredAbility* >::iterator i = mTrigs.begin(),
			                                        end = mTrigs.end();
		 i != end;
		 ++i )
		(*i)->setOwner( this );
	c.mTrigs.clear();
	
	// DISCONNECT triggers! (if it's going to GY or somewhere...)
}

void CardElement::connectTriggers()
{
	for( std::vector< TriggeredAbility* >::iterator i = mTrigs.begin(), end = mTrigs.end();
		 i != end;
		 ++i )
		(*i)->connect();
}

void CardElement::disconnectTriggers()
{
	for( std::vector< TriggeredAbility* >::iterator i = mTrigs.begin(), end = mTrigs.end();
		 i != end;
		 ++i )
		(*i)->disconnect();
}

CardElement::~CardElement()
{
	SafeDelete( mStatics );

	for( std::vector< ActivatedAbility* >::iterator i = mActives.begin(), end = mActives.end();
		 i != end;
		 ++i )
		SafeDelete( *i );

	for( std::vector< TriggeredAbility* >::iterator i = mTrigs.begin(), end = mTrigs.end();
		 i != end;
		 ++i )
		SafeDelete( *i );
}
