/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/CardMgr.h"
#include "core/BaseCard.h"

using namespace IzvorCommon::Core;

CardManager::~CardManager()
{
    for( CardMap::iterator i = mCache.begin();
		 i != mCache.end();
		 i++ )
        SafeDelete( i->second );
}

uint32 CardManager::getDBIDByName( const std::string& cardName ) const
{
    // matchinfo daje neku skalarnu ocenu pretrage, npr. ako se trazi Izvor Života,
    // query vraca:
    // Izvor Života, Druid Izvora, Izvorski Vuk i Zagađeno Izvorište
    // prvi rezultat ima najbolji matchinfo
    return boost::lexical_cast< uint32 >
        ( DB::Core::get().querySingle
		  ( "SELECT card_id FROM cards_fts4 WHERE cards_fts4 MATCH '" + cardName  +
			"' AND card_name LIKE '%" + cardName + "%' ORDER BY matchinfo(cards_fts4) "
			"ASC LIMIT 1 COLLATE NOCASE" )[ DB::ID ] );
}

const BaseCard* CardManager::getBaseCard( uint32 cardDBID )
{
    const BaseCard *pCard;
    if( !(pCard = findCached( cardDBID )) )
        pCard = fetch( cardDBID );

    if( !pCard ) throw ExcMsg( "CardManager::getBaseCard", "No such card: %d.", cardDBID );
    return pCard;
}

const BaseCard* CardManager::fetch( uint32 cardDBID )
{  
    DB::Record r;
    try {
        r = DB::Core::get().querySingle( "SELECT * from cards_fts4 WHERE card_id = " + 
										 boost::lexical_cast< std::string >( cardDBID ) );
    } catch( ExcMsg& ) { return NULL; }

    BaseCard *pCard = new BaseCard( r );
    mCache[cardDBID] = pCard;

    return pCard;
}

