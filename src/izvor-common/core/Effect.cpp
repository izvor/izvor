/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Effect.h"

using namespace IzvorCommon::Core;

void Effect::resolve()
{
	EffectManager::bigBadResolveEffect( this );
}


void Effect::print() const
{
	std::cout << "<effect " << mID << " >\n---\n";
	for( ArgumentMap::const_iterator i = mArgs.begin(), end = mArgs.end();
		 i != end;
		 ++i )
		std::cout << i->first << " -> " << i->second << std::endl;
	std::cout << "---\n</effect>\n";
}
