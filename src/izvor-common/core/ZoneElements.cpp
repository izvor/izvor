/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/ZoneElements.h"
#include "core/BaseCard.h"
#include "core/Ability.h"
#include "core/Game.h"
#include "core/Cost.h"
#include "core/Mana.h"
#include "core/CEFactory.h"

#include "ReqHandler.h"
#include "Requests.h"

using namespace IzvorCommon::Core;

bool HandElement::isPlayable() const
{
	Player *owner = const_cast< HandElement* >( this )->CardElement::getPlayerOwner();

	if( mCard->isOfType( BaseCard::IZNENADJENJE ) || 
		mStatics->hasAbility( StaticAbility::PRESRETAC ) )
		return owner->hasPriority();

	return (owner->getGame()->getState() == Game::STATE_MAIN1 ||
			owner->getGame()->getState() == Game::STATE_MAIN2) &&
		    owner->hasPriority() && owner->getGame()->getStack()->isEmpty();
}

void HandElement::resolve()
{
	if( mCard->isOfType( BaseCard::IZNENADJENJE ) ||
		mCard->isOfType( BaseCard::VRADZBINA ) ) {
		if( mActives.size() == 1 )
			mActives[0]->resolve();
		else sLog.error( "HandElement", "More than 1 active ability or no active abilities! "
						                "Not implemented stub." );
		return;
	}
	// permanents
	// insert into the game and connect to events
	else { 
		Permanent *perm = CardElementFactory::createPermanent
			( this, getPlayerOwner()->getGame()->getBattlefield() );
		getPlayerOwner()->getGame()->getBattlefield()->addCardElement( perm );
		perm->connectTriggers();

		TriggerManager::s_mPermanentEntersPlay.mEntered = perm;
		TriggerManager::s_mPermanentEntersPlay.notify();

		sReqHndl.pushReq( new GuiMoveCardRequest( perm->getID(),
												  GuiMoveCardRequest::DEST_PERMANENTS ) );
	}		
}

bool HandElement::cleanup( )
{
	if( mCard->isOfType( BaseCard::IZNENADJENJE ) ||
		mCard->isOfType( BaseCard::VRADZBINA ) )
		// dump it to GY
		getPlayerOwner()->getGraveyard()->addCardElement
			( new GraveyardElement( this ) );
	
	// delete me after this, return true
	return true;
}

bool HandElement::prevented()
{
	// dump to GY
	getPlayerOwner()->getGraveyard()->addCardElement
		( new GraveyardElement( this ) );

	// delete me after this
	return true;
}

bool HandElement::play() 
{
	if( mCard->isOfType( BaseCard::IZVOR ) ) playAsSource();

	if( !isPlayable() ) {
		sLog.warning( "HandElement::play", "Not playable." );
		return false;
	}
	if( !mCard->getBaseManaCost()->isEmpty() )
		// Pay up
		if( !mCard->getBaseManaCost()->pay( getPlayerOwner() ) ) return false;

	getPlayerOwner()->getHand()->removeCardElement( this );
	push( getPlayerOwner()->getGame()->getStack() );

	return true;
}

bool HandElement::playAsSource()
{
	Player *owner = getPlayerOwner();

	// not pushing onto the stack
	if( (owner->getGame()->getState() == Game::STATE_MAIN1 ||
		 owner->getGame()->getState() == Game::STATE_MAIN2) &&
		owner->hasPriority() && owner->getGame()->getStack()->isEmpty() &&
		owner->canPlaySourceThisTurn() ) {
		Permanent *perm = CardElementFactory::createPermanent( this, owner->getGame()->getBattlefield(), true );
		owner->getHand()->removeCardElement( this );
		owner->getGame()->getBattlefield()->addCardElement( perm );
		
		TriggerManager::s_mPermanentEntersPlay.mEntered = perm;
		TriggerManager::s_mPermanentEntersPlay.notify();
		return true;
	}

	return false;
}

void Permanent::sacrifice()
{
	sLog.warning( "Permanent::sacrifice", "Not implemented stub." );
}
