/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/CEFactory.h"
#include "core/BaseCard.h"
#include "core/Permanents.h"

using namespace IzvorCommon::Core;

Permanent* CardElementFactory::createPermanent( CardElement *ce, Battlefield *newOwner,
												bool asSource )
{
	if( asSource || ce->getBaseCard()->isOfType( BaseCard::IZVOR ) )
		return new Source( ce, newOwner );

	Permanent *pe;

	switch( ce->getBaseCard()->getBaseType() )
	{
		// TODO: ARTEFAKT | BICE
		// sredi sve ovo kao i BICE
	case BaseCard::ARTEFAKT:
		pe = new Artefact( newOwner, ce->getBaseCard(), ce->getID() );
		break;

	case BaseCard::BICE:
		pe = new Creature( ce, newOwner );
		break;

	case BaseCard::PROMENA:
		pe = new Aura( newOwner, ce->getBaseCard(), ce->getID() );
		break;

	default:
		pe = NULL;
		sLog.error( "CardElementFactory::createPermanent", 
					"Wrong BaseCard type, cannot create Permanent." );
	}

	return pe;
}
