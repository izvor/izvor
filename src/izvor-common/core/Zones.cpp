/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Zones.h"
#include "core/ZoneElements.h"
#include "core/CEFactory.h"
#include "core/Deck.h"
#include "core/Permanents.h"
#include "core/CardMgr.h"

#include "script/TriggerMgr.h"

#include "ReqHandler.h"
#include "Requests.h"

using namespace IzvorCommon::Core;

void Library::shuffle()
{
	std::ptrdiff_t (*myRndGen)( std::ptrdiff_t ) = random;
	std::random_shuffle( mCards.begin(), mCards.end(), myRndGen );
}

void Library::loadFromDeck()
{
	Deck *deck = this->getPlayerOwner()->getDeck();
	for( Deck::DeckContainer::iterator i = deck->mCards.begin();
		 i != deck->mCards.end();
		 i++ ) {
		const BaseCard *base = CardManager::get().getBaseCard( i->first );
		for( uint32 j = 0; j < i->second; j++ )
			addCardElement( new LibraryElement( this, base ) );
	}

	shuffle();
	sLog.message( "Library", "Loaded deck %s to library.", deck->getName().c_str() );
}

void Library::drawToHand( uint32 count )
{
	for( uint32 i = 0; i < count; i++ ) {
		if( !getSize() ) {
			m_LibraryEmptyEvt.notify();
			return;
		}

		LibraryElement *le = dynamic_cast< LibraryElement* >( mCards.back() );
		HandElement *he = new HandElement( le );
		getPlayerOwner()->getHand()->addCardElement( he );
		
		sReqHndl.pushReq( new GuiDrawCardRequest( he->getID() ) );

		mCards.pop_back();
		SafeDelete( le );

		TriggerManager::s_mDrawCardP1.mDrawn = he;
		TriggerManager::s_mDrawCardP1.notify();
	}
}	   

bool Hand::play( uint32 ceID, bool asSource )
{
	HandElement *card;
	if( !(card = (HandElement*) getCardElement( ceID )) ) return false;
	
	return (asSource) ? card->playAsSource() : card->play();
}

void Battlefield::addCardElement( Permanent *pe )
{
	CardSet::addCardElement( pe );
	m_PermanentEntersPlayEvt.pEntered = pe;
	m_PermanentEntersPlayEvt.notify();
}

void Battlefield::untapAll( Player *ctrl )
{
	for( CardSet::CardContainer::iterator i = mCards.begin();
		 i != mCards.end();
		 i++ ) {
		Permanent *p = dynamic_cast< Permanent* >( *i );
		if( ctrl == p->getController() )
			p->untap();
	}
}

void Battlefield::checkCreatureToughness()
{
	for( CardSet::CardContainer::iterator i = mCards.begin();
		 i != mCards.end();
		 i++ ) {
		// dynamic_cast will return NULL when working with pointers
		// or will throw a std::bad_cast exception if it fails
		Creature *creature = dynamic_cast< Creature* >( *i );
		// if( creature && creature->getToughness() < 0 ) {
		// 	getPlayerOwner()->getGraveyard()->addCardElement( creature );
		// 	removeCardElement( creature );
		// }
	}
}

