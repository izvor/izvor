/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/DeckParser.h"
#include "core/Deck.h"
#include "core/CardElement.h"
#include "core/CardMgr.h"
#include "core/BaseCard.h"

using namespace IzvorCommon::Core;

bool DeckParser::save()
{
    if( mDeck->isEmpty() ) return false;

    TiXmlComment *c;

    mXmlDoc = new TiXmlDocument;
    TiXmlDeclaration *decl = new TiXmlDeclaration( "1.0", "UTF-8", "yes" );
    mXmlDoc->LinkEndChild( decl );

    TiXmlElement *root = new TiXmlElement( "deck" );
    mXmlDoc->LinkEndChild( root );

    if( !mDeck->mName.empty() ) root->SetAttribute( "name", mDeck->mName );
    if( !mDeck->mAuthor.empty() ) root->SetAttribute( "author", mDeck->mAuthor );
    if( !mDeck->mDescription.empty() ) {
        TiXmlElement *descr = new TiXmlElement( "description" );
        descr->LinkEndChild( new TiXmlText( mDeck->mDescription ) );
        root->LinkEndChild( descr );
    }

    TiXmlElement *md = new TiXmlElement( "maindeck" );
    c = new TiXmlComment();
    c->SetValue(" bića ");
    md->LinkEndChild( c );

    root->LinkEndChild( md );

    // cuvamo karte koje smo vec ubacili
    std::list< uint32 > IDs;

    // bica
    for( Deck::DeckContainer::const_iterator i = mDeck->mCards.begin();
		 i != mDeck->mCards.end();
		 i++ ) {
		const BaseCard *base = CardManager::get().getBaseCard( i->first );
		if( base->isOfType( BaseCard::BICE ) &&
			std::find( IDs.begin(), IDs.end(), i->first ) == IDs.end() ) {

			TiXmlElement *card = new TiXmlElement( "card" );
			card->SetAttribute( "id", boost::lexical_cast< std::string >( i->first ) );

			uint8 cardCount = i->second;
			if( cardCount > 1 ) 
				card->SetAttribute( "count", boost::lexical_cast< std::string >( cardCount ) );

			c = new TiXmlComment();
			c->SetValue( ' ' + base->getInfo()->getName() + ' ');
			md->LinkEndChild( c );
			md->LinkEndChild( card );
			
			IDs.push_back( i->first );
		}
    }
	
    c = new TiXmlComment( " ostale magije " );
    md->LinkEndChild( c );

    IDs.clear();

    // ostale magije
    for( Deck::DeckContainer::const_iterator i = mDeck->mCards.begin();
		 i != mDeck->mCards.end();
		 i++ ) {
		const BaseCard *base = CardManager::get().getBaseCard( i->first );
		if( !( base->isOfType( BaseCard::BICE ) ) &&
			std::find( IDs.begin(), IDs.end(), i->first ) == IDs.end() ) {

			TiXmlElement *card = new TiXmlElement( "card" );
			card->SetAttribute( "id", boost::lexical_cast< std::string >( i->first ) );

			uint8 cardCount = i->second;
			if( cardCount > 1 )
				card->SetAttribute( "count", boost::lexical_cast< std::string >( cardCount ) );

			c = new TiXmlComment();
			c->SetValue( ' ' + base->getInfo()->getName() + ' ' );

			md->LinkEndChild( c );
			md->LinkEndChild( card );

			IDs.push_back( i->first );
		}
    }

    // sidedeck
    if( !mDeck->mSideCards.empty() ) {
        TiXmlElement *sd = new TiXmlElement( "sidedeck" );
        root->LinkEndChild( sd );    
        IDs.clear();

        for( Deck::DeckContainer::const_iterator i = mDeck->mSideCards.begin();
			 i != mDeck->mSideCards.end();
			 i++ ) {
			if( std::find( IDs.begin(), IDs.end(), i->first ) == IDs.end() ) {

				TiXmlElement *card = new TiXmlElement( "card" );
				card->SetAttribute( "id", boost::lexical_cast< std::string >( i->first ) );

				uint8 cardCount = i->second;
				if( cardCount > 1 )
					card->SetAttribute( "count", boost::lexical_cast< std::string >( cardCount ) );
				
				c = new TiXmlComment();
				c->SetValue( ' ' + CardManager::get().getBaseCard( i->first )->getInfo()->getName() + ' ' );

				sd->LinkEndChild( c );
				sd->LinkEndChild( card );

				IDs.push_back( i->first );
			}
        } 
    }

	return mXmlDoc->SaveFile( mXmlFile );
}

bool DeckParser::_parse( TiXmlNode *pParent )
{
    if( !pParent ) return false;

    // root tag = deck
    if( pParent->Type() != TiXmlNode::TINYXML_ELEMENT ||
        pParent->ValueStr() != "deck" ) return false;

    if( pParent->ToElement()->Attribute( "name" ) )
        mDeck->mName = pParent->ToElement()->Attribute( "name" );
    if( pParent->ToElement()->Attribute( "author" ) )
        mDeck->mAuthor = pParent->ToElement()->Attribute( "author" );

    for( TiXmlNode *pChild = pParent->FirstChild(); 
		 pChild; 
		 pChild = pChild->NextSibling() ) { // prvi nivo: description, maindeck, sidedeck, komentari

		// komentar
		if( pChild->Type() == TiXmlNode::TINYXML_COMMENT ) continue;

		// descr
		if( pChild->ValueStr() == "description" ) {
			if( pChild->FirstChild()->Value() )
				mDeck->mDescription = pChild->FirstChild()->ValueStr();
			else return false;
		}

		// maindeck ili sidedeck
		else if( pChild->ValueStr() == "maindeck" || pChild->ValueStr() == "sidedeck" ) {
			for( TiXmlNode *pCard = pChild->FirstChild();
				 pCard;
				 pCard = pCard->NextSibling() ) { // drugi nivo: card, komentari

				// komentar
				if( pCard->Type() == TiXmlNode::TINYXML_COMMENT ) continue;

				// card
				if( pCard->Type() != TiXmlNode::TINYXML_ELEMENT ||
					pCard->ValueStr() != "card" ) return false;
				if( pCard->ToElement()->Attribute( "id" ) == NULL ) return false;

				uint32 count = 1;
				uint32 cardDBID = boost::lexical_cast< uint32 >( pCard->ToElement()
																 ->Attribute( "id" ) );
				if( pCard->ToElement()->Attribute( "count" ) != NULL )
					count = boost::lexical_cast< uint32 >( pCard->ToElement()->Attribute( "count" ) );
				if( count > 4 || count < 1 ) return false;
				if( pChild->ValueStr() == "maindeck" ) mDeck->addCard( cardDBID, count );
				else mDeck->addSideCard( cardDBID, count );
			}
		}
		else return false;
    }

    // proveri da li je spil prazan i da li ima manje od 40 karti
    //if( mDeck->isEmpty() || mDeck->getDeck().size() < 40 ) return false;
    // zbog testiranja, posle cu vratiti
    if( mDeck->isEmpty() ) return false;

    return true;
}



