/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "core/Color.h"

using namespace IzvorCommon::Core;

std::tr1::unordered_map<const Color::Type, const std::string, boost::hash< const Color::Type > > Color::sTypeName = boost::assign::map_list_of
    ( BEZBOJNA,  "Bezbojna"  )
    ( CRVENA,    "Crvena"    )
    ( ZELENA,    "Zelena"    )
    ( PLAVA,     "Plava"     ) 
    ( ZUTA,      "Žuta"      )
    ( CRNA,      "Crna"      )
    ( NEPOZNATA, "Nepoznata" );

std::tr1::unordered_map<const Color::Type, const char, boost::hash< const Color::Type > > Color::sColorCode = boost::assign::map_list_of
    ( BEZBOJNA, 'B' )
    ( CRVENA,   'R' )
    ( ZELENA,   'Z' )
    ( PLAVA,    'P' )
    ( ZUTA,     'T' )
    ( CRNA,     'C' );

Color::Color( uint8 color )
    : mColor( color )
{
    if( color > SIZE ) {
        mColor = NEPOZNATA;
        sLog.warning( "Color", "Unknown color code: %d.", color );
    }
}

// std::ostream& operator<<( std::ostream& os, const Color& c )
// {

//     if( !c.mColor ) { os << Color::sTypeName[Color::NEPOZNATA]; return os; }
//     for( uint8 i = 0; i < Color::NUM; i++ )
//         if( c.hasColor( Color::getTypeByID( i ) ) )
//             os << Color::sTypeName[Color::getTypeByID( i )] << ' ';

//     return os;
// }

Color::Type Color::getTypeByCode( char code )
{
    switch( code ) {
    case 'B':
        return BEZBOJNA;
    case 'R':
        return CRVENA;
    case 'Z':
        return ZELENA;
    case 'P':
        return PLAVA;
    case 'T':
        return ZUTA;
    case 'C':
        return CRNA;
    }

    return NEPOZNATA;
}

Color::Type Color::getTypeByID( uint8 id )
{
    switch( id ) {
    case 0:
        return BEZBOJNA;
    case 1:
        return CRVENA;
    case 2:
        return ZELENA;
    case 3:
        return PLAVA;
    case 4:
        return ZUTA;
    case 5:
        return CRNA;
    }

    return NEPOZNATA;
}

std::string Color::getDescriptionStr() const
{
	std::string res;
	for( uint8 i = 0; i < NUM; i++ ) {
		Type colorType = getTypeByID( i );
		if( mColor & static_cast< uint8 >( colorType ) )
			res += getTypeStr( colorType ) + ' ';
	}

	return res;
}
