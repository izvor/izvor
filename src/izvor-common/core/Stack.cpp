/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Stack.h"
#include "core/StackElement.h"

using namespace IzvorCommon::Core;

void Stack::resolve()
{
	while( !mStack.empty() ) {
		StackElement *top = mStack.back();
		assert( top );

		if( top->isPrevented() ) top->prevented();
		else top->resolve();
		
		if( !top->isPrevented() ) top->cleanup();

		mStack.pop_back();
		SafeDelete( top );
	}
}

void Stack::print() const
{
	for( std::vector< StackElement* >::const_iterator i = mStack.begin(), end = mStack.end();
		 i != end;
		 ++i )
		std::cout << (*i)->getID();
}
