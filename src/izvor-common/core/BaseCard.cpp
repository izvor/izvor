/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic, thunder
*/

#include "IzvorCommonPCH.h"

#include "core/BaseCard.h"
#include "core/Color.h"
#include "core/Cost.h"

using namespace IzvorCommon::Core;

// ------------------- BaseCard::Info ------------------- //
std::tr1::unordered_map< const BaseCard::Info::Rarity, const std::string, 
						 boost::hash< const BaseCard::Info::Rarity > >
BaseCard::Info::sRarityName = boost::assign::map_list_of
    ( ZLATNA,            "Zlatna"    )
    ( SREBRNA,           "Srebrna"   )
    ( OBICNA,            "Obična"    )
    ( NEPOZNATA_RETKOST, "Nepoznata" );

std::tr1::unordered_map< const BaseCard::Info::Edition, const std::string, 
						 boost::hash< const BaseCard::Info::Edition > >
BaseCard::Info::sEditionName = boost::assign::map_list_of
    ( IZVORISTE,         "Izvorište"        )
    ( PLEMENSKI_RATOVI,  "Plemenski ratovi" )
    ( DOBA_SAVEZA,       "Doba Saveza"      )
    ( DOBA_MISTERIJA,    "Doba Misterija"   )
    ( TRECA_EDICIJA,     "Treća edicija"    )
    ( NEPOZNATA_EDICIJA, "Nepoznata"        );

std::tr1::unordered_map< const BaseCard::Info::Edition, const std::string, 
						 boost::hash< const BaseCard::Info::Edition > >
BaseCard::Info::sEditionImgPath = boost::assign::map_list_of
    ( IZVORISTE,         RES_PATH"/img/izvoriste/"        )
    ( PLEMENSKI_RATOVI,  RES_PATH"/img/plemenski_ratovi/" )
    ( DOBA_SAVEZA,       RES_PATH"/img/doba_saveza/"      )
    ( DOBA_MISTERIJA,    RES_PATH"/img/doba_misterija/"   )
    ( TRECA_EDICIJA,     RES_PATH"/img/treca_edicija/"    )
    ( NEPOZNATA_EDICIJA, ""                               );

std::tr1::unordered_map< const BaseCard::Info::Edition, const uint32, 
						 boost::hash< const BaseCard::Info::Edition > >
BaseCard::Info::sEditionSize = boost::assign::map_list_of
    ( IZVORISTE,         400 )
    ( PLEMENSKI_RATOVI,  390 )
    ( DOBA_SAVEZA,       216 )
    ( DOBA_MISTERIJA,    140 )
    ( TRECA_EDICIJA,     375 )
    ( NEPOZNATA_EDICIJA, 0   );

BaseCard::Info::Info( const DB::Record& r, BaseCard *parent )
    : mEdition( NEPOZNATA_EDICIJA ),
	  mReprintEdition( NEPOZNATA_EDICIJA ),
	  mNumber( 0 ),
	  mReprintNumber( 0 ),
	  mParent( parent )
{
	decode( r );
}

void BaseCard::Info::decode( const DB::Record& r )
{
	mName = r[DB::NAME];
    mRarity = getRarityByID( boost::lexical_cast< uint32 >( r[DB::RARITY] ) );
    mImageAuthor = r[DB::IMAGE_AUTHOR];
    mLore = r[DB::LORE];
	mAbilityStr = r[DB::ABILITY];

    // first try to get ID from parent card
    if( mParent && mParent->mDBID ) this->addEdition( mParent->mDBID );
    else this->addEdition( boost::lexical_cast< uint32 >( r[DB::ID] ) );

    uint32 reprint_id = boost::lexical_cast< uint32 >( r[DB::REPRINT_ID] );
    if( reprint_id ) this->addEdition( reprint_id );
}

BaseCard::Info::Rarity BaseCard::Info::getRarityByID( uint8 id )
{
    switch( id ) {
    case 0:
        return OBICNA;
    case 1:
        return SREBRNA;
    case 2:
        return ZLATNA;
    }

    return NEPOZNATA_RETKOST;
}

BaseCard::Info::Edition BaseCard::Info::getEditionByID( uint8 id )
{
    switch( id ) {
    case 0:
        return IZVORISTE;
    case 1:
        return PLEMENSKI_RATOVI;
    case 2:
        return DOBA_SAVEZA;
    case 3:
        return DOBA_MISTERIJA;
    case 4:
        return TRECA_EDICIJA;
    }

    return NEPOZNATA_EDICIJA;
}

uint8 BaseCard::getTypeByID( uint8 type_id )
{
    switch( type_id ) {
    case 0:
        return IZVOR;
    case 1:
        return ARTEFAKT;
    case 2:
        return ARTEFAKT | BICE;
    case 3:
        return BICE;
    case 4:
        return IZNENADJENJE;
    case 5:
        return VRADZBINA;
    case 6:
        return PROMENA;
    }

    return NEPOZNAT_TIP;
}
// ------------------- !BaseCard::Info ------------------- //
// ------------------- BaseCard ------------------- //

std::tr1::unordered_map< const BaseCard::Type, const std::string, 
						 boost::hash< const BaseCard::Type > > 
BaseCard::sTypeName = boost::assign::map_list_of
    ( IZVOR,         "Izvor"         )
    ( ARTEFAKT,      "Artefakt"      )
    ( BICE,          "Biće"          )
    ( IZNENADJENJE,  "Iznenađenje"   )
    ( VRADZBINA,     "Vradžbina"     )
    ( PROMENA,       "Promena"       )
    ( NEPOZNAT_TIP,  "Nepoznat"      );

void BaseCard::decode( const DB::Record& r )
{
	mDBID = boost::lexical_cast< uint32 >( r[DB::ID] );
	mBaseType = getTypeByID( boost::lexical_cast< uint32 >( r[DB::PRIMARY_TYPE] ) );
  
	mBaseSecondaryType.clear();
	std::istringstream iss( r[DB::SECONDARY_TYPE] );
	std::copy( std::istream_iterator< std::string >(iss), 
			   std::istream_iterator< std::string >(), 
			   std::back_inserter< std::list< std::string > >( mBaseSecondaryType ));

	SafeDelete( mBaseColor );
	SafeDelete( mBaseCost );
	SafeDelete( mInfo );

	mBaseColor = new Color( boost::lexical_cast< uint32 >( r[DB::COLOR] ) );
	mBaseCost  = new ManaCost( r, this );
	mInfo      = new Info( r, this );
  
	if( BaseCard::BICE & mBaseType ) {
		mBasePower = boost::lexical_cast< int32 >( r[DB::ATTACK] );
		mBaseToughness = boost::lexical_cast< int32 >( r[DB::DEFENSE] );
	}
}

BaseCard::~BaseCard()
{
    SafeDelete( mBaseColor );
    SafeDelete( mBaseCost );
    SafeDelete( mInfo );
}

std::string BaseCard::getBaseSecondaryTypeStr() const
{
    std::string ret;
    for( std::list<std::string>::const_iterator i = mBaseSecondaryType.begin();
		 i != mBaseSecondaryType.end();
		 i++ )
        ret += *i + ' ';

    return ret;
} 

// prebaci u kartu
bool BaseCard::isOfType( const std::string& type ) const
{
    // proveri primarne
    for( uint8 i = 0; i < TYPE_NUM; i++ )
        if( type == getTypeStrByID( i ) )
            return mBaseType == getTypeByID( i );

    // proveri sekundarne
    for( std::list< std::string >::const_iterator i = mBaseSecondaryType.begin();
		 i != mBaseSecondaryType.end();
		 i++ )
        if( *i == type )
            return true;
	
    return false;
}

// ------------------- !BaseCard ------------------- //
