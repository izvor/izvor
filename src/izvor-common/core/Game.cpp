/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Game.h"
#include "core/CardElement.h"
#include "core/ZoneElements.h"
#include "core/BaseCard.h"
#include "ReqHandler.h"
#include "Requests.h"

#include "gui/Common.h"
#include "gui/Core.h"
#include "gui/CardManager.h"

using namespace IzvorCommon::Core;

uint32 Game::s_mGameID = 0;

Game::StateAction Game::mStateActions[ STATE_MAX ] = { 
	&Game::doPreparation,
	&Game::doUntap,
	&Game::doUpkeep,
	&Game::doDraw,
	&Game::doMainOne,
	&Game::doCombatBeginning,
	&Game::doCombatDeclAtt,
	&Game::doCombatDeclBlk,
	&Game::doCombatExtraDmg,
	&Game::doCombatDmg,
	&Game::doCombatEnding,
	&Game::doMainTwo,
	&Game::doEndStep,
	&Game::doCleanup,
	&Game::doFinished 
};

Game::~Game()
{
	SafeDelete( mPlayerOne ); 
	SafeDelete( mPlayerTwo );

	SafeDelete( mStack );
	SafeDelete( mBattlefield );
	SafeDelete( mExile );
}

void Game::run()
{
	while( true ) {
		mCurrentState = runState();
		if( STATE_MAX == mCurrentState ) {
			// mParent->notifyFinished( this ); <-- implement it
			break;
		}
	}
	sLog.message( "Game", "Game ending." );
}

void Game::initGui()
{
	// NB: called from another thread (GuiThread)
	Gui::GameInitParameters& params = Gui::Core::getInitParameters();
	params.sPlayerHand = mPlayerOne->getHand()->getCardList();
	params.sOpponentHand = mPlayerTwo->getHand()->getCardList();
	params.sPlayerLibrary = mPlayerOne->getLibrary()->getCardList();
	params.sOpponentLibrary = mPlayerTwo->getLibrary()->getCardList();
	params.sPlayerId = mPlayerOne->getID();
	params.sOpponentId = mPlayerTwo->getID();
}

Game::State Game::doPreparation() 
{ 
	boost::mutex::scoped_lock lock( mMutex );

	// set active player and player priority
	// load decks and set up gameplay

	// sReqHndl.pushReq( new GuiCreateDeckRequest( mPlayerOne->getID() ) );

	mPlayerOne->getLibrary()->drawToHand( 7 );
	mPlayerTwo->getLibrary()->drawToHand( 7 );

	// decide who plays first
	//	if( random( 2 ) ) mPlayerOne->setActive( true );
	//	else mPlayerTwo->setActive( true );
	mPlayerOne->setActive( true );

	mFirstTurn = true;

	// now wait for gui to load the meshes and do init
	sLog.message( "Game", "Waiting for GUI init." );
	mCond.wait( mMutex );
	sLog.message( "Game", "GUI init done, preparing the game." );

	// on to first step
	sLog.message( "Game", "Ended game preparation." );
	return Game::STATE_UNTAP;
}

Game::State Game::doUntap() 
{
	// Active player should be able to decide which permanents to deactivate
	// during this step, but we will deactivate all for now
	getActivePlayer()->setPriority( true );
	getBattlefield()->untapAll( getActivePlayer() );

	return Game::STATE_UPKEEP; 
}
Game::State Game::doUpkeep() 
{ return Game::STATE_DRAW; }
Game::State Game::doDraw() 
{ return Game::STATE_MAIN1; }

Game::State Game::doMainOne() 
{ 
	boost::mutex::scoped_lock lock( mMutex );

	int count = 0;

	// while( !mInteractionDone ) {
	// 	GuiNoArgRequest *clickReq =  new GuiNoArgRequest( Request::REQ_GUI_CHOOSE_CARD );
	// 	sReqHndl.pushReq( clickReq );

	// 	mCond.wait( lock );
	// 	if( !mPlayerOne->getHand()->getCardElement( mClickedID ) ) {
	// 		sLog.error( "Game::doMainOne", "Selected card is not in player's hand." );
	// 		continue;
	// 	}

	// 	HandElement *he = ObjectManager::get().find< HandElement >( mClickedID );
	// 	assert( he );
	// 	he->play();
	// 	mStack->resolve();
		
	// 	mInteractionDone = true;
	// }

	mCond.wait( lock );
	return Game::STATE_MAX;	
//return Game::STATE_COMBAT_BEGINNING; }
}

Game::State Game::doCombatBeginning() 
{ return Game::STATE_COMBAT_DECL_ATT; }
Game::State Game::doCombatDeclAtt()
{ return Game::STATE_COMBAT_DECL_BLK; }
Game::State Game::doCombatDeclBlk()
{ return Game::STATE_COMBAT_EXTRA_DMG; } 
Game::State Game::doCombatExtraDmg() 
{ return Game::STATE_COMBAT_DMG; }
Game::State Game::doCombatDmg() 
{ return Game::STATE_COMBAT_ENDING; }
Game::State Game::doCombatEnding() 
{ return Game::STATE_MAIN2; }
		
Game::State Game::doMainTwo()
{ return Game::STATE_END_STEP; }

Game::State Game::doEndStep()
{ return Game::STATE_CLEANUP; }
Game::State Game::doCleanup() 
{ 
	// switch active players, a new turn begins
	return Game::STATE_UNTAP; 
}
		
Game::State Game::doFinished() 
{ 
	// do all the clean up needed, this state
	// needs to be triggered by a third-party
	// (such as Player, when his life gets <0, 
	// or when no cards are left to be drawn)
	//	return STATE_FINISHED;

	return STATE_MAX;
}

void Game::EndGameListener::eventOccured( Library::LibraryEmptyEvt *evt )
{
	std::cout << "Library empty!\n";
}

void Game::endGame( bool whoWon )
{
	sLog.message( "Game::endGame", "%s won!", (whoWon) ? "Player One" : "Player Two" );
	doFinished();
}

void Game::stateEffectsCheck()
{
	// "D21.51: igrac sa 0 ili manje zivota gubi partiju"
	if( mPlayerOne->getLife() < 0 ) 
		endGame( mPlayerTwo );
	if( mPlayerTwo->getLife() < 0 )
		endGame( mPlayerOne );
	
	// "D21.52: bice izdrzljivosti 0 ili manje ide na odlagaliste vlasnika,
	//  regeneracija ne moze zameniti ovaj dogadjaj"
	// mPlayerOne->getBattlefield()->checkCreatureToughness();
	// mPlayerTwo->getBattlefield()->checkCreatureToughness();
	
	// "D21.53: bice izdrzljivosti vece od 0 sa smrtonosnom stetom na sebi
	// biva unisteno. Smrtonosna steta = steta koja je >= izdrzljivosti bica.
	// Regeneracija moze zameniti ovaj dogadjaj."

	// mPlayerOne()->getBattlefield()->checkCreatureDmg();
	// mPlayerTwo()->getBattlefield()->checkCreatureDmg();
	
	// "D21.54: Oreol koji nije vezan za permanent ili menja neispravan permanent (?)
	// ide na odlagaliste vlasnika."
	
	// mPlayerOne()->getBattlefield()->checkEnchantmentAuras();
	// mPlayerTwo()->getBattlefield()->checkEnchantmentAuras();

	// "D21.55: Simbol bice u zoni razlicitoj od zone "u igri" prestaje da postoji."
	// mPlayerOne()->getBattlefield()->checkSymbolCreatures();
	// mPlayerTwo()->getBattlefield()->checkSymbolCreatures();

	// "D21.56: Igrac koji je pokusao da vuce kartu iz prazne biblioteke od poslednje
	// provere efekata stanja gubi partiju"
	// checkDrawCardFlags();

	// "D21.57: Kopija magije u zoni razlicitoj od niza prestaje da postoji. Kopija karte
	// u bilo kojoj zoni razlicitoj od niza ili zone "u igri" prestaje da postoji.

	// "D21.58: Oprema vezana za neispravan permanent skida se sa tog permanenta ali ostaje
	// u igri."
	//	mPlayerOne()->getBattlefield()->checkArtefactEquipments();
	
	// "D21.59: Ako permanent istovremeno ima +10/+10 i -10/-10 brojac na sebi, oba se uklanjaju
	// sa njega (daju 0)."
	// recursive check
}

void Game::givePriority( bool player )
{
	
}
