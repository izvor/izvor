/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Mana.h"

using namespace IzvorCommon::Core;

Mana::Mana()
{
	for( uint8 i = 0; i < Color::NUM; i++ )
		mPool[ Color::getTypeByID( i ) ] = 0;
}

void Mana::add( Mana *other )
{
	for( uint8 i = 0; i < Color::NUM; i++ ) {
		Color::Type type = Color::getTypeByID( i );
		mPool[type] += other->mPool[type];
	}
}

bool Mana::destroy( Mana *other )
{
	// first check, for consistency
	for( uint8 i = 0; i < Color::NUM; i++ ) {
		Color::Type type = Color::getTypeByID( i );
		if( mPool[type] < other->mPool[type] ) return false;
	}

	for( uint8 i = 0; i < Color::NUM; i++ ) {
		Color::Type type = Color::getTypeByID( i );
		mPool[type] -= other->mPool[type];
	}

	return true;
}

void Mana::print()
{
	for( uint8 i = 0; i < Color::NUM; i++ ) {
		Color::Type type = Color::getTypeByID( i );
		std::cout << Color::getTypeStr( type ) << " " << mPool[type] << std::endl;
	}
}
