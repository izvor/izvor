/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Cost.h"
#include "core/Mana.h"
#include "core/Symbol.h"
#include "core/Player.h"
#include "core/CardElement.h"
#include "core/ZoneElements.h"
#include "core/Ability.h"

using namespace IzvorCommon::Core;

// ---------- Cost ---------- //
Cost::~Cost()
{
	for( std::list< Symbol* >::iterator i = mSymbols.begin(), end = mSymbols.end();
		 i != end;
		 ++i )
		SafeDelete( *i );
}

bool Cost::convertToMana( Mana *dest ) const
{
	bool ret = true;

	for( std::list< Symbol* >::const_iterator i = mSymbols.begin();
		 i != mSymbols.end();
		 i++ )
		if( !(*i)->convertToMana( dest ) ) ret = false;

	return ret;
}

void Cost::print() const
{
	std::cout << "<cost>\n---\n";
	Mana dest;
	for( std::list< Symbol* >::const_iterator i = mSymbols.begin(), end = mSymbols.end();
		 i != end;
		 i++ ) {
		switch( (*i)->getType() )
		{
		case Symbol::MANA:
			// mrzi me da pisem print za ManaSymbol -.-
			(*i)->convertToMana( &dest );
			break;
		case Symbol::X:
			std::cout << "<X " << Color::getTypeStr( dynamic_cast< XSymbol* >( *i )->getColor() ) << " >\n";
			break;
		case Symbol::A:
			std::cout << "<TAP>";
			break;
		default: break;
		}		
	}
	dest.print();
	std::cout << "\n</cost>\n";
}

// ---------- !Cost ---------- //

// ---------- AbilityCost ---------- //
bool AbilityCost::pay( Player *targetPlayer )
{
	Mana cost;
	if( !convertToMana( &cost ) ) {// has X
		sLog.error( "AbilityCost::pay", "Arbitrary mana cost, need moar info stub." );
		// ask player what X is...
		// GUI takes lead here
		return false;
	}
	
	if( !targetPlayer->getManaPool()->destroy( &cost ) ) {
		sLog.warning( "ManaCost::pay", "Not enough mana." );
		return false;
	}

	Permanent *card = dynamic_cast< Permanent* >( mParent->getOwner() );
	Permanent *target = card;
	assert( card );

	for( std::list< Symbol* >::iterator i = mSymbols.begin();
		 i != mSymbols.end();
		 i++ ) {
		switch( (*i)->getType() ) {
		case Symbol::A:
			if( !card->tap() )
				return false;
			break;
		case Symbol::SACRIFICE:		   
			target = dynamic_cast< Permanent* >
				( dynamic_cast< SacrificeSymbol* >(*i)->getTarget() );
			assert( target );
			target->sacrifice();
			break;
		default:
			continue;
		}
	}

	return true;
}
// ---------- !AbilityCost ---------- //

// ---------- ManaCost ---------- //
bool ManaCost::pay( Player *targetPlayer )
{
	Mana cost;
	if( !convertToMana( &cost ) ) {// has X
		sLog.error( "ManaCost::pay", "Arbitrary mana cost, need moar info stub." );
		// ask player what X is...
		// GUI takes lead here
		return false;
	}
	
	if( !targetPlayer->getManaPool()->destroy( &cost ) ) {
		sLog.warning( "ManaCost::pay", "Not enough mana." );
		return false;
	}
	
	return true;
}

void ManaCost::decode( const DB::Record& r )
{
    std::string dbStr = r[DB::COST];

    if( !dbStr.length() ) {
		sLog.warning( "ManaCost::decode", "DB::COST record field empty." );
		return;
	}
	
	parseStr( dbStr, this );
}

void ManaCost::parseStr( const std::string& dbStr, Cost *dest )
{
    // first one to two symbols are Color::BEZBOJNA
    uint8 j = 0;
    while( j < dbStr.length() && dbStr[j] != 'R' && dbStr[j] != 'Z' && dbStr[j] != 'P' && 
        dbStr[j] != 'T' && dbStr[j] != 'C' )
        dest->addSymbol( createSymbol( Color::BEZBOJNA, dbStr[j++] ) );

    if( j < dbStr.length() ) {
        // there are unparsed tokens
        while( true ) {
            char c = dbStr[j];
            do {
                if( (uint8) (j + 1) < dbStr.length() )
                    if( isdigit( dbStr[j + 1]  ) ) { j++; continue; }
				dest->addSymbol( createSymbol( Color::getTypeByCode( c ), dbStr[j++] ) );
            } while( j < dbStr.length() &&
                (dbStr[j] == c || isdigit( dbStr[j] ) || dbStr[j] == 'X') );
            if( j == dbStr.length() ) break;
        }
    }
}

Symbol* ManaCost::createSymbol( const Color& color, char token )
{ 
    if( isdigit( token ) )
        return new ManaSymbol( color, token - '0' );
    else {
		uint32 quantity;

        switch( token ) {
        case 'R': case 'Z': case 'P': case 'T': case 'C':
			quantity = 1;
            break;

        case 'D':
			quantity = 10;
            break;

        case 'S':
			quantity = 16;
            break;

        case 'X':
			return new XSymbol( static_cast< Color::Type >( color.getRawColor() ) );

        default:
            sLog.warning( "ManaCost::createSymbol", "Wrong DB mana cost token: %c.", token );
			quantity = 1;
        }

		return new ManaSymbol( color, quantity );
    }

	sLog.error( "ManaCost::createSymbol", "Totally wrong DB mana cost token: %c.", token );
	return NULL;
}

// ---------- !ManaCost ---------- //
