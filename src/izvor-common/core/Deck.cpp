/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Deck.h"
#include "core/CardElement.h"
#include "core/BaseCard.h"

using namespace IzvorCommon::Core;

void Deck::addCard( uint32 cardDBID, uint32 count )
{
	assert( mCards.find( cardDBID ) == mCards.end() );
	mCards[ cardDBID ] = count;
}

void Deck::addSideCard( uint32 cardDBID, uint32 count )
{
	assert( mSideCards.find( cardDBID ) == mSideCards.end() );
	mSideCards[ cardDBID ] = count;
}

uint32 Deck::getCardCount( uint32 cardDBID, bool sideDeck ) const
{
	DeckContainer::const_iterator pos;
	if( !sideDeck ) {
		pos = mCards.find( cardDBID );
		if( pos == mCards.end() ) return 0;
	}
	else {
		pos = mSideCards.find( cardDBID );
		if( pos == mSideCards.end() ) return 0;
	}

	return pos->second;
}
