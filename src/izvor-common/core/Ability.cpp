
/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, mikronac, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/Ability.h"
#include "core/Effect.h"

using namespace IzvorCommon::Core;

void StackAbility::print() const
{	
	std::cout << "<effects>\n----\n";
	for( std::list< Effect* >::const_iterator i = mEffects.begin(), end = mEffects.end();
		 i != end;
		 ++i )
		(*i)->print();
	std::cout << "</effects>";
}

StackAbility::~StackAbility()
{
	for( std::list< Effect* >::iterator i = mEffects.begin(), end = mEffects.end();
		 i != end;
		 ++i )
		SafeDelete( *i );
}

void StackAbility::resolve() 
{
	for( std::list< Effect* >::iterator i = mEffects.begin(), end = mEffects.end();
		 i != end;
		 ++i )
		(*i)->resolve();
}

void ActivatedAbility::print() const
{
	StackAbility::print();
	mCost.print();
}

void TriggeredAbility::print() const
{
	StackAbility::print();
	std::cout << "Trigger: " << getType();
}

void TriggeredAbility::eventOccured( IzvorCommon::TriggerManager::DrawCardEvent *evt )
{
	CardElement *owner = dynamic_cast< CardElement* >( getOwner() );
	assert( owner );

	TriggerManager::onEventOccured( evt, owner->getID() );
}

void TriggeredAbility::eventOccured( IzvorCommon::TriggerManager::PermanentEntersPlayEvent *evt )
{
	CardElement *owner = dynamic_cast< CardElement* >( getOwner() );
	assert( owner );
	TriggerManager::onEventOccured( evt, getOwner()->getID() );
}
// disconnect!
//TriggeredAbility::~TriggeredAbility()

	
