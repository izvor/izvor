/*
    -----------------------------------------------------------------------
    LICENSE:
    -----------------------------------------------------------------------
    This file is part of Izvor: Izvori Magije (C) TCG simulator
    Copyright 2010, 2011 EGDC++ Team
    For the latest info visit: http://sites.google.com/site/etfgamedevcrew
    -----------------------------------------------------------------------
    Izvor is free software: you can redistribute it and/or modify it under
    the terms of the GNU Lesser General Public License as published by the 
    Free Software Foundation, either version 3 of the License, or (at your 
    option) any later version.

    Izvor is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
    License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Izvor.  If not, see <http://www.gnu.org/licenses/>.
    -----------------------------------------------------------------------
    Author: Stefan Pekic, dradojevic
*/

#include "IzvorCommonPCH.h"

#include "core/CardSet.h"
#include "core/CardElement.h"
#include "core/BaseCard.h"

using namespace IzvorCommon::Core;

CardSet::~CardSet()
{
	for( CardContainer::iterator i = mCards.begin(), end = mCards.end();
		 i != end;
		 ++i )
		SafeDelete( *i );
}

void CardSet::addCardElement( CardElement *ce )
{
	mCards.push_back( ce );
}

bool CardSet::removeCardElement( CardElement *ce )
{
	for( CardContainer::iterator i = mCards.begin();
		 i != mCards.end();
		 ++i ) {
		if( ce == *i ) { 
			mCards.erase( i ); 
			return true; 
		}
	}

	return false;
}

CardElement* CardSet::getCardElement( uint32 ceID ) const
{
	for( CardContainer::const_iterator i = mCards.begin();
		 i != mCards.end();
		 i++ )
		if( ceID == (*i)->getID() ) return (*i);

	return NULL;
}

void CardSet::print()
{
	for( CardContainer::const_iterator i = mCards.begin();
		 i != mCards.end();
		 i++ )
		std::cout << (*i)->getID() << ": " << (*i)->getBaseCard()->getInfo()->getName() 
				  << " : " << (*i)->getTypeStr() << std::endl;
}

std::list< Object::ObjectID >* CardSet::getCardList() const
{
	std::list< Object::ObjectID > *list = new std::list< Object::ObjectID >();
	for( CardContainer::const_iterator i = mCards.begin();
		 i != mCards.end();
		 i++ )
		list->push_back( (*i)->getID() );

	return list;
}
