#include "IzvorCommonPCH.h"

#include "network/PktRep.h"
#include "marshal/MarshalOpcodes.h"
#include "marshal/MarshalStream.h"

bool marshal( const PktRep* rep, Buffer& into )
{
    MarshalStream v;
    return v.save( rep, into );
}

bool marshalDeflate( const PktRep* rep, Buffer& into, const uint32 deflationLimit )
{
    Buffer data;
    if( !marshal( rep, data ) )
        return false;

    if( data.size() >= deflationLimit )
        return deflateData( data, into );
    else {
        into.append( data.begin< uint8 >(), data.end< uint8 >() );
        return true;
    }
}

MarshalStream::MarshalStream()
    : mBuffer( NULL )
{}

bool MarshalStream::save( const PktRep* rep, Buffer& into )
{
    mBuffer = &into;
    bool res = saveStream( rep );
    mBuffer = NULL;

    return res;
}

bool MarshalStream::saveStream( const PktRep* rep )
{
    if( rep == NULL )
        return false;

    put< uint8 >( MarshalHeaderByte );

    return rep->visit( *this );
}

bool MarshalStream::visitInteger( const PktInt* rep )
{
    const int32 val = rep->value();
    put< uint8 >( Op_PktInt );
    put< int32 >( val );

    return true;
}

bool MarshalStream::visitBoolean( const PktBool* rep )
{
    if( rep->value() )
        put< uint8 >( Op_PktTrue );
    else 
        put< uint8 >( Op_PktFalse );

    return true;
}

bool MarshalStream::visitTuple( const PktTuple* rep )
{
    uint32 size = rep->size();
    put< uint8 >( Op_PktTuple );
    putSizeEx( size );

    return PktVisitor::visitTuple( rep );
}

bool MarshalStream::visitString( const PktString* rep )
{
    size_t len = rep->content().size();

    if( len == 0 ) put< uint8 >( Op_PktEmptyString );
    else if( len == 1 ) {
        put< uint8 >( Op_PktCharString );
        put< uint8 >( rep->content()[0] );
    }
    else {
        // implement common string table lookup
        put< uint8 >( Op_PktLongString );
        putSizeEx( len );
        put( rep->content().begin(), rep->content().end() );
    }

    return true;
}

