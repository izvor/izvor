#include "IzvorCommonPCH.h"

#include "marshal/UnmarshalStream.h"
#include "marshal/MarshalOpcodes.h"

PktRep* unmarshal( const Buffer& data )
{
    UnmarshalStream v;
    return v.load( data );
}

PktRep* inflateUnmarshal( const Buffer& data )
{
    if( isDeflated( data ) ) {
        sLog.warning( "Unmarshal", "Inflating" );
        Buffer inflatedData;
        if( !inflateData( data, inflatedData ) )
            return NULL;

        return unmarshal( inflatedData );
    }
    else return unmarshal( data );
}

/************************************************************************/
/* UnmarshalStream                                                      */
/************************************************************************/

PktRep* ( UnmarshalStream::* const UnmarshalStream::sLoadMap[ PktRepOpcodeMask + 1 ] )() =
{
    &UnmarshalStream::loadError,
    &UnmarshalStream::loadInteger,
    &UnmarshalStream::loadBoolTrue,
    &UnmarshalStream::loadBoolFalse,
    &UnmarshalStream::loadStringEmpty,
    &UnmarshalStream::loadStringChar,
    &UnmarshalStream::loadStringLong,
    &UnmarshalStream::loadTuple,
    &UnmarshalStream::loadList
};

PktRep* UnmarshalStream::load( const Buffer& data )
{
    mInItr = data.begin< uint8 >();
    PktRep* res = loadStream( data.size() );
    mInItr = Buffer::ConstIterator< uint8 >();
    return res;
}

PktRep* UnmarshalStream::loadStream( size_t streamLength )
{
    const uint8 header = read< uint8 >();
    if( MarshalHeaderByte != header ) {
        sLog.error( "Unmarshal", "Invalid stream received (header byte 0x%X).", header );
        return NULL;
    }

    //  const uint32 saveCount = read< uint32 >(); // broj objekata
    //  sLog.warning( "UnmarshalStream", "streamLength: %d, saveCount: %d", streamLength, saveCount );
    createObjectStore( streamLength - sizeof( uint8 ), 1 );// - sizeof( uint32 ), saveCount );
    PktRep *rep = loadRep();
    sLog.warning( "LoadStream", "Done" );
    destroyObjectStore();
    return rep;
}

PktRep* UnmarshalStream::loadRep()
{
    const int8 header = read< uint8 >();

    const bool flagUnknown = ( header & PktRepUnknownMask );
    const bool flagSave = ( header & PktRepSaveMask );
    const uint8 opcode = ( header & PktRepOpcodeMask );

    if( flagUnknown )
        sLog.warning( "Unmarshal", "Encountered flagUnknown in header 0x%X.", header );

    const uint32 storageIndex = ( (flagSave) ? getStorageIndex() : 0 );
    sLog.warning( "loadRep", "si: %d, op: %d", storageIndex, opcode );

    PktRep* rep = ( this->*sLoadMap[ opcode ] )();

    if( 0 != storageIndex ) storeObject( storageIndex, rep );

    return rep;
}

void UnmarshalStream::createObjectStore( size_t streamLength, uint32 saveCount )
{
    destroyObjectStore();

    if( 0 < saveCount ) {
        mStoreIndexItr = ( ( mInItr + streamLength ).as< uint32 >() - saveCount );
        mStoredObjects = new PktList( saveCount );
    }
}

void UnmarshalStream::destroyObjectStore()
{
    mStoreIndexItr = Buffer::ConstIterator< uint32 >();
    PktSafeDecRef( mStoredObjects );
}

void UnmarshalStream::storeObject( uint32 index, PktRep* object )
{
    if( 0 < index ) {
        PktIncRef( object );
        mStoredObjects->setItem( --index, object );
    }
}

PktRep* UnmarshalStream::loadStringLong()
{
    const uint32 len = readSizeEx();
    const Buffer::ConstIterator< char > str = read< char >( len );

    return new PktString( str, str + len );
}

PktRep* UnmarshalStream::loadStringChar()
{
    const Buffer::ConstIterator< char > str = read< char >( 1 );

    return new PktString( str, str + 1 );
}

PktRep* UnmarshalStream::loadTuple()
{
    const uint32 count = readSizeEx();
    PktTuple* tuple = new PktTuple( count );

    for( uint32 i = 0; i < count; i++ )
    {
        PktRep* rep = loadRep();
        if( NULL == rep )
        {
            PktDecRef( tuple );
            return NULL;
        }

        tuple->setItem( i, rep );
    }

    return tuple;
}

PktRep* UnmarshalStream::loadError()
{
    sLog.error( "UnmarshalStream", "Wrong opcode." );
    return NULL;
}

PktRep* UnmarshalStream::loadList()
{
    const uint32 count = readSizeEx();
    PktList* list = new PktList( count );

    for( uint32 i = 0; i < count; i++ )
    {
        PktRep* rep = loadRep();
        if( NULL == rep )
        {
            PktDecRef( list );
            return NULL;
        }

        list->setItem( i, rep );
    }

    return list;
}
