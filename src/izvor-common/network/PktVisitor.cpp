#include "IzvorCommonPCH.h"

#include "network/PktVisitor.h"
#include "network/PktRep.h"

bool PktVisitor::visitTuple( const PktTuple* rep )
{
    PktTuple::const_iterator cur, end;
    cur = rep->begin();
    end = rep->end();
    for( ; cur != end; cur++ ) {
        if( !(*cur)->visit( *this ) )
            return false;
    }

    return true;
}

bool PktVisitor::visitList( const PktList* rep )
{
    PktList::const_iterator cur, end;
    cur = rep->begin();
    end = rep->end();

    for( ; cur != end; cur++ ) {
        if( !(*cur)->visit( *this ) )
            return false;
    }

    return true;
}
