#include "IzvorCommonPCH.h"

#include "network/PktVisitor.h"
#include "network/PktDumpVisitor.h"
#include "network/PktRep.h"

PktRep::PktRep( Type type )
    : RefObject( 1 ),
    mType( type )
{}

PktRep::~PktRep() {}

void PktRep::dump() const
{
    PktDumpVisitor dumper;

    visit( dumper );
}

PktInt::PktInt( const int32 i )
    : PktRep( PktRep::PktIntType ),
    mValue( i )
{}

PktInt::PktInt( const PktInt& oth )
    : PktRep( PktRep::PktIntType ),
    mValue( oth.value() )
{}

PktRep* PktInt::clone() const
{
    return new PktInt( *this );
}

bool PktInt::visit( PktVisitor& v ) const
{
    return v.visitInteger( this );
}

PktBool::PktBool( bool b )
    : PktRep( PktRep::PktBoolType ),
    mValue( b )
{}
PktBool::PktBool( const PktBool& oth )
    : PktRep( PktRep::PktBoolType ),
    mValue( oth.value() )
{}

PktRep* PktBool::clone() const {
    return new PktBool( *this );
}

bool PktBool::visit( PktVisitor& v ) const
{
    return v.visitBoolean( this );
}

PktString::PktString( const std::string& s )
    : PktRep( PktRep::PktStringType ),
    mValue( s )
{}

PktString::PktString( const char* str, size_t len )
    : PktRep( PktRep::PktStringType ),
    mValue( str, len )
{}

PktString::PktString( const PktString& oth )
    : PktRep( PktRep::PktStringType ),
    mValue( oth.mValue )
{}

PktRep* PktString::clone() const
{
    return new PktString( *this );
}

bool PktString::visit( PktVisitor& v ) const
{
    return v.visitString( this );
}

PktTuple::PktTuple( size_t item_count )
    : PktRep( PktRep::PktTupleType ),
    items( item_count, NULL )
{}

PktTuple::PktTuple( const PktTuple& oth )
    : PktRep( PktRep::PktTupleType ),
    items()
{
    *this = oth;
}

PktTuple::~PktTuple()
{
    clear();
}

PktRep* PktTuple::clone() const
{
    return new PktTuple( *this );
}

bool PktTuple::visit( PktVisitor& v ) const
{
    return v.visitTuple( this );
}

void PktTuple::clear()
{
    iterator cur, end;
    cur = items.begin();
    end = items.end();
    for( ; cur != end; cur++ )
        PktSafeDecRef( *cur );

    items.clear();
}

PktTuple& PktTuple::operator=( const PktTuple& rhs )
{
    if( this != &rhs ) {
        clear();
        items.resize( rhs.size() );

        iterator cur, end;
        cur = items.begin();
        end = items.end();

        const_iterator cur_rhs, end_rhs;
        cur_rhs = rhs.begin();
        end_rhs = rhs.end();
        for( ; cur != end && cur_rhs != end_rhs; cur++, cur_rhs++ ) {
            if( *cur_rhs == NULL )
                *cur = NULL;
            else
                *cur = (*cur_rhs)->clone();
        }

    }

    return *this;
}


PktList::PktList( size_t item_count )
    : PktRep( PktRep::PktListType ),
    items( item_count, NULL )
{}

PktList::PktList( const PktList& oth )
    : PktRep( PktRep::PktListType ),
    items()
{
    *this = oth;
}

PktList::~PktList()
{
    clear();
}

PktRep* PktList::clone() const
{
    return new PktList( *this );
}

bool PktList::visit( PktVisitor& v ) const
{
    return v.visitList( this );
}

void PktList::clear()
{
    iterator cur, end;
    cur = items.begin();
    end = items.end();
    for( ; cur != end; cur++ )
        PktSafeDecRef( *cur );

    items.clear();
}

PktList& PktList::operator=( const PktList& oth )
{
    if( this != &oth ) {
        clear();
        items.resize( oth.size() );

        iterator cur, end;
        cur = items.begin();
        end = items.end();

        const_iterator cur_oth, end_oth;
        cur_oth = oth.begin();
        end_oth = oth.end();

        for( ; cur != end && cur_oth != end_oth; cur++, cur_oth++ ) {
            if( *cur_oth == NULL )
                *cur = NULL;
            else *cur = (*cur_oth)->clone();
        }
    }
    return *this;
}
