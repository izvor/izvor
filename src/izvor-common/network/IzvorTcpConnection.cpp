#include "IzvorCommonPCH.h"

#include "network/IzvorTcpConnection.h"

#include "marshal/MarshalStream.h"
#include "marshal/UnmarshalStream.h"
//#include "network/Packet.h"
//#include "network/PacketFactory.h"

const uint32 IzvorTcpConnection::TIMEOUT_MS = 10 * 60 * 1000; // 10 minutes
const uint32 IzvorTcpConnection::PACKET_SIZE_LIMIT = 10 * 1024 * 1024; // 10 megabytes

IzvorTcpConnection::IzvorTcpConnection()
    : TcpConnection()
    //    mTimeoutTimer( TIMEOUT_MS )
{
}

IzvorTcpConnection::IzvorTcpConnection( Socket *sock, uint32 rIP, uint16 rPort )
    : TcpConnection( sock, rIP, rPort )
    //    mTimeoutTimer( TIMEOUT_MS )
{
}

void IzvorTcpConnection::queueRep( PktRep *rep )
{
    Buffer *buf = new Buffer;

    // make room for length part
    const Buffer::Iterator<uint32> bufLen = buf->end<uint32>();
    buf->resizeAt( bufLen, 1 );

    // marshal it and deflate it
    if( !marshalDeflate( rep, *buf ) )
        sLog.error( "Network", "Failed to marshal new packet." );
    else {
        // sLog.dump( "queuePacket", &(*buf)[0], buf->size(), "poslacu (pre lepljenja velicine): " );
        // write the size part
        *bufLen = ( buf->size() - sizeof( uint32 ) );

        //    sLog.dump( "queuePacket", &(*buf)[0], buf->size(), "poslacu: " );
        send( &buf );
    }

    SafeDelete( buf );
}

PktRep* IzvorTcpConnection::popRep()
{
    Buffer *packet = NULL;
    PktRep *res = NULL;

    {
        MutexLock lock( mMInQueue );
        packet = mInQueue.popPacket();
    }

    if( NULL != packet ) {
        //sLog.dump( "queuePacket", &(*packet)[0], packet->size(), "primio: " );
        // inflate it and decode it
        res = inflateUnmarshal( *packet );
        if( !res )
            sLog.error( "Network", "Failed to unmarshall incoming packet." );
    }

    SafeDelete( packet );
    return res;
}


bool IzvorTcpConnection::processReceivedData( char* errbuf )
{
    if( errbuf )
        errbuf[0] = 0;

    {
        MutexLock lock( mMInQueue );

        // put bytes into packetizer
        mInQueue.inputData( *mRecvBuf );
        // process packetizer
        mInQueue.process();
    }

    return true;
}


bool IzvorTcpConnection::recvData( char* errbuf )
{
    if( !TcpConnection::recvData( errbuf ) )
        return false;

    // if( mTimeoutTimer.Check() )
    // {
    //     if( errbuf )
    //         snprintf( errbuf, TCPCONN_ERRBUF_SIZE, "EVETCPConnection::RecvData(): Connection timeout" );
    //     return false;
    // }

    return true;
}

void IzvorTcpConnection::clearBuffers()
{
    TcpConnection::clearBuffers();

    // mTimeoutTimer.Start();

    {
        MutexLock lock( mMInQueue );

        mInQueue.clearBuffers();
    }
}
