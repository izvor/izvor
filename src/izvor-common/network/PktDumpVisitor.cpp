#include "IzvorCommonPCH.h"

#include "network/PktDumpVisitor.h"
#include "network/PktRep.h"

PktDumpVisitor::PktDumpVisitor()
{}

bool PktDumpVisitor::visitInteger( const PktInt* rep )
{
    sLog.debug( "PktDumpVisitor", "Integer field: %d", rep->value() );
    return true;
}

bool PktDumpVisitor::visitBoolean( const PktBool* rep )
{
    sLog.debug( "PktDumpVisitor", "Boolean field: %s", rep->value() ? "true" : "false" );
    return true;
}

bool PktDumpVisitor::visitString( const PktString* rep )
{
    sLog.debug( "PktDumpVisitor", "String field: '%s'", rep->content().c_str() );
    return true;
}

bool PktDumpVisitor::visitTuple( const PktTuple* rep )
{
    if( rep->empty() )
        sLog.debug( "PktDumpVisitor", "Tuple: empty" );
    else {
        sLog.debug( "PktDumpVisitor", "Tuple: %lu elements", rep->size() );

        PktTuple::const_iterator cur, _end;
        cur = rep->begin();
        _end = rep->end();

        for( ; cur != _end; cur++ ) {
            bool res = (*cur)->visit( *this );
            if( !res ) return false;
        }
    }

    return true;
}

bool PktDumpVisitor::visitList( const PktList* rep )
{
    if( rep->empty() ) 
        sLog.debug( "PktDumpVisitor", "List: empty" );
    else {
        sLog.debug( "PktDumpVisitor", "List: %lu elements", rep->size() );

        PktList::const_iterator cur, _end;
        cur = rep->begin();
        _end = rep->end();

        for( ; cur != _end; cur++ ) {
            bool res = (*cur)->visit( *this );
            if( !res )
                return false;
        }
    }

    return true;
}
